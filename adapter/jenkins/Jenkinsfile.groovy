node('master') {
        stage('Checkout') {
            checkout scm
        }

        def revertReleaseFunction = load 'jenkins/revert_release.groovy'
        def teamsWebhookUrl = "https://outlook.office.com/webhook/72164f68-7137-472f-aae8-792b957ee134@5a91ba94-7ca6-44bb-a0b3-1fc225b4e6f3/JenkinsCI/5fe12c90e65a4fc9bd8bb289cd6e8847/fefe6d1e-33fb-420e-aaf0-c8f0eb93132f"
        def mvnHome = tool 'Maven 3.3.9'
        def mvn = "${mvnHome}/bin/mvn"

        stage('Run tests') {
            try {
                sh "${mvnHome}/bin/mvn javadoc:javadoc clean verify"

            } catch (e) {
                notifyTeams("FAILED", "${teamsWebhookUrl}")
                currentBuild.result = 'FAILED'
                throw e
            } finally {
                junit allowEmptyResults: true, testResults: '**/target/surefire-reports/*.xml'
                junit allowEmptyResults: true, testResults: '**/target/failsafe-reports/*.xml'
                jacoco()
            }
        }

        stage('Publish') {
            sh "${mvn} -U -Pshade deploy -DrepositoryId=telenav.snapshot -DskipTests"
            archiveArtifacts '**/target/*shaded*.jar'
        }

        if (params.Release) {
            def releaseStatus = false
            def releaseVersion = ''
            stage('Release') {
                if (params.Version) {
                    releaseVersion = params.Version
                } else {
                    def snapshotIndex = pom.version.indexOf('-SNAPSHOT')
                    if (snapshotIndex > 0) {
                        releaseVersion = pom.version.substring(0, snapshotIndex)
                    } else {
                        releaseVersion = pom.version
                    }

                }
                sh "${mvn} clean -Pshade -DreleaseVersion=${releaseVersion} release:clean release:prepare release:perform"
                releaseStatus = true
                if (releaseStatus) {
                    archiveArtifacts '**/target/*.jar'
                    manager.addBadge("star-gold.gif", "release")
                    notifyTeams("SUCCESS", "${teamsWebhookUrl}")
                    currentBuild.result = 'SUCCESS'
                } else {
                    revertReleaseFunction.revertRelease("${initialVersion}", "${releaseVersion}", "${pom.artifactId}")
                    notifyTeams("FAILED", "${teamsWebhookUrl}")
                    currentBuild.result = 'FAILED'
                }
            }
        }

    notifyTeams("SUCCESS", "${teamsWebhookUrl}")
    currentBuild.result = 'SUCCESS'

}

def notifyTeams(String buildStatus, String url) {
    def message = "${buildStatus}: `${env.JOB_NAME}` #${env.BUILD_NUMBER}:\n${env.BUILD_URL}"

    if (buildStatus == 'SUCCESS') {
        color = 'BDFFC3'
    } else if (buildStatus == 'FAILED') {
        color = 'FF9FA1'
    } else {
        color = 'D4DADF'
    }
    office365ConnectorSend message: "${message}", status: "${buildStatus}", webhookUrl: "${url}", color: "${color}"
}