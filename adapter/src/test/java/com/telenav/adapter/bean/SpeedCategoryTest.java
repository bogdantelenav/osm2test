package com.telenav.adapter.bean;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


/**
 * Unit tests for {@link SpeedCategory}.
 *
 * @author petrum
 */
public class SpeedCategoryTest {

    @Test
    public void categorizeWithGreaterThan130TheMinimumValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(131), is(SpeedCategory.SPEED_CAT_GREATER_THAN_130));
    }

    @Test
    public void categorizeWithGreaterThan130ARandomValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(160), is(SpeedCategory.SPEED_CAT_GREATER_THAN_130));
    }

    @Test
    public void categorizeWithBetween101And130TheMinimumValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(101), is(SpeedCategory.SPEED_CAT_BETWEEN_101_130));
    }

    @Test
    public void categorizeWithBetween101And130ARandomValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(120), is(SpeedCategory.SPEED_CAT_BETWEEN_101_130));
    }

    @Test
    public void categorizeWithBetween101And130TheMaximumValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(130), is(SpeedCategory.SPEED_CAT_BETWEEN_101_130));
    }

    @Test
    public void categorizeWithBetween91And100TheMinimumValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(91), is(SpeedCategory.SPEED_CAT_BETWEEN_91_100));
    }

    @Test
    public void categorizeWithBetween91And100ARandomValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(95), is(SpeedCategory.SPEED_CAT_BETWEEN_91_100));
    }

    @Test
    public void categorizeWithBetween91And100TheMaximumValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(100), is(SpeedCategory.SPEED_CAT_BETWEEN_91_100));
    }

    @Test
    public void categorizeWithBetween71And90TheMinimumValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(71), is(SpeedCategory.SPEED_CAT_BETWEEN_71_90));
    }

    @Test
    public void categorizeWithBetween71And90ARandomValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(75), is(SpeedCategory.SPEED_CAT_BETWEEN_71_90));
    }

    @Test
    public void categorizeWithBetween71And90TheMaximumValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(90), is(SpeedCategory.SPEED_CAT_BETWEEN_71_90));
    }

    @Test
    public void categorizeWithBetween51And70TheMinimumValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(51), is(SpeedCategory.SPEED_CAT_BETWEEN_51_70));
    }

    @Test
    public void categorizeWithBetween51And70ARandomValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(68), is(SpeedCategory.SPEED_CAT_BETWEEN_51_70));
    }

    @Test
    public void categorizeWithBetween51And70TheMaximumValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(70), is(SpeedCategory.SPEED_CAT_BETWEEN_51_70));
    }

    @Test
    public void categorizeWithBetween31And50TheMinimumValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(31), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }

    @Test
    public void categorizeWithBetween31And50ARandomValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(48), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }

    @Test
    public void categorizeWithBetween31And50TheMaximumValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(50), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }

    @Test
    public void categorizeWithLessThan11TheMaximumValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(10), is(SpeedCategory.SPEED_CAT_LESS_THAN_11));
    }

    @Test
    public void categorizeWithLessThan11ARandomValueOfTheCategory() {
        assertThat(SpeedCategory.getSpeedCategoryForSpeed(5), is(SpeedCategory.SPEED_CAT_LESS_THAN_11));
    }
}
