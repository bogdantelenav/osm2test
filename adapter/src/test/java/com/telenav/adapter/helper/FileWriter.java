package com.telenav.adapter.helper;

import org.apache.spark.api.java.JavaSparkContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author liviuc
 */
public class FileWriter {

    public static void writeAttributesJobSidefile(final String sidefileContent,
                                                  final String sidefilePath,
                                                  final JavaSparkContext sparkContext) {
        sparkContext.parallelize(Arrays.asList(sidefileContent)).coalesce(1).saveAsTextFile(sidefilePath);
    }

    public static void writeAttributesJobSidefile(final List<Long> entityIds,
                                                  final List<String> sidefileTags,
                                                  final String sidefilePath,
                                                  final JavaSparkContext javaSparkContext) {
        javaSparkContext.parallelize(sidefileContent(entityIds, sidefileTags)).coalesce(1).saveAsTextFile(sidefilePath);
    }

    public static void writeAttributesJobConfigFile(final List<String> tags,
                                                    final String configPath,
                                                    final JavaSparkContext javaSparkContext) {
        javaSparkContext.parallelize(tags).coalesce(1).saveAsTextFile(configPath);
    }

    private static List<String> sidefileContent(final List<Long> ids,
                                                final List<String> tags) {
        final List<String> sidefileContent = new ArrayList<>();
        for (final String tag : tags) {
            final List<String> records = ids.stream()
                    .map(id -> id + "," + tag + "," + "mockValue")
                    .collect(Collectors.toList());
            sidefileContent.addAll(records);
        }
        return sidefileContent;
    }

}
