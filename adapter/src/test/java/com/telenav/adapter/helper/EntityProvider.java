package com.telenav.adapter.helper;

import com.telenav.map.entity.*;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.*;

/**
 * @author liviuc
 */
@FunctionalInterface
public interface EntityProvider<E extends StandardEntity & TagAdjuster<E>> {

    EntityProvider<RawRelation> RAW_RELATIONS_ENTITY_PROVIDER = (final List<Long> ids,
                                                                 final List<String> tagList) -> {
        final List<RawRelation> relations = new ArrayList<>();
        final Iterator<Long> iterator = ids.iterator();
        final RawRelation relation = new RawRelation(iterator.next(), Collections.emptyList(), tags(tagList.iterator()));
        relations.add(relation);
        return relations;
    };

    EntityProvider<Node> NODES_ENTITY_PROVIDER = (final List<Long> ids,
                                                  final List<String> tagList) -> {
        final List<Node> nodes = new ArrayList<>();
        final Iterator<Long> iterator = ids.iterator();
        final Node node = new Node(iterator.next(), Latitude.ZERO, Longitude.ZERO, tags(tagList.iterator()));
        nodes.add(node);
        return nodes;
    };

    EntityProvider<RawWay> WAYS_ENTITY_PROVIDER = (final List<Long> ids,
                                                   final List<String> tagList) -> {
        final List<RawWay> ways = new ArrayList<>();
        final Iterator<Long> iterator = ids.iterator();
        final RawWay rawWay = new RawWay(iterator.next(), Collections.emptyList(), tags(tagList.iterator()));
        ways.add(rawWay);

        return ways;
    };

    static Map<String, String> tags(final Iterator<String> tagsIterator) {
        final Map<String, String> tags = new HashMap<>();
        while (tagsIterator.hasNext()) {
            tags.put(tagsIterator.next(), "mockValue");
        }
        tags.put("toKeep", "mockValue");
        return tags;
    }

    default JavaRDD<E> rdd(final List<Long> ids,
                           final List<String> tagList,
                           final JavaSparkContext sparkContext) {
        return sparkContext.parallelize(entities(ids, tagList));
    }

    List<E> entities(List<Long> ids, List<String> tags);
}