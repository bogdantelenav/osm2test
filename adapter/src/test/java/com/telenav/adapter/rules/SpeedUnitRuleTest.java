package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


/**
 * Unit tests for {@link SpeedUnitRule}.
 *
 * @author ioanao
 */
public class SpeedUnitRuleTest extends BaseTest {

    @Test
    public void doesNotChangeAnythingInCaseOfNonExistingSpeedUnitTag() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.ONEWAY.unwrap(), "1");

        final WaySection adaptedWaySection = new SpeedUnitRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(1));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "1"));
    }

    @Test
    public void addSpeedUnitTag() {
        final WaySection waySection =
                waysection(3, 1).withTag(OsmTagKey.ONEWAY.unwrap(), "1").withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M");

        final WaySection adaptedWaySection = new SpeedUnitRule().apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(3));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "1"));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(UnidbTagKey.SPEED_UNIT.unwrap(), "M"));
    }
}