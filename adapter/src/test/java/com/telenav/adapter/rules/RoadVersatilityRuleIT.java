package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * @author roxanal
 */
public class RoadVersatilityRuleIT extends BaseTest {

    @Test
    public void motorcarFromProbesAndOsm() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "no")
                .withTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.PROBES), "yes");
        final WaySection adaptedWaySection = new RoadVersatilityRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(4, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.unwrap(), "yes"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.withSource(), TnSourceValue.PROBES.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.archivedKeyName(), "no"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.OSM), "no"));
    }

    @Test
    public void motorcarFromManualEdits() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.DELETED), "yes");
        final WaySection adaptedWaySection = new RoadVersatilityRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(1, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection
                .hasTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.DELETED), "yes"));
    }

    @Test
    public void motorcarFromOsm() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "no");
        final WaySection adaptedWaySection = new RoadVersatilityRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(3, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.unwrap(), "no"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.withSource(), TnSourceValue.OSM.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.archivedKeyName(), "no"));
    }

    @Test
    public void motorcarFromProbesAndManualEdits() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.PROBES), "yes")
                .withTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.DELETED), "yes");
        final WaySection adaptedWaySection = new RoadVersatilityRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(2, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection
                .hasTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.PROBES), "yes"));
        assertTrue(adaptedWaySection
                .hasTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.DELETED), "yes"));
    }

    @Test
    public void motorcarFromManualEditsAndOsm() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "no")
                .withTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.MANUAL), "no");
        final WaySection adaptedWaySection = new RoadVersatilityRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(4, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.unwrap(), "no"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.archivedKeyName(), "no"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.withSource(), TnSourceValue.MANUAL.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.telenavPrefix(), "no"));
    }

    @Test
    public void motorcarFromProbesAndManualEditsAndOsm() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "no")
                .withTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.MANUAL), "no")
                .withTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.PROBES), "yes");
        final WaySection adaptedWaySection = new RoadVersatilityRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(5, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.unwrap(), "no"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.archivedKeyName(), "no"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.withSource(), TnSourceValue.MANUAL.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.telenavPrefix(), "no"));
        assertTrue(adaptedWaySection
                .hasTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.PROBES), "yes"));
    }

    @Test
    public void motorcarFromProbesAndMotorcycleManualEditsAndOsm() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.MOTORCYCLE.unwrap(), "yes")
                .withTag(OsmTagKey.MOTORCYCLE.withSource(TnSourceValue.MANUAL), "yes")
                .withTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.PROBES), "no");
        final WaySection adaptedWaySection = new RoadVersatilityRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(6, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCYCLE.unwrap(), "yes"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCYCLE.archivedKeyName(), "yes"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCYCLE.withSource(), TnSourceValue.MANUAL.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCYCLE.telenavPrefix(), "yes"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.withSource(), TnSourceValue.PROBES.unwrap()));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.unwrap(), "no"));
    }
}