package com.telenav.adapter.rules.node;

import com.telenav.adapter.BaseTest;
import com.telenav.map.entity.Node;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Map;
import java.util.stream.Collectors;

import static com.telenav.datapipelinecore.osm.OsmTagKey.ACCESS;
import static com.telenav.datapipelinecore.osm.OsmTagKey.EXCEPT;
import static com.telenav.datapipelinecore.unidb.ApplicableToTagValue.*;
import static com.telenav.datapipelinecore.unidb.UnidbTagKey.APPLICABLE_TO;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JUnitParamsRunner.class)
public class NodeApplicableToTagRuleTest extends BaseTest {
    private ApplicableToTagRuleDatasets applicableToTagRule;

    @Before
    public void init() {
        applicableToTagRule = new ApplicableToTagRuleDatasets();
    }

    @Test
    @Parameters({"tourist_bus", "coach"})
    public void vehicleAdditionTagNoOnNode(final String tagVehicle) {
        final Node node = node(1)
                .withTag(tagVehicle, "no");

        final Map<String, String> enhancedTags = applicableToTagRule.apply(node.tags());

        assertTrue(enhancedTags.containsKey(APPLICABLE_TO.unwrap()));
        assertEquals(enhancedTags.get(APPLICABLE_TO.unwrap()), BUS.unwrap());
    }

    @Test
    @Parameters({
            "tourist_bus, access_through_traffic;delivery;emergency;foot;hov;motorcar;motorcycle;taxi;truck",
            "coach, access_through_traffic;delivery;emergency;foot;hov;motorcar;motorcycle;taxi;truck"})
    public void vehicleAdditionTagYesOnNode(final String tagVehicle, final String expected) {
        Node node = node(1)
                .withTag(tagVehicle, "yes");

        final Map<String, String> enhancedTags = applicableToTagRule.apply(node.tags());

        assertTrue(enhancedTags.containsKey(APPLICABLE_TO.unwrap()));
        assertEquals(expected, enhancedTags.get(APPLICABLE_TO.unwrap()));
    }

    @Test
    @Parameters({"hgv, truck",
            "motorcar, motorcar",
            "bus, bus",
            "motorcycle, motorcycle",
            "motor_vehicle,  access_through_traffic;bus;delivery;emergency;hov;motorcar;motorcycle;taxi;truck"})
    public void restrictionTagOnNode(final String tagVehicle, final String expected) {
        final Node node = node(3)
                .withTag("restriction:" + tagVehicle, "no_right_turn");

        final Map<String, String> enhancedTags = applicableToTagRule.apply(node.tags());

        assertTrue(enhancedTags.containsKey(APPLICABLE_TO.unwrap()));
        assertEquals(expected, enhancedTags.get(APPLICABLE_TO.unwrap()));
    }

    @Test
    @Parameters({
            "psv, access_through_traffic;delivery;emergency;foot;hov;motorcar;motorcycle;truck",
            "hgv, access_through_traffic;bus;delivery;emergency;foot;hov;motorcar;motorcycle;taxi",
            "motorcar, access_through_traffic;bus;delivery;emergency;foot;hov;motorcycle;taxi;truck",
            "emergency, access_through_traffic;bus;delivery;foot;hov;motorcar;motorcycle;taxi;truck",})
    public void exceptTagOnNode(String tagVehicle, String expected) {
        final Node node = node(1)
                .withTag(EXCEPT.unwrap(), tagVehicle);

        final Map<String, String> enhancedTags = applicableToTagRule.apply(node.tags());

        assertTrue(enhancedTags.containsKey(APPLICABLE_TO.unwrap()));
        assertEquals(expected, enhancedTags.get(APPLICABLE_TO.unwrap()));
    }

    @Test
    @Parameters({"yes", "designated", "permissive"})
    public void accessTagOnNodeExist(final String yes) {
        final Node node = node(1)
                .withTag(ACCESS.unwrap(), yes);

        final Map<String, String> enhancedTags = applicableToTagRule.apply(node.tags());

        assertTrue(enhancedTags.containsKey(APPLICABLE_TO.unwrap()));
        assertEquals(enhancedTags.get(APPLICABLE_TO.unwrap()), ACCESS_THROUGH_TRAFFIC.unwrap());
    }

    @Test
    @Parameters({"motorcar", "bus", "taxi", "hov", "foot", "truck", "delivery", "emergency", "motorcycle"})
    public void accessYesVehicleNoValue(final String vehicle) {
        final Node node = node(1)
                .withTag(ACCESS.unwrap(), "yes")
                .withTag(vehicle, "no");

        final Map<String, String> enhancedTags = applicableToTagRule.apply(node.tags());

        assertTrue(enhancedTags.containsKey(APPLICABLE_TO.unwrap()));
        assertEquals(applicableToValues
                        .stream()
                        .filter(v -> !v.equals(vehicle))
                        .collect(Collectors.joining(";")),
                enhancedTags.get(APPLICABLE_TO.unwrap()));
    }
}
