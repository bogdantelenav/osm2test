package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.rules.whitelist.GrabWhiteList;
import com.telenav.adapter.rules.whitelist.NgxWhiteList;
import com.telenav.datapipelinecore.osm.OsmSourceTagKey;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * Unit tests for {@link CleanupRule}.
 *
 * @author ioanao
 */
public class CleanupRuleTest extends BaseTest {
    private static WaySection waySectionForManualFlags;
    private static WaySection waySectionForCleanup;

    @BeforeClass
    public static void init() {
        waySectionForManualFlags = waysection(3, 1)
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "residential")
                .withTag("tn__source:oneway", "manual")
                .withTag("tn__source:maxspeed", "manual")
                .withTag("tn__source:maxspeed:forward", "manual")
                .withTag("tn__source:maxspeed:backward", "manual")
                .withTag("tn__source:geometry", "manual")
                .withTag("tn__source:highway", "manual")
                .withTag("tn__source:name", "manual")
                .withTag("tn__source:destination", "manual")
                .withTag("tn__source:toll", "manual");

        waySectionForCleanup = waysection(3, 1)
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(TnTagKey.TN_ORIGINAL_ID.unwrap(), "123")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "1")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "1")
                .withTag(OsmTagKey.ONEWAY.telenavPrefix(), "1")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.archivedKeyName(), "1")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.archivedKeyName(), "1")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "1")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "1")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.IMAGES), "1")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES), "1")
                .withTag(TnTagKey.TN_VALIDATION_STATUS.unwrap(), "true");
    }

    @Test
    public void waySectionWithoutTags() {
        WaySection waySection = waysection(3, 1);
        final WaySection adaptedOneWaySection = new CleanupRule<WaySection>(new NgxWhiteList()).apply(waySection);
        assertEquals(waySection, adaptedOneWaySection);
    }

    @Test
    public void waySectionWithoutTNTags() {
        final WaySection waySection = waysection(3, 1).withTag("highway", "motorway");
        final WaySection adaptedOneWaySection = new CleanupRule<WaySection>(new NgxWhiteList()).apply(waySection);
        assertEquals(waySection, adaptedOneWaySection);
    }

    @Test
    public void waySectionWithTNTagsAndNgxRules() {
        final WaySection adaptedWaySection =
                new CleanupRule<WaySection>(new NgxWhiteList()).apply(waySectionForCleanup);

        assertEquals(waySectionForCleanup.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySectionForCleanup.nodes(), adaptedWaySection.nodes());
        assertEquals(6, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "yes"));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_ORIGINAL_ID.unwrap(), "123"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "1"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "1"));
        assertTrue(adaptedWaySection
                .hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.IMAGES), "1"));
    }

    private String manualTag(final String feature) {
        return OsmSourceTagKey.SOURCE.telenavPrefix() + ":" + feature;
    }

    @Test
    public void doNotWhiteListManualEditsWhenNgxWhiteList() {
        final WaySection adaptedWaySection = new CleanupRule<WaySection>(new NgxWhiteList()).apply(waySectionForManualFlags);

        assertEquals(waySectionForManualFlags.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySectionForManualFlags.nodes(), adaptedWaySection.nodes());
        assertEquals(2, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "yes"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), "residential"));
    }

}