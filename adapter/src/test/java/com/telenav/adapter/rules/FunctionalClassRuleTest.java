package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import com.telenav.adapter.bean.RoadFunctionalClass;
import com.telenav.adapter.component.extractors.RoadFunctionalClassExtractor;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;


/**
 * @author adrianal
 */
@RunWith(MockitoJUnitRunner.class)
public class FunctionalClassRuleTest {

    private static WaySection waySection;

    @Mock
    private RoadFunctionalClassExtractor extractor;


    @BeforeClass
    public static void init() {
        final Node node1 = new Node(1, Latitude.degrees(24.32), Longitude.degrees(53.32));
        final Node node2 = new Node(2, Latitude.degrees(34.32), Longitude.degrees(12.32));
        waySection = new WaySection(3L, Arrays.asList(node1, node2), Collections.emptyMap()).withSequenceNumber(1);
    }

    @Test
    public void addFCTag() {
        Mockito.when(extractor.extract(waySection.tags())).thenReturn(Optional.of(RoadFunctionalClass.FIRST_CLASS));
        final WaySection waySectionWithFC = new FunctionalClassRule(extractor).apply(waySection);

        Mockito.verify(extractor).extract(waySection.tags());
        assertThat(waySectionWithFC.strictIdentifier().wayId(), is(3L));
        assertThat(waySectionWithFC.sequenceNumber(), is((short) 1));
        assertThat(waySectionWithFC.tags().values(), hasSize(1));
        assertThat(waySectionWithFC.tags(), hasEntry(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap(),
                RoadFunctionalClass.identifierFor(RoadFunctionalClass.FIRST_CLASS) + ""));
    }

    @Test
    public void noFCTag() {
        Mockito.when(extractor.extract(waySection.tags())).thenReturn(Optional.empty());

        final FunctionalClassRule functionalClassEnhancer = new FunctionalClassRule(extractor);
        final WaySection waySectionWithFC = functionalClassEnhancer.apply(waySection);

        Mockito.verify(extractor).extract(waySection.tags());
        assertThat(waySectionWithFC, is(waySection));
    }
}
