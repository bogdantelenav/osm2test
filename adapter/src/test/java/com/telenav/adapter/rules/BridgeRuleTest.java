package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.BridgeExtractor;
import com.telenav.adapter.component.mapper.OpenStreetMapToUniDbBridgeMapper;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagValue;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.WaySection;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

/**
 * @author Andrei Puf
 */
@RunWith(JUnitParamsRunner.class)
public class BridgeRuleTest extends BaseTest {
    BridgeRule bridgeRule;

    @Before
    public void init() {
        bridgeRule = new BridgeRule(new BridgeExtractor(), new OpenStreetMapToUniDbBridgeMapper());
    }


    @Test
    @Parameters({"Sourceboardwalk","covered", "skybridge", "overpass", "movable", "truss", "trestle", "aqueduct",
            "yes", "viaduct", "boardwalk", "cantilever", "simple_brunnel", "suspension", "true", "bent", "pontoon", "footbridge",
            "pier", "log_bridge", "bridge", "causeway"})
    public void yesBridge(String tagValue) {
        final RawWay rawWay = rawWay(1)
                .withTag(OsmTagKey.BRIDGE.unwrap(), tagValue);
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.BRIDGE.unwrap(), tagValue);
        final RawWay enhancedRawWay = (RawWay) bridgeRule.apply(rawWay);
        final WaySection enhancedWaySection = (WaySection) bridgeRule.apply(waySection);
        assertTrue("Bridge tag not present.",
                enhancedRawWay.hasKey(UnidbTagKey.BRIDGE.unwrap()));
        assertTrue("Bridge tag not present.",
                enhancedWaySection.hasKey(UnidbTagKey.BRIDGE.unwrap()));
        assertEquals("Bridge value not unified to 'yes'.",
                UnidbTagValue.YES.unwrap(), enhancedRawWay.tags().get(UnidbTagKey.BRIDGE.unwrap()));
        assertEquals("Bridge value not unified to 'yes'.",
                UnidbTagValue.YES.unwrap(), enhancedWaySection.tags().get(UnidbTagKey.BRIDGE.unwrap()));
    }

    @Test
    @Parameters({"pedestrian", "low_water_crossing", "proposed", "no", "random_value"}) /* Only several of them since all non-whitelisted will be removed */
    public void randomValues(String tagValue) {
        final RawWay way = rawWay(1)
                .withTag(OsmTagKey.BRIDGE.unwrap(), tagValue);
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.BRIDGE.unwrap(), tagValue);
        final RawWay enhancedRawWay = (RawWay) bridgeRule.apply(way);
        final WaySection enhancedWaySection = (WaySection) bridgeRule.apply(waySection);
        assertFalse(String.format("Non-normalized '%s' bridge values not removed.", tagValue),
                enhancedRawWay.hasKey(UnidbTagKey.BRIDGE.unwrap()));
        assertFalse(String.format("Non-normalized '%s' bridge values not removed.", tagValue),
                enhancedWaySection.hasKey(UnidbTagKey.BRIDGE.unwrap()));
    }
}
