package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;


/**
 * Unit tests for {@link CleanNavigableTagRule}.
 *
 * @author odrab
 */
public class CleanSpeedLimitRuleTest extends BaseTest {
    final Map<Long, Integer> speedLimitByUsStates = new HashMap<Long, Integer>() {{
        put(1L, 50);
        put(3L, 35);
    }};
    final Map<Long, Integer> speedLimitByMexStates = new HashMap<>();
    final Map<String,String> speedLimitsByEuCountries = new HashMap<String,String>(){{
        put("ROU","130");
        put("LVA","100");
    }};

    @Test
    public void invalidMaxspeedUSA() {
        final WaySection waySectionToClean = waysection(3, 1)
                .withTag("iso", "USA")
                .withTag("l2:right", "1")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "300")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "300")
                .withTag("tagToRemain", "");
        final WaySection waySection = waysection(3, 1)
                .withTag("iso", "USA")
                .withTag("l2:right", "1")
                .withTag("tagToRemain", "");
        final WaySection cleanedWaySection = new CleanSpeedLimitRule(speedLimitByUsStates, speedLimitsByEuCountries).apply(waySectionToClean);
        assertEquals(waySection, cleanedWaySection);
    }

    @Test
    public void invalidMinspeedUSA() {
        final WaySection waySectionToClean = waysection(3, 1)
                .withTag("iso", "USA")
                .withTag("l2:right", "1")
                .withTag(OsmTagKey.MINSPEED_BACKWARD.unwrap(), "300")
                .withTag(OsmTagKey.MINSPEED_FORWARD.unwrap(), "300")
                .withTag("tagToRemain", "");
        final WaySection waySection = waysection(3, 1)
                .withTag("iso", "USA")
                .withTag("l2:right", "1")
                .withTag("tagToRemain", "");
        final WaySection cleanedWaySection = new CleanSpeedLimitRule(speedLimitByUsStates, speedLimitsByEuCountries).apply(waySectionToClean);
        assertEquals(waySection, cleanedWaySection);
    }

    @Test
    public void invalidMinspeedMEX() {
        final WaySection waySectionToClean = waysection(3, 1)
                .withTag("iso", "MEX")
                .withTag(OsmTagKey.MINSPEED_BACKWARD.unwrap(), "300")
                .withTag(OsmTagKey.MINSPEED_FORWARD.unwrap(), "300")
                .withTag("tagToRemain", "");
        final WaySection waySection = waysection(3, 1)
                .withTag("iso", "MEX")
                .withTag("tagToRemain", "");
        final WaySection cleanedWaySection = new CleanSpeedLimitRule(speedLimitByMexStates, speedLimitsByEuCountries).apply(waySectionToClean);
        assertEquals(waySection, cleanedWaySection);
    }

    @Test
    public void invalidAdminHierarchy() {
        final WaySection waySectionToClean = waysection(3, 1)
                .withTag("l2:right", null)
                .withTag("iso", null)
                .withTag("tagToRemain", "");
        final WaySection waySection = waysection(3, 1)
                .withTag("l2:right", null)
                .withTag("iso", null)
                .withTag("tagToRemain", "");
        final WaySection cleanedWaySection = new CleanSpeedLimitRule(speedLimitByMexStates, speedLimitsByEuCountries).apply(waySectionToClean);
        assertEquals(waySection, cleanedWaySection);
    }

    @Test
    public void emptySpeedLimitMap() {
        final WaySection waySectionToClean = waysection(3, 1)
                .withTag("l2:right", "123")
                .withTag("iso", "USA")
                .withTag("tagToRemain", "");
        final WaySection waySection = waysection(3, 1)
                .withTag("l2:right", "123")
                .withTag("iso", "USA")
                .withTag("tagToRemain", "");
        final WaySection cleanedWaySection = new CleanSpeedLimitRule(speedLimitByMexStates, speedLimitsByEuCountries).apply(waySectionToClean);
        assertEquals(waySection, cleanedWaySection);
    }

    @Test
    public void testCleanSpeedLimitRuleNotDivisibleBy5() {
        final WaySection waySectionToClean = waysection(3, 1)
                .withTag("iso", "USA")
                .withTag("l2:right", "1")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "37")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "37")
                .withTag("tagToRemain", "");
        final WaySection waySection = waysection(3, 1)
                .withTag("iso", "USA")
                .withTag("l2:right", "1")
                .withTag("tagToRemain", "");
        final WaySection cleanedWaySection = new CleanSpeedLimitRule(speedLimitByUsStates, speedLimitsByEuCountries).apply(waySectionToClean);
        assertEquals(waySection, cleanedWaySection);
    }

    @Test
    public void testCleanSpeedLimitRuleLimitEqualsStateLimit() {
        final WaySection waySectionToClean = waysection(3, 1)
                .withTag("iso", "USA")
                .withTag("l2:right", "3")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "35")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "35");
        final WaySection waySection = waysection(3, 1)
                .withTag("iso", "USA")
                .withTag("l2:right", "3")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "35")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "35");
        final WaySection cleanedWaySection = new CleanSpeedLimitRule(speedLimitByUsStates, speedLimitsByEuCountries).apply(waySectionToClean);
        assertEquals(waySection, cleanedWaySection);
    }

    @Test
    public void testCleanSpeedLimitRuleLimitGreaterThanCountryLimit() {
        final WaySection waySectionToClean = waysection(3, 1)
                .withTag("iso", "ROU")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "155");
        final WaySection waySection = waysection(3, 1)
                .withTag("iso", "ROU");
        final WaySection cleanedWaySection = new CleanSpeedLimitRule(speedLimitByUsStates, speedLimitsByEuCountries).apply(waySectionToClean);
        assertEquals(waySection, cleanedWaySection);
    }

    @Test
    public void testCleanSpeedLimitRuleLimitSmallerThanCountryLimitROU() {
        final WaySection waySectionToClean = waysection(3, 1)
                .withTag("iso", "ROU")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "110");
        final WaySection waySection = waysection(3, 1)
                .withTag("iso", "ROU")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "110");
        final WaySection cleanedWaySection = new CleanSpeedLimitRule(speedLimitByUsStates, speedLimitsByEuCountries).apply(waySectionToClean);
        assertEquals(waySection, cleanedWaySection);
    }

    @Test
    public void testCleanSpeedLimitRuleLimitSmallerThanCountryLimitLVA() {
        final WaySection waySectionToClean = waysection(3, 1)
                .withTag("iso", "LVA")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "110");
        final WaySection waySection = waysection(3, 1)
                .withTag("iso", "LVA");
        final WaySection cleanedWaySection = new CleanSpeedLimitRule(speedLimitByUsStates, speedLimitsByEuCountries).apply(waySectionToClean);
        assertEquals(waySection, cleanedWaySection);
    }

    // https://www.openstreetmap.org/way/127382455#map=9/56.1823/26.9014
    @Test
    public void testCleanSpeedLimitLVAFromOsm(){
        final WaySection LVAWaySection = waysection(127382455,2)
                .withTag("iso","LVA")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "110")
                .withTag("surface", "asphalt")
                .withTag( "source:motorcar", "osm").withTag("highway","trunk");

        final WaySection adaptedLVAWaySection = new CleanSpeedLimitRule(speedLimitByUsStates, speedLimitsByEuCountries).apply(LVAWaySection);

        final WaySection LVAWaySectionCorrected = waysection(127382455,2)
                .withTag("iso","LVA")
                .withTag("surface", "asphalt")
                .withTag( "source:motorcar", "osm").withTag("highway","trunk");

        assertEquals(LVAWaySectionCorrected,adaptedLVAWaySection);

    }

}