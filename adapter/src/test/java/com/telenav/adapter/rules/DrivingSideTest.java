package com.telenav.adapter.rules;

import com.telenav.adapter.bean.DrivingSide;
import com.telenav.adapter.component.DrivingSideFinder;
import com.telenav.map.entity.Node;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.junit.Test;

import java.io.IOException;

public class DrivingSideTest {

    @Test
    public void testGreatBritain() throws IOException {

        final DrivingSideFinder drivingSideFinder = new DrivingSideFinder();

        final Node london = new Node(107775, Latitude.degrees(51.5073219), Longitude.degrees(-0.1276474));

        final DrivingSide drivingSide = drivingSideFinder.getDrivingSide(london);
        assert drivingSide == DrivingSide.LEFT;
    }

    @Test
    public void testGibraltar() throws IOException {

        final DrivingSideFinder drivingSideFinder = new DrivingSideFinder();

        final Node gibraltar = new Node(267933614, Latitude.degrees(36.1408070), Longitude.degrees(-5.3541295));

        final DrivingSide drivingSide = drivingSideFinder.getDrivingSide(gibraltar);
        assert drivingSide == DrivingSide.RIGHT;
    }

    @Test
    public void testGermany() throws IOException {

        final DrivingSideFinder drivingSideFinder = new DrivingSideFinder();

        final Node berlin = new Node(240109189, Latitude.degrees(52.5170365), Longitude.degrees(13.3888599));

        final DrivingSide drivingSide = drivingSideFinder.getDrivingSide(berlin);
        assert drivingSide == DrivingSide.RIGHT;
    }

    @Test
    public void testFrance() throws IOException {

        final DrivingSideFinder drivingSideFinder = new DrivingSideFinder();

        final Node paris = new Node(17807753, Latitude.degrees(48.8566969), Longitude.degrees(2.3514616));

        final DrivingSide drivingSide = drivingSideFinder.getDrivingSide(paris);
        assert drivingSide == DrivingSide.RIGHT;
    }

    @Test
    public void testIndia() throws IOException {

        final DrivingSideFinder drivingSideFinder = new DrivingSideFinder();

        final Node mumbai = new Node(16173235, Latitude.degrees(19.0759899), Longitude.degrees(72.8773928));

        final DrivingSide drivingSide = drivingSideFinder.getDrivingSide(mumbai);
        assert drivingSide == DrivingSide.LEFT;
    }

    @Test
    public void testCanada() {

        final DrivingSideFinder drivingSideFinder = new DrivingSideFinder();

        final Node ottawa = new Node(18886011, Latitude.degrees(45.4211060), Longitude.degrees(-75.6903080));

        final DrivingSide drivingSide = drivingSideFinder.getDrivingSide(ottawa);
        assert drivingSide == DrivingSide.RIGHT;
    }

    @Test
    public void testMexico() {

        final DrivingSideFinder drivingSideFinder = new DrivingSideFinder();

        final Node mexicoCity = new Node(62270270, Latitude.degrees(19.4326296), Longitude.degrees(-99.1331785));

        final DrivingSide drivingSide = drivingSideFinder.getDrivingSide(mexicoCity);
        assert drivingSide == DrivingSide.RIGHT;
    }

    @Test
    public void testUnitedStatesCalifornia() {

        final DrivingSideFinder drivingSideFinder = new DrivingSideFinder();

        final Node sanFrancisco = new Node(26819236, Latitude.degrees(37.7790262), Longitude.degrees(-122.4199061));

        final DrivingSide drivingSide = drivingSideFinder.getDrivingSide(sanFrancisco);
        assert drivingSide == DrivingSide.RIGHT;
    }

    @Test
    public void testUnitedStatesVirginIslands() {

        final DrivingSideFinder drivingSideFinder = new DrivingSideFinder();

        final Node charlotteAmalie = new Node(501349170, Latitude.degrees(18.3390800), Longitude.degrees(-64.9146800));

        final DrivingSide drivingSide = drivingSideFinder.getDrivingSide(charlotteAmalie);
        assert drivingSide == DrivingSide.LEFT;
    }
}
