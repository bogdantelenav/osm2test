package com.telenav.adapter.rules.node;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

public class CleanNodeTagsRuleTest {

    @Test
    public void testCleanTrafficLightsTags() {

        final CleanNodeTagsRule rule = new CleanNodeTagsRule();

        final Map<String, String> expectedTags = new HashMap<>();

        expectedTags.put(OsmTagKey.HIGHWAY.unwrap(), "crossing");
        expectedTags.put("crossing", "traffic-lights");

        final Map<String, String> actualTags = rule.apply(expectedTags);

        Assert.assertFalse(actualTags.containsKey(OsmTagKey.ONEWAY.unwrap()));
        Assert.assertFalse(actualTags.containsKey("crossing"));
    }
}
