package com.telenav.adapter.rules;

import com.telenav.adapter.bean.SpeedCategory;
import com.telenav.adapter.component.extractors.HighwayBasedSpeedCategoryExtractor;
import com.telenav.adapter.component.extractors.MaxSpeedBasedSpeedCategoryExtractor;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertThat;


/**
 * @author petrum
 */
@RunWith(MockitoJUnitRunner.class)
public class SpeedCategoryRuleTest {

    private static WaySection waySection;

    @Mock
    private HighwayBasedSpeedCategoryExtractor highwayBasedSpeedCategoryExtractor;

    @Mock
    private MaxSpeedBasedSpeedCategoryExtractor maxSpeedBasedSpeedCategoryExtractor;

    @BeforeClass
    public static void init() {
        final Node node1 = new Node(1, Latitude.degrees(24.32), Longitude.degrees(53.32));
        final Node node2 = new Node(2, Latitude.degrees(34.32), Longitude.degrees(12.32));
        waySection = new WaySection(3L, Arrays.asList(node1, node2), Collections.emptyMap());
    }


    @Test
    public void addSpeedCategoryTagBasedOnMaxSpeedTag() {
        Mockito.when(maxSpeedBasedSpeedCategoryExtractor.extract(waySection.tags()))
                .thenReturn(Optional.of(SpeedCategory.SPEED_CAT_GREATER_THAN_130));
        Mockito.when(highwayBasedSpeedCategoryExtractor.extract(waySection.tags()))
                .thenReturn(Optional.of(SpeedCategory.SPEED_CAT_LESS_THAN_11));
        final WaySection enhancedWaySection =
                new SpeedCategoryRule(highwayBasedSpeedCategoryExtractor, maxSpeedBasedSpeedCategoryExtractor)
                        .apply(waySection);

        assertThat(enhancedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(enhancedWaySection.nodes(), is(waySection.nodes()));
        assertThat(enhancedWaySection.tags().size(), is(1));
        assertThat(enhancedWaySection.tags(), hasEntry(RoadCategoryTagKey.SPEED_CATEGORY.unwrap(), "1"));
    }

    @Test
    public void addSpeedCategoryTagBasedOnHighwayTag() {
        Mockito.when(maxSpeedBasedSpeedCategoryExtractor.extract(waySection.tags())).thenReturn(Optional.empty());
        Mockito.when(highwayBasedSpeedCategoryExtractor.extract(waySection.tags()))
                .thenReturn(Optional.of(SpeedCategory.SPEED_CAT_BETWEEN_51_70));
        final WaySection enhancedWaySection =
                new SpeedCategoryRule(highwayBasedSpeedCategoryExtractor, maxSpeedBasedSpeedCategoryExtractor)
                        .apply(waySection);

        assertThat(enhancedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(enhancedWaySection.nodes(), is(waySection.nodes()));
        assertThat(enhancedWaySection.tags().size(), is(1));
        assertThat(enhancedWaySection.tags(), hasEntry(RoadCategoryTagKey.SPEED_CATEGORY.unwrap(), "7"));
    }

    @Test
    public void noSpeedCategoryTagAdded() {
        Mockito.when(maxSpeedBasedSpeedCategoryExtractor.extract(waySection.tags())).thenReturn(Optional.empty());
        Mockito.when(highwayBasedSpeedCategoryExtractor.extract(waySection.tags())).thenReturn(Optional.empty());

        final WaySection waySectionWithSC =
                new SpeedCategoryRule(highwayBasedSpeedCategoryExtractor, maxSpeedBasedSpeedCategoryExtractor)
                        .apply(waySection);

        assertThat(waySectionWithSC, is(waySection));
    }
}
