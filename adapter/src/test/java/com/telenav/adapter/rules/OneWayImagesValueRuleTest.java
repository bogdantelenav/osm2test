package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * @author dianat2
 */
public class OneWayImagesValueRuleTest extends BaseTest {

    private OneWayImagesValueRule oneWayImagesValueRule = new OneWayImagesValueRule();

    @Test
    public void adaptOneWayImagesValue_hasForwardValue_valueIsYesInsteadOf1() {
        // given
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), DefaultTagValue.FORWARD_ONEWAY.unwrap());

        Map<String, String> expectedTags = new HashMap<>();
        expectedTags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "yes");

        // when
        final WaySection result = oneWayImagesValueRule.apply(waySection);

        // then
        assertThat(result.strictIdentifier()).isEqualTo(waySection.strictIdentifier());
        assertThat(result.nodes()).containsExactlyInAnyOrderElementsOf(waySection.nodes());
        assertThat(result.tags().size()).isEqualTo(expectedTags.size());
        assertThat(result.tags()).containsAllEntriesOf(expectedTags);
    }

    @Test
    public void adaptOneWayImagesValue_hasBackwardValue_valueIsYesInsteadOfMinus1() {
        // given
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), DefaultTagValue.BACKWARD_ONEWAY.unwrap());

        Map<String, String> expectedTags = new HashMap<>();
        expectedTags.put(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES), "-1");

        // when
        final WaySection result = oneWayImagesValueRule.apply(waySection);

        // then
        assertThat(result.strictIdentifier()).isEqualTo(waySection.strictIdentifier());
        assertThat(result.nodes()).containsExactlyInAnyOrderElementsOf(waySection.nodes());
        assertThat(result.tags().size()).isEqualTo(expectedTags.size());
        assertThat(result.tags()).containsAllEntriesOf(expectedTags);
    }

    @Test
    public void adaptOneWayImagesValue_notForwardNorBackward_doNothing() {
        // given
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "yes");

        Map<String, String> expectedTags = new HashMap<>();
        expectedTags.put(OsmTagKey.MOTORCAR.unwrap(), "yes");

        // when
        final WaySection result = oneWayImagesValueRule.apply(waySection);

        // then
        assertThat(result.strictIdentifier()).isEqualTo(waySection.strictIdentifier());
        assertThat(result.nodes()).containsExactlyInAnyOrderElementsOf(waySection.nodes());
        assertThat(result.tags().size()).isEqualTo(expectedTags.size());
        assertThat(result.tags()).containsAllEntriesOf(expectedTags);
    }

}