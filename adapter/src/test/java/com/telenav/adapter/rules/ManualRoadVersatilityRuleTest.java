package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmSourceTagKey;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import static com.telenav.datapipelinecore.osm.OsmHighwayValue.MOTORWAY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * @author roxanal
 */
public class ManualRoadVersatilityRuleTest extends BaseTest {

    @Test
    public void manualEditAndMotorcarValuePresent() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.MOTORCAR.unwrap(), "no")
                .withTag(OsmSourceTagKey.SOURCE.telenavPrefix() + ":" + OsmTagKey.MOTORCAR.unwrap(),
                        TnSourceValue.MANUAL.unwrap());
        final WaySection adaptedWaySection = new ManualRoadVersatilityRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(2, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.unwrap(), "no"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.MANUAL), "no"));
    }

    @Test
    public void manualEditAndMotorcycleAndMotorcarValuePresent() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.MOTORCAR.unwrap(), "no")
                .withTag(OsmTagKey.MOTORCYCLE.unwrap(), "yes")
                .withTag(OsmSourceTagKey.SOURCE.telenavPrefix() + ":" + OsmTagKey.MOTORCAR.unwrap(),
                        TnSourceValue.MANUAL.unwrap());
        final WaySection adaptedWaySection = new ManualRoadVersatilityRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(3, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.unwrap(), "no"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCYCLE.unwrap(), "yes"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCAR.withSource(TnSourceValue.MANUAL), "no"));
    }

    @Test
    public void onlyManualEditPresent() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmSourceTagKey.SOURCE.telenavPrefix() + ":" + OsmTagKey.MOTORCYCLE.unwrap(),
                        TnSourceValue.MANUAL.unwrap());
        final WaySection adaptedWaySection = new ManualRoadVersatilityRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(1, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection
                .hasTag(OsmTagKey.MOTORCYCLE.withSource(TnSourceValue.MANUAL), TnSourceValue.MISSING.unwrap()));
    }

    @Test
    public void onlyMotorcycleValuePresent() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.MOTORCYCLE.unwrap(), "no");
        final WaySection adaptedWaySection = new ManualRoadVersatilityRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(1, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MOTORCYCLE.unwrap(), "no"));
    }

    @Test
    public void manualEditAndMotorcycleAndMotorcarValueMissing() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap());
        final WaySection adaptedWaySection = new ManualRoadVersatilityRule().apply(waySection);

        assertEquals(waySection.strictIdentifier(), adaptedWaySection.strictIdentifier());
        assertEquals(waySection.nodes(), adaptedWaySection.nodes());
        assertEquals(1, adaptedWaySection.tags().size());
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
    }
}