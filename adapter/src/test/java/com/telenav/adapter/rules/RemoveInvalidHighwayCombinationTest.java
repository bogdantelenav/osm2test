package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class RemoveInvalidHighwayCombinationTest extends BaseTest {
    @Test
    public void testHighwayAndRouteFerry() {
       //Given
        Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.ROUTE.unwrap(), "ferry");
        tags.put(OsmTagKey.HIGHWAY.unwrap(), "service");
        WaySection waySection = waysection(1, 1).withTags(tags);
        //when
        RemoveInvalidHighwayCombination removeInvalidHighwayCombination = new RemoveInvalidHighwayCombination();
        waySection = removeInvalidHighwayCombination.apply(waySection);
        //Then
        assertTrue(waySection.hasTag(OsmTagKey.ROUTE.unwrap(), "ferry"));
        assertFalse(waySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), "service"));

    }
}
