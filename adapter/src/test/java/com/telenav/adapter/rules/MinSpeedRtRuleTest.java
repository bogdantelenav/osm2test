package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;


/**
 * Unit tests for {@link MinSpeedRtRule} class.
 *
 * @author petrum
 */
public class MinSpeedRtRuleTest {

    private static WaySection basicWaySection;


    @BeforeClass
    public static void init() {
        final Node node1 = new Node(1, Latitude.degrees(24.32), Longitude.degrees(53.32));
        basicWaySection =
                new WaySection(3L, Collections.singletonList(node1), Collections.emptyMap()).withSequenceNumber(1);
    }


    @Test
    public void doNotAddAnyTagInCaseOfMissingSpeedUnit() {
        final WaySection waySection = basicWaySection
                .withTag(OsmTagKey.MINSPEED_FORWARD.unwrap(), "30")
                .withTag(OsmTagKey.MINSPEED_BACKWARD.unwrap(), "30")
                .withTag(OsmTagKey.MINSPEED.unwrap(), "50");
        final WaySection adaptedWaySection = new MinSpeedRtRule().apply(waySection);

        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void addMinSpeedValuesWithoutAnyChange() {
        final WaySection waySection = basicWaySection
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MINSPEED_FORWARD.unwrap(), "30 mph")
                .withTag(OsmTagKey.MINSPEED_BACKWARD.unwrap(), "31 mph")
                .withTag(OsmTagKey.MINSPEED.unwrap(), "32 mph");
        final WaySection adaptedWaySection = new MinSpeedRtRule().apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier(), is(basicWaySection.strictIdentifier()));
        assertThat(adaptedWaySection.sequenceNumber(), is(basicWaySection.sequenceNumber()));
        assertThat(adaptedWaySection.nodes(), is(basicWaySection.nodes()));
        assertThat(adaptedWaySection.tags().values(), hasSize(7));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_FORWARD.unwrap(), "30 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_BACKWARD.unwrap(), "31 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED.unwrap(), "32 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_FORWARD.telenavPrefix(), "30"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_BACKWARD.telenavPrefix(), "30"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED.telenavPrefix(), "30"));
    }

    @Test
    public void addMinSpeedValuesWithNormalizedValues() {
        final WaySection waySection = basicWaySection
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MINSPEED_FORWARD.unwrap(), "30 mph")
                .withTag(OsmTagKey.MINSPEED_BACKWARD.unwrap(), "31 mph")
                .withTag(OsmTagKey.MINSPEED.unwrap(), "32");
        final WaySection adaptedWaySection = new MinSpeedRtRule().apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier(), is(basicWaySection.strictIdentifier()));
        assertThat(adaptedWaySection.sequenceNumber(), is(basicWaySection.sequenceNumber()));
        assertThat(adaptedWaySection.nodes(), is(basicWaySection.nodes()));
        assertThat(adaptedWaySection.tags().values(), hasSize(7));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_FORWARD.unwrap(), "30 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_BACKWARD.unwrap(), "31 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED.unwrap(), "32"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_FORWARD.telenavPrefix(), "30"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MINSPEED_BACKWARD.telenavPrefix(), "30"));

    }
}
