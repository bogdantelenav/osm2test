package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.AccessExtractor;
import com.telenav.adapter.component.mapper.OpenStreetMapToUniDbAccessMapper;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagValue;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.WaySection;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class WaysAccessRuleTest extends BaseTest {
    WaysAccessRule waysAccessRule;

    @Before
    public void init() {
        waysAccessRule = new WaysAccessRule(new AccessExtractor(), new OpenStreetMapToUniDbAccessMapper());
    }

    @Test
    @Parameters({"yes", "permissive", "destination", "public", "fee", "agreement use", "right of way",
            "official", "emergency", "agreement_use", "community_space", "carpool"})
    public void publicRoad(String tagValue) {
        final RawWay rawWay = rawWay(1)
                .withTag(OsmTagKey.ACCESS.unwrap(), tagValue);
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.ACCESS.unwrap(), tagValue);
        final RawWay enhancedRawWay = (RawWay) waysAccessRule.apply(rawWay);
        final WaySection enhancedWaySection = (WaySection) waysAccessRule.apply(waySection);

        assertTrue("Access tag not present for public road.",
                enhancedRawWay.hasKey(UnidbTagKey.ACCESS.unwrap()));
        assertTrue("Access tag not present for public road.",
                enhancedWaySection.hasKey(UnidbTagKey.ACCESS.unwrap()));
        assertEquals("Access value not unified to 'public' for public road.",
                UnidbTagValue.PUBLIC.unwrap(), enhancedRawWay.tags().get(UnidbTagKey.ACCESS.unwrap()));
        assertEquals("Access value not unified to 'public' for public road.",
                UnidbTagValue.PUBLIC.unwrap(), enhancedWaySection.tags().get(UnidbTagKey.ACCESS.unwrap()));

    }

    @Test
    @Parameters({"private", "customers", "no", "permit", "staff", "residents", "employees", "students", "restricted",
            "airspace", "residential", "military", "delivery", "customers;employees", "agricultural", "members",
            "students;visitors", "visitors", "private;customers", "permit\\,_metered", "forestry", "multifamily",
            "discouraged", "agricultural;forestry", "university", "customer", "customers;private"})
    public void privateRoad(String tagValue) {
        final RawWay rawWay = rawWay(1)
                .withTag(OsmTagKey.ACCESS.unwrap(), tagValue);
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.ACCESS.unwrap(), tagValue);
        final RawWay enhancedRawWay = (RawWay) waysAccessRule.apply(rawWay);
        final WaySection enhancedWaySection = (WaySection) waysAccessRule.apply(waySection);

        assertTrue("Access tag not present for public road.",
                enhancedRawWay.hasKey(UnidbTagKey.ACCESS.unwrap()));
        assertTrue("Access tag not present for public road.",
                enhancedWaySection.hasKey(UnidbTagKey.ACCESS.unwrap()));
        assertEquals("Access value not unified to 'public' for public road.",
                UnidbTagValue.PRIVATE.unwrap(), enhancedRawWay.tags().get(UnidbTagKey.ACCESS.unwrap()));
        assertEquals("Access value not unified to 'public' for public road.",
                UnidbTagValue.PRIVATE.unwrap(), enhancedWaySection.tags().get(UnidbTagKey.ACCESS.unwrap()));

    }

    @Test
    @Parameters({"rand1", "rand2", "handicapped", "psv", "non-motorized-only", "south_riding_residents",
            "fleet", "Timothy_Christian_Elementary_School"}) /* Only several of them since all non-whitelisted will be removed */
    public void randomValues(String tagValue) {
        final RawWay rawWay = rawWay(1)
                .withTag(OsmTagKey.ACCESS.unwrap(), tagValue);
        final WaySection waySection = waysection(1)
                .withTag(OsmTagKey.ACCESS.unwrap(), tagValue);
        final RawWay enhancedRawWay = (RawWay) waysAccessRule.apply(rawWay);
        final WaySection enhancedWaySection = (WaySection) waysAccessRule.apply(waySection);

        assertFalse(String.format("Non-unified '%s' access tag not removed.", tagValue),
                enhancedRawWay.hasKey(UnidbTagKey.ACCESS.unwrap()));
        assertFalse(String.format("Non-unified '%s' access tag not removed.", tagValue),
                enhancedWaySection.hasKey(UnidbTagKey.ACCESS.unwrap()));
    }
}
