package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.map.entity.RawWay;
import org.junit.Before;
import org.junit.Test;

import static com.telenav.datapipelinecore.unidb.UnidbTagKey.*;
import static com.telenav.datapipelinecore.unidb.UnidbTagValue.*;
import static org.junit.Assert.*;

/**
 * Unit tests for {@link CartoLineRule}
 *
 * @author irinap
 */
public class CartoLineRuleTest extends BaseTest {
    private final String DUMMY_VALUE = "tag_value";
    private CartoLineRule<RawWay> rawWayRule;

    @Before
    public void initRule() {
        rawWayRule = new CartoLineRule<>();
    }

    /* test on raw ways */

    @Test
    public void testRawWayNoRailwayTag() {
        final RawWay rawWay = rawWay(0);
        final RawWay adaptedRawWay = rawWayRule.apply(rawWay);
        assertEquals(rawWay, adaptedRawWay);
    }

    @Test
    public void testRawWayWithRailwayTag() {
        final RawWay rawWay = rawWay(0).withTag(RAILWAY.unwrap(), DUMMY_VALUE);
        final RawWay adaptedRawWay = rawWayRule.apply(rawWay);
        assertEquals(rawWay, adaptedRawWay);
    }

    @Test
    public void testRawWayWithRailwayTagWithRailRailwayValue() {
        final RawWay rawWay = rawWay(0).withTag(RAILWAY.unwrap(), RAIL.unwrap());
        final RawWay adaptedRawWay = rawWayRule.apply(rawWay);
        assertNotEquals(rawWay, adaptedRawWay);
        assertTrue(adaptedRawWay.hasTag(TYPE.unwrap(), CARTO_LINE.unwrap()));
    }

    @Test
    public void testRawWayWithRailwayTagWithLightRailRailwayValue() {
        final RawWay rawWay = rawWay(0).withTag(RAILWAY.unwrap(), LIGHT_RAIL.unwrap());
        final RawWay adaptedRawWay = rawWayRule.apply(rawWay);
        assertNotEquals(rawWay, adaptedRawWay);
        assertTrue(adaptedRawWay.hasTag(TYPE.unwrap(), CARTO_LINE.unwrap()));
    }

    @Test
    public void testRawWayWithRailwayTagWithSubwayRailwayValue() {
        final RawWay rawWay = rawWay(0).withTag(RAILWAY.unwrap(), SUBWAY.unwrap());
        final RawWay adaptedRawWay = rawWayRule.apply(rawWay);
        assertNotEquals(rawWay, adaptedRawWay);
        assertTrue(adaptedRawWay.hasTag(TYPE.unwrap(), CARTO_LINE.unwrap()));
    }


    @Test
    public void testRawWayWithWaterwayTag() {
        final RawWay rawWay = rawWay(0).withTag(WATERWAY.unwrap(), DUMMY_VALUE);
        final RawWay adaptedRawWay = rawWayRule.apply(rawWay);
        assertEquals(rawWay, adaptedRawWay);
    }

    @Test
    public void testRawWayWithWaterwayTagWithRiverValue() {
        final RawWay rawWay = rawWay(0).withTag(WATERWAY.unwrap(), RIVER.unwrap());
        final RawWay adaptedRawWay = rawWayRule.apply(rawWay);
        assertNotEquals(rawWay, adaptedRawWay);
        assertTrue(adaptedRawWay.hasTag(TYPE.unwrap(), CARTO_LINE.unwrap()));
    }

    @Test
    public void testRawWayWithWaterwayTagWithCanalValue() {
        final RawWay rawWay = rawWay(0).withTag(WATERWAY.unwrap(), CANAL.unwrap());
        final RawWay adaptedRawWay = rawWayRule.apply(rawWay);
        assertNotEquals(rawWay, adaptedRawWay);
        assertTrue(adaptedRawWay.hasTag(TYPE.unwrap(), CARTO_LINE.unwrap()));
    }

    @Test
    public void testRawWayWithWaterwayTagWithWadiValue() {
        final RawWay rawWay = rawWay(0).withTag(WATERWAY.unwrap(), WADI.unwrap());
        final RawWay adaptedRawWay = rawWayRule.apply(rawWay);
        assertNotEquals(rawWay, adaptedRawWay);
        assertTrue(adaptedRawWay.hasTag(TYPE.unwrap(), CARTO_LINE.unwrap()));
    }

    @Test
    public void testRawWayWithBoundaryTag() {
        final RawWay rawWay = rawWay(0).withTag(BOUNDARY.unwrap(), "any");
        final RawWay adaptedRawWay = rawWayRule.apply(rawWay);
        assertNotEquals(rawWay, adaptedRawWay);
        assertTrue(adaptedRawWay.hasTag(TYPE.unwrap(), CARTO_LINE.unwrap()));
    }
}

