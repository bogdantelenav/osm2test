package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.adminTags.AdminL1NameExtractor;
import com.telenav.adapter.component.extractors.HighwayExtractor;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnAdminValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;


/**
 * @author adrianal
 */
public class MotorcycleNoRuleTest extends BaseTest {

    private MotorcycleNoRule motorcycleNoRule;


    @Before
    public void initRule() {
        motorcycleNoRule = new MotorcycleNoRule(new HighwayExtractor(), new AdminL1NameExtractor());
    }

    @Test
    public void addMotorcicleNoOnMotroway() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "motorway")
                .withTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap());
        final WaySection adaptedWaySection = motorcycleNoRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(3));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.HIGHWAY.unwrap(), "motorway"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.MOTORCYCLE.unwrap(), "no"));
        assertThat(adaptedWaySection.tags(),
                hasEntry(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap()));
    }

    @Test
    public void addMotorcicleNoOnMotrowayLink() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "motorway_link")
                .withTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap());
        final WaySection adaptedWaySection = motorcycleNoRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(3));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.HIGHWAY.unwrap(), "motorway_link"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.MOTORCYCLE.unwrap(), "no"));
        assertThat(adaptedWaySection.tags(),
                hasEntry(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap()));
    }

    @Test
    public void doNotAddMotorcicleNoOnOtherHighway() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "primary")
                .withTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap());
        final WaySection adaptedWaySection = motorcycleNoRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(2));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.HIGHWAY.unwrap(), "primary"));
        assertThat(adaptedWaySection.tags(),
                hasEntry(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap()));
    }

    @Test
    public void doNotAddMotorcicleNoWhenNoHighwayTag() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.ONEWAY.unwrap(), "no")
                .withTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap());
        final WaySection adaptedWaySection = motorcycleNoRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(2));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "no"));
        assertThat(adaptedWaySection.tags(),
                hasEntry(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap()));
    }

    @Test
    public void doNotAddMotorcicleNoOnMotrowayLinkWhenNotRegionNotSupported() {
        final WaySection waySection = waysection(3, 1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "motorway_link")
                .withTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), "Michigan");
        final WaySection adaptedWaySection = motorcycleNoRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(2));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.HIGHWAY.unwrap(), "motorway_link"));
        assertThat(adaptedWaySection.tags(), hasEntry(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), "Michigan"));
    }
}