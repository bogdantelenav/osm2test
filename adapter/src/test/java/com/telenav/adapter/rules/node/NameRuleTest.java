package com.telenav.adapter.rules.node;

import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;

public class NameRuleTest {

    @Test
    public void test() {

        final NameRule target = new NameRule();

        final String nameEn = "name:en";
        final String nameDe = "name:de";
        final String nameFr = "name:fr";
        final String value = "value";

        final Map<String, String> expectedTags = new HashMap<>();

        expectedTags.put(nameEn, value);
        expectedTags.put(nameDe, value);
        expectedTags.put(nameFr, value);

        final Map<String, String> actualTags = target.apply(expectedTags);

        final List<String> expectedMissingKeys = Arrays.asList(nameEn, nameDe, nameFr);
        final List<String> expectedKeys = Arrays.asList("name:eng", "name:deu", "name:fra");

        for (final String expectedMissingKey : expectedMissingKeys) {
            assertFalse(actualTags.containsKey(expectedMissingKey));
        }

        for (final String expectedKey : expectedKeys) {
            assertEquals(actualTags.get(expectedKey), value);
        }
    }

    @Test
    public void testUnknownTwoLetterLanguage() {

        final NameRule target = new NameRule();

        final String nameSh = "name:sh";
        final String value = "value";

        final Map<String, String> expectedTags = new HashMap<>();

        expectedTags.put(nameSh, value);

        final Map<String, String> actualTags = target.apply(expectedTags);

        final List<String> expectedMissingKeys = Arrays.asList(nameSh);

        // If the sh is unknown, we don't remove it.
        final List<String> expectedKeys = Arrays.asList("name:sh");

        for (final String expectedMissingKey : expectedMissingKeys) {
            assertTrue(actualTags.containsKey(expectedMissingKey));
        }

        for (final String expectedKey : expectedKeys) {
            assertEquals(actualTags.get(expectedKey), value);
        }
    }
}
