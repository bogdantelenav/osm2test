package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.MaxSpeedRtExtractor;
import com.telenav.adapter.component.extractors.OneWayRtExtractor;
import com.telenav.datapipelinecore.osm.OsmPostFix;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.*;


/**
 * Unit tests for {@link MaxSpeedRtRule}.
 *
 * @author petrum
 */
public class MaxSpeedRtRuleTest {

    private static WaySection basicWaySection;


    @BeforeClass
    public static void init() {
        final Node node1 = new Node(1, Latitude.degrees(24.32), Longitude.degrees(53.32));
        basicWaySection =
                new WaySection(3L, Collections.singletonList(node1), Collections.emptyMap()).withSequenceNumber(1);
    }


    @Test
    public void doNotAddAnyTagInCaseOfMissingSpeedUnit() {
        final WaySection waySection = basicWaySection.withTag(OsmTagKey.MAXSPEED.unwrap(), "50");
        final WaySection adaptedWaySection =
                new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);
        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void doNotAddAnyTagInCaseOfWrongBackwardMaxSpeedValue() {
        final WaySection waySection = MaxSpeedRtRuleTest.basicWaySection
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "test");
        final WaySection adaptedWaySection =
                new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertEquals(waySection, adaptedWaySection);
    }

    @Test
    public void addMaxSpeedBackwardAndIgnoreOtherWrongValues() {
        final WaySection waySection = basicWaySection
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "65 - 7")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "65 mph")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "60 mp");
        final WaySection adaptedWaySection =
                new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier(), is(basicWaySection.strictIdentifier()));
        assertThat(adaptedWaySection.sequenceNumber(), is(basicWaySection.sequenceNumber()));
        assertThat(adaptedWaySection.nodes(), is(basicWaySection.nodes()));
        assertThat(adaptedWaySection.tags().values(), hasSize(6));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "65 - 7"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "65 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "65"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.unwrap(), "60 mp"));
    }

    @Test
    public void addMaxSpeedForwardAndIgnoreOtherWrongValues() {
        final WaySection waySection = basicWaySection
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "65 mph")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "65 mp")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "60 mph");
        final WaySection adaptedWaySection =
                new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier(), is(basicWaySection.strictIdentifier()));
        assertThat(adaptedWaySection.sequenceNumber(), is(basicWaySection.sequenceNumber()));
        assertThat(adaptedWaySection.nodes(), is(basicWaySection.nodes()));
        assertThat(adaptedWaySection.tags().values(), hasSize(6));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "65"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "65"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.unwrap(), "60 mph"));
    }

    @Test
    public void addMaxSpeedForwardAndBackwardFromMaxSpeedIfOneWayTagIsMissing() {
        final WaySection waySection = basicWaySection
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "K")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "60 mph");
        final WaySection adaptedWaySection =
                new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier(), is(basicWaySection.strictIdentifier()));
        assertThat(adaptedWaySection.sequenceNumber(), is(basicWaySection.sequenceNumber()));
        assertThat(adaptedWaySection.nodes(), is(basicWaySection.nodes()));
        assertThat(adaptedWaySection.tags().values(), hasSize(4));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "K"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.unwrap(), "60 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "95"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "95"));
    }

    @Test
    public void addMaxSpeedForwardAndBackwardWithDifferentUnits() {
        final WaySection waySection = basicWaySection
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "K")
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "65 mph")
                .withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "65")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "60 mph");
        final WaySection adaptedWaySection =
                new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier(), is(basicWaySection.strictIdentifier()));
        assertThat(adaptedWaySection.sequenceNumber(), is(basicWaySection.sequenceNumber()));
        assertThat(adaptedWaySection.nodes(), is(basicWaySection.nodes()));
        assertThat(adaptedWaySection.tags().values(), hasSize(6));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "K"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.unwrap(), "60 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "100"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "65"));
    }

    @Test
    public void addMaxSpeedForwardAndBackwardFromMaxSpeedIfOneWayTagIsNo() {
        final WaySection waySection = basicWaySection
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50")
                .withTag(OsmTagKey.ONEWAY.unwrap(), "no")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "K");
        final WaySection adaptedWaySection =
                new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier(), is(basicWaySection.strictIdentifier()));
        assertThat(adaptedWaySection.sequenceNumber(), is(basicWaySection.sequenceNumber()));
        assertThat(adaptedWaySection.nodes(), is(basicWaySection.nodes()));
        assertThat(adaptedWaySection.tags().values(), hasSize(5));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.unwrap(), "50"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "50"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "50"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "no"));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "K"));
    }

    @Test
    public void addMaxSpeedForwardFromMaxSpeedIfOneWayTagIs1() {
        final WaySection waySection = basicWaySection
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "1")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "60 mph");
        final WaySection adaptedWaySection =
                new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier(), is(basicWaySection.strictIdentifier()));
        assertThat(adaptedWaySection.sequenceNumber(), is(basicWaySection.sequenceNumber()));
        assertThat(adaptedWaySection.nodes(), is(basicWaySection.nodes()));
        assertThat(adaptedWaySection.tags().values(), hasSize(4));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "1"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.unwrap(), "60 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "60"));
    }

    @Test
    public void addMaxSpeedForwardFromMaxSpeedIfOneWayTagIsYes() {
        final WaySection waySection = basicWaySection
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "yes")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "60 mph");
        final WaySection adaptedWaySection =
                new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier(), is(basicWaySection.strictIdentifier()));
        assertThat(adaptedWaySection.sequenceNumber(), is(basicWaySection.sequenceNumber()));
        assertThat(adaptedWaySection.nodes(), is(basicWaySection.nodes()));
        assertThat(adaptedWaySection.tags().values(), hasSize(4));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES), "yes"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.unwrap(), "60 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "60"));
    }

    @Test
    public void addMaxSpeedBackwardFromMaxSpeedIfOneWayTagIsReversed() {
        final WaySection waySection = basicWaySection
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50")
                .withTag(OsmTagKey.ONEWAY.unwrap(), "-1")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M");
        final WaySection adaptedWaySection =
                new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier(), is(basicWaySection.strictIdentifier()));
        assertThat(adaptedWaySection.sequenceNumber(), is(basicWaySection.sequenceNumber()));
        assertThat(adaptedWaySection.nodes(), is(basicWaySection.nodes()));
        assertThat(adaptedWaySection.tags().values(), hasSize(4));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.unwrap(), "50"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "50"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "-1"));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
    }

    @Test
    public void addMaxSpeedForwardAndMaxSpeedConditional() {
        final WaySection waySection = basicWaySection
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "50 mph")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED_CONDITIONAL.unwrap(), "50");
        final WaySection adaptedWaySection =
                new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier(), is(basicWaySection.strictIdentifier()));
        assertThat(adaptedWaySection.sequenceNumber(), is(basicWaySection.sequenceNumber()));
        assertThat(adaptedWaySection.nodes(), is(basicWaySection.nodes()));
        assertThat(adaptedWaySection.tags().values(), hasSize(5));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "50 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "50"));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.postFixed(OsmPostFix.CONDITIONAL.unwrap()), "50"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_CONDITIONAL.telenavPrefix(), "50"));
    }

    @Test
    public void doNotAddMaxSpeedConditionalInCaseOfInvalidValue() {
        final WaySection waySection = basicWaySection
                .withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "50 mph")
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED_CONDITIONAL.unwrap(), "50m");
        final WaySection adaptedWaySection =
                new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier(), is(basicWaySection.strictIdentifier()));
        assertThat(adaptedWaySection.sequenceNumber(), is(basicWaySection.sequenceNumber()));
        assertThat(adaptedWaySection.nodes(), is(basicWaySection.nodes()));
        assertThat(adaptedWaySection.tags().values(), hasSize(5));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "50 mph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "50"));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.postFixed(OsmPostFix.CONDITIONAL.unwrap()), "50m"));
    }

    @Test
    public void speedLimitWithSpeedUnitMph() {
        final WaySection waySection = basicWaySection.withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "30")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "residential");

        final WaySection adaptedWaySection = new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "30"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "30"));
    }

    @Test
    public void speedLimitWithSpeedUnitKph() {
        final WaySection waySection = basicWaySection.withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "K")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "30 kph")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "residential");

        final WaySection adaptedWaySection = new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "30"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "30"));
    }

    @Test
    public void speedLimitWithDirtyData() {
        final WaySection waySection = basicWaySection.withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "K")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "30 kfadfasd")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "residential");

        final WaySection adaptedWaySection = new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "30"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "30"));
    }

    @Test
    public void speedLimitWithNoData() {
        final WaySection waySection = basicWaySection.withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "K")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "residential");

        final WaySection adaptedWaySection = new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertFalse(adaptedWaySection.tags().containsKey(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix()));
        assertFalse(adaptedWaySection.tags().containsKey(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix()));
    }
    @Test
    public void addMaxSpeedForwardAndBackwardWithSpeedUnitMilesAndValueOfMaxspeedKph() {
        final WaySection waySection = basicWaySection
                .withTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "97 kph");
        final WaySection adaptedWaySection =
                new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()).apply(waySection);

        assertThat(adaptedWaySection.strictIdentifier(), is(basicWaySection.strictIdentifier()));
        assertThat(adaptedWaySection.sequenceNumber(), is(basicWaySection.sequenceNumber()));
        assertThat(adaptedWaySection.nodes(), is(basicWaySection.nodes()));
        assertThat(adaptedWaySection.tags().values(), hasSize(4));
        assertTrue(adaptedWaySection.hasTag(TnTagKey.TN_SPEED_UNIT.unwrap(), "M"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED.unwrap(), "97 kph"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "60"));
        assertTrue(adaptedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "60"));
    }
}
