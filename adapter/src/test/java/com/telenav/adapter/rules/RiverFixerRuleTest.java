package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.map.entity.RawWay;
import org.junit.Test;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 * @author volodymyrl
 */
public class RiverFixerRuleTest extends BaseTest {

    private final RiverFixerRule<RawWay> riverFixerRule = new RiverFixerRule<>();

    @Test
    public void cartoLineWaySectionWithWaterRiver() {
        final RawWay way = rawWay(1)
                .withTag(OsmTagKey.WATER.unwrap(), "river")
                .withTag(UnidbTagKey.TYPE.unwrap(), "carto_line");
        final RawWay adaptedWay = riverFixerRule.apply(way);

        assertThat(adaptedWay.tags().values(), hasSize(2));
        assertTrue(adaptedWay.hasTag(UnidbTagKey.TYPE.unwrap(), "carto_line"));
        assertTrue(adaptedWay.hasTag(UnidbTagKey.WATERWAY.unwrap(), "river"));
    }

    @Test
    public void waySectionWithWaterRiverAndWithoutCartoLine() {
        final RawWay way = rawWay(2)
                .withTag(OsmTagKey.WATER.unwrap(), "river");
        final RawWay adaptedWay = riverFixerRule.apply(way);

        assertThat(adaptedWay.tags().values(), hasSize(0));
    }
}