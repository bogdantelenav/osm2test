package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.adminTags.AdminL1NameExtractor;
import com.telenav.adapter.component.extractors.HighwayExtractor;
import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnAdminValue;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


/**
 * @author ioanao
 */
public class DerivedSpeedLimitRuleIT extends BaseTest {

    private final DerivedSpeedLimitRule rule =
            new DerivedSpeedLimitRule(new HighwayExtractor(), new AdminL1NameExtractor());

    @Test
    public void doNotAddSpeedLimitForIrrelevantHighway() {
        final String irrelevantHighwayValue = OsmHighwayValue.PATH.unwrap();
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.HIGHWAY.unwrap(), irrelevantHighwayValue)
                .withTag(OsmTagKey.ONEWAY.unwrap(), "1")
                .withTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap());

        assertEquals(waySection, rule.apply(waySection));
    }

    @Test
    public void doNotAddSpeedLimitForUnknownAdminLevel() {
        final String unknownAdminLevel = "TEST";
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap())
                .withTag(OsmTagKey.ONEWAY.unwrap(), "1").withTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), unknownAdminLevel);

        assertEquals(waySection, rule.apply(waySection));
    }

    @Test
    public void doNotAddSpeedLimitWhenMaxSpeedForwardIsPresent() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "30")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap())
                .withTag(OsmTagKey.ONEWAY.unwrap(), "1")
                .withTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap());

        assertEquals(waySection, rule.apply(waySection));
    }

    @Test
    public void doNotAddSpeedLimitWhenMaxSpeedBackwardIsPresent() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "30")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap())
                .withTag(OsmTagKey.ONEWAY.unwrap(), "-1")
                .withTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap());

        assertEquals(waySection, rule.apply(waySection));
    }

    @Test
    public void addSpeedLimitWhenMaxSpeedBackwardIsAbsent() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "30")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap())
                .withTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap());

        final WaySection enhancedWaySection = rule.apply(waySection);
        assertThat(enhancedWaySection.tags().size(), is(5));
        assertTrue(enhancedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "30"));
        assertTrue(enhancedWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap()));
        assertTrue(
                enhancedWaySection.hasTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap()));
        assertTrue(enhancedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "100"));
        assertTrue(enhancedWaySection
                .hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.DERIVED_SPEED_CATEGORY.unwrap()));
    }

    @Test
    public void addSpeedLimitWhenMaxSpeedForwardIsAbsent() {
        final WaySection waySection = waysection(3, 1).withTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "30")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.UNCLASSIFIED.unwrap())
                .withTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap());

        final WaySection enhancedWaySection = rule.apply(waySection);
        assertThat(enhancedWaySection.tags().size(), is(5));
        assertTrue(enhancedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "50"));
        assertTrue(enhancedWaySection
                .hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.DERIVED_SPEED_CATEGORY.unwrap()));
        assertTrue(enhancedWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.UNCLASSIFIED.unwrap()));
        assertTrue(
                enhancedWaySection.hasTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap()));
        assertTrue(enhancedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "30"));
    }

    @Test
    public void addSpeedLimitWhenMaxSpeedForwardAndBackwardAreAbsent() {
        final WaySection waySection = waysection(3,  1)
                .withTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRUNK.unwrap())
                .withTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap())
                .withTag(OsmTagKey.ONEWAY.unwrap(), "no");

        final WaySection enhancedWaySection = rule.apply(waySection);
        assertThat(enhancedWaySection.tags().size(), is(7));
        assertTrue(enhancedWaySection.hasTag(OsmTagKey.MAXSPEED_FORWARD.unwrap(), "80"));
        assertTrue(enhancedWaySection
                .hasTag(OsmTagKey.MAXSPEED_FORWARD.withSource(), TnSourceValue.DERIVED_SPEED_CATEGORY.unwrap()));
        assertTrue(enhancedWaySection.hasTag(OsmTagKey.MAXSPEED_BACKWARD.unwrap(), "80"));
        assertTrue(enhancedWaySection
                .hasTag(OsmTagKey.MAXSPEED_BACKWARD.withSource(), TnSourceValue.DERIVED_SPEED_CATEGORY.unwrap()));
        assertTrue(enhancedWaySection.hasTag(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRUNK.unwrap()));
        assertTrue(
                enhancedWaySection.hasTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), TnAdminValue.L1_NAME_JAKARTA.unwrap()));
        assertTrue(enhancedWaySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "no"));
    }
}