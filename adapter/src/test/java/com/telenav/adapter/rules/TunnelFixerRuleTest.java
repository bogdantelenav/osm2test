package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

@RunWith(JUnitParamsRunner.class)
public class TunnelFixerRuleTest {
    private List<Node> nodes = Arrays.asList(
            new Node(1, Latitude.MAXIMUM, Longitude.MAXIMUM, null),
            new Node(2, Latitude.MAXIMUM, Longitude.MAXIMUM, null));

    @Test
    @Parameters({"railway",
            "driveway",
            "cross",
            "cycleway",
            "avalanche_protector",
            "railway passage",
            "*",
            "left\\|through\\|through",
            "1",
            "building_passage1",
            "destination",
            "building_passage2",
            "under_bridge",
            "passageway",
            "bui",
            "service",
            "yes",
            "footway"})
    public void testValidTunnelValues(String tunnel) {
        TunnelFixerRule rule = new TunnelFixerRule();
        WaySection ws = new WaySection(1, 1, nodes, null).withTag(OsmTagKey.TUNNEL.unwrap(), tunnel);
        WaySection result = rule.apply(ws);
        Assert.assertTrue("Failed case: " + tunnel, result.hasTag(OsmTagKey.TUNNEL.unwrap(), "yes"));
    }

    @Test
    @Parameters({"No\\,_open", "no", "-1"})
    public void testNoTunnelValues(String tunnel) {
        TunnelFixerRule rule = new TunnelFixerRule();
        WaySection ws = new WaySection(1, 1, nodes, null).withTag(OsmTagKey.TUNNEL.unwrap(), tunnel);
        WaySection result = rule.apply(ws);
        Assert.assertFalse("Failed case: " + tunnel, result.hasKey(OsmTagKey.TUNNEL.unwrap()));
    }
}
