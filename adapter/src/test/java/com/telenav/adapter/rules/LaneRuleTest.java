package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.LaneExtractor;
import com.telenav.adapter.component.extractors.OneWayExtractor;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;

import static com.telenav.datapipelinecore.osm.OsmHighwayValue.MOTORWAY;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;


/**
 * @author odrab
 */
public class LaneRuleTest extends BaseTest {

    private LaneRule laneRule;


    @Before
    public void initRule() {
        laneRule = new LaneRule(new LaneExtractor(), new OneWayExtractor());
    }

    @Test
    public void testLane2WithDoubleWay() {
        final WaySection waySection =
                waysection(3, 1).withTag(OsmTagKey.LANES.unwrap(), "2").withTag(OsmTagKey.ONEWAY.unwrap(), "no");
        final WaySection adaptedWaySection = laneRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(5));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.LANES.unwrap(), "2"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "no"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.LANES_FORWARD.unwrap(), "1"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.LANES_BACKWARD.unwrap(), "1"));
    }

    @Test
    public void testOneWayForward() {
        final WaySection waySection =
                waysection(3, 1).withTag(OsmTagKey.LANES.unwrap(), "2").withTag(OsmTagKey.ONEWAY.unwrap(), "yes");
        final WaySection adaptedWaySection = laneRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(4));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.LANES.unwrap(), "2"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "yes"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.LANES_FORWARD.unwrap(), "2"));
        // | is not added after the last "yes" when there is no "-1"
        assertThat(adaptedWaySection.tags(), not(hasEntry("oneway:lanes", "yes|yes|")));
        assertThat(adaptedWaySection.tags(), hasEntry("oneway:lanes", "yes|yes"));
    }

    @Test
    public void testOneWayBackward() {
        final WaySection waySection =
                waysection(3, 1).withTag(OsmTagKey.LANES.unwrap(), "2").withTag(OsmTagKey.ONEWAY.unwrap(), "-1");
        final WaySection adaptedWaySection = laneRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(4));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.LANES.unwrap(), "2"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "-1"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.LANES_BACKWARD.unwrap(), "2"));
        // | is not added before the first "-1" when there was no "yes"
        assertThat(adaptedWaySection.tags(), not(hasEntry("oneway:lanes", "|-1|-1")));
        assertThat(adaptedWaySection.tags(), hasEntry("oneway:lanes", "-1|-1"));
    }

    @Test
    public void testLaneGreaterThan2WithoutOneWay() {
        final WaySection waySection =
                waysection(3, 1).withTag(OsmTagKey.LANES.unwrap(), "5").withTag(OsmTagKey.ONEWAY.unwrap(), "no");
        final WaySection adaptedWaySection = laneRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(2));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.LANES.unwrap(), "5"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "no"));
        assertThat(adaptedWaySection.tags(), not(hasKey(OsmTagKey.LANES_FORWARD.unwrap())));
        assertThat(adaptedWaySection.tags(), not(hasKey(OsmTagKey.LANES_BACKWARD.unwrap())));
    }

    @Test
    public void testLaneGreaterThanMaximumLaneNumberPerSenseWithOneWay() {
        final WaySection waySection =
                waysection(3, 1).withTag(OsmTagKey.LANES.unwrap(), "20").withTag(OsmTagKey.ONEWAY.unwrap(), "yes");
        final WaySection adaptedWaySection = laneRule.apply(waySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(4));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.LANES.unwrap(), "15"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.ONEWAY.unwrap(), "yes"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.LANES_FORWARD.unwrap(), "15"));
        assertThat(adaptedWaySection.tags(), not(hasKey(OsmTagKey.LANES_BACKWARD.unwrap())));
    }

    @Test
    public void testForCompleteWaySection() {
        final WaySection completeWaySection = waysection(3, 1).withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()).withTag(OsmTagKey.NAME.telenavPrefix(), "Park Dr")
                .withTag(OsmTagKey.ALT_NAME.telenavPrefix(), "Detroit-Toledo").withTag(OsmTagKey.REF.telenavPrefix(), "MI-85 N")
                .withTag(TnTagKey.TN_FUNCTIONAL_CLASS_UPGRADE.unwrap(), "4")
                .withTag(OsmTagKey.JUNCTION.unwrap(), "roundabout").withTag(TnTagKey.TN_ROAD_TYPE.unwrap(), "1")
                .withTag(OsmTagKey.MAXSPEED.unwrap(), "50 mph").withTag(OsmTagKey.LANES.unwrap(), "2")
                .withTag(OsmTagKey.JUNCTION.unwrap() + ":ref", "1A")
                .withTag(TnTagKey.TN_FUNCTIONAL_CLASS.unwrap(), "2");
        final WaySection adaptedWaySection = laneRule.apply(completeWaySection);
        assertThat(adaptedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(adaptedWaySection.sequenceNumber(), is((short) 1));
        assertThat(adaptedWaySection.tags().values(), hasSize(14));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.LANES.unwrap(), "2"));
        assertThat(adaptedWaySection.tags(), hasEntry(OsmTagKey.HIGHWAY.unwrap(), MOTORWAY.unwrap()));
    }

    @Test
    public void testForInvalidLanesWaySection() {
        WaySection completeWaySection = waysection(3, 1)
                .withTag(OsmTagKey.LANES.unwrap(), "-1");
        WaySection adaptedWaySection = laneRule.apply(completeWaySection);
        assertThat(adaptedWaySection.tags(), not(hasKey(OsmTagKey.LANES.unwrap())));

        completeWaySection = waysection(3, 1)
                .withTag(OsmTagKey.LANES.unwrap(), "1;2");
        adaptedWaySection = laneRule.apply(completeWaySection);
        assertThat(adaptedWaySection.tags(), not(hasKey(OsmTagKey.LANES.unwrap())));

        completeWaySection = waysection(3, 1)
                .withTag(OsmTagKey.LANES_FORWARD.unwrap(), "-1");
        adaptedWaySection = laneRule.apply(completeWaySection);
        assertThat(adaptedWaySection.tags(), not(hasKey(OsmTagKey.LANES_FORWARD.unwrap())));

        completeWaySection = waysection(3, 1)
                .withTag(OsmTagKey.LANES_BACKWARD.unwrap(), "-1");
        adaptedWaySection = laneRule.apply(completeWaySection);
        assertThat(adaptedWaySection.tags(), not(hasKey(OsmTagKey.LANES_BACKWARD.unwrap())));

        completeWaySection = waysection(3, 1)
                .withTag(OsmTagKey.LANES.unwrap(), "31");
        adaptedWaySection = laneRule.apply(completeWaySection);
        assertThat(adaptedWaySection.tags(), not(hasKey(OsmTagKey.LANES.unwrap())));

        completeWaySection = waysection(3, 1)
                .withTag(OsmTagKey.LANES.unwrap(), "29");
        adaptedWaySection = laneRule.apply(completeWaySection);
        assertThat(adaptedWaySection.tags().get(OsmTagKey.LANES.unwrap()), is("15"));
    }

    @Test
    public void testOnewayLanesForBidirectionalRoadsWithEqualNumberOfLanes() {
        WaySection initialWaySection = waysection(3, 1)
                .withTag(OsmTagKey.LANES.unwrap(), "4")
                .withTag(OsmTagKey.LANES_FORWARD.unwrap(), "2")
                .withTag(OsmTagKey.LANES_BACKWARD.unwrap(), "2");
        WaySection adaptedWaySection = laneRule.apply(initialWaySection);

        Map<String, String> adaptedTags = adaptedWaySection.tags();
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES.unwrap(),"4"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES_FORWARD.unwrap(), "2"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES_BACKWARD.unwrap(),"2"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.ONEWAY.postFixed(OsmTagKey.LANES.unwrap()), "yes|yes|-1|-1"));
    }

    @Test
    public void testOnewayLanesForBidirectionalRoadsWith3Forward2Backward() {
        WaySection initialWaySection = waysection(3, 1)
                .withTag(OsmTagKey.LANES.unwrap(), "5")
                .withTag(OsmTagKey.LANES_FORWARD.unwrap(), "3")
                .withTag(OsmTagKey.LANES_BACKWARD.unwrap(), "2");
        WaySection adaptedWaySection = laneRule.apply(initialWaySection);

        Map<String, String> adaptedTags = adaptedWaySection.tags();
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES.unwrap(),"5"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES_FORWARD.unwrap(), "3"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES_BACKWARD.unwrap(),"2"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.ONEWAY.postFixed(OsmTagKey.LANES.unwrap()), "yes|yes|yes|-1|-1"));
    }


    @Test
    public void testOnewayLanesForBidirectionalRoadsWith1Forward3Backward() {
        WaySection initialWaySection = waysection(3, 1)
                .withTag(OsmTagKey.LANES.unwrap(), "4")
                .withTag(OsmTagKey.LANES_FORWARD.unwrap(), "1")
                .withTag(OsmTagKey.LANES_BACKWARD.unwrap(), "3");
        WaySection adaptedWaySection = laneRule.apply(initialWaySection);

        Map<String, String> adaptedTags = adaptedWaySection.tags();
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES.unwrap(),"4"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES_FORWARD.unwrap(), "1"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES_BACKWARD.unwrap(),"3"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.ONEWAY.postFixed(OsmTagKey.LANES.unwrap()), "yes|-1|-1|-1"));
    }

    @Test
    public void testOnewayLanesForBidirectionalRoadsWithLanesAndLanesForward() {
        WaySection initialWaySection = waysection(3, 1)
                .withTag(OsmTagKey.LANES.unwrap(), "4")
                .withTag(OsmTagKey.LANES_FORWARD.unwrap(), "1");
        WaySection adaptedWaySection = laneRule.apply(initialWaySection);

        Map<String, String> adaptedTags = adaptedWaySection.tags();
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES.unwrap(),"4"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES_FORWARD.unwrap(),"1"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.ONEWAY.postFixed(OsmTagKey.LANES.unwrap()),"yes|-1|-1|-1"));
    }

    @Test
    public void testOnewayLanesForBidirectionalRoadsWithLanesAndLanesBackward() {
        WaySection initialWaySection = waysection(3, 1)
                .withTag(OsmTagKey.LANES.unwrap(), "5")
                .withTag(OsmTagKey.LANES_BACKWARD.unwrap(), "2");
        WaySection adaptedWaySection = laneRule.apply(initialWaySection);

        Map<String, String> adaptedTags = adaptedWaySection.tags();
        assertThat(adaptedTags, hasEntry(OsmTagKey.LANES.unwrap(),"5"));
        assertThat(adaptedTags, hasEntry(OsmTagKey.LANES_BACKWARD.unwrap(),"2"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.ONEWAY.postFixed(OsmTagKey.LANES.unwrap()), "yes|yes|yes|-1|-1"));
    }

    @Test
    public void testOneWayWithDirtyData(){
        WaySection initialWaySection = waysection(3,1).withTag(OsmTagKey.ONEWAY.unwrap(), "dirtyData").withTag(OsmTagKey.LANES.unwrap(), "2");
        WaySection adaptedWaySection = laneRule.apply(initialWaySection);
        Map<String, String> adaptedTags = adaptedWaySection.tags();

        assertThat(adaptedTags,hasEntry(OsmTagKey.ONEWAY.unwrap(),"dirtyData"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES.unwrap(),"2"));
        assertFalse(adaptedTags.containsKey(OsmTagKey.LANES_FORWARD.unwrap()));
        assertFalse(adaptedTags.containsKey(OsmTagKey.LANES_BACKWARD.unwrap()));
    }

    @Test
    public void testOneWayLanesBackwardLanesRemoveForward(){
        final WaySection waySection =
                waysection(3, 1).withTag(OsmTagKey.LANES.unwrap(), "2").withTag(OsmTagKey.ONEWAY.unwrap(), "-1")
                .withTag(OsmTagKey.LANES_FORWARD.unwrap(), "1").withTag(OsmTagKey.LANES_BACKWARD.unwrap(), "1");
        final WaySection adaptedWaySection = laneRule.apply(waySection);
        Map<String, String> adaptedTags = adaptedWaySection.tags();

        assertThat(adaptedTags,hasEntry(OsmTagKey.ONEWAY.unwrap(),"-1"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES.unwrap(),"2"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES_BACKWARD.unwrap(),"2"));
        assertFalse(adaptedTags.containsKey(OsmTagKey.LANES_FORWARD.unwrap()));
        assertThat(adaptedTags,hasEntry(OsmTagKey.ONEWAY.postFixed(OsmTagKey.LANES.unwrap()),"-1|-1"));
    }

    @Test
    public void testOneWayLanesForwardLanesRemoveBackward(){
        final WaySection waySection =
                waysection(3, 1).withTag(OsmTagKey.LANES.unwrap(), "2").withTag(OsmTagKey.ONEWAY.unwrap(), "yes")
                        .withTag(OsmTagKey.LANES_FORWARD.unwrap(), "1").withTag(OsmTagKey.LANES_BACKWARD.unwrap(), "1");
        final WaySection adaptedWaySection = laneRule.apply(waySection);
        Map<String, String> adaptedTags = adaptedWaySection.tags();

        assertThat(adaptedTags,hasEntry(OsmTagKey.ONEWAY.unwrap(),"yes"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES.unwrap(),"2"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES_FORWARD.unwrap(),"2"));
        assertFalse(adaptedTags.containsKey(OsmTagKey.LANES_BACKWARD.unwrap()));
        assertThat(adaptedTags,hasEntry(OsmTagKey.ONEWAY.postFixed(OsmTagKey.LANES.unwrap()),"yes|yes"));
    }

    @Test
    public void testOneWayLanes2WithNoOneWay(){
        final WaySection waySection =
                waysection(3, 1).withTag(OsmTagKey.LANES.unwrap(), "2");
        final WaySection adaptedWaySection = laneRule.apply(waySection);
        Map<String, String> adaptedTags = adaptedWaySection.tags();

        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES.unwrap(),"2"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES_FORWARD.unwrap(),"1"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.LANES_BACKWARD.unwrap(),"1"));
        assertThat(adaptedTags,hasEntry(OsmTagKey.ONEWAY.postFixed(OsmTagKey.LANES.unwrap()),"yes|-1"));
    }
}