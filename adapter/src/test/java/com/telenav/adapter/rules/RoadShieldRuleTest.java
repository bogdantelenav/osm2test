package com.telenav.adapter.rules;

import com.telenav.adapter.BaseTest;
import com.telenav.adapter.component.extractors.LanguageExtractor;
import com.telenav.adapter.component.extractors.RoadShieldExtractor;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;
import org.junit.BeforeClass;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


/**
 * @author martap
 */
public class RoadShieldRuleTest extends BaseTest {

    private static RoadShieldRule roadShieldRule;

    @BeforeClass
    public static void initRule() {
        roadShieldRule = new RoadShieldRule(new RoadShieldExtractor(), new LanguageExtractor());
    }

    @Test
    public void withoutLanguageCode() {
        final WaySection waySection = waysection(3,  1)
                .withTag(TnTagKey.TN_SHIELD_ICON.unwrap(), "100");
        final WaySection enhancedWaySection = roadShieldRule.apply(waySection);
        assertEquals(waySection, enhancedWaySection);
    }

    @Test
    public void withoutRoadShield() {
        final WaySection waySection = waysection(3,  1)
                .withTag(TnTagKey.TN_LANGUAGE_CODE.unwrap(), "eng");
        final WaySection enhancedWaySection = roadShieldRule.apply(waySection);
        assertEquals(waySection, enhancedWaySection);
    }

    @Test
    public void withoutRoadShieldAndLanguage() {
        final WaySection waySection = waysection(3,  1);
        final WaySection enhancedWaySection = roadShieldRule.apply(waySection);
        assertEquals(waySection, enhancedWaySection);
    }

    @Test
    public void oneIconOK() {
        final WaySection waySection = waysection(3,  1)
                .withTag(TnTagKey.TN_SHIELD_ICON.unwrap(), "100")
                .withTag(TnTagKey.TN_LANGUAGE_CODE.unwrap(), "eng");
        final WaySection enhancedWaySection = roadShieldRule.apply(waySection);
        assertThat(enhancedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(enhancedWaySection.sequenceNumber(), is((short) 1));
        assertThat(enhancedWaySection.tags().values(), hasSize(3));
        assertThat(enhancedWaySection.tags(), hasEntry("tn__shield_icon", "100"));
        assertThat(enhancedWaySection.tags(), hasEntry("tn__language_code", "eng"));
        assertThat(enhancedWaySection.tags(), hasEntry("ref:eng:si", "100"));
    }

    @Test
    public void multipleIconsOK() {
        final WaySection waySection = waysection(3,  1)
                .withTag(TnTagKey.TN_SHIELD_ICON.unwrap(), "100")
                .withTag(TnTagKey.TN_SHIELD_ICON.postFixed((short) 1), "101")
                .withTag(TnTagKey.TN_SHIELD_ICON.postFixed((short) 2), "102")
                .withTag(TnTagKey.TN_SHIELD_ICON.postFixed((short) 3), "103")
                .withTag(TnTagKey.TN_LANGUAGE_CODE.unwrap(), "eng");
        final WaySection enhancedWaySection = roadShieldRule.apply(waySection);
        assertThat(enhancedWaySection.strictIdentifier().wayId(), is(3L));
        assertThat(enhancedWaySection.sequenceNumber(), is((short) 1));
        assertThat(enhancedWaySection.tags().values(), hasSize(9));
        assertThat(enhancedWaySection.tags(), hasEntry("tn__language_code", "eng"));
        assertThat(enhancedWaySection.tags(), hasEntry("tn__shield_icon", "100"));
        assertThat(enhancedWaySection.tags(), hasEntry("ref:eng:si", "100"));
        assertThat(enhancedWaySection.tags(), hasEntry("tn__shield_icon_1", "101"));
        assertThat(enhancedWaySection.tags(), hasEntry("ref_1:eng:si", "101"));
        assertThat(enhancedWaySection.tags(), hasEntry("tn__shield_icon_2", "102"));
        assertThat(enhancedWaySection.tags(), hasEntry("ref_2:eng:si", "102"));
        assertThat(enhancedWaySection.tags(), hasEntry("tn__shield_icon_3", "103"));
        assertThat(enhancedWaySection.tags(), hasEntry("ref_3:eng:si", "103"));
    }
}