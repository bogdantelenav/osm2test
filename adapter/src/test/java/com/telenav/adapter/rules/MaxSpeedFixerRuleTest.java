package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.MaxSpeedExtractor;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Arrays;
import java.util.List;

@RunWith(JUnitParamsRunner.class)
public class MaxSpeedFixerRuleTest {
    private static final String FAIL_MESSAGE = "Failed case: tags->'%s' = %s";

    private List<Node> nodes = Arrays.asList(
            new Node(1, Latitude.MAXIMUM, Longitude.MAXIMUM, null),
            new Node(2, Latitude.MAXIMUM, Longitude.MAXIMUM, null));

    @Test
    @Parameters(method = "validSpeedValues")
    public void testSpeedFixer(String tag, String initial, String expected) {
        MaxSpeedFixerRule rule = new MaxSpeedFixerRule(new MaxSpeedExtractor());
        WaySection ws = new WaySection(1, 1, nodes, null).withTag(tag, initial);
        WaySection result = rule.apply(ws);
        Assert.assertTrue(
                String.format(FAIL_MESSAGE, tag, initial),
                result.hasTag(tag, expected));
    }

    @Test
    @Parameters(method = "invalidSpeedValues")
    public void testInvalidSpeedValues(String tag, String initial) {
        MaxSpeedFixerRule rule = new MaxSpeedFixerRule(new MaxSpeedExtractor());
        WaySection ws = new WaySection(1, 1, nodes, null).withTag(tag, initial);
        WaySection result = rule.apply(ws);
        Assert.assertFalse(
                String.format(FAIL_MESSAGE, tag, initial),
                result.hasKey(tag));
    }

    private Object[] validSpeedValues() {
        return new Object[] {
                new Object[] {"maxspeed", "30 mph", "30"},
                new Object[] {"maxspeed", "30mph", "30"},
                new Object[] {"maxspeed:advisory:backward", "20 mph @ school", "20"},
                new Object[] {"maxspeed:advisory:forward", "110 mph", "110"},
                new Object[] {"maxspeed:backward", " 35 mph ", "35"},
                new Object[] {"maxspeed:backward:conditional", "20 mph @ school", "20"},
                new Object[] {"maxspeed:forward", " 110 ", "110"},
                new Object[] {"maxspeed:forward:conditional", "25 mph @ (Mo-Fr 07:00-09:00, 13:30-15:30; PH off; SH off)", "25"},
                new Object[] {"maxspeed:forward", "\"20, 30, 40\"", "20"},
                new Object[] {"maxspeed:forward", "30 mph;signals", "30"},
                new Object[] {"maxspeed:forward", "50 mph; 55 mph", "50"},
                new Object[] {"maxspeed:backward:conditional", "3-0", "3"},
                new Object[] {"maxspeed:forward", "060", "60"},
                new Object[] {"maxspeed", "\"25,35\"", "25"},
                new Object[] {"maxspeed", "50;40", "50"},
                new Object[] {"maxspeed", "20-30", "20"},
                new Object[]{"maxspeed", "45-", "45"},
                new Object[]{"maxspeed", "45, east", "45"},
                new Object[]{"maxspeed", "35=", "35"},
                new Object[]{"maxspeed", "05 mph", "5"},
                new Object[]{"maxspeed", "35'", "35"},
                new Object[]{"maxspeed", "5g", "5"},
                new Object[]{"maxspeed", "`50", "50"},
                new Object[] {"maxspeed", "17.5 mph", "17"},
        };
    }

    private Object[] invalidSpeedValues() {
        return new Object[] {
                new Object[] {"maxspeed", "slow"},
                new Object[] {"maxspeed:advisory:backward", "Calle Marginal"},
                new Object[] {"maxspeed:advisory:forward", "East 2nd Avenue"},
                new Object[] {"maxspeed:backward", "NA"},
                new Object[] {"maxspeed:forward", "PR-3353"},
                new Object[] {"maxspeed:forward:conditional", "FR:rural"},
                new Object[] {"maxspeed", "S/N"},
                new Object[] {"maxspeed:advisory:backward", "E"},
                new Object[] {"maxspeed:advisory:forward", "1515 mph"},
                new Object[] {"maxspeed:backward", "9080"},
                new Object[] {"maxspeed:backward:conditional", "&0"},
                new Object[] {"maxspeed:forward:conditional", "0 mph"},
                new Object[] {"maxspeed", "US 67"}
        };
    }
}
