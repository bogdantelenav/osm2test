package com.telenav.adapter;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.datapipelinecore.io.writer.RddWriter;
import com.telenav.map.entity.*;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.SparkSession;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import scala.Tuple2;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

import static com.telenav.adapter.utils.OsmKeys.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * @author liviuc
 */
@RunWith(JUnitParamsRunner.class)
public class NgxNodesAdapterJobTest {

    // Temporary paths.
    private static final String TEMPORARY_PARQUET_FOLDER = "src/test/resources/temp";
    // Entities ids.
    private static final List<Long> WAY_SECTION_IDS = Arrays.asList(3L);
    private static final List<Long> WAY_IDS = Arrays.asList(9L);

    private static final Map<Long, List<Long>> WAY_SECTIONS_NODE_IDS = new HashMap<>();
    private static final Map<Long, List<Long>> WAYS_NODE_IDS = new HashMap<>();
    private static final List<Long> NODE_IDS = Arrays.asList(1L, 2L, 3L, 4L, 5L, 6L, 7L, 8L);
    private static final List<Long> RELATION_IDS = Arrays.asList(11L, 22L, 33L, 44L, 55L, 66L);
    private static Map<Long, Node> inputNodes = new HashMap<>();
    private static Map<Long, Node> enhancedNodes;

    private static final String NODES_INPUT = TEMPORARY_PARQUET_FOLDER + "/NgxNodesAdapterJob/input/nodes";
    private static final String WAYS_INPUT = TEMPORARY_PARQUET_FOLDER + "/NgxNodesAdapterJob/input/ways";
    private static final String WAY_SECTIONS_INPUT = TEMPORARY_PARQUET_FOLDER + "/NgxNodesAdapterJob/input/waySections";
    private static final String RELATIONS_INPUT = TEMPORARY_PARQUET_FOLDER + "/NgxNodesAdapterJob/input/relations";
    private static final String OUTPUT = TEMPORARY_PARQUET_FOLDER + "/NgxNodesAdapterJob/output";

    static {
        WAY_SECTIONS_NODE_IDS.put(3L, Arrays.asList(4L, 6L));
        WAYS_NODE_IDS.put(9L, Arrays.asList(7L, 8L));
    }

    @BeforeClass
    public static void init() throws IOException {
        if(new File(TEMPORARY_PARQUET_FOLDER).exists()) {
            Files.walk(Paths.get(TEMPORARY_PARQUET_FOLDER))
                    .sorted(Comparator.reverseOrder())
                    .map(path -> path.toFile())
                    .forEach(File::delete);
        }
        if (!new File(TEMPORARY_PARQUET_FOLDER).mkdirs()) {
            throw new IOException(String.format("Temporary directory %s could not be created", TEMPORARY_PARQUET_FOLDER));
        }

        try(SparkSession sparkSession = new JobInitializer.Builder()
                .appName("AttributesIntegratorJobTest")
                .runLocal(true)
                .build().sparkSession()) {
            prepareTestData(nodesProvider(NODE_IDS),
                    waySectionsProvider(WAY_SECTION_IDS),
                    waysProvider(WAY_IDS),
                    relationsProvider(RELATION_IDS),
                    JavaSparkContext.fromSparkContext(sparkSession.sparkContext()));
        }

        NgxNodesAdapterJob.main(args());

        enhancedNodes = readParquetNodesToInMemoryMap();
    }

    @AfterClass // Comment this if for whatever reason there is a need for the generated parquet files
    public static void clean() throws IOException {
        Files.walk(Paths.get(TEMPORARY_PARQUET_FOLDER))
                .sorted(Comparator.reverseOrder())
                .map(path -> path.toFile())
                .forEach(File::delete);
    }

    @Test
    @Parameters({"1","2","3","4","5","6", "7", "8"})
    public void testEnhancementForNodeId(String nodeId) throws IOException {

            assertNodeEnhancement(Integer.parseInt(nodeId));
    }

    private static Map<Long, Node> readParquetNodesToInMemoryMap() {
        try (SparkSession sparkSession = new JobInitializer.Builder()
                .appName("AttributesIntegratorJobTest")
                .runLocal(true)
                .build().sparkSession()) {
            final JavaRDD<Node> enhancedNodes = RddReader.NodeReader
                    .read(JavaSparkContext.fromSparkContext(sparkSession.sparkContext()),
                            OUTPUT + "/parquet");

            return enhancedNodes
                    .mapToPair(node -> new Tuple2<>(node.id(), node))
                    .collectAsMap();
        }
    }

    private void assertNodeEnhancement(int nodeId) {
        assertTrue(String.format("Node id %d not found", nodeId), enhancedNodes.containsKey(nodeId));
                        if (nodeId == 1) {
                            // San Francisco
                            assertNode(enhancedNodes.get(nodeId),
                                    "type", "city_center",
                                    "name", "San Francisco",
                                    "name:eng", "San Francisco",
                                    PLACE_TAG_KEY_IN_OSM, "city",
                                    IS_IN_STATE_CODE_TAG_KEY_IN_OSM, "US",
                                    "cat_id", "4444",
                                    "iso", "USA");
                        } else if (nodeId == 2) {
                            // Atlanta
                            assertNode(enhancedNodes.get(nodeId),
                                    "type", "city_center",
                                    PLACE_TAG_KEY_IN_OSM, "neighborhood",
                                    ADMIN_LEVEL_TAG_KEY_IN_OSM, "L3",
                                    IS_IN_STATE_CODE_TAG_KEY_IN_OSM, "US",
                                    "name", "Atlanta",
                                    "name:eng", "Atlanta",
                                    "cat_id", "9709",
                                    "iso", "USA");
                        } else if (nodeId == 3) {
                            // Sacramento
                            // Not enhanced because no relation was matching it
                            // Assert that it has same nodes as input
                            assertNode(enhancedNodes.get(nodeId), "name", "Sacramento",
                                    PLACE_TAG_KEY_IN_OSM, "neighborhood",
                                    ADMIN_LEVEL_TAG_KEY_IN_OSM, "6",
                                    CAPITAL_TAG_KEY_IN_OSM, "4",
                                    IS_IN_STATE_CODE_TAG_KEY_IN_OSM, "US",
                                    "iso", "USA");
                        } else if (nodeId == 4) {
                            // Node from non-navigable (raw) way
                            assertNode(enhancedNodes.get(nodeId), "type", "node");
                        } else if (nodeId == 5) {
                            // Node from non-navigable (raw) way
                            assertNode(enhancedNodes.get(nodeId));
                        } else if (nodeId == 6) {
                            // Node from non-navigable (raw) way
                            assertNode(enhancedNodes.get(nodeId), "type", "node");
                        }else if (nodeId == 7) {
                            // Node from non-navigable (raw) way
                            assertNode(enhancedNodes.get(nodeId), "type", "node");
                        } else if (nodeId == 8) {
                            // Node from non-navigable (raw) way
                            assertNode(enhancedNodes.get(nodeId), "type", "node" );
                        }

    }

    private static void prepareTestData(final EntityProvider<Node> nodesProvider,
                                 final EntityProvider<WaySection> waySectionsProvider,
                                 final EntityProvider<RawWay> waysProvider,
                                 final EntityProvider<RawRelation> relationsProvider,
                                 final JavaSparkContext javaSparkContext) {
        // Write entities.
        RddWriter.NodeWriter.write(javaSparkContext, nodesProvider.rdd(javaSparkContext), NODES_INPUT);

        RddWriter.WaySectionWriter.write(javaSparkContext, waySectionsProvider.rdd(javaSparkContext), WAY_SECTIONS_INPUT);

        RddWriter.RawWaysWriter.write(javaSparkContext, waysProvider.rdd(javaSparkContext), WAYS_INPUT);

        RddWriter.RawRelationWriter.write(javaSparkContext, relationsProvider.rdd(javaSparkContext), RELATIONS_INPUT);
    }

    private static String[] args() {

        return new String[]{
                "nodes_parquet=" + NODES_INPUT,
                "relations_parquet=" + RELATIONS_INPUT,
                "way_sections_parquet=" + WAY_SECTIONS_INPUT,
                "ways_parquet=" + WAYS_INPUT,

                "compute_stats=" + false,
                "saveAsText=" + false,
                "map_data_provided=" + false,

                "output_dir=" + OUTPUT,
                "run_local=" + true
        };
    }

    private static EntityProvider<Node> nodesProvider(final List<Long> ids) {
        return () -> {
            final List<Node> nodes = new ArrayList<>();
            final Iterator<Long> iterator = ids.iterator();

            final Node node1 =
                    new Node(iterator.next(), Latitude.degrees(37.7790262), Longitude.degrees(-122.4199060))
                            .withTags(tags("name", "San Francisco",
                                    PLACE_TAG_KEY_IN_OSM, "city",
                                    IS_IN_STATE_CODE_TAG_KEY_IN_OSM, "US",
                                    "iso", "USA"));
            nodes.add(node1);
            inputNodes.put(node1.id(), node1);

            final Node node2 =
                    new Node(iterator.next(), Latitude.degrees(33.7489924), Longitude.degrees(-84.3902644))
                            .withTags(tags("name", "Atlanta",
                                    PLACE_TAG_KEY_IN_OSM, "neighborhood",
                                    ADMIN_LEVEL_TAG_KEY_IN_OSM, "6",
                                    CAPITAL_TAG_KEY_IN_OSM, "4",
                                    IS_IN_STATE_CODE_TAG_KEY_IN_OSM, "US",
                                    "iso", "USA"));
            nodes.add(node2);
            inputNodes.put(node2.id(), node2);

            final Node node3 =
                    new Node(iterator.next(), Latitude.degrees(38.5810606), Longitude.degrees(-121.4938950))
                            .withTags(tags("name", "Sacramento",
                                    PLACE_TAG_KEY_IN_OSM, "neighborhood",
                                    ADMIN_LEVEL_TAG_KEY_IN_OSM, "6",
                                    CAPITAL_TAG_KEY_IN_OSM, "4",
                                    IS_IN_STATE_CODE_TAG_KEY_IN_OSM, "US",
                                    "iso", "USA"));
            nodes.add(node3);
            inputNodes.put(node3.id(), node3);

            // Non-navigable nodes
            final Node node4 =
                    new Node(iterator.next(), Latitude.MAXIMUM, Longitude.MAXIMUM);
            nodes.add(node4);
            inputNodes.put(node4.id(), node4);

            final Node node5 =
                    new Node(iterator.next(), Latitude.MAXIMUM, Longitude.MAXIMUM);
            nodes.add(node5);
            inputNodes.put(node5.id(), node5);

            final Node node6 =
                    new Node(iterator.next(), Latitude.MAXIMUM, Longitude.MAXIMUM);
            nodes.add(node6);
            inputNodes.put(node6.id(), node6);

            final Node node7 =
                    new Node(iterator.next(), Latitude.MAXIMUM, Longitude.MAXIMUM);
            nodes.add(node7);
            inputNodes.put(node7.id(), node7);

            final Node node8 =
                    new Node(iterator.next(), Latitude.MAXIMUM, Longitude.MAXIMUM);
            nodes.add(node8);
            inputNodes.put(node8.id(), node8);

            return nodes;
        };
    }

    private static Map<String, String> tags(final String... tags) {
        final Map<String, String> map = new HashMap<>();
        for (int i = 0; i < tags.length; i += 2) {
            map.put(tags[i], tags[i + 1]);
        }
        return map;
    }

    private static EntityProvider<WaySection> waySectionsProvider(final List<Long> ids) {
        return () -> {
            final List<WaySection> waySections = new ArrayList<>();
            Iterator<Long> iterator = ids.iterator();
            Map<String, String> tags = new HashMap<>();
            Long wayId = iterator.next();
            final WaySection waySection1 = new WaySection(wayId, nodes(WAY_SECTIONS_NODE_IDS.get(wayId)), tags);
            waySections.add(waySection1);
            return waySections;
        };
    }

    private static EntityProvider<RawWay> waysProvider(final List<Long> ids) {
        return () -> {
            final List<RawWay> rawWays = new ArrayList<>();
            Iterator<Long> iterator = ids.iterator();
            Map<String, String> tags = new HashMap<>();
            Long wayId = iterator.next();
            final RawWay rawWay = new RawWay(wayId, WAYS_NODE_IDS.get(wayId), tags);
            rawWays.add(rawWay);
            return rawWays;
        };
    }

    private static EntityProvider<RawRelation> relationsProvider(final List<Long> ids) {
        return () -> {
            final List<RawRelation> relations = new ArrayList<>();
            final Iterator<Long> iterator = ids.iterator();
            List<RawRelation.Member> members = new ArrayList<>();

            // San Francisco relation
            members = new ArrayList<>();
            members.add(new RawRelation.Member(1L, RawRelation.MemberType.NODE, "label"));
            final RawRelation relation4 = new RawRelation(iterator.next(), members,
                    tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, "6",
                            "name", "San Francisco"));
            relations.add(relation4);

            // Atlanta relation
            members = new ArrayList<>();
            members.add(new RawRelation.Member(2L, RawRelation.MemberType.NODE, "admin_centre"));
            final RawRelation relation5 = new RawRelation(iterator.next(), members,
                    tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, "6",
                            "name", "Atlanta"));
            relations.add(relation5);

            // Georgia relation
            members = new ArrayList<>();
            members.add(new RawRelation.Member(2L, RawRelation.MemberType.NODE, "admin_centre"));
            final RawRelation relation6 = new RawRelation(iterator.next(), members,
                    tags(ADMIN_LEVEL_TAG_KEY_IN_OSM, "8",
                            "name", "Georgia"));
            relations.add(relation6);

            return relations;
        };
    }

    private static List<Node> nodes(final List<Long> nodeIds) {
        return nodeIds.stream()
                .map(id -> inputNodes.get(id))
                .collect(Collectors.toList());
    }

    private void assertNode(final Node node,
                            final String... tags) {

        final int expectedTagsSize = tags.length / 2;
        final int actualTagsSize = node.tags().size();
        assertEquals(expectedTagsSize, actualTagsSize);

        for (int i = 0; i < tags.length; i += 2) {
            assertTrue(node.hasTag(tags[i], tags[i + 1]));
        }
    }

    @FunctionalInterface
    private interface EntityProvider<E extends StandardEntity & TagAdjuster<E>> {

        default JavaRDD<E> rdd(final JavaSparkContext sparkContext) {
            return sparkContext.parallelize(entities());
        }

        List<E> entities();
    }
}
