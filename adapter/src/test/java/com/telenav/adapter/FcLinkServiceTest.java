package com.telenav.adapter;

import com.telenav.adapter.component.fc.FcLinkService;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.junit.Test;

import java.util.*;

import static com.telenav.adapter.component.fc.TagService.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class FcLinkServiceTest {

    private final Map<Long, Node> nodesById = new HashMap<>();

    @Test
    public void testWhenOneWay() {

        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.sql.parquet.binaryAsString", "true");
        sparkConf.setMaster("local[1]");
        sparkConf.setAppName("Test");

        final JavaSparkContext javaSparkContext =
                JavaSparkContext.fromSparkContext(SparkContext.getOrCreate(sparkConf));

        // way sections for way 21172563
        final List<Node> nodes21172563 = Arrays.asList(
                node(227761125, HIGHWAY_TAG, "motorway_junction"),
                node(227761147));

        final WaySection ws21172563 = waySection(21172563, 1, nodes21172563,
                tags(HIGHWAY_TAG, "motorway_link", "oneway", "yes"));

        // way sections for way 39276788 sequence number 2
        final List<Node> nodes39276788_2 = Arrays.asList(
                node(227761997),
                node(1550908615));

        final WaySection ws39276788_2 = waySection(39276788, 2, nodes39276788_2,
                tags(HIGHWAY_TAG, "motorway", FC_TAG, "1", "oneway", "yes"));

        // way sections for way 697471682
        final List<Node> nodes697471682 = Arrays.asList(
                node(227806563),
                node(227761102, HIGHWAY_TAG, "motorway_junction"));

        final WaySection ws697471682 = waySection(697471682, 2, nodes697471682,
                tags(HIGHWAY_TAG, "motorway", FC_TAG, "1", "oneway", "yes"));

        // way sections for way 39276788 sequence number 1
        final List<Node> nodes39276788_1 = Arrays.asList(
                node(227859651),
                node(227761997));

        final WaySection ws39276788_1 = waySection(39276788, 2, nodes39276788_1,
                tags(HIGHWAY_TAG, "motorway", FC_TAG, "1", "oneway", "yes"));

        // way sections for way 474518308 sequence number 1
        final List<Node> nodes474518308_1 = Arrays.asList(
                node(227761102),
                node(227761125, HIGHWAY_TAG, "motorway_junction"));

        final WaySection ws474518308_1 = waySection(474518308, 2, nodes474518308_1,
                tags(HIGHWAY_TAG, "motorway_link", FC_TAG, "5", "oneway", "yes"));

        // way sections for way 39276791 sequence number 1
        final List<Node> nodes39276791_1 = Arrays.asList(
                node(227762007),
                node(227761997));

        final WaySection ws39276791_1 = waySection(39276791, 1, nodes39276791_1,
                tags(HIGHWAY_TAG, "motorway_link", FC_TAG, "5", "oneway", "yes"));

        // way sections for way 21172638 sequence number 1
        final List<Node> nodes21172638_1 = Arrays.asList(
                node(227761125, HIGHWAY_TAG, "motorway_junction"),
                node(227762045));

        final WaySection ws21172638_1 = waySection(21172638, 1, nodes21172638_1,
                tags(HIGHWAY_TAG, "motorway_link", FC_TAG, "5", "oneway", "yes"));

        // way sections for way 74057219 sequence number 1
        final List<Node> nodes74057219_1 = Arrays.asList(
                node(227762031),
                node(227762007));

        final WaySection ws74057219_1 = waySection(74057219, 1, nodes74057219_1,
                tags(HIGHWAY_TAG, "motorway_link", FC_TAG, "5", "oneway", "yes"));

        // way sections for way 74057235 sequence number 1
        final List<Node> nodes74057235_1 = Arrays.asList(
                node(227762045),
                node(227762031));

        final WaySection ws74057235_1 = waySection(74057235, 1, nodes74057235_1,
                tags(HIGHWAY_TAG, "motorway_link", FC_TAG, "5", "oneway", "yes"));

        // way sections for way 34387171 sequence number 1
        final List<Node> nodes34387171_1 = Arrays.asList(
                node(227761102, "motorway", "motorway_junction"),
                node(227741551));

        final WaySection ws34387171_1 = waySection(34387171, 1, nodes34387171_1,
                tags(HIGHWAY_TAG, "motorway", FC_TAG, "2", "oneway", "yes"));

        // way sections for way 74057191 sequence number 1
        final List<Node> nodes74057191_1 = Arrays.asList(
                node(227761147),
                node(227761153));

        final WaySection ws74057191_1 = waySection(74057191, 1, nodes74057191_1,
                tags(HIGHWAY_TAG, "motorway_link", FC_TAG, "5", "oneway", "yes"));

        // way sections for way 74057224 sequence number 1
        final List<Node> nodes74057224_1 = Arrays.asList(
                node(227858721),
                node(227761153));

        final WaySection ws74057224_1 = waySection(74057224, 1, nodes74057224_1,
                tags(HIGHWAY_TAG, "motorway", FC_TAG, "1", "oneway", "yes"));

        // way sections for way 74057224 sequence number 2
        final List<Node> nodes74057224_2 = Arrays.asList(
                node(227761153),
                node(227858737));

        final WaySection ws74057224_2 = waySection(74057224, 2, nodes74057224_2,
                tags(HIGHWAY_TAG, "motorway", FC_TAG, "1", "oneway", "yes"));

        final JavaRDD<WaySection> waySections = javaSparkContext.parallelize(Arrays.asList(
                ws21172563,
                ws39276788_2,
                ws697471682,
                ws39276788_1,
                ws474518308_1,
                ws39276791_1,
                ws21172638_1,
                ws74057219_1,
                ws74057235_1,
                ws34387171_1,
                ws74057191_1,
                ws74057224_1,
                ws74057224_2
        ));

        final JavaRDD<WaySection> result = FcLinkService.assignFcToLinks(waySections, javaSparkContext);
        result.filter(waySection -> waySection.tags().get(HIGHWAY_TAG).contains("link"))
                .foreach(waySection -> assertTrue(waySection.hasTag(FC_TAG, "1")));
    }

    @Test
    public void testWhenOneWay2() {

        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.sql.parquet.binaryAsString", "true");
        sparkConf.setMaster("local[1]");
        sparkConf.setAppName("Test");

        final JavaSparkContext javaSparkContext =
                JavaSparkContext.fromSparkContext(SparkContext.getOrCreate(sparkConf));

        // way sections for way 74057230
        final List<Node> nodes74057230 = Arrays.asList(
                node(227821589, HIGHWAY_TAG, "motorway_junction"),
                node(227762271, HIGHWAY_TAG, "motorway_junction"));

        final WaySection ws74057230 = waySection(74057230, 1, nodes74057230,
                tags(HIGHWAY_TAG, "motorway", "oneway", "yes", FC_TAG, "2"));

        // way sections for way 21172644
        final List<Node> nodes21172644 = Arrays.asList(
                node(227762271, HIGHWAY_TAG, "motorway_junction"),
                node(227753500, HIGHWAY_TAG, "motorway_junction"));

        final WaySection ws21172644 = waySection(21172644, 1, nodes21172644,
                tags(HIGHWAY_TAG, "motorway", "oneway", "yes", FC_TAG, "2"));

        // way sections for way 48659713
        final List<Node> nodes48659713 = Arrays.asList(
                node(227753500, HIGHWAY_TAG, "motorway_junction"),
                node(227762312));

        final WaySection ws48659713 = waySection(48659713, 1, nodes48659713,
                tags(HIGHWAY_TAG, "motorway_link", "oneway", "yes"));

        // way sections for way 39276789
        final List<Node> nodes39276789 = Arrays.asList(
                node(227762312),
                node(339906156));

        final WaySection ws39276789 = waySection(39276789, 1, nodes39276789,
                tags(HIGHWAY_TAG, "motorway_link", "oneway", "yes"));

        // way sections for way 141685484
        final List<Node> nodes141685484 = Arrays.asList(
                node(339906156),
                node(470733987));

        final WaySection ws141685484 = waySection(141685484, 1, nodes141685484,
                tags(HIGHWAY_TAG, "motorway", "oneway", "yes", FC_TAG, "1"));

        // way sections for way 21172049
        final List<Node> nodes21172049 = Arrays.asList(
                node(227753500, HIGHWAY_TAG, "motorway_junction"),
                node(470733989));

        final WaySection ws21172049 = waySection(21172049, 1, nodes21172049,
                tags(HIGHWAY_TAG, "motorway_link", "oneway", "yes"));

        // way sections for way 39276786
        final List<Node> nodes39276786 = Arrays.asList(
                node(470733989),
                node(227753592));

        final WaySection ws39276786 = waySection(39276786, 1, nodes39276786,
                tags(HIGHWAY_TAG, "motorway_link", "oneway", "yes"));

        // way sections for way 39276787
        final List<Node> nodes39276787 = Arrays.asList(
                node(227753592),
                node(227753636));

        final WaySection ws39276787 = waySection(39276787, 1, nodes39276787,
                tags(HIGHWAY_TAG, "motorway_link", "oneway", "yes"));

        // way sections for way 39276795
        final List<Node> nodes39276795 = Arrays.asList(
                node(227753636),
                node(470733990));

        final WaySection ws39276795 = waySection(39276795, 1, nodes39276795,
                tags(HIGHWAY_TAG, "motorway_link", "oneway", "yes"));

        // way sections for way 39276794
        final List<Node> nodes39276794 = Arrays.asList(
                node(470733990),
                node(227753645));

        final WaySection ws39276794 = waySection(39276794, 1, nodes39276794,
                tags(HIGHWAY_TAG, "motorway_link", "oneway", "yes"));

        // way sections for way 108003954
        final List<Node> nodes108003954 = Arrays.asList(
                node(227753645),
                node(227858721));

        final WaySection ws108003954 = waySection(108003954, 1, nodes108003954,
                tags(HIGHWAY_TAG, "motorway", "oneway", "yes", FC_TAG, "1"));

        final JavaRDD<WaySection> waySections = javaSparkContext.parallelize(Arrays.asList(
                ws74057230,
                ws21172644,
                ws48659713,
                ws39276789,
                ws141685484,
                ws21172049,
                ws39276786,
                ws39276787,
                ws39276795,
                ws39276794,
                ws108003954
        ));

        final JavaRDD<WaySection> result = FcLinkService.assignFcToLinks(waySections, javaSparkContext);
        result.filter(waySection -> waySection.tags().get(HIGHWAY_TAG).contains("link"))
                .foreach(waySection -> assertTrue(waySection.hasTag(FC_TAG, "2")));
    }

    @Test
    public void testWhenOneWay3() {

        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.sql.parquet.binaryAsString", "true");
        sparkConf.setMaster("local[1]");
        sparkConf.setAppName("Test");

        final JavaSparkContext javaSparkContext =
                JavaSparkContext.fromSparkContext(SparkContext.getOrCreate(sparkConf));

        // way sections for way 39276781
        final List<Node> nodes39276781_1 = Arrays.asList(
                node(470733987),
                node(227754191, HIGHWAY_TAG, "motorway_junction"));

        final WaySection ws39276781_1 = waySection(39276781, 1, nodes39276781_1,
                tags(HIGHWAY_TAG, "motorway", "oneway", "yes", FC_TAG, "1"));

        // way sections for way 39276781
        final List<Node> nodes39276781_2 = Arrays.asList(
                node(227754191, HIGHWAY_TAG, "motorway_junction"),
                node(227754191));

        final WaySection ws39276781_2 = waySection(39276781, 2, nodes39276781_2,
                tags(HIGHWAY_TAG, "motorway", "oneway", "yes", FC_TAG, "1"));

        // way sections for way 21172081
        final List<Node> nodes21172081 = Arrays.asList(
                node(227754191),
                node(227754179));

        final WaySection ws21172081 = waySection(21172081, 1, nodes21172081,
                tags(HIGHWAY_TAG, "motorway_link", "oneway", "yes"));

        // way sections for way 21180091
        final List<Node> nodes21180091 = Arrays.asList(
                node(227754179),
                node(227823878));

        final WaySection ws21180091 = waySection(21180091, 1, nodes21180091,
                tags(HIGHWAY_TAG, "motorway", "oneway", "yes",  FC_TAG, "2"));

        final JavaRDD<WaySection> waySections = javaSparkContext.parallelize(Arrays.asList(
                ws39276781_1,
                ws39276781_2,
                ws21172081,
                ws21180091
        ));

        final JavaRDD<WaySection> result = FcLinkService.assignFcToLinks(waySections, javaSparkContext);
        result.filter(waySection -> waySection.tags().get(HIGHWAY_TAG).contains("link"))
                .foreach(waySection -> assertTrue(waySection.hasTag(FC_TAG, "2")));
    }

    @Test
    public void testWhenNotOneWay() {

        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.sql.parquet.binaryAsString", "true");
        sparkConf.setMaster("local[1]");
        sparkConf.setAppName("Test");

        final JavaSparkContext javaSparkContext =
                JavaSparkContext.fromSparkContext(SparkContext.getOrCreate(sparkConf));

        // way sections for way 24134580
        final List<Node> nodes24134580 = Arrays.asList(
                node(261358030),
                node(261389618));

        final WaySection ws24134580 = waySection(24134580, 1, nodes24134580,
                tags(HIGHWAY_TAG, "residential", FC_TAG, "4"));

        // way sections for way 512230658 sequence number 2
        final List<Node> nodes512230658_2 = Arrays.asList(
                node(261466204),
                node(261358030));

        final WaySection ws512230658_2 = waySection(512230658, 2, nodes512230658_2,
                tags(HIGHWAY_TAG, "primary", FC_TAG, "3", "oneway", "yes"));

        // way sections for way 49166180 sequence number 1
        final List<Node> nodes49166180_1 = Arrays.asList(
                node(261358030),
                node(261496736));

        final WaySection ws49166180_1 = waySection(49166180, 1, nodes49166180_1,
                tags(HIGHWAY_TAG, "primary", FC_TAG, "3"));

        // way sections for way 24117321 sequence number 1
        final List<Node> nodes24117321_1 = Arrays.asList(
                node(261358030),
                node(1014132264));

        final WaySection ws24117321_1 = waySection(24117321, 1, nodes24117321_1,
                tags(HIGHWAY_TAG, "trunk_link", FC_TAG, "5", "oneway", "no"));

        // way sections for way 50330882 sequence number 1
        final List<Node> nodes50330882_1 = Arrays.asList(
                node(1014132264),
                node(1014131154, HIGHWAY_TAG, "motorway_junction"));

        final WaySection ws50330882_1 = waySection(50330882, 1, nodes50330882_1,
                tags(HIGHWAY_TAG, "trunk_link", FC_TAG, "5", "oneway", "yes"));

        // way sections for way 149827995 sequence number 1
        final List<Node> nodes149827995_1 = Arrays.asList(
                node(1014131636, HIGHWAY_TAG, "motorway_junction"),
                node(1014131154, HIGHWAY_TAG, "motorway_junction"));

        final WaySection ws149827995_1 = waySection(149827995, 1,
                nodes149827995_1, tags(HIGHWAY_TAG, "trunk", FC_TAG, "3", "oneway", "no"));

        // way sections for way 24117324 sequence number 1
        final List<Node> nodes24117324_1 = Arrays.asList(
                node(1014131636, HIGHWAY_TAG, "motorway_junction"),
                node(1014132264));

        final WaySection ws24117324_1 = waySection(24117324, 1, nodes24117324_1,
                tags(HIGHWAY_TAG, "trunk_link", FC_TAG, "5", "oneway", "yes"));

        // way sections for way 149827997 sequence number 1
        final List<Node> nodes149827997_1 = Arrays.asList(
                node(1014131154, HIGHWAY_TAG, "motorway_junction"),
                node(1014129384));

        final WaySection ws149827997_1 = waySection(149827997, 1, nodes149827997_1,
                tags(HIGHWAY_TAG, "trunk", FC_TAG, "3", "oneway", "no"));

        // way sections for way 149827999 sequence number 1
        final List<Node> nodes149827999_1 = Arrays.asList(
                node(1014136007),
                node(1014131636, HIGHWAY_TAG, "motorway_junction"));

        final WaySection ws149827999_1 = waySection(149827999, 1, nodes149827999_1,
                tags(HIGHWAY_TAG, "trunk", FC_TAG, "3", "oneway", "no"));

        // way sections for way 24117675 sequence number 1
        final List<Node> nodes24117675_1 = Arrays.asList(
                node(1014131154, HIGHWAY_TAG, "motorway_junction"),
                node(261358502));

        final WaySection ws24117675_1 = waySection(24117675, 1, nodes24117675_1,
                tags(HIGHWAY_TAG, "trunk_link", FC_TAG, "5", "oneway", "yes"));

        // way sections for way 50330883 sequence number 1
        final List<Node> nodes50330883_1 = Arrays.asList(
                node(261358502),
                node(1014131636, HIGHWAY_TAG, "motorway_junction"));

        final WaySection ws50330883_1 = waySection(50330883, 1, nodes50330883_1,
                tags(HIGHWAY_TAG, "trunk_link", FC_TAG, "3", "oneway", "yes"));

        // way sections for way 24117437 sequence number 1
        final List<Node> nodes24117437_1 = Arrays.asList(
                node(261358498),
                node(1014134204));

        final WaySection ws24117437_1 = waySection(24117437, 1, nodes24117437_1,
                tags(HIGHWAY_TAG, "trunk_link", FC_TAG, "5", "oneway", "no"));

        // way sections for way 24117437 sequence number 2
        final List<Node> nodes24117437_2 = Arrays.asList(
                node(1014134204),
                node(6277587607L));

        final WaySection ws24117437_2 = waySection(24117437, 2, nodes24117437_2,
                tags(HIGHWAY_TAG, "trunk_link", FC_TAG, "5", "oneway", "no"));

        // way sections for way 24117437 sequence number 3
        final List<Node> nodes24117437_3 = Arrays.asList(
                node(6277587607L),
                node(261358502));

        final WaySection ws24117437_3 = waySection(24117437, 3, nodes24117437_3,
                tags(HIGHWAY_TAG, "trunk_link", FC_TAG, "5", "oneway", "no"));

        // way sections for way 24126189 sequence number 1
        final List<Node> nodes24126189_1 = Arrays.asList(
                node(261428123),
                node(261358498));

        final WaySection ws24126189_1 = waySection(24126189, 1, nodes24126189_1,
                tags(HIGHWAY_TAG, "tertiary", FC_TAG, "4"));

        // way sections for way 91564768 sequence number 2
        final List<Node> nodes91564768_2 = Arrays.asList(
                node(261358498),
                node(261428131));

        final WaySection ws91564768_2 = waySection(91564768, 1, nodes91564768_2,
                tags(HIGHWAY_TAG, "residential", FC_TAG, "4"));

        final JavaRDD<WaySection> waySections = javaSparkContext.parallelize(Arrays.asList(
                ws24134580,
                ws512230658_2,
                ws49166180_1,
                ws149827995_1,
                ws24117324_1,
                ws24117321_1,
                ws50330882_1,
                ws149827997_1,
                ws24117675_1,
                ws50330883_1,
                ws24117437_1,
                ws24117437_2,
                ws24117437_3,
                ws24126189_1,
                ws91564768_2,
                ws149827999_1
        ));

        final JavaRDD<WaySection> result = FcLinkService.assignFcToLinks(waySections, javaSparkContext);
        result.filter(waySection -> waySection.tags().get(HIGHWAY_TAG).contains(LINK_SUFFIX_VALUE))
        .foreach(waySection -> {
            final long id = waySection.id();
            final String fc = waySection.tags().get(FC_TAG);
            if (id == 4000024117324001L) {
                assertEquals(fc, "3");
            } else if (id == 4000024117321001L) {
                int x  = 1;
                assertEquals(fc, "3");
            } else if (id == 4000050330882001L) {
                assertEquals(fc, "3");
            } else if (id == 4000024117675001L) {
                assertEquals(fc, "3");
            } else if (id == 4000050330883001L) {
                int x = 1;
                assertEquals(fc, "3"); //
            } else if (id == 4000024117437001L) {
                int x = 1;
                assertEquals(fc, "4");
            } else if (id == 4000024117437002L) {
                int x = 1;
                assertEquals(fc, "4");
            } else if (id == 4000024117437003L) {
                int x = 1;
                assertEquals(fc, "4");
            }
        });
    }

    private Node node(final long nodeId,
                      final String... keysAndValues) {

        if (nodesById.containsKey(nodeId)) {

            return nodesById.get(nodeId);

        }

        final Node node = new Node(nodeId, Latitude.MAXIMUM, Longitude.MAXIMUM, tags(keysAndValues));

        nodesById.put(nodeId, node);

        return node;

    }

    private Map<String, String> tags(final String... keysAndValues) {

        final Map<String, String> tags = new TreeMap<>();

        for (int i = 0; i < keysAndValues.length; i += 2) {

            tags.put(keysAndValues[i], keysAndValues[i + 1]);

        }

        return tags;
    }

    private WaySection waySection(final long wayId,
                                  final int sequenceNumber,
                                  final List<Node> nodes,
                                  final Map<String, String> tags) {
        return new WaySection(wayId, sequenceNumber, nodes, tags);
    }
}
