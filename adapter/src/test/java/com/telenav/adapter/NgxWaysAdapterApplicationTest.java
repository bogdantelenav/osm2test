package com.telenav.adapter;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.map.entity.RawWay;
import org.apache.spark.api.java.JavaRDD;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.junit.Assert.*;


/**
 * @author dianat2
 */
public class NgxWaysAdapterApplicationTest extends BaseTest {

    private static JavaRDD<RawWay> ways;

    @BeforeClass
    public static void init() {
        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Tests")
                .runLocal(true)
                .build();
        final RawWay way1 = rawWay(1)
                .withTag(TnTagKey.TN_LANGUAGE_CODE.unwrap(), "eng")
                .withTag(OsmTagKey.NAME.telenavPrefix(), "Main")
                .withTag(OsmTagKey.ALT_NAME.telenavPrefix(), "Alt Main")
                .withTag(TnTagKey.TN_COUNTRY_ISO_CODE.unwrap(), "USA")
                .withTag(TnTagKey.TN_ADMIN_L1_NAME.unwrap(), "Michigan")
                .withTag(TnTagKey.TN_ADMIN_L2_NAME.unwrap(), "Detroit")
                .withTag(OsmTagKey.FOOT.unwrap(), "permissive")
                .withTag(OsmTagKey.ROUTE.unwrap(), "ferry");
        final RawWay way2 = rawWay(2)
                .withTag(OsmTagKey.ROUTE.unwrap(), "shuttle_train")
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "yes")
                .withTag(OsmTagKey.MOTORCYCLE.unwrap(), "yes")
                .withTag(OsmTagKey.HIGHWAY.unwrap(), "escape");
        final RawWay way3 = rawWay(3)
                .withTag(OsmTagKey.MOTORCAR.unwrap(), "no")
                .withTag(OsmTagKey.MOTORCYCLE.unwrap(), "no")
                .withTag(OsmTagKey.BUS.unwrap(), "no")
                .withTag(OsmTagKey.HOV.unwrap(), "no");
        ways = jobInitializer.javaSparkContext().parallelize(Arrays.asList(way1, way2, way3));
    }

    @Test
    public void adaptAllTags() {
        final List<RawWay> adaptedWays = new NgxWaysAdapterApplication().adapt(ways).collect();
        assertThat(adaptedWays.size(), is(3));
        final Map<String, String> tagsWay1 = adaptedWays.get(0).tags();

        assertThat(tagsWay1.get(TnTagKey.TN_LANGUAGE_CODE.withoutTelenavPrefix()), is("eng"));
        assertThat(tagsWay1.get(TnTagKey.TN_COUNTRY_ISO_CODE.withoutTelenavPrefix()), is("USA"));
        assertThat(tagsWay1.get(OsmTagKey.FOOT.unwrap()), is("yes"));
        // route=shuttle_train and route=ferry should be in way sections only, rt and rst will not be added here
        assertFalse(tagsWay1.containsKey(RoadCategoryTagKey.ROAD_TYPE.unwrap()));

        final Map<String, String> tagsWay2 = adaptedWays.get(1).tags();
        // route=shuttle_train and route=ferry should be in way sections only, rt and rst will not be added here
        assertThat(tagsWay2.get(OsmTagKey.ROUTE.unwrap()), is("shuttle_train"));
        assertFalse(tagsWay2.containsKey(UnidbTagKey.RAIL_FERRY.unwrap()));
        assertFalse(tagsWay2.containsKey(RoadCategoryTagKey.ROAD_TYPE.unwrap()));

        assertNull(tagsWay2.get(OsmTagKey.MOTORCAR.unwrap()));
        assertThat(tagsWay2.get(OsmTagKey.MOTORCYCLE.unwrap()), is("yes"));
        assertFalse(tagsWay2.containsKey(OsmTagKey.HIGHWAY.unwrap()));
        assertThat(tagsWay2.get(OsmTagKey.ACCESS.unwrap()), is("private"));

        final Map<String, String> tagsWay3 = adaptedWays.get(2).tags();
        assertEquals(2, tagsWay3.size());
    }
}