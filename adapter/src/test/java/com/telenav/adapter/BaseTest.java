package com.telenav.adapter;

import com.telenav.map.entity.Node;
import com.telenav.map.entity.RawRelation;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.WaySection;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import com.telenav.map.storage.RawNodeSchema;
import com.telenav.map.storage.RawRelationSchema;
import com.telenav.map.storage.RawWaySchema;
import com.telenav.map.storage.WaySectionSchema;
import com.telenav.map.storage.write.RawNodeToRowConverter;
import com.telenav.map.storage.write.RawRelationToRowConverter;
import com.telenav.map.storage.write.RawWayToRowConverter;
import com.telenav.map.storage.write.WaySectionToRowConverter;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author adrianal
 */
public class BaseTest {

    protected static Dataset<Row> rawWayRddToDataSet(final JavaRDD<RawWay> rawWaysRdd,
                                                     final SQLContext sqlContext) {
        final JavaRDD<Row> waySectionsRowRDD = rawWaysRdd.map(new RawWayToRowConverter()::apply);
        return sqlContext.createDataFrame(waySectionsRowRDD, new RawWaySchema().asStructType());
    }

    protected static Dataset<Row> waySectionRddToDataSet(final JavaRDD<WaySection> waySectionsRdd,
                                                         final SQLContext sqlContext) {
        final JavaRDD<Row> waySectionRowsRdd = waySectionsRdd.map(new WaySectionToRowConverter()::apply);
        return sqlContext.createDataFrame(waySectionRowsRdd, new WaySectionSchema().asStructType());
    }

    protected static Dataset<Row> nodeRddToDataSet(final JavaRDD<Node> nodesRdd,
                                                   final SQLContext sqlContext) {
        final JavaRDD<Row> nodesRowRDD = nodesRdd.map(new RawNodeToRowConverter()::apply);
        return sqlContext.createDataFrame(nodesRowRDD, new RawNodeSchema().asStructType());
    }

    protected static Dataset<Row> relationRddToDataSet(final JavaRDD<RawRelation> relationsRdd,
                                                       final SQLContext sqlContext) {
        final JavaRDD<Row> relationsRowRDD = relationsRdd.map(new RawRelationToRowConverter()::apply);
        return sqlContext.createDataFrame(relationsRowRDD, new RawRelationSchema().asStructType());
    }

    protected static JavaRDD<WaySection> rdd(JavaSparkContext javaSparkContext, WaySection... waySections) {
        return javaSparkContext.parallelize(Arrays.asList(waySections));
    }

    protected static WaySection waysection(int wayId) {
        final List<Node> nodes =
                Collections.singletonList(new Node(1L, Latitude.degrees(21.1234), Longitude.degrees(34.1234)));
        return new WaySection(wayId, 1, nodes, Collections.emptyMap());
    }

    protected static WaySection waysection(int wayId, int sequenceNumber) {
        final List<Node> nodes =
                Collections.singletonList(new Node(1L, Latitude.degrees(21.1234), Longitude.degrees(34.1234)));
        return new WaySection(wayId, sequenceNumber, nodes, Collections.emptyMap());
    }

    protected static Node node(int nodeId) {
        return new Node(nodeId, Latitude.degrees(123.456), Longitude.degrees(34.3456));
    }

    protected static RawWay rawWay(int wayId) {
        final List<Long> nodeIds = Collections.singletonList(1L);
        return new RawWay(wayId, nodeIds);
    }

    protected static RawWay rawWay(final int wayId,
                                   final List<Long> nodeIds) {
        return new RawWay(wayId, nodeIds);
    }

    protected WaySection waysection(int wayId, int sequenceNumber, Node... nodes) {
        return new WaySection(wayId, sequenceNumber, Arrays.asList(nodes), Collections.emptyMap());
    }

    protected WaySection waysection() {
        List<Node> nodes =
                Collections.singletonList(new Node(1L, Latitude.degrees(21.1234), Longitude.degrees(34.1234)));
        return new WaySection(1, 1, nodes, Collections.emptyMap());
    }
}