package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.telenav.TnTagKey;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertEquals;


/**
 * Unit tests for {@link LanguageExtractor}.
 *
 * @author ioanao
 */
public class LanguageExtractorTest {

    private final LanguageExtractor extractor = new LanguageExtractor();


    @Test
    public void emptyCollectionOfTags() {
        final Map<String, String> tags = Collections.emptyMap();
        final Optional<String> language = extractor.extract(tags);

        assertFalse(language.isPresent());
    }

    @Test
    public void tagsWithoutLanguage() {
        final Map<String, String> tags = Collections.singletonMap("oneway", "false");
        final Optional<String> language = extractor.extract(tags);

        assertFalse(language.isPresent());
    }

    @Test
    public void tagsWithLanguage() {
        final Map<String, String> tags = new HashMap<>();
        tags.put("oneway", "false");
        tags.put(TnTagKey.TN_LANGUAGE_CODE.unwrap(), "fr");
        tags.put("highway", "motorway");
        final Optional<String> language = extractor.extract(tags);

        assertEquals(language.get(), "fr");
    }
}