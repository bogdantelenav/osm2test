package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.adapter.bean.SpeedCategory;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


/**
 * Unit tests for {@link MaxSpeedBasedSpeedCategoryExtractor}.
 *
 * @author petrum
 */
public class MaxSpeedBasedSpeedCategoryExtractorTest {

    @Test
    public void returnNoCategoryWhenMaxSpeedTagIsNotPresent() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        final Optional<SpeedCategory> speedCategory = new MaxSpeedBasedSpeedCategoryExtractor().extract(tags);

        assertFalse(speedCategory.isPresent());
    }

    @Test
    public void calculateSpeedCategoryWhenMaxSpeedForwardTagIsInKm() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(TnTagKey.TN_SPEED_UNIT.unwrap(), "K");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20");
        final Optional<SpeedCategory> speedCategory = new MaxSpeedBasedSpeedCategoryExtractor().extract(tags);

        assertTrue(speedCategory.isPresent());
        assertThat(speedCategory.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_11_30));
    }

    @Test
    public void calculateSpeedCategoryWhenMaxSpeedForwardTagIsInMiles() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(TnTagKey.TN_SPEED_UNIT.unwrap(), "M");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20");
        final Optional<SpeedCategory> speedCategory = new MaxSpeedBasedSpeedCategoryExtractor().extract(tags);

        assertTrue(speedCategory.isPresent());
        assertThat(speedCategory.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }

    @Test
    public void calculateSpeedCategoryWhenMaxSpeedBackwardTagIsInKm() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(TnTagKey.TN_SPEED_UNIT.unwrap(), "K");
        tags.put(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20");
        final Optional<SpeedCategory> speedCategory = new MaxSpeedBasedSpeedCategoryExtractor().extract(tags);

        assertTrue(speedCategory.isPresent());
        assertThat(speedCategory.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_11_30));
    }

    @Test
    public void calculateSpeedCategoryWhenMaxSpeedBackwardTagIsInMiles() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(TnTagKey.TN_SPEED_UNIT.unwrap(), "M");
        tags.put(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "20");
        final Optional<SpeedCategory> speedCategory = new MaxSpeedBasedSpeedCategoryExtractor().extract(tags);

        assertTrue(speedCategory.isPresent());
        assertThat(speedCategory.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }

    @Test
    public void calculateSpeedCategoryWhenMaxSpeedBackwardAndForwardAreInMilesAndIntoTheSameCategory() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(TnTagKey.TN_SPEED_UNIT.unwrap(), "M");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20");
        tags.put(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "21");
        final Optional<SpeedCategory> speedCategory = new MaxSpeedBasedSpeedCategoryExtractor().extract(tags);

        assertTrue(speedCategory.isPresent());
        assertThat(speedCategory.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }

    @Test
    public void calculateSpeedCategoryWhenMaxSpeedBackwardAndForwardAreInKmAndIntoTheSameCategory() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(TnTagKey.TN_SPEED_UNIT.unwrap(), "K");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20");
        tags.put(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "21");
        final Optional<SpeedCategory> speedCategory = new MaxSpeedBasedSpeedCategoryExtractor().extract(tags);

        assertTrue(speedCategory.isPresent());
        assertThat(speedCategory.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_11_30));
    }

    @Test
    public void calculateSpeedCategoryWhenMaxSpeedBackwardAndForwardAreInKmAndInDifferentCategories() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(TnTagKey.TN_SPEED_UNIT.unwrap(), "K");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20");
        tags.put(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "41");
        final Optional<SpeedCategory> speedCategory = new MaxSpeedBasedSpeedCategoryExtractor().extract(tags);

        assertFalse(speedCategory.isPresent());
    }

    @Test
    public void calculateSpeedCategoryWhenMaxSpeedBackwardAndForwardAreInMilesAndInDifferentCategories() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(TnTagKey.TN_SPEED_UNIT.unwrap(), "M");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "20");
        tags.put(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "41");
        final Optional<SpeedCategory> speedCategory = new MaxSpeedBasedSpeedCategoryExtractor().extract(tags);

        assertFalse(speedCategory.isPresent());
    }

    @Test
    public void calculateSpeedCategoryBasedOnImageSpeedAndTnSpeed() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(TnTagKey.TN_SPEED_UNIT.unwrap(), "K");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "5");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.IMAGES), "20");
        tags.put(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "25");
        final Optional<SpeedCategory> speedCategory = new MaxSpeedBasedSpeedCategoryExtractor().extract(tags);

        assertTrue(speedCategory.isPresent());
        assertThat(speedCategory.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_11_30));
    }

    @Test
    public void calculateSpeedCategoryBasedOnImageSpeeds() {
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        tags.put(TnTagKey.TN_SPEED_UNIT.unwrap(), "K");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), "5");
        tags.put(OsmTagKey.MAXSPEED_FORWARD.withSource(TnSourceValue.IMAGES), "35");
        tags.put(OsmTagKey.MAXSPEED_BACKWARD.withSource(TnSourceValue.IMAGES), "31");
        tags.put(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), "25");
        final Optional<SpeedCategory> speedCategory = new MaxSpeedBasedSpeedCategoryExtractor().extract(tags);

        assertTrue(speedCategory.isPresent());
        assertThat(speedCategory.get(), is(SpeedCategory.SPEED_CAT_BETWEEN_31_50));
    }
}
