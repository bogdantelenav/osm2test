package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.HashMap;
import java.util.Optional;

import static org.hamcrest.Matchers.not;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Unit tests for {@link RouteExtractor}
 *
 * @author petrum
 */
@RunWith(JUnitParamsRunner.class)
public class RouteExtractorTest {
    private static final RouteExtractor ROUTE_EXTRACTOR = new RouteExtractor();

    private static final HashMap<String, String> TAGS = new HashMap<>();

    @Before
    public void initTags() {
        TAGS.put(OsmTagKey.FOOT.unwrap(), "yes");
        TAGS.put(OsmTagKey.JUNCTION.unwrap(), "valueJunction");
        TAGS.put(OsmTagKey.NAME.unwrap(), "valueName");
        TAGS.put("key1", "value1");
        TAGS.put("key2", "value2");
    }

    @Test
    public void withRouteFerry() {
        HashMap<String, String> tags = new HashMap<>(TAGS);
        tags.put(OsmTagKey.ROUTE.unwrap(), "ferry");

        Optional<String> extractorValue = ROUTE_EXTRACTOR.extract(tags);

        assertTrue(extractorValue.isPresent());
        assertThat(extractorValue.get(), is("ferry"));
    }

    @Test
    @Parameters({
            "bus",
            "road",
            "hiking",
            "bicycle",
            "foot",
            "mtb",
            "bike"
    })
    public void withRouteDifferentThanFerry(final String routeValue) {
        HashMap<String, String> tags = new HashMap<>(TAGS);
        tags.put(OsmTagKey.ROUTE.unwrap(), routeValue);

        Optional<String> extractorValue = ROUTE_EXTRACTOR.extract(tags);

        assertTrue(extractorValue.isPresent());
        assertThat(extractorValue.get(), not("ferry"));
    }

    @Test
    public void withoutRouteTag() {
        HashMap<String, String> tags = new HashMap<>(TAGS);

        Optional<String> extractorValue = ROUTE_EXTRACTOR.extract(tags);

        assertFalse(extractorValue.isPresent());
    }
}
