package com.telenav.adapter.component.mapper;

import com.telenav.adapter.BaseTest;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagValue;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * @author Andrei Puf
 */
@RunWith(JUnitParamsRunner.class)
public class OpenStreetMapToUniDbAccessMapperTest extends BaseTest {

    OpenStreetMapToUniDbAccessMapper openStreetMapToUniDbAccessMapper;

    @Before
    public void init() {
        openStreetMapToUniDbAccessMapper = new OpenStreetMapToUniDbAccessMapper();
    }

    @Test
    @Parameters({"yes", "permissive", "destination", "public", "fee", "agreement use", "right of way",
            "official", "emergency", "agreement_use", "community_space", "carpool"})
    public void mapToPublic(String tagValue) {
        final Optional<UnidbTagValue> unidbTagValue = openStreetMapToUniDbAccessMapper.map(tagValue);
        assertTrue("Access tag value not mapped to UniDb tag value",
                unidbTagValue.isPresent());
        assertEquals("Public access tag value not mapped to public UniDb tag value",
                UnidbTagValue.PUBLIC,
                unidbTagValue.get());
    }

    @Test
    @Parameters({"private", "customers", "no", "permit", "staff", "residents", "employees", "students", "restricted",
            "airspace", "residential", "military", "delivery", "customers;employees", "agricultural", "members",
            "students;visitors", "visitors", "private;customers", "permit\\,_metered", "forestry", "multifamily",
            "discouraged", "agricultural;forestry", "university", "customer", "customers;private"})
    public void mapToPrivate(String tagValue) {
        final Optional<UnidbTagValue> unidbTagValue = openStreetMapToUniDbAccessMapper.map(tagValue);
        assertTrue("Access tag value not mapped to UniDb tag value",
                unidbTagValue.isPresent());
        assertEquals("Private access tag value not mapped to private UniDb tag value",
                UnidbTagValue.PRIVATE,
                unidbTagValue.get());
    }

    @Test
    @Parameters({"rand1", "rand2", "private\\,public"})
    public void doNotMap(String tagValue) {
        final Optional<UnidbTagValue> unidbTagValue = openStreetMapToUniDbAccessMapper.map(tagValue);
        assertFalse("Access tag value mapped to UniDb tag value",
                unidbTagValue.isPresent());
    }
}
