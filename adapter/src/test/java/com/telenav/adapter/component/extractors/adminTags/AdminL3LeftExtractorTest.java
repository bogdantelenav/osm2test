package com.telenav.adapter.component.extractors.adminTags;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


/**
 * @author dianat2
 */
public class AdminL3LeftExtractorTest {

    private final AdminL3LeftExtractor extractor = new AdminL3LeftExtractor();

    @Test
    public void extract_entityHasAdminL3Left_adminL3LeftValue() {
        // Given
        final Map<String, String> tags = new HashMap<>();
        tags.put(TnTagKey.TN_ADMIN_L3_LEFT.unwrap(), "11111");

        // When
        final Optional<String> result = extractor.extract(tags);

        // Then
        assertTrue(result.isPresent());
        assertThat(result.get(), is("11111"));
    }

    @Test
    public void extract_entityHasNoAdminL3Left_optionalEmpty() {
        // Given
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), "motorway");

        // When
        final Optional<String> result = extractor.extract(tags);

        // Then
        assertFalse(result.isPresent());
    }
}