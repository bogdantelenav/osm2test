package com.telenav.adapter.component.extractors;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.telenav.TnTagKey.TN_SHIELD_ICON;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


/**
 * @author martap
 */
public class RoadShieldExtractorTest {

    private final RoadShieldExtractor roadShieldExtractor = new RoadShieldExtractor();
    private Map<String, String> wsTags;

    @Before
    public void initWsTags() {
        wsTags = new HashMap<>();
    }

    @Test
    public void oneRoadShield() {
        wsTags.put(TN_SHIELD_ICON.unwrap(), "100");
        final Optional<Map<String, String>> rsValues = roadShieldExtractor.extract(wsTags);

        assertTrue("Missing road shield value", rsValues.isPresent());
        assertThat("Invalid road shield number.", rsValues.get().size(), is(1));
        assertThat("Invalid road shield value extracted.", rsValues.get().get(TN_SHIELD_ICON.unwrap()), is("100"));
    }

    @Test
    public void moreRoadShields() {
        wsTags.put(TN_SHIELD_ICON.unwrap(), "100");
        wsTags.put(TN_SHIELD_ICON.postFixed((short) 1), "101");
        wsTags.put(TN_SHIELD_ICON.postFixed((short) 2), "102");
        final Optional<Map<String, String>> rsValues = roadShieldExtractor.extract(wsTags);

        assertTrue("Missing road shield value", rsValues.isPresent());
        assertThat("Invalid road shield number.", rsValues.get().size(), is(3));
        assertThat("Invalid road shield value extracted.", rsValues.get().get(TN_SHIELD_ICON.unwrap()), is("100"));
        assertThat("Invalid road shield value extracted.", rsValues.get().get(TN_SHIELD_ICON.postFixed((short) 1)),
                is("101"));
        assertThat("Invalid road shield value extracted.", rsValues.get().get(TN_SHIELD_ICON.postFixed((short) 2)),
                is("102"));
    }

    @Test
    public void withoutRoadShield() {
        final Optional<Map<String, String>> rsValues = roadShieldExtractor.extract(wsTags);
        assertThat("There should be no road shield value extracted.", rsValues, is(Optional.empty()));
    }
}