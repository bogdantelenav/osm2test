package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmRelationTagKey;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.adapter.bean.RoadFunctionalClass;
import org.junit.Test;

import java.util.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;


/**
 * @author petrum
 */
public class RoadFunctionalClassExtractorTest {

    private final RoadFunctionalClassExtractor extractor = new RoadFunctionalClassExtractor();

    @Test
    public void motorway() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY.unwrap());
        final Optional<RoadFunctionalClass> fc = extractor.extract(tags);
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.FIRST_CLASS));
    }

    @Test
    public void motorwayLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.MOTORWAY_LINK.unwrap());
        final Optional<RoadFunctionalClass> fc = extractor.extract(tags);
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.FIRST_CLASS));
    }

    @Test
    public void trunk() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRUNK.unwrap());
        final Optional<RoadFunctionalClass> fc = extractor.extract(tags);
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.SECOND_CLASS));
    }

    @Test
    public void trunkLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TRUNK_LINK.unwrap());
        final Optional<RoadFunctionalClass> fc = extractor.extract(tags);
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.SECOND_CLASS));
    }

    @Test
    public void primary() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.PRIMARY.unwrap());
        final Optional<RoadFunctionalClass> fc = extractor.extract(tags);
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.SECOND_CLASS));
    }

    @Test
    public void primaryLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.PRIMARY_LINK.unwrap());
        final Optional<RoadFunctionalClass> fc = extractor.extract(tags);
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.SECOND_CLASS));
    }

    //TODO test trunk link, primary link with bifurcation, road angle etc

    @Test
    public void secondary() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SECONDARY.unwrap());
        final Optional<RoadFunctionalClass> fc = extractor.extract(tags);
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.THIRD_CLASS));
    }

    @Test
    public void secondaryLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SECONDARY_LINK.unwrap());
        final Optional<RoadFunctionalClass> fc = extractor.extract(tags);
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.THIRD_CLASS));
    }

    @Test
    public void residential() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL.unwrap());
        final Optional<RoadFunctionalClass> fc = extractor.extract(tags);
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.FIFTH_CLASS));
    }

    @Test
    public void residentialLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.RESIDENTIAL_LINK.unwrap());
        final Optional<RoadFunctionalClass> fc = extractor.extract(tags);
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.FIFTH_CLASS));
    }

    @Test
    public void service() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.SERVICE.unwrap());
        final Optional<RoadFunctionalClass> fc = extractor.extract(tags);
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.FIFTH_CLASS));
    }

    @Test
    public void tertiary() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TERTIARY.unwrap());
        final Optional<RoadFunctionalClass> fc = extractor.extract(tags);
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.FOURTH_CLASS));
    }

    @Test
    public void tertiaryLink() {
        final Map<String, String> tags =
                Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), OsmHighwayValue.TERTIARY.unwrap());
        final Optional<RoadFunctionalClass> fc = extractor.extract(tags);
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.FOURTH_CLASS));

    }

    @Test
    public void ferry() {
        final Optional<RoadFunctionalClass> fc =
                extractor.extract(Collections.singletonMap(OsmRelationTagKey.ROUTE.unwrap(), "ferry"));
        assertTrue("Functional Class should not be null", fc.isPresent());
        assertThat(fc.get(), is(RoadFunctionalClass.SECOND_CLASS));
    }

    @Test
    public void highwayNotInTXDList() {
        final Optional<RoadFunctionalClass> fc =
                extractor.extract(Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), "emergency_bay"));
        assertTrue("Functional Class should not be null", fc.isPresent());

    }

    @Test
    public void restOfHighwayValues() {
        final String[] values =
                { "road", "track", "unclassified", "undefined", "unknown", "living_street", "private", "footway",
                        "pedestrian", "steps", "bridleway", "construction", "cycleway", "path", "bus_guideway" };
        Arrays.stream(values).forEach(value -> {
            final Optional<RoadFunctionalClass> fc =
                    extractor.extract(Collections.singletonMap(OsmTagKey.HIGHWAY.unwrap(), value));
            assertTrue("Functional Class should not be null", fc.isPresent());
            assertThat(fc.get(), is(RoadFunctionalClass.FIFTH_CLASS));
        });
    }
}