package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


/**
 * @author roxanal
 */
public class RoundaboutExtractorExtractorTest extends BaseExtractorTest {

    private final RoundaboutExtractor extractor = new RoundaboutExtractor();

    @Test
    public void testRoundaboutTag() {
        final Optional<String> roundaboutTag = extractor.extract(roundaboutTags("roundabout"));
        assertThat(roundaboutTag.get(), is("roundabout"));
    }

    @Test
    public void testWithoutNameTag() {
        final Optional<String> roundaboutTag = extractor.extract(highwayWayTags((OsmHighwayValue.MOTORWAY.unwrap())));
        assertThat(roundaboutTag, is(Optional.empty()));
    }
}