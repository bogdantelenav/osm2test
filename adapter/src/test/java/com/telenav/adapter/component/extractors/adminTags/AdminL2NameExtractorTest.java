package com.telenav.adapter.component.extractors.adminTags;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;


/**
 * @author adrianal
 */
public class AdminL2NameExtractorTest {

    private final AdminL2NameExtractor extractor = new AdminL2NameExtractor();

    @Test
    public void extract_entityHasAdminL2Name_adminL1NameValue() {
        // Given
        final Map<String, String> tags = new HashMap<>();
        tags.put(TnTagKey.TN_ADMIN_L2_NAME.unwrap(), "Cluj");

        // When
        final Optional<String> result = extractor.extract(tags);

        // Then
        assertTrue(result.isPresent());
        assertThat(result.get(), is("Cluj"));
    }

    @Test
    public void extract_entityHasAdminL2Name_optionalEmpty() {
        // Given
        final Map<String, String> tags = new HashMap<>();
        tags.put(OsmTagKey.HIGHWAY.unwrap(), "motorway");

        // When
        final Optional<String> result = extractor.extract(tags);

        // Then
        assertFalse(result.isPresent());
    }
}