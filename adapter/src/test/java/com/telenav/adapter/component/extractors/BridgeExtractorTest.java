package com.telenav.adapter.component.extractors;

import org.junit.Before;
import org.junit.Test;

import java.util.Collections;
import java.util.Map;
import java.util.Optional;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


/**
 * @author Andrei Puf
 */
public class BridgeExtractorTest {
    BridgeExtractor bridgeExtractor;

    @Before
    public void init() {
        bridgeExtractor = new BridgeExtractor();
    }

    @Test
    public void extractBridge() {
        final Map<String, String> tags = Collections.singletonMap("bridge", "true");
        final Optional<String> bridge = bridgeExtractor.extract(tags);

        assertTrue("Bridge tag is not present.", bridge.isPresent());
    }

    @Test
    public void doNotExtractBridge() {
        final Map<String, String> tags = Collections.singletonMap("test", "true");
        final Optional<String> bridge = bridgeExtractor.extract(tags);

        assertFalse("Bridge tag is present.", bridge.isPresent());
    }
}
