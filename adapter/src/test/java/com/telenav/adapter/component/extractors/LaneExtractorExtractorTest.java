package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import org.junit.Test;

import java.util.Optional;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


/**
 * @author roxanal
 */
public class LaneExtractorExtractorTest extends BaseExtractorTest {

    private final LaneExtractor extractor = new LaneExtractor();

    @Test
    public void testWithLane() {
        final Optional<String> laneValue = extractor.extract(laneTags(("2")));
        assertThat(laneValue.get(), is("2"));
    }

    @Test
    public void testWithoutLane() {
        final Optional<String> laneValue = extractor.extract(highwayWayTags((OsmHighwayValue.MOTORWAY.unwrap())));
        assertThat("There should be no lane tag!", laneValue, is(Optional.empty()));
    }
}