package com.telenav.adapter.component.mapper;

import com.telenav.datapipelinecore.unidb.UnidbTagValue;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.Optional;

import static org.junit.Assert.*;

/**
 * @author Andrei Puf
 */
@RunWith(JUnitParamsRunner.class)
public class OpenStreetMapToUniDbSurfaceMapperTest {

    OpenStreetMapToUniDbSurfaceMapper openStreetMapToUniDbSurfaceMapper;

    @Before
    public void init() {
        openStreetMapToUniDbSurfaceMapper = new OpenStreetMapToUniDbSurfaceMapper();
    }

    @Test
    @Parameters({"unpaved","sand", "grass", "earth", "pebblestone", "ground", "gravel", "compacted", "mud",
            "dirt", "rock"})
    public void mapToUnpaved(String tagValue) {
        final Optional<UnidbTagValue> unidbTagValue = openStreetMapToUniDbSurfaceMapper.map(tagValue);
        assertTrue("Surface tag value not mapped to UniDb tag value",
                unidbTagValue.isPresent());
        assertEquals("Unpaved surface tag value not mapped to unpaved UniDb tag value",
                UnidbTagValue.UNPAVED,
                unidbTagValue.get());
    }

    @Test
    @Parameters({"rand1", "rand2", "paved"})
    public void doNotMap(String tagValue) {
        final Optional<UnidbTagValue> unidbTagValue = openStreetMapToUniDbSurfaceMapper.map(tagValue);
        assertFalse("Invalid surface tag value mapped to UniDb tag value",
                unidbTagValue.isPresent());
    }
}
