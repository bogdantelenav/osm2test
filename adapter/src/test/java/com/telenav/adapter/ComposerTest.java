package com.telenav.adapter;

import com.telenav.adapter.component.ComposerDataset;
import com.telenav.map.compose.Composer;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.RawWay;
import com.telenav.map.geometry.Latitude;
import com.telenav.map.geometry.Longitude;
import com.telenav.map.storage.NodeSchema;
import com.telenav.map.storage.RawWaySchema;
import com.telenav.map.storage.write.NodeToRowConverter;
import com.telenav.map.storage.write.RawWayToRowConverter;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SQLContext;
import org.junit.Test;

import java.util.*;

public class ComposerTest {

    @Test
    public void testRdds() {

        final SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.sql.parquet.binaryAsString", "true");
        sparkConf.setMaster("local[1]");
        sparkConf.setAppName("Test");

        final JavaSparkContext javaSparkContext =
                JavaSparkContext.fromSparkContext(SparkContext.getOrCreate(sparkConf));

        final List<Long> nodes1 = Arrays.asList(1L, 2L, 3L);
        final RawWay rawWay = new RawWay(1L, nodes1);
        JavaRDD<RawWay> rawWays = javaSparkContext.parallelize(Arrays.asList(rawWay));

        final Node node1 = new Node(1L, Latitude.MAXIMUM, Longitude.MAXIMUM);
        final Node node2 = new Node(2L, Latitude.MAXIMUM, Longitude.MAXIMUM);
        final Node node3 = new Node(3L, Latitude.MAXIMUM, Longitude.MAXIMUM);
        final JavaRDD<Node> nodes = javaSparkContext.parallelize(Arrays.asList(node1, node2, node3));

        final Composer composer = new Composer(true);
        composer.compose(rawWays, nodes);
    }

    @Test
    public void testDatasets() {

        final SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.sql.parquet.binaryAsString", "true");
        sparkConf.setMaster("local[1]");
        sparkConf.setAppName("Test");

        final JavaSparkContext javaSparkContext =
                JavaSparkContext.fromSparkContext(SparkContext.getOrCreate(sparkConf));

        final List<Long> nodes1 = Arrays.asList(1L, 2L, 3L);
        final RawWay rawWay = new RawWay(1L, nodes1);
        JavaRDD<RawWay> rawWays = javaSparkContext.parallelize(Arrays.asList(rawWay));

        final Node node1 = new Node(1L, Latitude.MAXIMUM, Longitude.MAXIMUM, tags("place", "city"));
        final Node node2 = new Node(2L, Latitude.MAXIMUM, Longitude.MAXIMUM);
        final Node node3 = new Node(3L, Latitude.MAXIMUM, Longitude.MAXIMUM);
        final JavaRDD<Node> nodes = javaSparkContext.parallelize(Arrays.asList(node1, node2, node3));

        final SQLContext sqlContext = new SQLContext(javaSparkContext);

        final JavaRDD<Row> rawWaysRowRDD = rawWays.map(new RawWayToRowConverter()::apply);
        final Dataset<Row> rawWaysDataset = sqlContext.createDataFrame(rawWaysRowRDD, new RawWaySchema().asStructType());

        final JavaRDD<Row> nodesRowRDD = nodes.map(new NodeToRowConverter()::apply);
        final Dataset<Row> nodesDataset = sqlContext.createDataFrame(nodesRowRDD, new NodeSchema().asStructType());

        final Dataset<Row> nodesIndexedByRawWays = new ComposerDataset(true)
                .compose(rawWaysDataset, nodesDataset);

    }

    private Map<String, String> tags(final String... keyValuePairs) {
        final Map<String, String> tags = new HashMap<>();
        for (int i = 0; i < keyValuePairs.length; i += 2) {
            tags.put(keyValuePairs[i], keyValuePairs[i + 1]);
        }
        return tags;
    }
}
