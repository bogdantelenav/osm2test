package com.telenav.adapter;

import com.telenav.adapter.func.RelationMemberExplodeFunc;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;

import static com.telenav.adapter.func.RelationMemberExplodeFunc.RELATION_MEMBER_SCHEMA;
import static org.apache.spark.sql.functions.*;

/**
 * @author liviuc
 */
public class RelationService {

    public static Dataset<Row> nodeCityCenterRelations(final Dataset<Row> relations) {
        return relations.flatMap(RelationMemberExplodeFunc::map, RowEncoder.apply(RELATION_MEMBER_SCHEMA.asStructType()))
                .groupBy("nodeId")
                .agg(collect_list(struct(
                        col("relationId"),
                        col("relationTags").as("tags"),
                        col("member"))).as("memberRelations"));
    }
}
