package com.telenav.adapter;

import com.telenav.adapter.component.repository.ResourceRepository;
import com.telenav.adapter.entity.LangCodeBag;
import com.telenav.adapter.entity.dto.MemberRelation;
import com.telenav.adapter.rules.node.ApplicableToTagRuleDatasets;
import com.telenav.adapter.rules.node.CityCenterRule;
import com.telenav.adapter.rules.node.CleanNodeTagsRule;
import com.telenav.adapter.rules.node.NameRule;
import com.telenav.adapter.utils.RowUtils;
import com.telenav.datapipelinecore.bean.Member;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import scala.collection.JavaConverters;
import scala.collection.mutable.WrappedArray;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * The current class performs node enhancing.
 *
 * @author liviuc
 */
public class NgxNodesAdapterApplication {

    private static final NameRule NAME_RULE = new NameRule();
    private static final CleanNodeTagsRule CLEAN_NODE_RULE = new CleanNodeTagsRule();
    private static final ApplicableToTagRuleDatasets APPLICABLE_TO_TAG_RULE = new ApplicableToTagRuleDatasets();

    private static List<LangCodeBag> LANG_CODE_BAGS;

    static {
        try {
            LANG_CODE_BAGS = ResourceRepository.getLangCode();
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     *
     * @param relationsRow spark row containing node member relations.
     * @return transformed row into {@link MemberRelation}
     */
    private static List<MemberRelation> memberRelations(final WrappedArray.ofRef relationsRow) {
        final List<MemberRelation> memberRelations = new ArrayList<>();

        if (relationsRow.size() > 0) {
            final WrappedArray.ofRef wrapper1 = (WrappedArray.ofRef) (relationsRow).apply(0);
            for (int i = 0; i < wrapper1.size(); i++) {

                final Row relationMemberRow = (Row) wrapper1.apply(i);
                // Get relation data.
                final Long relationId = relationMemberRow.getLong(0);
                final WrappedArray.ofRef tagsArray = (WrappedArray.ofRef) relationMemberRow.get(1);
                final Map<String, String> relationTags = new HashMap<>();
                final int tagsSize = tagsArray.size();
                for (int idx = 0; idx < tagsSize; idx++) {
                    final Row entryRow = (Row) tagsArray.apply(idx);
                    relationTags.put(entryRow.getString(0), // key
                            entryRow.getString(1) // value
                    );
                }
                // Get member.
                final Row memberRow = (Row) relationMemberRow.get(2);
                final Long memberId = memberRow.getLong(0);
                final String memberRole = memberRow.getString(1);
                final String memberType = memberRow.getString(2);
                final Member member = new Member(memberId, memberRole, memberType);

                memberRelations.add(new MemberRelation(relationId, member, relationTags));
            }
        }

        return memberRelations;
    }

    /**
     *
     * @param nodesWithCityCenterRelations dataset with node related data, having the following schema:
     * root
     *  |-- id: long (nullable = true)
     *  |-- latitude: double (nullable = true)
     *  |-- longitude: double (nullable = true)
     *  |-- nodeTags: map (nullable = true)
     *  |    |-- key: string
     *  |    |-- value: string (valueContainsNull = true)
     *  |-- memberRelations: array (nullable = true)
     *  |    |-- element: array (containsNull = true)
     *  |    |    |-- element: struct (containsNull = true)
     *  |    |    |    |-- relationId: long (nullable = false)
     *  |    |    |    |-- tags: array (nullable = false)
     *  |    |    |    |    |-- element: struct (containsNull = false)
     *  |    |    |    |    |    |-- key: string (nullable = false)
     *  |    |    |    |    |    |-- value: string (nullable = true)
     *  |    |    |    |-- member: struct (nullable = false)
     *  |    |    |    |    |-- id: long (nullable = false)
     *  |    |    |    |    |-- role: string (nullable = false)
     *  |    |    |    |    |-- type: string (nullable = false)
     * @return nodes data with original node schema:
     * root
     *  |-- ID: long (nullable = true)
     *  |-- TAGS: map (nullable = true)
     *  |    |-- key: string
     *  |    |-- value: string (valueContainsNull = true)
     *  |-- LATITUDE: double (nullable = true)
     *  |-- LONGITUDE: double (nullable = true)
     */
    public Dataset<Row> adapt(final Dataset<Row> nodesWithCityCenterRelations) {
        return nodesWithCityCenterRelations.map(new EnhancementFunction(),
                RowEncoder.apply(nodesWithCityCenterRelations.schema()))
                .withColumnRenamed("nodeTags", "tags")
                .drop("memberRelations");
    }

    /**
     * This class gets a nodes data record (i.e. nodesWithCityCenterRelations) and returns the same record
     * but with tag enhancements
     *
     * @author liviuc
     */
    private static class EnhancementFunction implements MapFunction<Row, Row> {

        /**
         * Both input and output have the same schema:
         * root
         *  |-- id: long (nullable = true)
         *  |-- latitude: double (nullable = true)
         *  |-- longitude: double (nullable = true)
         *  |-- nodeTags: map (nullable = true)
         *  |    |-- key: string
         *  |    |-- value: string (valueContainsNull = true)
         *  |-- memberRelations: array (nullable = true)
         *  |    |-- element: array (containsNull = true)
         *  |    |    |-- element: struct (containsNull = true)
         *  |    |    |    |-- relationId: long (nullable = false)
         *  |    |    |    |-- tags: array (nullable = false)
         *  |    |    |    |    |-- element: struct (containsNull = false)
         *  |    |    |    |    |    |-- key: string (nullable = false)
         *  |    |    |    |    |    |-- value: string (nullable = true)
         *  |    |    |    |-- member: struct (nullable = false)
         *  |    |    |    |    |-- id: long (nullable = false)
         *  |    |    |    |    |-- role: string (nullable = false)
         *  |    |    |    |    |-- type: string (nullable = false)
         *
         * @param row spark node row.
         * @return enhanced spark node row.
         */
        @Override
        public Row call(final Row row) {

            final long nodeId = row.getAs("id");
            final double latitude = row.getAs("latitude");
            final double longitude = row.getAs("longitude");
            final WrappedArray.ofRef relationsRow = (WrappedArray.ofRef) row.get(row.fieldIndex("memberRelations"));

            List<Row> initialTags = row.getList(row.fieldIndex("nodeTags"));

            Map<String, String> tags = RowUtils.extractTags(initialTags);

            tags = new CityCenterRule(nodeId,
                    latitude,
                    longitude,
                    memberRelations(relationsRow),
                    LANG_CODE_BAGS)
                    .apply(tags);
            tags = NAME_RULE.apply(tags);
            tags = CLEAN_NODE_RULE.apply(tags);
            tags = APPLICABLE_TO_TAG_RULE.apply(tags);

            return RowFactory.create(nodeId,
                    JavaConverters.asScalaBufferConverter(RowUtils.toTagsList(tags)).asScala(),
                    latitude,
                    longitude);
        }
    }
}
