package com.telenav.adapter.component.extractors;

import com.telenav.map.entity.RawRelation;
import org.apache.spark.api.java.function.PairFlatMapFunction;
import scala.Tuple2;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

public class WayAdminLevelExtractor implements PairFlatMapFunction<RawRelation, Long, String> {
    private static final String OSM_ADMIN_LEVEL_TAG_KEY = "admin_level";

    @Override
    public Iterator<Tuple2<Long, String>> call(final RawRelation rawRelation) {

        final List<RawRelation.Member> ways = rawRelation.members()
                .stream()
                .filter(member -> member.type() == RawRelation.MemberType.WAY)
                .collect(Collectors.toList());

        if (ways.size() > 0) {
            final String adminLevel = rawRelation.tags().get(OSM_ADMIN_LEVEL_TAG_KEY);
            if (adminLevel != null) {
                return ways.stream()
                        .map(wayMember -> new Tuple2<Long, String>(wayMember.id(), adminLevel))
                        .iterator();
            }
        }

        return Collections.emptyIterator();
    }
}
