package com.telenav.adapter.component;

import com.telenav.adapter.entity.dto.MemberRelation;
import org.apache.spark.sql.Row;

import java.util.*;

import static com.telenav.adapter.utils.OsmKeys.ADMIN_LEVEL_TAG_KEY_IN_OSM;

/**
 * @author liviuc
 */
public class CityService {

    private static final Set<String> CITY_ADMIN_LEVEL = new HashSet<>(Arrays.asList("6"));

    public static Boolean isCityMemberRelation(final MemberRelation memberRelation) {
        return CITY_ADMIN_LEVEL.contains(memberRelation.getTags().get("admin_level"));
    }
}
