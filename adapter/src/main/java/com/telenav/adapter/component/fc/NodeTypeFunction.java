package com.telenav.adapter.component.fc;

import com.telenav.ost.spark.storage.Column;
import com.telenav.ost.spark.storage.Schema;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema;
import org.apache.spark.sql.types.DataTypes;
import scala.collection.Map;
import scala.collection.mutable.WrappedArray;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

import static com.telenav.datapipelinecore.unidb.RoadCategoryTagKey.FUNCTIONAL_CLASS;

/**
 * @author liviuc
 */
public class NodeTypeFunction {

    private static final String FUNCTIONAL_CLASS_KEY = FUNCTIONAL_CLASS.unwrap();
    private static final String NODE_TYPE = "node";
    private static final String SHAPE_POINT_TYPE = "shape_point";
    private static final String WAY_TAGS_FIELD = "TAGS";
    private static final String WAY_SECTION_TAGS_FIELD = "tags";

    /**
     * The current method receives as input:
     * - way record (can be either raw way or way section)
     * - name of the necessary fields to extract the required data
     *
     * @param way         way record (raw way or way section).
     * @param nodeIdField name of the node id field.
     * @param nodesField  name of the nodes field.
     * @param tagsField   name of the tags field.
     * @return record with data regarding type and navn tags by node id.
     */
    public static Iterator<Row> call(final Row way,
                                     final String nodeIdField,
                                     final String nodesField,
                                     final String tagsField) {

        final List<Row> result = new ArrayList<>();

        final boolean isNavigable = isNavigable(way, tagsField);

        final WrappedArray nodes = way.getAs(way.fieldIndex(nodesField));
        result.add(shapePointData(nodes,
                0,
                NODE_TYPE,
                nodeIdField,
                isNavigable));
        for (int i = 1; i < nodes.size() - 1; i++) {
            result.add(shapePointData(nodes,
                    i,
                    SHAPE_POINT_TYPE,
                    nodeIdField,
                    isNavigable));
        }
        result.add(shapePointData(nodes,
                nodes.size() - 1,
                NODE_TYPE,
                nodeIdField,
                isNavigable));

        return result.iterator();
    }

    private static Row shapePointData(final WrappedArray nodes,
                                      final int idx,
                                      final String nodeType,
                                      final String nodeIdField,
                                      final boolean isNavigable) {
        final GenericRowWithSchema shapeNode = (GenericRowWithSchema) nodes.apply(idx);
        final long id = shapeNode.getLong(shapeNode.fieldIndex(nodeIdField));
        final String navn = (isNavigable) ? "y" : null;
        return RowFactory.create(id, nodeType, navn);
    }

    private static boolean isNavigable(final Row row,
                                       final String tagsField) {
        if (WAY_TAGS_FIELD.equals(tagsField)) {
            final Map<String, String> tags = row.getMap(row.fieldIndex(tagsField));
            return tags.contains(FUNCTIONAL_CLASS_KEY);
        }
        if (WAY_SECTION_TAGS_FIELD.equals(tagsField)) {
            final List<Row> tagRows = row.getList(row.fieldIndex("tags"));
            for (final Row tag : tagRows) {
                // key
                if (FUNCTIONAL_CLASS_KEY.equals(tag.getString(0))) {
                    return true;
                }
            }
            return false;
        }
        throw new RuntimeException("Invalid tags field.");
    }

    /**
     * Schema for the node data wrapper record containing information regarding the
     * node type and the navn tags.
     */
    public static class NodeTypeSchema extends Schema {

        public static final Column NODE_ID;
        public static final Column NODE_TYPE;
        public static final Column NAVN;

        static {
            NODE_ID = new Column("nodeId", DataTypes.LongType, false);
            NODE_TYPE = new Column("nodeType", DataTypes.StringType, false);
            NAVN = new Column("navn", DataTypes.StringType, true);
        }

        public NodeTypeSchema() {
        }

        @Override
        protected Stream<Column> columns() {
            return Stream.of(NODE_ID, NODE_TYPE, NAVN);
        }
    }
}
