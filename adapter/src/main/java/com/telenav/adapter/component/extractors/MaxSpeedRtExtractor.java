package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;

import java.io.Serializable;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


/**
 * Extracts the maxSpeed tags ('maxspeed:forward', 'maxspeed:backward' or 'maxspeed').
 *
 * @author petrum
 */
public class MaxSpeedRtExtractor implements Serializable, Extractor<Map<String, String>> {

    @Override
    public Optional<Map<String, String>> extract(final Map<String, String> tagMap) {
        final Map<String, String> relevantTags = tagMap
                .keySet()
                .stream()
                .filter(key -> key.equals(OsmTagKey.MAXSPEED_FORWARD.unwrap())
                        || key.equals(OsmTagKey.MAXSPEED_BACKWARD.unwrap())
                        || key.equals(OsmTagKey.MAXSPEED.unwrap()))
                .collect(Collectors.toMap(key -> key, tagMap::get));
        if (relevantTags.containsKey(OsmTagKey.MAXSPEED.unwrap()) && relevantTags.size() > 1) {
            relevantTags.remove(OsmTagKey.MAXSPEED.unwrap());
        }
        return relevantTags.isEmpty() ? Optional.empty() : Optional.of(relevantTags);
    }
}
