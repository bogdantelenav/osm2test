package com.telenav.adapter.component.extractors.adminTags;

import com.telenav.adapter.component.extractors.Extractor;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.telenav.TnTagKey.TN_ADMIN_L2_NAME;


/**
 * Extract the 'tn__admin_l2_name' tag's value
 *
 * @author dianat2
 */
public class AdminL2NameExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting 'tn__admin_l2_name' tag!");
        if (tags.containsKey(TN_ADMIN_L2_NAME.unwrap())) {
            return Optional.of(tags.get(TN_ADMIN_L2_NAME.unwrap()));
        }
        return Optional.empty();
    }
}