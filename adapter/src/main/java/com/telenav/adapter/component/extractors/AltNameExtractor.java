package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.osm.OsmTagKey.ALT_NAME;

/**
 * Extract the 'alt_name' tag's value
 *
 * @author liviuc
 */
public class AltNameExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting 'alt_name' tag!");
        if (tags.containsKey(ALT_NAME.unwrap())) {
            return Optional.of(tags.get(ALT_NAME.unwrap()));
        }
        return Optional.empty();
    }
}
