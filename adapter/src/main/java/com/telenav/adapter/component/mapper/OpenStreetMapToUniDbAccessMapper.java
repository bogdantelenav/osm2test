package com.telenav.adapter.component.mapper;

import com.telenav.datapipelinecore.osm.OsmTagValue;
import com.telenav.datapipelinecore.unidb.UnidbTagValue;

import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.telenav.datapipelinecore.osm.OsmTagValue.OsmTagDelimiter.SEMICOLON;

/**
 * Access Open Street Map tag values to Access UniDb tag values mapper.
 *
 * @author Andrei Puf
 */
public class OpenStreetMapToUniDbAccessMapper extends OpenStreetMapToUniDbMapper  {

    /** Set of values that will be unified to "public" */
    private static final Set<OsmTagValue> PUBLIC_ACCESS_VALUES = new HashSet<>();

    /** Set of values that will be unified to "private" */
    private static final Set<OsmTagValue> PRIVATE_ACCESS_VALUES = new HashSet<>();

    /** Map of UniDb tag values as key and a set of convertible to that value Open Street Map tag values. */
    private static final Map<UnidbTagValue,Set<OsmTagValue>> UNI_DB_TO_OSM_MAP = new HashMap();

    static {
        PUBLIC_ACCESS_VALUES.add(OsmTagValue.PUBLIC);
        PUBLIC_ACCESS_VALUES.add(OsmTagValue.YES);
        PUBLIC_ACCESS_VALUES.add(OsmTagValue.PERMISSIVE);
        PUBLIC_ACCESS_VALUES.add(OsmTagValue.DESTINATION);
        PUBLIC_ACCESS_VALUES.add(OsmTagValue.FEE);
        PUBLIC_ACCESS_VALUES.add(OsmTagValue.AGREEMENT_USE);
        PUBLIC_ACCESS_VALUES.add(OsmTagValue.RIGHT_OF_WAY);
        PUBLIC_ACCESS_VALUES.add(OsmTagValue.OFFICIAL);
        PUBLIC_ACCESS_VALUES.add(OsmTagValue.EMERGENCY);
        PUBLIC_ACCESS_VALUES.add(OsmTagValue.AGREEMENT_USE_);
        PUBLIC_ACCESS_VALUES.add(OsmTagValue.COMMUNITY_SPACE);
        PUBLIC_ACCESS_VALUES.add(OsmTagValue.CAR_POOL);
        PUBLIC_ACCESS_VALUES.add(OsmTagValue.SERVICE);

        PRIVATE_ACCESS_VALUES.add(OsmTagValue.PRIVATE);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.CUSTOMERS);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.NO);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.PERMIT);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.STAFF);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.RESIDENTS);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.EMPLOYEES);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.STUDENTS);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.RESTRICTED);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.AIRSPACE);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.RESIDENTIAL);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.MILITARY);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.DELIVERY);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.AGRICULTURAL);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.MEMBERS);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.VISITORS);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.PERMIT_METERED);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.FORESTRY);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.MULTIFAMILY);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.DISCOURAGED);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.UNIVERSITY);
        PRIVATE_ACCESS_VALUES.add(OsmTagValue.CUSTOMER);

        UNI_DB_TO_OSM_MAP.put(UnidbTagValue.PUBLIC, PUBLIC_ACCESS_VALUES);
        UNI_DB_TO_OSM_MAP.put(UnidbTagValue.PRIVATE, PRIVATE_ACCESS_VALUES);
    }

    /**
     * Maps a string value representing an Open Street Map access tag to a UniDb access tag value.
     * @param accessValue The Open Street Map access tag value containing one or more tag values (Ex: value = "value1;value2;value2").
     * @return A UniDb access tag value object representing the mapping to UniDb access value or empty otherwise.
     */
    public Optional<UnidbTagValue> map(String accessValue) {
       return super.map(UNI_DB_TO_OSM_MAP, accessValue);
    }
}
