package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.osm.OsmTagKey.ALT_NAME;


/**
 * Extract the 'tn__alt_name' tag's value
 *
 * @author roxanal
 */
public class TnAltNameExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting 'tn__alt_name' tag!");
        if (tags.containsKey(ALT_NAME.telenavPrefix())) {
            return Optional.of(tags.get(ALT_NAME.telenavPrefix()));
        }
        return Optional.empty();
    }
}