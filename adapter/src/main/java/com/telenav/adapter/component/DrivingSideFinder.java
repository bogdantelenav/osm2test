package com.telenav.adapter.component;

import com.telenav.adapter.bean.DrivingSide;
import com.telenav.adapter.component.repository.ResourceRepository;
import com.telenav.adapter.entity.CountryDrivingSide;
import com.telenav.adapter.utils.IsoUtil;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import com.telenav.relationenhancer.core.component.geometry.FixedGeometryFactory;
import com.telenav.relationenhancer.core.component.geometry.PolygonRetriever;
import com.telenav.relationenhancer.core.component.geometry.RegionGeometryRetriever;
import com.telenav.relationenhancer.core.dto.Multipolygon;
import com.telenav.relationenhancer.core.entity.json.OsmGeometry;
import com.telenav.relationenhancer.core.utils.CartesianUtil;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.MultiPolygon;
import org.locationtech.jts.geom.Point;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author liviuc
 */
public class DrivingSideFinder {

    private static final String US = "US";
    private static final GeometryFactory GEOMETRY_FACTORY = new FixedGeometryFactory();
    private static final Map<Long, DrivingSide> DRIVING_SIDE_BY_WAY_ID = new HashMap<>();
    private static Map<String, DrivingSide> COUNTRY_ISO_2_TO_DRIVING_SIDE;
    private static List<MultiPolygon> VIRGIN_ISLANDS;

    static {
        try {

            COUNTRY_ISO_2_TO_DRIVING_SIDE = countryIso2ToDrivingSide();
            VIRGIN_ISLANDS = CartesianUtil.cartesian(virginIslands(), GEOMETRY_FACTORY);

        } catch (final IOException e) {

            throw new RuntimeException(e);
        }
    }

    /**
     * The method returns the driving side corresponding to the way section.
     * First it checks whether the way section contains iso related info on its tags and
     * if that's not the case, then it performs country localization based on the way section's
     * nodes geographical coordinates.
     *
     * @param waySection - way section to get the driving side for
     * @return driving side corresponding to the way section.
     */
    public static DrivingSide getDrivingSideFromWaySection(final WaySection waySection) {
        final long wayId = waySection.strictIdentifier().wayId();

        // First check if the driving side is cached.
        DrivingSide drivingSide = fromDrivingSideCache(wayId);
        if (drivingSide != null) {
            return drivingSide;
        }

        // First try use the iso tag from the waySection.
        drivingSide = fromIsoTag(waySection);
        if (drivingSide != null) {
            // Cache the driving side.
            DRIVING_SIDE_BY_WAY_ID.put(wayId, drivingSide);
            return drivingSide;
        }

        /*
            If that didn't work, iterate through all the nodes and return the firs driving side found.
            This approach makes sure that in case the driving side cannot be located
            for a specific nodes, further attempts will be made across the entire way sections.
        */
        drivingSide = fromNodes(waySection);
        if (drivingSide != null) {
            DRIVING_SIDE_BY_WAY_ID.put(wayId, drivingSide);

        }

        return drivingSide;
    }

    /**
     * The current method returns driving side corresponding to the given node by performing
     * country localization based on the node's coordinates.
     *
     * @param node node.
     * @return driving side corresponding to the node.
     */
    public static DrivingSide getDrivingSide(final Node node) {

        final String iso2 = IsoUtil.getIso2(node);

        final DrivingSide drivingSide = drivingSide(iso2, node);

        if (drivingSide == null) {
            throw new RuntimeException("no driving side found for iso2: " + iso2 +
                    " for node: " + node.id());
        }

        return drivingSide;
    }

    private static DrivingSide fromDrivingSideCache(final long wayId) {
        if (DRIVING_SIDE_BY_WAY_ID.containsKey(wayId)) {
            return DRIVING_SIDE_BY_WAY_ID.get(wayId);
        }
        return null;
    }

    private static DrivingSide fromIsoTag(final WaySection waySection) {
        final String iso2 = IsoUtil.getIso2(waySection);
        return drivingSide(iso2, waySection.firstNode());
    }

    private static DrivingSide fromNodes(final WaySection waySection) {
        DrivingSide drivingSide = null;
        for (final Node node : waySection.nodes()) {

            drivingSide = getDrivingSide(node);

            if (drivingSide != null) {
                // Cache the driving side.
                break;
            }
        }
        return drivingSide;
    }

    private static DrivingSide drivingSide(final String iso2,
                                           final Node node) {
        if (US.equals(iso2)) {

            if (isInVirginIslands(node)) {

                return DrivingSide.LEFT;
            }
        }
        return COUNTRY_ISO_2_TO_DRIVING_SIDE.get(iso2);
    }

    private static Map<String, DrivingSide> countryIso2ToDrivingSide() throws IOException {

        final List<CountryDrivingSide> drivingSides = ResourceRepository.getDrivingSide();

        final Map<String, String> countriesToIso2s = ResourceRepository.getCountryNameToIso2();

        final Map<String, DrivingSide> countryIso2ToDrivingSide = new HashMap<>();

        for (final CountryDrivingSide side : drivingSides) {

            final String iso2 = countriesToIso2s.get(side.getCountryName());

            countryIso2ToDrivingSide.put(iso2, side.getDrivingSide());
        }

        return countryIso2ToDrivingSide;
    }

    private static List<Multipolygon> virginIslands() throws IOException {

        final RegionGeometryRetriever regionGeometryRetriever = new RegionGeometryRetriever();

        final String json = ResourceRepository.readText("/geojson/us-virgin-islands.json");

        final OsmGeometry osmGeometry = regionGeometryRetriever.getFromJson(json);

        return PolygonRetriever.get(osmGeometry);
    }

    private static boolean isInVirginIslands(final Node node) {

        final Coordinate coordinate = CartesianUtil.cartesian(node.longitude().asDegrees(), node.latitude().asDegrees());

        final Point point = GEOMETRY_FACTORY.createPoint(coordinate);

        for (final MultiPolygon virginIslandMultipolygon : VIRGIN_ISLANDS) {

            if (point.within(virginIslandMultipolygon)) {
                return true;
            }

        }

        return false;
    }
}
