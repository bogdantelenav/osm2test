package com.telenav.adapter.component.extractors.cityCenterExtractor;

import com.telenav.adapter.component.extractors.Extractor;

import java.util.Map;
import java.util.Optional;

public class CityCenterExtractor implements Extractor<String> {

    public final String tagKeyInOsm;
    public String alternateKey;

    public CityCenterExtractor(final String tagKeyInOsm) {
        this.tagKeyInOsm = tagKeyInOsm;
    }

    public CityCenterExtractor(final String tagKeyInOsm,
                               final String alternateKey) {
        this.tagKeyInOsm = tagKeyInOsm;
        this.alternateKey = alternateKey;
    }

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        if (alternateKey != null && tags.containsKey(alternateKey)) {
            return Optional.of(tags.get(alternateKey));
        }
        if (tags.containsKey(tagKeyInOsm)) {
            return Optional.of(tags.get(tagKeyInOsm));
        }
        return Optional.empty();
    }
}
