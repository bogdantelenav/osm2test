package com.telenav.adapter.component.attributes.enhancement;

import com.telenav.adapter.component.attributes.enumeration.Detection;
import org.apache.commons.lang.math.NumberUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.telenav.adapter.component.attributes.constant.Constants.*;

/**
 * @author liviuc
 */
public class TurnRestrictionEnhancement extends Enhancement<String> {

    public TurnRestrictionEnhancement(final String idKey,
                                      final String... tags) {
        super(idKey, tags);
        this.detection = Detection.TURN_RESTRICTION;
    }

    public static class SidefileBasedBuilder {

        private String turnRestrictionType;
        private String turnRestrictionData;

        public List<Enhancement<String>> enhancement() {
            final List<Enhancement<String>> result = new ArrayList<>();

            final List<String> memberIds = new ArrayList<>();
            for (final String membersDescriptor : turnRestrictionData.split(WAY_SECTION_TURN_RESTRICTION_INFO_SEPARATOR)) {
                for (String memberData : membersDescriptor.split(WAY_SECTION_STRICT_IDENTIFIER_SEPARATOR)) {
                    if (NumberUtils.isNumber(memberData)) {
                        memberIds.add(memberData);
                    }
                }
            }

            final String idKey = String.join("_", memberIds);

            final Enhancement<String> enhancement =
                    new TurnRestrictionEnhancement(idKey,
                            "type", "turn_restriction",
                            RESTRICTION_TYPE_TAG, turnRestrictionType,
                            RESTRICTION_DATA_TAG, turnRestrictionData);

            result.add(enhancement);

            return result;
        }

        public SidefileBasedBuilder turnRestrictionType(final String turnRestrictionType) {
            this.turnRestrictionType = turnRestrictionType;
            return this;
        }

        public SidefileBasedBuilder data(final String data) {
            this.turnRestrictionData = data;
            return this;
        }
    }
}
