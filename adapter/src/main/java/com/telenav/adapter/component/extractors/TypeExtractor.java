package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.unidb.UnidbTagKey.*;

/**
 * Extracts 'type' tag value.
 *
 * @author irinap
 */
public class TypeExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(Map<String, String> tags) {
        return tags.containsKey(TYPE.unwrap()) ? Optional.of(tags.get(TYPE.unwrap())) : Optional.empty();
    }
}
