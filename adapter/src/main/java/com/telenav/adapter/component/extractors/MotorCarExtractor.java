package com.telenav.adapter.component.extractors;


import com.telenav.datapipelinecore.osm.OsmTagKey;


/**
 * Extracts the 'motorcar' way tag
 *
 * @author catalinm
 */
public class MotorCarExtractor extends SingleTagExtractor {

    public MotorCarExtractor() {
        super(OsmTagKey.MOTORCAR.unwrap());
    }

}