package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

/**
 * The class is responsible for extraction the value of a single tag
 * 
 * @author andriyz
 *
 */
public abstract class SingleTagExtractor implements Extractor<String>{

    private static final long serialVersionUID = 1L;
    private final String tagKey;

    /**
     * Initializes the SingleTagExtractor using the tag key.
     * @param tagKey osm tag key
     */
    public SingleTagExtractor(String tagKey) {
        this.tagKey = tagKey;
    }

    /**
     * Extracts the value of the tag.
     *
     * @param tags a way section's tags
     *
     * @return An Optional of String representing the tag's value.
     */
    @Override
    public final Optional<String> extract(Map<String, String> tags) {
        return Optional.ofNullable(tags.get(tagKey));
    }

    public final String getTagKey() {
        return tagKey;
    }
}
