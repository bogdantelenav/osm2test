package com.telenav.adapter.component.attributes.service;

import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.datapipelinecore.io.writer.RddWriter;
import com.telenav.map.entity.RawRelation;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.util.CollectionAccumulator;

import java.util.Set;

/**
 * The current service is responsible for enhancing relations with sidefile data (attributes or detections)
 *
 * @author liviuc
 */
public class RelationAttributesProcessingService extends AttributesProcessingService<RawRelation, String> {

    private final String trWaySectionsInputPath;
    private final String detectionsSource;

    /**
     * @param relationsPath          input path of entities to be enhanced.
     * @param trWaySectionsInputPath path of way sections to use for turn-restriction enhancing.
     * @param relationsOutputPath    output path of enhanced relations.
     * @param relationsSidefilePaths sidefiles input path.
     * @param relationsConfigPath    config file input path.
     * @param broadcastJoin          parameter describing whether to perform a broadcast join or not.
     * @param detectionsSource       source of external enhancements.
     * @param logs                   logs.
     */
    public RelationAttributesProcessingService(final String relationsPath,
                                               final String trWaySectionsInputPath,
                                               final String relationsOutputPath,
                                               final String relationsSidefilePaths,
                                               final String relationsConfigPath,
                                               final boolean broadcastJoin,
                                               final String detectionsSource,
                                               final CollectionAccumulator<String> logs) {
        super(relationsPath,
                relationsOutputPath,
                relationsSidefilePaths,
                relationsConfigPath,
                broadcastJoin,
                logs);
        this.trWaySectionsInputPath = trWaySectionsInputPath;
        this.detectionsSource = detectionsSource;
    }

    @Override
    protected void write(final JavaRDD<RawRelation> rdd,
                         final String outputPath,
                         final JavaSparkContext javaSparkContext) {
        RddWriter.RawRelationWriter.write(javaSparkContext, rdd, outputPath);
    }

    @Override
    protected JavaRDD<RawRelation> read(final String relationsPath,
                                        final JavaSparkContext javaSparkContext) {
        return RddReader.RawRelationReader.read(javaSparkContext, relationsPath);
    }

    @Override
    protected AttributesService<RawRelation, String> attributesService(final Set<String> blackList) {
        return new RelationAttributeService(blackList, broadcastJoin, trWaySectionsInputPath, detectionsSource, logs);
    }
}
