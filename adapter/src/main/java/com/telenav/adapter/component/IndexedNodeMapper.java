package com.telenav.adapter.component;

import com.telenav.adapter.bean.IndexedNode;
import org.apache.spark.sql.Row;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * @author liviuc
 */
public class IndexedNodeMapper {

    public static Iterator<IndexedNode> map(final Row rawWay) {

        final long wayId = rawWay.getLong(rawWay.fieldIndex("id"));

        final List<Row> nodesRow = rawWay.getList(rawWay.fieldIndex("nodes"));

        final List<IndexedNode> indexedNodes = new ArrayList<>(nodesRow.size());
        for (final Row node : nodesRow) {
            indexedNodes.add(IndexedNode.builder()
                    .wayId(wayId)
                    .index(node.getInt(node.fieldIndex("index")))
                    .nodeId(node.getLong(node.fieldIndex("nodeId")))
                    .build());
        }

        return indexedNodes.iterator();
    }
}
