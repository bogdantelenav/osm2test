package com.telenav.adapter.component.extractors;

import static com.telenav.datapipelinecore.osm.OsmTagKey.HOV;

/**
 * Extracts the 'hov' tag's value
 *
 * @author odrab
 */
public class HovExtractor extends SingleTagExtractor {

    public HovExtractor() {
        super(HOV.unwrap());
    }
}