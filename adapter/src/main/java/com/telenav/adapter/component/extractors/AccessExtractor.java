package com.telenav.adapter.component.extractors;

import static com.telenav.datapipelinecore.osm.OsmTagKey.ACCESS;

/**
 * Extracts 'access' tag value.
 */
public class AccessExtractor extends SingleTagExtractor {

    public AccessExtractor() {
        super(ACCESS.unwrap());
    }
}
