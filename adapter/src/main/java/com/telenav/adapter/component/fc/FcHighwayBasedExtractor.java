package com.telenav.adapter.component.fc;

import java.util.Map;

/**
 * @author liviuc
 */
public interface FcHighwayBasedExtractor {

    FcHighwayBasedExtractor MEX_EXTRACTOR = (tags) -> {

        final String highway = tags.get("highway");
        switch (highway) {

            case "motorway":
                return FcInfo.FC_THIRD;

            case "motorway_link":
                return FcInfo.FC_THIRD;

            case "trunk":
                return FcInfo.FC_THIRD;

            case "trunk_link":
                return FcInfo.FC_THIRD;

            case "primary":
                return FcInfo.FC_THIRD;

            case "primary_link":
                return FcInfo.FC_THIRD;

            case "secondary":
                return FcInfo.FC_FOURTH;

            case "secondary_link":
                return FcInfo.FC_FOURTH;

            case "tartiary":
                return FcInfo.FC_FOURTH;

            case "tertiary_link":
                return FcInfo.FC_FOURTH;

            default:
                return FcInfo.FC_FIFTH;
        }
    };

    FcHighwayBasedExtractor CAN_EXTRACTOR = (tags) -> {
            return "5";
    };

    static String fcByCountry(final String iso,
                              final Map<String, String> tags) {

        switch (iso) {
            case "MEX":
                return MEX_EXTRACTOR.fc(tags);
            case "CAN":
                return CAN_EXTRACTOR.fc(tags);
            default:
                throw new RuntimeException("FC by country not implemented for iso=" + iso);
        }
    }

    String fc(final Map<String, String> tags);
}
