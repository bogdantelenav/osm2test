package com.telenav.adapter.component;

public class TwoPointWaySectionKey {

    public static final String OSM_NAME_TAG_KEY = "name";
    public static final String OSM_HIGHWAY_TAG_KEY = "highway";
    public static final String OSM_ONEWAY_TAG_KEY = "oneway";
    public static final String KEY_SLOPE_INFO = "slope";
    public static final String SEPARATOR = ":";
}
