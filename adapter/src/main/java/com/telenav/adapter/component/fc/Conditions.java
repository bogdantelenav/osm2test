package com.telenav.adapter.component.fc;

/**
 * @author liviuc
 */
public interface Conditions<T> {

    boolean isTrue(final T t);
}
