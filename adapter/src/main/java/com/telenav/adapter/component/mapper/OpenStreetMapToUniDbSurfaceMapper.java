package com.telenav.adapter.component.mapper;

import com.telenav.datapipelinecore.osm.OsmTagValue;
import com.telenav.datapipelinecore.unidb.UnidbTagValue;

import java.util.*;
/**
 * Surface Open Street Map tag values to Surface UniDb tag values mapper.
 *
 * @author Andrei Puf
 */
public class OpenStreetMapToUniDbSurfaceMapper extends OpenStreetMapToUniDbMapper {

    /** Set of values that will be unified to 'unpaved'. */
    public static final Set<OsmTagValue> UNPAVED_SURFACE_VALUES = new HashSet();

    /** Map of UniDb tag values as key and a set of convertible to that value Open Street Map tag values. */
    private static final Map<UnidbTagValue,Set<OsmTagValue>> UNI_DB_TO_OSM_MAP = new HashMap();

    static {

        UNPAVED_SURFACE_VALUES.add(OsmTagValue.UNPAVED);
        UNPAVED_SURFACE_VALUES.add(OsmTagValue.SAND);
        UNPAVED_SURFACE_VALUES.add(OsmTagValue.GRASS);
        UNPAVED_SURFACE_VALUES.add(OsmTagValue.EARTH);
        UNPAVED_SURFACE_VALUES.add(OsmTagValue.PEBBLESTONE);
        UNPAVED_SURFACE_VALUES.add(OsmTagValue.GROUND);
        UNPAVED_SURFACE_VALUES.add(OsmTagValue.GRAVEL);
        UNPAVED_SURFACE_VALUES.add(OsmTagValue.COMPACTED);
        UNPAVED_SURFACE_VALUES.add(OsmTagValue.MUD);
        UNPAVED_SURFACE_VALUES.add(OsmTagValue.DIRT);
        UNPAVED_SURFACE_VALUES.add(OsmTagValue.ROCK);

        UNI_DB_TO_OSM_MAP.put(UnidbTagValue.UNPAVED, UNPAVED_SURFACE_VALUES);
    }

    public Optional<UnidbTagValue> map(String surfaceValue) {
        return super.map(UNI_DB_TO_OSM_MAP, surfaceValue);
    }
}
