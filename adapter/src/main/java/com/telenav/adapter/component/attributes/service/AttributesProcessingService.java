package com.telenav.adapter.component.attributes.service;

import com.telenav.adapter.component.attributes.enumeration.Detection;
import com.telenav.map.entity.StandardEntity;
import com.telenav.map.entity.TagAdjuster;
import lombok.NonNull;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.util.CollectionAccumulator;

import java.util.*;
import java.util.stream.Collectors;

/**
 * The current service is responsible for enhancing entities with sidefile data (attributes or detections)
 * The attributes to be replaced are read, the entities cleared by them and then enhancements are added.
 *
 * @author liviuc
 */
public abstract class AttributesProcessingService<E extends StandardEntity & TagAdjuster<E>, Identifier> {

    protected final boolean broadcastJoin;
    protected final CollectionAccumulator<String> logs;
    private final String entitiesInputPath;
    private final String entitiesOutputPath;
    private final String enhancementsInputPaths;
    private final String configPath;

    /**
     * @param entitiesInputPath      input path of entities to be enhanced.
     * @param entitiesOutputPath     output path of enhanced entities.
     * @param enhancementsInputPaths sidefiles input path.
     * @param configPath             config file input path.
     * @param broadcastJoin          parameter describing whether to perform a broadcast join or not.
     * @param logs                   logs.
     */
    public AttributesProcessingService(@NonNull final String entitiesInputPath,
                                       @NonNull final String entitiesOutputPath,
                                       final String enhancementsInputPaths,
                                       final String configPath,
                                       final boolean broadcastJoin,
                                       final CollectionAccumulator<String> logs) {
        this.entitiesInputPath = entitiesInputPath;
        this.entitiesOutputPath = entitiesOutputPath;
        this.enhancementsInputPaths = enhancementsInputPaths;
        this.configPath = configPath;
        this.broadcastJoin = broadcastJoin;
        this.logs = logs;
    }

    private static Iterator<String> blacklistConfigValue(final String val) {
        List<String> result = new ArrayList<>();
        if (val.endsWith("_detection")) {
            final String detectionAsString = val.replace("_detection", "");
            result.addAll(Detection.valueOf(detectionAsString.toUpperCase()).tagsToRemove());
        } else {
            result = Arrays.asList(val);
        }
        return result.iterator();
    }

    /**
     * Core method for enhancement.
     * Read entities.
     * Read enhancements.
     * Read attributes from config file.
     * Enhance entities using a {@link AttributesService}
     * Save entities.
     *
     * @param javaSparkContext The spark context used to process the data.
     */
    public void process(final JavaSparkContext javaSparkContext) {
        // Read entities.
        final JavaRDD<E> entities = read(entitiesInputPath, javaSparkContext);

        if (isValidForEnhancement()) {
            // Read enhancements.
            final JavaRDD<String> enhancements = readEnhancements(javaSparkContext);

            // Read osm attributes that will be removed and potentially replaced with enhancement data.
            final Set<String> attributesBlackList = blacklist(configPath, javaSparkContext);

            // Get the attributes service.
            final AttributesService<E, Identifier> attributesService = attributesService(attributesBlackList);

            // Enhance entities.
            final JavaRDD<E> enhancedEntities = attributesService.process(entities, enhancements, javaSparkContext);

            // Save entities.
            write(enhancedEntities, entitiesOutputPath, javaSparkContext);
        } else {
            write(entities, entitiesOutputPath, javaSparkContext);
        }
    }

    private JavaRDD<String> readEnhancements(final JavaSparkContext javaSparkContext) {
        JavaRDD<String> enhancementsFromAllPaths = javaSparkContext.emptyRDD();

        for (final String path : enhancementsInputPaths.trim().split(",")) {
            final JavaRDD<String> enhancements = javaSparkContext.textFile(path);
            enhancementsFromAllPaths = enhancementsFromAllPaths.union(enhancements);
        }

        return enhancementsFromAllPaths;
    }

    private Set<String> blacklist(final String configPath,
                                  final JavaSparkContext javaSparkContext) {
        return javaSparkContext.textFile(configPath)
                .flatMap(AttributesProcessingService::blacklistConfigValue)
                .collect()
                .stream()
                .collect(Collectors.toSet());
    }

    protected abstract void write(final JavaRDD<E> rdd,
                                  final String outputPath,
                                  final JavaSparkContext javaSparkContext);

    protected abstract JavaRDD<E> read(final String entitiesInputPath,
                                       final JavaSparkContext javaSparkContext);

    protected abstract AttributesService<E, Identifier> attributesService(final Set<String> blackList);

    private boolean isValidForEnhancement() {
        return !StringUtils.isEmpty(configPath) && !StringUtils.isEmpty(enhancementsInputPaths);
    }
}

