package com.telenav.adapter.component.attributes.constant;

/**
 * @author liviuc
 */
public class Constants {

    /**
     * Separates the data within the sidefile records.
     */
    public static final String RECORD_SEPARATOR = ",";
    /**
     * Separates the way_id, start_point_id and end_point_id within the way section identifier field
     * from the turn restrictions record.
     */
    public static final String WAY_SECTION_TURN_RESTRICTION_INFO_SEPARATOR = ":";
    /**
     * Separates the way_id, start_point_id and end_point_id within the way section identifier field
     * from the turn lanes record.
     */
    public static final String WAY_SECTION_STRICT_IDENTIFIER_SEPARATOR = "_";
    /**
     * Turn restriction related constants;
     */
    public static final String RESTRICTION_TYPE_TAG = "restriction_type";
    public static final String RESTRICTION_DATA_TAG = "restriction_data";

}
