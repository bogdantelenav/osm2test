package com.telenav.adapter.component.mapper;

import com.telenav.datapipelinecore.osm.OsmTagValue;
import com.telenav.datapipelinecore.unidb.UnidbTagValue;

import java.io.Serializable;
import java.util.*;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.telenav.datapipelinecore.osm.OsmTagValue.OsmTagDelimiter.SEMICOLON;

/**
 * Open Street Map to UniDb mapper.
 *
 * @author Andrei Puf
 */
public abstract class OpenStreetMapToUniDbMapper implements Serializable {

    /**
     * Maps a string value representing an Open Street Map tag to a UniDb tag using a {@link Map} of UniDb values to
     * Open Street Map values.
     *
     * @param tagValueMapping The mapping having the keys UniDb values and the values a {@link Set} of Open Street Map values
     * @param osmTagValue The Open Street Map tag value containing one or more tag values (Ex: value = "value1;value2;value2").
     *
     * @return A UniDb tag value object representing the mapping to UniDb value or empty otherwise.
     */
    protected Optional<UnidbTagValue> map(Map<UnidbTagValue, Set<OsmTagValue>> tagValueMapping, String osmTagValue) {

        return tagValueMapping.keySet().stream()
                .map(new OsmToUniDbMapper(tagValueMapping, osmTagValue))
                .reduce(new UniDbReducer()).get();
    }

    /**
     * Mapper for mapping an {@link UnidbTagValue} tag value to an {@link Optional} of {@link UnidbTagValue}
     * based on a string representing the Open Street Map tag value and a map representing the possible mapping
     * between the Open Street Map Tag values and UniDb tag values.
     */
    class OsmToUniDbMapper implements Function<UnidbTagValue, Optional<UnidbTagValue>> {

        private final Map<UnidbTagValue, Set<OsmTagValue>> tagValueMapping;
        private final List<String> osmTagValueParts;

        /**
         * Initializes an instance of OsmToUniDbMapper using a map of UniDb to Open Street Map tag values and
         * an Open Street Map tag value.
         * @param tagValueMapping A map of UniDb to Open Street Map tag values
         * @param osmTagValue An Open Street Map tag value
         */
        OsmToUniDbMapper(Map<UnidbTagValue, Set<OsmTagValue>> tagValueMapping, String osmTagValue) {
            this.tagValueMapping = tagValueMapping;
            osmTagValueParts = Arrays.asList(osmTagValue.split(SEMICOLON.unwrap()));
        }

        /** Unify Open Street Map value to UniDb value if possible.
         *
         * @param unidbTagValue UniDb tag value to unify to.
         *
         * @return An optional of the UniDb value that Open Street Map value was mapped to or empty otherwise.
         */
        @Override
        public Optional<UnidbTagValue> apply(UnidbTagValue unidbTagValue) {
            final Set<String> osmTagValues = tagValueMapping.get(unidbTagValue).stream()
                    .map(osmTagValue -> osmTagValue.unwrap()).collect(Collectors.toSet());
            final Optional<Boolean> isOsmValueMappableToUniDbTagValue = osmTagValueParts.stream()
                    .map(tagValuePart -> osmTagValues.contains(tagValuePart))
                    .reduce((isTagValuePart1Contained, isTagValuePart2Contained) ->
                            isTagValuePart1Contained && isTagValuePart1Contained);
            if (!isOsmValueMappableToUniDbTagValue.orElse(false)) {
                return Optional.empty();
            }
            return Optional.of(unidbTagValue);
        }
    }

    /**
     * Reducer for reducing a stream of {@link Optional} of {@link UnidbTagValue} to
     * a single {@link Optional} of {@link UnidbTagValue} resulting the mapping of the
     * Open Street Map tag value to the UniDb tag value or an empty {@link Optional} if
     * the Open Street Map tag value cannot be mapped to a UniDb tag value.
     */
    class UniDbReducer implements BinaryOperator<Optional<UnidbTagValue>> {

        /**
         * Reduces the stream of mapped values to only one which will be the UniDb Value the Open Street Map will be
         * mapped to. In case the stream cannot be reduced in means the components of the Open Street Map value
         * cannot be mapped to UniDb.
         *
         * @param partialUnidbTagValue1  First partial UniDb tag value.
         * @param partialUnidbTagValue2  Second partial UniDb tag value.
         *
         * @return A unified UniDb tag value.
         */
        @Override
        public Optional<UnidbTagValue> apply(Optional<UnidbTagValue> partialUnidbTagValue1,
                                             Optional<UnidbTagValue> partialUnidbTagValue2) {
            if (partialUnidbTagValue1.isPresent() && partialUnidbTagValue2.isPresent()) {
                return Optional.empty();                     /* A tag value cannot be mapped to two UniDb values */
            } else if(partialUnidbTagValue1.isPresent()) {
                return partialUnidbTagValue1;
            } else if (partialUnidbTagValue2.isPresent()) {
                return partialUnidbTagValue2;
            }
            return Optional.empty();
        }
    }

}
