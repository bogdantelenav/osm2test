package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;

/**
 * Extracts the 'access' way tag
 *
* @author andriyz
 */
public class AccessExractor extends SingleTagExtractor {

    public AccessExractor() {
        super(OsmTagKey.ACCESS.unwrap());
    }

}