package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;

import java.util.Map;
import java.util.Optional;


/**
 * Extracts the 'lane' tag's value
 *
 * @author roxanal
 */
public class LaneExtractor implements Extractor<String> {

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        if (tags.containsKey(OsmTagKey.LANES.unwrap())) {
            return Optional.of(tags.get(OsmTagKey.LANES.unwrap()));
        }
        return Optional.empty();
    }

    /**
     * Extracts a specific tag.
     *
     * @param tags Tags map to extract from.
     * @param key The tag to look for.
     * @return Optional of tag value or empty otherwise.
     */
    public Optional<String> extract(final Map<String, String> tags, final String key) {
        if (tags.containsKey(key)) {
            return Optional.of(tags.get(key));
        }
        return Optional.empty();
    }
}