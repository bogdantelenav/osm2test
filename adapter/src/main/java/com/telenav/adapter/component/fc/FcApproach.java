package com.telenav.adapter.component.fc;

import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.WaySection;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @author liviuc .
 */
public abstract class FcApproach implements Serializable {

    protected List<String> countries;

    public FcApproach(final List<String> countries) {
        this.countries = countries;
    }

    public boolean isValidForCountry(final String iso) {
        return countries.contains(iso);
    }

    public abstract String fc(final WaySection waySection);

    protected String fc(final Map<String, String> tags) {
        return tags.get(RoadCategoryTagKey.FUNCTIONAL_CLASS.unwrap());
    }
}
