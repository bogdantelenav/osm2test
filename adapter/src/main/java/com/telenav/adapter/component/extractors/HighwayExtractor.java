package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;


/**
 * Extract the highway value.
 *
 * @author andriyz
 */
public class HighwayExtractor extends SingleTagExtractor {

    public HighwayExtractor() {
        super(OsmTagKey.HIGHWAY.unwrap());
    }
}