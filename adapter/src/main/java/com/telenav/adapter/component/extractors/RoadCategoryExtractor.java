package com.telenav.adapter.component.extractors;

import com.telenav.adapter.bean.Tag;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.telenav.TnTagKey.*;


/**
 * Extract the 'tn__road_type'/ 'tn__road_subtype'/ 'tn__functional_class' / 'tn__functional_class_upgrade' /
 * 'tn__speed_category' / 'tn__ramp' / 'tn__intersection-link' / 'tn__connection_road' / 'tn__dual_way' tag's value
 *
 * @author roxanal
 */
public class RoadCategoryExtractor implements Extractor<List<Tag>> {

    @Override
    public Optional<List<Tag>> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting road category tags!");
        final List<Tag> fcPairs = new ArrayList<>();
        for (final String key : tags.keySet()) {
            if (isRoadCategoryTag(key)) {
                fcPairs.add(new Tag(key, tags.get(key)));
            }
        }
        return fcPairs.isEmpty() ? Optional.empty() : Optional.of(fcPairs);
    }

    private boolean isRoadCategoryTag(final String tag) {
        return tag.equals(TN_ROAD_TYPE.unwrap()) || tag.equals(TN_ROAD_SUBTYPE.unwrap()) || tag
                .equals(TN_FUNCTIONAL_CLASS.unwrap()) || tag.equals(TN_FUNCTIONAL_CLASS_UPGRADE.unwrap()) || tag
                .equals(TN_SPEED_CATEGORY.unwrap()) || tag.equals(TN_RAMP.unwrap()) || tag
                .equals(TN_INTERSECTION_LINK.unwrap()) || tag.equals(TN_CONNECTION_ROAD.unwrap()) || tag
                .equals(TN_DUAL_WAY.unwrap());
    }
}