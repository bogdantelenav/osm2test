package com.telenav.adapter.component;

import com.telenav.fcd.locator.CountryLocator;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * @author liviuc .
 */
public class CountryLocatorHolder {

    private static final Map<Integer, String> COORDINATES_HASH_TO_ISO = new HashMap<>();
    private static CountryLocator COUNTRY_LOCATOR;

    static {
        try {

            COUNTRY_LOCATOR = new CountryLocator();

        } catch (final IOException e) {

            throw new RuntimeException(e);
        }
    }

    public static synchronized String getCountryForCoordinates(final double latitudeDegrees,
                                                               final double longitudeDegrees) {

        final int hash = Objects.hash(latitudeDegrees, longitudeDegrees);

        if (COORDINATES_HASH_TO_ISO.containsKey(hash)) {
            return COORDINATES_HASH_TO_ISO.get(hash);
        }

        final String iso = COUNTRY_LOCATOR.getCountryForCoordinates(latitudeDegrees, longitudeDegrees);

        COORDINATES_HASH_TO_ISO.put(hash, iso);

        return iso;
    }
}
