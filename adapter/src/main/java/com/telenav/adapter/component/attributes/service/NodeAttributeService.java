package com.telenav.adapter.component.attributes.service;

import com.telenav.adapter.component.attributes.helper.EntityIdentifierHelper;
import com.telenav.map.entity.Node;

import java.util.Set;

import static com.telenav.adapter.component.attributes.helper.EntityIdentifierHelper.DEFAULT_IDENTIFIER_HELPER;

/**
 * The current service matches the enhancement data to nodes.
 *
 * @author liviuc
 */
public class NodeAttributeService extends AttributesService<Node, String> {

    /**
     * @param configSpecifiedTagKeys describes the tag keys / detection types that are taken in consideration for node enhancement
     *                               (see {@link AttributesService}).
     * @param broadcastJoin          describes whether to perform a broadcast join or not.
     */
    public NodeAttributeService(final Set<String> configSpecifiedTagKeys,
                                final boolean broadcastJoin) {
        super(configSpecifiedTagKeys, broadcastJoin, null);
    }

    @Override
    protected EntityIdentifierHelper<Node, String> identifierService() {
        return DEFAULT_IDENTIFIER_HELPER;
    }
}
