package com.telenav.adapter.component.fc;

import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * @author liviuc
 */
public class TagService {

    /*
        FC
     */
    public static final String FC_TAG = "functional_class";
    public static final String NETWORK_TAG = "network";
    public static final String NAME_TAG = "name";
    public static final String TN_ISO_TAG = "tn__iso";
    public static final String ISO_TAG = "iso";
    public static final String TYPE_TAG = "type";
    public static final String ROUTE_TAG = "route";
    public static final String REF_TAG = "ref";
    public static final String HIGHWAY_TAG = "highway";
    public static final String ONEWAY_TAG = "oneway";
    public static final String MOTORWAY_JUNCTION_VALUE = "motorway_junction";
    public static final String LINK_SUFFIX_VALUE = "_link";
    public static final String TIGER_CFCC = "tiger:cfcc";
    public static final String TIGER_NAME_BASE = "tiger:name_base";
    /*
        lanes
     */
    public static final String LANES = "lanes";
    public static final String DIVIDER_LANES = "divider:lanes";
    public static final String TURN_LANES = "turn:lanes";
    public static final String TYPE_LANES = "type:lanes";
    public static final String LANES_FORWARD = "lanes:forward";
    public static final String LANES_BACKWARD = "lanes:backward";
    public static final String LANE_CAT = "lane_cat";

    public static String network(final Map<String, String> tags) {
        return tags.get(NETWORK_TAG);
    }

    public static String name(final Map<String, String> tags) {
        return tags.get(NAME_TAG);
    }

    public static String iso(final Map<String, String> tags) {
        final String iso = tags.get(ISO_TAG);
        return (StringUtils.isEmpty(iso)) ? tags.get(TN_ISO_TAG) : iso;
    }

    public static String ref(final Map<String, String> tags) {
        return tags.get(REF_TAG);
    }

    public static String oneway(final Map<String, String> tags) {
        return tags.get(ONEWAY_TAG);
    }

    public static void addIfNonEmpty(final Map<String, String> tags,
                              final String key,
                              final String value) {
        if (!StringUtils.isEmpty(value)) {
            tags.put(key, value);
        }
    }
}
