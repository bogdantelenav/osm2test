package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmRelationTagKey;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.adapter.bean.RoadSubType;

import java.io.Serializable;
import java.util.*;

import static com.telenav.datapipelinecore.osm.OsmHighwayValue.*;


/**
 * Extracts the road sub type from the way section tags.
 *
 * @author petrum
 */
public class RoadSubTypeExtractor implements Serializable, Extractor<RoadSubType> {

    private static final Map<OsmHighwayValue, RoadSubType> roadSubTypeForHighway = new HashMap<>();
    private static final Map<OsmHighwayValue, RoadSubType> roadSubTypeForDoubleDigitizedHighway = new HashMap<>();

    static {
        roadSubTypeForHighway.put(MOTORWAY, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(MOTORWAY_LINK, RoadSubType.RAMP);
        roadSubTypeForHighway.put(TRUNK, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(TRUNK_LINK, RoadSubType.CONNECTING_ROAD);
        roadSubTypeForHighway.put(PRIMARY_LINK, RoadSubType.CONNECTING_ROAD);
        roadSubTypeForHighway.put(SECONDARY_LINK, RoadSubType.CONNECTING_ROAD);
        roadSubTypeForHighway.put(RESIDENTIAL_LINK, RoadSubType.CONNECTING_ROAD);
        roadSubTypeForHighway.put(TERTIARY_LINK, RoadSubType.CONNECTING_ROAD);
        roadSubTypeForHighway.put(PRIMARY, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(SECONDARY, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(RESIDENTIAL, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(TERTIARY, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(UNCLASSIFIED, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(SERVICE, RoadSubType.SERVICE);
        roadSubTypeForHighway.put(ROAD, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(TRACK, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(UNDEFINED, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(UNKNOWN, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(LIVING_STREET, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(PRIVATE, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(FOOTWAY, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(PEDESTRIAN, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(STEPS, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(BRIDLEWAY, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(CONSTRUCTION, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(PATH, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(CYCLEWAY, RoadSubType.DEFAULT);
        roadSubTypeForHighway.put(BUS_GUIDEWAY, RoadSubType.DEFAULT);

        roadSubTypeForDoubleDigitizedHighway.put(MOTORWAY, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(MOTORWAY_LINK, RoadSubType.RAMP);
        roadSubTypeForDoubleDigitizedHighway.put(TRUNK, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(TRUNK_LINK, RoadSubType.CONNECTING_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(PRIMARY_LINK, RoadSubType.CONNECTING_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(SECONDARY_LINK, RoadSubType.CONNECTING_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(RESIDENTIAL_LINK, RoadSubType.CONNECTING_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(TERTIARY_LINK, RoadSubType.CONNECTING_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(PRIMARY, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(SECONDARY, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(RESIDENTIAL, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(TERTIARY, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(UNCLASSIFIED, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(SERVICE, RoadSubType.SERVICE);
        roadSubTypeForDoubleDigitizedHighway.put(ROAD, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(TRACK, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(UNDEFINED, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(UNKNOWN, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(LIVING_STREET, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(PRIVATE, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(FOOTWAY, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(PEDESTRIAN, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(STEPS, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(BRIDLEWAY, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(CONSTRUCTION, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(PATH, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(CYCLEWAY, RoadSubType.MAIN_ROAD);
        roadSubTypeForDoubleDigitizedHighway.put(BUS_GUIDEWAY, RoadSubType.MAIN_ROAD);
    }

    /**
     * Extracts the road sub type from the way section tags. Ferry, tunnel, oneways
     * and bridges are special cases.
     *
     * @param tags the way section tags as a map
     * @return the Optionlal of RST deduced by applying a set of rules on the tags. Empty optional if no rules apply
     */
    public Optional<RoadSubType> extract(final Map<String, String> tags) {
        final List<RoadSubType> subTypes = new ArrayList<>();
        final boolean isDoubleDigitized = isDoubleDigitized(tags);
        if (FERRY_VALUE.equals(tags.get(OsmRelationTagKey.ROUTE.unwrap()))) {
            return Optional.of(isDoubleDigitized ? RoadSubType.MAIN_ROAD : RoadSubType.DEFAULT);
        }
        if (SHUTTLE_TRAIN_VALUE.equals(tags.get(OsmRelationTagKey.ROUTE.unwrap()))) {
            return Optional.of(isDoubleDigitized ? RoadSubType.MAIN_ROAD : RoadSubType.DEFAULT);
        }
        if (YES_VALUE.equals(tags.get(TnTagKey.TN_CONNECTION_ROAD.unwrap()))) {
            subTypes.add(RoadSubType.CONNECTING_ROAD);
        }
        if (YES_VALUE.equals(tags.get(TnTagKey.TN_INTERSECTION_LINK.unwrap()))) {
            subTypes.add(RoadSubType.INTERSECTION_LINK);
        }
        if (YES_VALUE.equals(tags.get(TnTagKey.TN_RAMP.unwrap()))) {
            subTypes.add(RoadSubType.RAMP);
        }
        if (YES_VALUE.equals(tags.get(TnTagKey.TN_DUAL_WAY.unwrap()))) {
            subTypes.add(RoadSubType.MAIN_ROAD);
        }

        final String highway = tags.get(OsmTagKey.HIGHWAY.unwrap());
        if (highway != null) {
            if (YES_VALUE.equals(tags.get(OsmTagKey.TUNNEL.unwrap()))) {
                subTypes.add(RoadSubType.TUNNEL);
            }
            if (YES_VALUE.equals(tags.get(OsmTagKey.BRIDGE.unwrap()))) {
                subTypes.add(RoadSubType.BRIDGE);

            }
            if (ROUNDABOUT_VALUE.equals(tags.get(OsmTagKey.JUNCTION.unwrap()))) {
                subTypes.add(RoadSubType.ROUNDABOUT);
            }

            if (MOTORWAY.unwrap().equals(highway) ||
                    OsmHighwayValue.TRUNK.unwrap().equals(highway)) {
                final String onewayValue = tags.get(OsmTagKey.ONEWAY.unwrap());
                if (YES_VALUE.equals(onewayValue) || "-1".equals(onewayValue) || "1".equals(onewayValue)) {
                    subTypes.add(isDoubleDigitized ? RoadSubType.MAIN_ROAD : RoadSubType.DEFAULT);
                }
            }
            OsmHighwayValue.optionalValueOf(highway)
                    .ifPresent(osmHighwayValues1 -> subTypes.add(
                            isDoubleDigitized ? roadSubTypeForDoubleDigitizedHighway.get(osmHighwayValues1) :
                                    roadSubTypeForHighway.get(osmHighwayValues1)
                    ));

        }
        if (subTypes.isEmpty()) {
            return Optional.of(RoadSubType.DEFAULT);
        }

        return Optional.of(highestPriority(subTypes));
    }

    private boolean isDoubleDigitized(final Map<String, String> tags) {
        return tags.containsKey("rst") &&
                tags.get("rst").equals("2");
    }

    RoadSubType highestPriority(final List<RoadSubType> list) {
        if (list.isEmpty()) {
            throw new IllegalArgumentException("List cannot be empty");
        }
        return list
                .stream()
                .max(Comparator.comparing(RoadSubType::priorityFor))
                .get();
    }
}
