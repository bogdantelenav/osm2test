package com.telenav.adapter.component.repository;

import com.opencsv.CSVReader;
import com.telenav.adapter.bean.DrivingSide;
import com.telenav.adapter.entity.*;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.telenav.adapter.component.fc.FcInfo.ROUTES_ISO_WHITELIST;
import static com.telenav.adapter.component.fc.FcInfo.ROUTES_RELATIONS_ISO_WHITELIST;

/**
 * @author liviuc
 */
public class ResourceRepository {

    public static Map<String, Map<String, String>> getRoutesRelationsMapping(final List<String> isos) throws IOException {

        final Map<String, Map<String, String>> fcRoutesByCountry = new HashMap<>();

        for (final String country : whitelist(isos, ROUTES_RELATIONS_ISO_WHITELIST)) {

            final CSVReader reader = getCsvReader("/routes/relations/" + country, ';');

            final List<String[]> strings = reader.readAll();

            final Map<String, String> routesByFc = new HashMap<>();
            for (String[] string : strings) {

                final String fc = string[0];
                final String routes = string[1];
                routesByFc.put(fc, routes);
            }

            fcRoutesByCountry.put(country, routesByFc);

        }
        return fcRoutesByCountry;
    }

    public static Map<String, Map<String, String>> getRoutesMapping() throws IOException {

        final Map<String, Map<String, String>> fcRoutesByCountry = new HashMap<>();

        for (final String country : ROUTES_ISO_WHITELIST) {

            final CSVReader reader = getCsvReader("/routes/ways/" + country, ';');

            final List<String[]> strings = reader.readAll();

            final Map<String, String> routesByFc = new HashMap<>();
            for (String[] string : strings) {

                final String fc = string[0];
                final String routes = string[1];
                routesByFc.put(fc, routes);
            }

            fcRoutesByCountry.put(country, routesByFc);

        }
        return fcRoutesByCountry;
    }

    public static List<String> getFcIsos() throws IOException {
        return new CustomCsvReader<String>()
                .read(getCsvReader("/fc-iso3s.csv", ' '),
                        new FcIsoCsvRecordMappingFunc());
    }

    public static List<LangCodeBag> getLangCode() throws IOException {
        return new CustomCsvReader<LangCodeBag>()
                .read(getCsvReader("/country-code-to-lang-code.csv"),
                        new LangCodeBagCsvRecordMappingFunc());
    }

    public static List<AdministrativeOrder> getAdministrativeOrders() throws IOException {
        return new CustomCsvReader<AdministrativeOrder>()
                .read(getCsvReader("/administrative-order.csv"),
                        new AdministrativeOrdersCsvRecordMappingFunc());
    }

    public static List<AdministrativeLevelMapping> getAdministrativeLevelsMapping() throws IOException {
        return new CustomCsvReader<AdministrativeLevelMapping>()
                .read(getCsvReader("/administrative-levels-mapping.csv"),
                        new AdministrativeLevelMappingsCsvRecordMappingFunc());
    }

    public static List<Iso> getIsos() throws IOException {
        return new CustomCsvReader<Iso>()
                .read(getCsvReader("/countries-codes-and-coordinates.csv"),
                        new IsoCsvRecordMappingFunc());
    }

    public static List<CountryDrivingSide> getDrivingSide() throws IOException {
        return new CustomCsvReader()
                .read(getCsvReader("/countries-codes-to-driving-side.csv"),
                        new CountryDrivingSideCsvRecordMappingFunc());
    }

    public static Map<String, String> getCountryNameToIso2() throws IOException {
        final CSVReader reader = getCsvReader("/countries-codes-and-coordinates.csv");
        return readCsvToMap(reader);
    }

    private static Map<String, String> readCsvToMap(final CSVReader reader) throws IOException {
        reader.skip(1);
        final Map<String, String> map = new HashMap<>();
        reader.readAll().forEach(entry -> map.put(entry[0], entry[1]));
        return map;
    }

    public static Map<String, String> getSpeedLimitRange() throws IOException {
        final CSVReader reader = getCsvReader("/speed_limit_range.csv");
        return readCsvToMap(reader);
    }

    public static Map<String,String> getSpeedLimitsEU() throws IOException{
        final CSVReader reader = getCsvReader("/speed_limits_EU.csv");
        return readCsvToMap(reader);
    }

    private static CSVReader getCsvReader(final String resourceName, final char separator) throws IOException {
        return new CSVReader(new InputStreamReader(getResourceStream(resourceName)), separator);
    }

    private static CSVReader getCsvReader(final String resourceName) throws IOException {
        return new CSVReader(new InputStreamReader(getResourceStream(resourceName)));
    }

    private static InputStream getResourceStream(final String resourceName) throws IOException {
        final InputStream resourceStream = resourceStream(resourceName);
        return resourceStream;
    }

    public static String readText(final String resourceName) throws IOException {
        final InputStream resourceStream = resourceStream(resourceName);
        return IOUtils.toString(resourceStream, StandardCharsets.UTF_8);
    }

    private static InputStream resourceStream(final String resourceName) throws IOException {
        final InputStream resourceStream = ResourceRepository.class.getResourceAsStream(resourceName);
        if (resourceStream == null) {
            throwMissingResourceException(resourceName);
        }
        return resourceStream;
    }

    private static List<String> whitelist(final List<String> data,
                                          final List<String> whiteList) {
        return data.stream()
                .filter(val -> whiteList.contains(val))
                .collect(Collectors.toList());
    }

    private static void throwMissingResourceException(final String resourceName) throws IOException {
        throw new IOException("Missing resource: " + resourceName);
    }

    @FunctionalInterface
    private interface CsvRecordMappingFunc<T> extends Function<String[], T> {

    }

    private static class CustomCsvReader<T> {

        List<T> read(final CSVReader reader,
                     final CsvRecordMappingFunc<T> mappingFunc) throws IOException {
            reader.skip(1);
            return reader.readAll()
                    .stream()
                    .map(mappingFunc)
                    .collect(Collectors.toList());
        }
    }

    private static class CountryDrivingSideCsvRecordMappingFunc implements CsvRecordMappingFunc<CountryDrivingSide> {

        @Override
        public CountryDrivingSide apply(final String[] record) {
            return CountryDrivingSide.builder()
                    .countryName(record[0])
                    .drivingSide(DrivingSide.valueOf(record[1]))
                    .build();
        }
    }

    private static class IsoCsvRecordMappingFunc implements CsvRecordMappingFunc<Iso> {

        @Override
        public Iso apply(final String[] record) {
            return Iso.builder()
                    .iso2(record[1])
                    .iso3(record[2])
                    .build();
        }
    }

    private static class LangCodeBagCsvRecordMappingFunc implements CsvRecordMappingFunc<LangCodeBag> {

        @Override
        public LangCodeBag apply(final String[] record) {
            return LangCodeBag.builder()
                    .iso2(record[0])
                    .langCodes(Arrays.asList(record[3].split("-")))
                    .build();
        }
    }

    private static class FcIsoCsvRecordMappingFunc implements CsvRecordMappingFunc<String> {

        @Override
        public String apply(final String[] record) {
            return record[0];
        }
    }

    private static class AdministrativeOrdersCsvRecordMappingFunc implements CsvRecordMappingFunc<AdministrativeOrder> {

        @Override
        public AdministrativeOrder apply(final String[] record) {
            return AdministrativeOrder.builder()
                    .adminOrder(record[0])
                    .adminType(record[1])
                    .build();
        }
    }

    private static class AdministrativeLevelMappingsCsvRecordMappingFunc implements CsvRecordMappingFunc<AdministrativeLevelMapping> {

        @Override
        public AdministrativeLevelMapping apply(final String[] record) {
            return AdministrativeLevelMapping.builder()
                    .region(record[0])
                    .country(record[1])
                    .l1_Admin_Type(record[2])
                    .l2_Admin_Type(record[3])
                    .l3_Admin_Type(record[4])
                    .l4_Admin_Type(record[5])
                    .l5_Admin_Type(record[6])
                    .build();
        }
    }
}
