package com.telenav.adapter.component.extractors;

import com.telenav.adapter.bean.RoadType;
import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmRelationTagKey;
import com.telenav.datapipelinecore.osm.OsmTagKey;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

import static com.telenav.adapter.NgxWaySectionsAdapterApplication.CUSTOM_WAY_ID_TAG_KEY;
import static com.telenav.datapipelinecore.osm.OsmHighwayValue.*;


/**
 * Extracts the road type from the way section tags.
 *
 * @author petrum
 */
public class RoadTypeExtractor implements Serializable, Extractor<RoadType> {

    private static final Map<OsmHighwayValue, RoadType> roadTypeForHighway = new HashMap<>();
    private static final Map<OsmHighwayValue, RoadType> roadTypeForHighwayUrban = new HashMap<>();
    private static final String OSM_ADMIN_LEVEL_TAG_KEY = "admin_level";
    private static final String OSM_BORDER_TYPE_TAG_KEY = "border_type";
    private static final List<String> URBAN_BORDER_TYPES = Arrays.asList("city", "town", "município");
    private static final List<String> URBAN_ADMIN_LEVELS = Arrays.asList("7", "8", "9", "10", "11", "12");

    static {
        roadTypeForHighway.put(MOTORWAY, RoadType.FREEWAY);
        roadTypeForHighway.put(MOTORWAY_LINK, RoadType.FREEWAY);
        roadTypeForHighway.put(TRUNK, RoadType.HIGHWAY);
        roadTypeForHighway.put(TRUNK_LINK, RoadType.HIGHWAY);
        roadTypeForHighway.put(PRIMARY, RoadType.THROUGHWAY);
        roadTypeForHighway.put(PRIMARY_LINK, RoadType.THROUGHWAY);
        roadTypeForHighway.put(SECONDARY, RoadType.THROUGHWAY);
        roadTypeForHighway.put(SECONDARY_LINK, RoadType.THROUGHWAY);
        roadTypeForHighway.put(RESIDENTIAL, RoadType.LOCAL);
        roadTypeForHighway.put(RESIDENTIAL_LINK, RoadType.LOCAL);
        roadTypeForHighway.put(TERTIARY, RoadType.LOCAL);
        roadTypeForHighway.put(TERTIARY_LINK, RoadType.LOCAL);
        roadTypeForHighway.put(UNCLASSIFIED, RoadType.LOCAL);
        roadTypeForHighway.put(SERVICE, RoadType.VERY_LOW_SPEED);
        roadTypeForHighway.put(ROAD, RoadType.VERY_LOW_SPEED);
        roadTypeForHighway.put(TRACK, RoadType.VERY_LOW_SPEED);
        roadTypeForHighway.put(UNDEFINED, RoadType.VERY_LOW_SPEED);
        roadTypeForHighway.put(UNKNOWN, RoadType.VERY_LOW_SPEED);
        roadTypeForHighway.put(LIVING_STREET, RoadType.PRIVATE_ROAD);
        roadTypeForHighway.put(PRIVATE, RoadType.PRIVATE_ROAD);
        roadTypeForHighway.put(FOOTWAY, RoadType.WALKWAY);
        roadTypeForHighway.put(PEDESTRIAN, RoadType.WALKWAY);
        roadTypeForHighway.put(STEPS, RoadType.WALKWAY);
        roadTypeForHighway.put(BRIDLEWAY, RoadType.NON_NAVIGABLE);
        roadTypeForHighway.put(CYCLEWAY, RoadType.NON_NAVIGABLE);
        roadTypeForHighway.put(CONSTRUCTION, RoadType.NON_NAVIGABLE);
        roadTypeForHighway.put(PATH, RoadType.WALKWAY);
        roadTypeForHighway.put(BUS_GUIDEWAY, RoadType.PUBLIC_VEHICLE_ONLY);

        roadTypeForHighwayUrban.put(MOTORWAY, RoadType.FREEWAY_URBAN);
        roadTypeForHighwayUrban.put(MOTORWAY_LINK, RoadType.FREEWAY_URBAN);
    }

    private final String MPH = "mph";
    private final int KM_30 = 30;
    private final int KM_50 = 50;
    private final int KM_90 = 90;
    private Map<Long, String> broadcastedWayIdToAdminLevel;

    /**
     * Extracts the road type from the way section tags. Ferries or service roads are
     * special cases.
     *
     * @param tags the way section tags as a map
     * @return Optional of RT deduced by applying a set of rules on the tags or empty optional in case no rules can
     * be applied
     */
    public Optional<RoadType> extract(final Map<String, String> tags) {
        if (FERRY_VALUE.equals(tags.get(OsmRelationTagKey.ROUTE.unwrap()))) {
            return Optional.of(RoadType.FERRY);
        }

        final Optional<String> routeValueOptional =
                tags.containsKey(OsmRelationTagKey.ROUTE.unwrap()) ?
                        Optional.of(tags.get(OsmRelationTagKey.ROUTE.unwrap())) :
                        Optional.empty();

        if (routeValueOptional.isPresent() && SHUTTLE_TRAIN_VALUE.equals(routeValueOptional.get())) {
            final Optional<String> motorcarValueOptional =
                    tags.containsKey(OsmTagKey.MOTORCAR.unwrap()) ?
                            Optional.of(tags.get(OsmTagKey.MOTORCAR.unwrap())) :
                            Optional.empty();
            final Optional<String> motorcycleValueOptional =
                    tags.containsKey(OsmTagKey.MOTORCYCLE.unwrap()) ?
                            Optional.of(tags.get(OsmTagKey.MOTORCYCLE.unwrap())) :
                            Optional.empty();

            if (motorcarValueOptional.isPresent() && motorcycleValueOptional.isPresent() &&
                    motorcarValueOptional.get().equals(YES_VALUE) && motorcycleValueOptional.get().equals(YES_VALUE)) {
                return Optional.of(RoadType.RAIL_FERRY);
            }
        }

        final List<RoadType> roadTypes = new ArrayList<>();
        final String highwayValue = tags.get(OsmTagKey.HIGHWAY.unwrap());

        if (highwayValue != null) {
            if (PRIVATE_VALUE.equals(tags.get(OsmTagKey.ACCESS.unwrap()))) {
                roadTypes.add(RoadType.PRIVATE_ROAD);
            }

            if (NO_VALUE.equals(tags.get(OsmTagKey.ACCESS.unwrap()))) {
                roadTypes.add(RoadType.NON_NAVIGABLE);
            }

            final String maxspeed = tags.get(OsmTagKey.MAXSPEED.unwrap());
            if (maxspeed != null && !maxspeed.isEmpty()) {
                final int speed = detectSpeed(maxspeed);
                //if (isLocalRoad(highwayValue))
                {
                    if (speed < KM_30) {
                        roadTypes.add(RoadType.VERY_LOW_SPEED);
                    }
                    if (speed >= KM_30 && speed < KM_50) {
                        roadTypes.add(RoadType.LOCAL);
                    }
                }
                //if (isThroughWayRoad(highwayValue))
                {
                    if (speed >= KM_50 && speed < KM_90) {
                        roadTypes.add(RoadType.THROUGHWAY);
                    }
                }
                //if (isNationalRoad(highwayValue))
                {
                    if (speed >= KM_90) {
                        roadTypes.add(RoadType.HIGHWAY);
                    }
                }
            }

            OsmHighwayValue.optionalValueOf(highwayValue)
                .ifPresent(osmHighwayValues -> {
                    if ((MOTORWAY == osmHighwayValues ||
                            MOTORWAY_LINK == osmHighwayValues) &&
                            isUrban(tags)) {
                        roadTypes.add(roadTypeForHighwayUrban.get(osmHighwayValues));
                    } else {
                        roadTypes.add(roadTypeForHighway.get(osmHighwayValues));
                    }
                });
        }

        if (roadTypes.isEmpty()) {
            return Optional.of(RoadType.NON_NAVIGABLE);
        }

        return Optional.of(highestPriority(roadTypes));
    }

    private boolean isUrban(final Map<String, String> tags) {

        return tags.containsKey(OSM_BORDER_TYPE_TAG_KEY) &&
                URBAN_BORDER_TYPES.contains(tags.get(OSM_BORDER_TYPE_TAG_KEY)) ||
                tags.containsKey(OSM_ADMIN_LEVEL_TAG_KEY) &&
                        isUrbanAdminLevel(tags.get(OSM_ADMIN_LEVEL_TAG_KEY)) ||
                hasUrbanAdminLevelFromRelation(tags);
    }

    private boolean hasUrbanAdminLevelFromRelation(final Map<String, String> tags) {
        if (tags.containsKey(CUSTOM_WAY_ID_TAG_KEY)) {
            final long wayId = Long.parseLong(tags.get(CUSTOM_WAY_ID_TAG_KEY));
            if (broadcastedWayIdToAdminLevel != null) {
                final String adminLevel = broadcastedWayIdToAdminLevel.get(wayId);
                return isUrbanAdminLevel(adminLevel);
            }
        }
        return false;
    }

    private boolean isUrbanAdminLevel(final String adminLevel) {
        if (adminLevel == null) {
            return false;
        }
        return URBAN_ADMIN_LEVELS.contains(adminLevel);
    }

    private RoadType highestPriority(final List<RoadType> list) {
        if (list.isEmpty()) {
            throw new IllegalArgumentException("List cannot be empty");
        }
        Optional<RoadType> highestPriorityRt = list.stream().max(Comparator.comparing(RoadType::priorityFor));
        return highestPriorityRt.get();
    }

    private boolean isNumeric(final String input) {
        try {
            Integer.parseInt(input);
            return true;
        } catch (final NumberFormatException e) {
            return false;
        }
    }

    public int detectSpeed(final String maxspeed) {
        if (maxspeed == null || maxspeed.isEmpty()) {
            throw new IllegalArgumentException("Speed value cannot be null or empty");
        }
        int speed = 0;
        if (isNumeric(maxspeed)) {
            speed = Integer.parseInt(maxspeed);
        } else if (maxspeed.contains(MPH)) {
            final String[] parts = maxspeed.split(" ");
            if (isNumeric(parts[0])) {
                speed = ((Double) (Integer.parseInt(parts[0]) * 1.609344)).intValue();
            }
        }
        return speed;
    }

    private boolean isNationalRoad(final String highwayValue) {
        return isRoadType(RoadType.HIGHWAY, highwayValue);
    }

    private boolean isThroughWayRoad(final String highwayValue) {
        return isRoadType(RoadType.THROUGHWAY, highwayValue);
    }

    public boolean isLocalRoad(final String highwayValue) {
        return isRoadType(RoadType.LOCAL, highwayValue);
    }

    private boolean isRoadType(final RoadType roadType,
                               final String highwayValue) {
        if (highwayValue == null) {
            throw new IllegalArgumentException("Highway type value cannot be null");
        }
        final List<String> localRoads = roadTypeForHighway.entrySet().stream().
                filter(entry -> entry.getValue().equals(roadType)).
                map(entry -> entry.getKey().unwrap()).
                collect(Collectors.toList());
        return localRoads.contains(highwayValue);
    }

    public void setBroadcastedWayIdToAdminLevel(final Map<Long, String> broadcastedWayIdToAdminLevel) {
        this.broadcastedWayIdToAdminLevel = broadcastedWayIdToAdminLevel;
    }
}
