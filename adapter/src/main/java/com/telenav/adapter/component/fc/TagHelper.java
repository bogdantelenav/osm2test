package com.telenav.adapter.component.fc;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author liviuc.
 */
public class TagHelper {

    public static String[] split(final String tag) {
        return tag.split(";");
    }

    public static String fcFromTag(final String tag,
                                   final List<String> fcPriorityByCountry,
                                   final Map<String, List<String>> routesByFcs) {

        if (routesByFcs != null) {

            if (!StringUtils.isEmpty(tag)) {

                final String[] routesFromTag = split(tag);

                for (String fc : fcPriorityByCountry) {

                    final List<String> routesRegexp = routesByFcs.get(fc);

                    for (final String route : routesFromTag) {

                        final String trimedRoute = route.trim();

                        for (final String routeRegexp : routesRegexp) {

                            if (Pattern.matches(routeRegexp, trimedRoute)) {

                                return fc;
                            }
                        }
                    }
                }
            }
        }

        return null;
    }
}
