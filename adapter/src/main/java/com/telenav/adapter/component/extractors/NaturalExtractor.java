package com.telenav.adapter.component.extractors;

import com.telenav.datapipelinecore.osm.OsmTagKey;

/**
 * Extracts the 'natural' way tag.
 *
 * @author volodymyrl
 */
public class NaturalExtractor extends SingleTagExtractor {

    public NaturalExtractor() {
        super(OsmTagKey.NATURAL.unwrap());
    }
}
