package com.telenav.adapter.component.extractors;

import com.telenav.adapter.component.fc.TagService;
import com.telenav.datapipelinecore.osm.OsmTagKey;

import java.util.Map;
import java.util.Optional;


/**
 * Extract turn lane tag value.
 *
 * @author adrianal
 */
public class TurnLaneExtractor implements Extractor<String> {

    public static final String TURN_LANES = TagService.TURN_LANES;

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        //LOGGER.info("Extracting " + TURN_LANES + " tag's value!");
        if (tags.containsKey(OsmTagKey.TURN_LANES.unwrap())) {
            return Optional.of(tags.get(OsmTagKey.TURN_LANES.unwrap()));
        }
        return Optional.empty();
    }

    public Optional<String> extract(final Map<String, String> tags, final String key) {
        //LOGGER.info("Extracting '" + key + "' tag's value!");
        if (tags.containsKey(key)) {
            return Optional.of(tags.get(key));
        }
        return Optional.empty();
    }
}