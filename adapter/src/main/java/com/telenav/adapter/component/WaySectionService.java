package com.telenav.adapter.component;

import com.telenav.adapter.component.fc.TagService;
import com.telenav.adapter.component.repository.ResourceRepository;
import com.telenav.map.entity.WaySection;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @author liviuc .
 */
@Slf4j
public class WaySectionService {

    public static List<String> distinctIsos(final JavaRDD<WaySection> sectionedWays) {

        return sectionedWays.map(waySection -> TagService.iso(waySection.tags()))
                .distinct()
                .filter(Objects::nonNull)
                .collect();
    }

    public static Map<String, Map<String, String>> routeRelationsMappingBroadcast(final JavaRDD<WaySection> sectionedWays,
                                                                                  final JavaSparkContext javaSparkContext) throws IOException {

        final List<String> isos = WaySectionService.distinctIsos(sectionedWays);

        final Map<String, Map<String, String>> routesRelationsMapping =
                ResourceRepository.getRoutesRelationsMapping(isos);

        return javaSparkContext.broadcast(routesRelationsMapping).getValue();
    }
}
