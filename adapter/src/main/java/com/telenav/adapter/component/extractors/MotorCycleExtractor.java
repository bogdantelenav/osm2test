package com.telenav.adapter.component.extractors;

import java.util.Map;
import java.util.Optional;

import static com.telenav.datapipelinecore.osm.OsmTagKey.MOTORCYCLE;

/**
 * Extracts the 'motorcycle' way tag.
 *
 * @author petrum
 */
public class MotorCycleExtractor implements Extractor<String>{

    @Override
    public Optional<String> extract(final Map<String, String> tags) {
        if (hasMotorCarTag(tags)) {
            return Optional.of(tags.get(MOTORCYCLE.unwrap()));
        }
        return Optional.empty();
    }

    private boolean hasMotorCarTag(final Map<String, String> tags) {
        return tags.containsKey(MOTORCYCLE.unwrap());
    }
}
