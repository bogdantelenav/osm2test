package com.telenav.adapter.component.attributes.helper;

import com.telenav.adapter.component.attributes.enhancement.Enhancement;
import com.telenav.adapter.component.attributes.enumeration.Detection;
import com.telenav.map.entity.StandardEntity;
import com.telenav.map.entity.TagAdjuster;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.telenav.adapter.component.fc.TagService.ONEWAY_TAG;

/**
 * This component enhances an entity by adding tags from it's corresponding {@link Enhancement}s.
 *
 * @author liviuc
 */
public class EntityEnhancerHelper<E extends StandardEntity & TagAdjuster<E>, Identifier> implements Serializable {

    private static final LanesEntityEnhancerHelper lanesHelper = new LanesEntityEnhancerHelper();

    /**
     * Adds tags representing enhancing data to the entity.
     *
     * @param e            entity.
     * @param enhancements enhancement dat corresponding to the entity.
     * @return enhanced entity.
     */
    public E enhance(final E e,
                     final List<Enhancement<Identifier>> enhancements) {
        if (enhancements == null || enhancements.isEmpty()) {
            return e;
        }

        final List<Enhancement<Identifier>> laneEnhancements = new ArrayList<>();

        final Map<String, String> newTags = new HashMap<>();
        for (final Enhancement enhancement : enhancements) {

            if (enhancement.getDetection() == Detection.LANES) {
                laneEnhancements.add(enhancement);
                continue;
            }

            final Map<String, String> enhancementTags = enhancement.getEnhancementTags();

            for (final Map.Entry<String, String> enhancementTag : enhancementTags.entrySet()) {
                newTags.put(enhancementTag.getKey(), enhancementTag.getValue());
            }
        }

        final boolean oneway = "yes".equalsIgnoreCase(e.tags().get(ONEWAY_TAG)) || "-1".equalsIgnoreCase(e.tags().get(ONEWAY_TAG));

        newTags.putAll(lanesHelper.fromLaneEnhancements(laneEnhancements, oneway));
        return e.withTags(newTags);
    }
}
