package com.telenav.adapter.component.fc;

import com.telenav.map.entity.WaySection;
import org.apache.commons.collections.CollectionUtils;

import java.util.*;

import static com.telenav.adapter.component.fc.CanadaRelationNetworkRefApproach.NO_FC_FLAG_VALUE;

/**
 * @author liviuc .
 */
public class RelationNetworkBased extends FcApproach {

    private final Map<Long, List<String>> wayIdsToIsoFc;
    private List<String> allowedFcs = Arrays.asList("1", "2", "3", "4", "5");

    public RelationNetworkBased(final List<String> countries,
                                final Map<Long, List<String>> wayIdsToIsoFc) {
        super(countries);
        this.wayIdsToIsoFc = wayIdsToIsoFc;
    }

    @Override
    public String fc(final WaySection waySection) {

        final Map<String, String> tags = waySection.tags();

        // Get fc related data for way section
        final List<String> isoFcs = wayIdsToIsoFc.get(waySection.strictIdentifier().wayId());

        boolean addedFc = false;

        // Set default fc.
        String previousFc = NO_FC_FLAG_VALUE;

        // Get fc from data by way section.
        String result = null;

        if (!CollectionUtils.isEmpty(isoFcs)) {

            final Optional<String> min = isoFcs.stream()
                    .filter(val -> allowedFcs.contains(val))
                    .filter(Objects::nonNull)
                    .min(Comparator.comparing(String::toString));

            if (min.isPresent()) {

                result = min.get();

                previousFc = result;

                addedFc = true;
            }
        }

        // Assign fc depending on the previous data availability.
        final String prevResult = (result != null) ? result : previousFc;

        final String iso = TagService.iso(tags);

        // For some country particular checks might be needed in order to correctly identify the fcs.
        switch (iso) {

            case "CAN": {

                if (tags.containsKey("ref")) {

                    result = CanadaRelationNetworkRefApproach.fc(waySection, isoFcs, prevResult);

                    if (!NO_FC_FLAG_VALUE.equals(result)) {

                        addedFc = true;

                    } else {

                        result = prevResult;

                    }
                }

                if (!addedFc) {

                    result = FcService.downgradeFc();

                }

            }
        }

        return result;
    }
}
