package com.telenav.adapter.component.attributes.enumeration;

import com.google.common.collect.Sets;
import com.telenav.adapter.component.fc.TagService;

import java.util.Collections;
import java.util.Set;

import static com.telenav.adapter.component.fc.TagService.*;

/**
 * @author liviuc
 */
public enum Detection {

    ATTRIBUTE(Collections.EMPTY_SET,
            Collections.EMPTY_SET),
    TURN_RESTRICTION(Sets.newHashSet("no_u_turn", "no_left_turn", "no_right_turn", "no_through", "no_straight_on"),
            Collections.EMPTY_SET),
    LANES(Sets.newHashSet(TagService.LANES),
            Sets.newHashSet(DIVIDER_LANES, TURN_LANES, TYPE_LANES, LANES_FORWARD, LANES_BACKWARD, LANE_CAT, TagService.LANES)),
    SPEED_LIMIT(Sets.newHashSet("speed_limit"),
            Sets.newHashSet("maxspeed:forward", "maxspeed:backward")),
    ONEWAY(Sets.newHashSet("oneway", "ONE_WAY_STRAIGHT_AU,", "ONE_WAY_RIGHT_AU", "ONE_WAY_LEFT_AU"),
            Sets.newHashSet("oneway"));

    private final Set<String> detectionTypes;
    private final Set<String> tagsToRemove;

    Detection(final Set<String> values,
              final Set<String> tagsToRemove) {
        this.detectionTypes = values;
        this.tagsToRemove = tagsToRemove;
    }

    public static Detection detection(final String detectionType) {
        for (final Detection detection : Detection.values()) {
            if (detection.contains(detectionType)) {
                return detection;
            }
        }
        return Detection.ATTRIBUTE;
    }

    private boolean contains(final String detectionType) {
        return detectionTypes.contains(detectionType);
    }

    public Set<String> tagsToRemove() {
        return tagsToRemove;
    }
}
