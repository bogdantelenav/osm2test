package com.telenav.adapter.rules;

import com.telenav.adapter.bean.SpeedCategory;
import com.telenav.adapter.component.extractors.MotorCarExtractor;
import com.telenav.adapter.component.extractors.MotorCycleExtractor;
import com.telenav.adapter.component.extractors.RouteExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.map.entity.TagAdjuster;

import java.util.Map;
import java.util.Optional;

/**
 * A rule that adds "rail_ferry=yes" and "rt=11" when "route=shuttle_train", "motorcar=yes" and
 * "motorcycle=yes" exists, and delete "route=shuttle_train".
 *
 * There already exists a rule that adds "rt=11" for way-sections. But it's necessary to add that
 * tag in ways too. So, even though way-sections already have "rt=11", it's necessary for ways.
 *
 * @author petrum
 */
public class RailFerryRule<T extends TagAdjuster<T>> extends Enhancer<T> {
    private final RouteExtractor routeExtractor;
    private final MotorCarExtractor motorCarExtractor;
    private final MotorCycleExtractor motorCycleExtractor;

    public RailFerryRule(final RouteExtractor routeExtractor, final MotorCarExtractor motorCarExtractor,
                         final MotorCycleExtractor motorCycleExtractor) {
        this.routeExtractor = routeExtractor;
        this.motorCarExtractor = motorCarExtractor;
        this.motorCycleExtractor = motorCycleExtractor;
    }

    @Override
    public T apply(final T entity) {
        Map<String, String> tags = entity.tags();
        final Optional<String> routeTag = routeExtractor.extract(tags);
        final Optional<String> motorCarTag = motorCarExtractor.extract(tags);
        final Optional<String> motorCycleTag = motorCycleExtractor.extract(tags);

        if(isRailFerry(tags, routeTag, motorCarTag, motorCycleTag)){
            return entity.withoutTag(OsmTagKey.ROUTE.unwrap())
                    .withTag(UnidbTagKey.RAIL_FERRY.unwrap(), "yes");
        }

        return entity;
    }

    private boolean isRailFerry(final Map<String, String> tags, final Optional<String> routeTag,
                                final Optional<String> motorCarTag, final Optional<String> motorCycleTag){
        return routeTag.isPresent() && motorCarTag.isPresent() && motorCycleTag.isPresent() &&
                tags.get(OsmTagKey.ROUTE.unwrap()).equals("shuttle_train") &&
                tags.get(OsmTagKey.MOTORCAR.unwrap()).equals("yes") &&
                tags.get(OsmTagKey.MOTORCYCLE.unwrap()).equals("yes");
    }
}