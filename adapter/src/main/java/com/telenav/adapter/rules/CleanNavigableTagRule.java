package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.RawWay;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

/**
 *
 * @author odrab
 */
public class CleanNavigableTagRule extends Enhancer<RawWay> {

    private static final HashSet<String> NAVIGABLE_TAGS_TO_CLEAN =
            new HashSet<>(Arrays.asList(OsmTagKey.MINSPEED.unwrap(), OsmTagKey.MAXSPEED.unwrap(),
                    OsmTagKey.LANES.unwrap(), OsmTagKey.HOV.unwrap()
            ));

    // remove all tags which keys include navigable keywords
    @Override
    public RawWay apply(final RawWay way) {
        Map<String, String> tags = way.tags();
        HashMap<String, String> newTags = new HashMap<>(tags);
        tags.keySet().iterator().forEachRemaining(key ->
                NAVIGABLE_TAGS_TO_CLEAN.forEach(navigableTag -> {
                            if (key.contains(navigableTag)) {
                                newTags.remove(key);
                            }
                        }
                )
        );
        return new RawWay(way.id(), way.nodeIds(), newTags);
    }
}