package com.telenav.adapter.rules.whitelist;

import java.io.Serializable;
import java.util.List;


/**
 * @author adrianal
 */
public interface WhiteList extends Serializable {

    List<String> tags();
}