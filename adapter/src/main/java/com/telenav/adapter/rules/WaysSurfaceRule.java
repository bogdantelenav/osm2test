package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.SurfaceExtractor;
import com.telenav.adapter.component.mapper.OpenStreetMapToUniDbSurfaceMapper;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagValue;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagValue;
import com.telenav.map.entity.RawWay;
import com.telenav.map.entity.TagAdjuster;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Surface tag rule for unifying various values according to the UniDb specs.
 * @see SurfaceExtractor
 * @see OpenStreetMapToUniDbSurfaceMapper
 *
 * @param <T> The entity that will have the surface values unified.
 * @author Andrei Puf
 */
public class WaysSurfaceRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private final SurfaceExtractor surfaceExtractor;
    private final OpenStreetMapToUniDbSurfaceMapper openStreetMapToUniDbSurfaceMapper;

    /**
     * Initializes the WaysSurfaceRule using a surfaceExtractor reference.
     * @param surfaceExtractor extractor for surface tag value
     * @param openStreetMapToUniDbSurfaceMapper mapper
     */
    public WaysSurfaceRule(SurfaceExtractor surfaceExtractor,
                           OpenStreetMapToUniDbSurfaceMapper openStreetMapToUniDbSurfaceMapper) {
        this.surfaceExtractor = surfaceExtractor;
        this.openStreetMapToUniDbSurfaceMapper = openStreetMapToUniDbSurfaceMapper;
    }

    /**
     * Apply the surface rule on a RawWay reference.
     * @param entity way representation (way/waySection) as it comes from OSM
     *
     * @return An updated raw way representation according to UniDb specs
     */
    @Override
    public T apply(final T entity) {
        final Map<String, String> tags = entity.tags();
        final Optional<String> surfaceValue = surfaceExtractor.extract(tags);
        final Optional<UnidbTagValue> uniDbSurfaceValue;

        if (!surfaceValue.isPresent()) {
            return entity;
        }

        uniDbSurfaceValue = openStreetMapToUniDbSurfaceMapper.map(surfaceValue.get());

        if (uniDbSurfaceValue.isPresent()) {
            return entity.withTag(UnidbTagKey.SURAFCE.unwrap(), uniDbSurfaceValue.get().unwrap());
        }

        return entity.withoutTag(UnidbTagKey.SURAFCE.unwrap());
    }
}
