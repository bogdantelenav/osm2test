package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.HovExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;
import java.util.Map;
import java.util.Optional;


/**
 * Map 'hov' tage to NGX values
 *
 * @author odrab
 */
public class HovRule extends Enhancer<WaySection> {

    private final HovExtractor hovExtractor;

    public HovRule(final HovExtractor hovExtractor) {
        this.hovExtractor = hovExtractor;
    }

    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> waySectionTags = waySection.tags();
        final Optional<String> hovValue = hovExtractor.extract(waySectionTags);
        if (hovValue.isPresent()) {
            switch (hovValue.get()) {
                case "yes":
                case "no":
                case "designated":
                    break;
                default:
                    waySectionTags.remove(OsmTagKey.HOV.unwrap());
            }
           return waySection.replaceTags(waySectionTags);
        }
        return waySection;
    }
}