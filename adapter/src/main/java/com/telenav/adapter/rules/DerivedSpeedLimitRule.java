package com.telenav.adapter.rules;

import com.google.common.collect.Sets;
import com.telenav.adapter.component.extractors.adminTags.AdminL1NameExtractor;
import com.telenav.adapter.component.extractors.HighwayExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmHighwayValue;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnAdminValue;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import com.telenav.map.entity.WaySection;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


/**
 * In case of missing max speed tags ('maxspeed:forward' or 'maxspeed:backward') the rule populates those tags with
 * values derived from the road category.
 *
 * @author ioanao
 */
public class DerivedSpeedLimitRule extends Enhancer<WaySection> {

    private final HighwayExtractor highwayExtractor;
    private final AdminL1NameExtractor adminL1NameExtractor;


    public DerivedSpeedLimitRule(final HighwayExtractor highwayExtractor,
            final AdminL1NameExtractor adminL1NameExtractor) {
        super(Sets.newHashSet(MaxSpeedRule.class, OneWayRule.class));
        this.highwayExtractor = highwayExtractor;
        this.adminL1NameExtractor = adminL1NameExtractor;
    }


    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> waySectionTags = waySection.tags();
        final Optional<String> derivedSpeedLimit = computeDerivedSpeedLimit(waySectionTags);
        if (derivedSpeedLimit.isPresent()) {
            if (shouldHaveMaxSpeedForward(waySection)) {
                waySectionTags.putAll(buildSpeedLimitTags(OsmTagKey.MAXSPEED_FORWARD, derivedSpeedLimit.get()));
            }
            if (shouldHaveMaxSpeedBackward(waySection)) {
                waySectionTags.putAll(buildSpeedLimitTags(OsmTagKey.MAXSPEED_BACKWARD, derivedSpeedLimit.get()));
            }
        }
        return waySection.withTags(waySectionTags);
    }

    private Map<String, String> buildSpeedLimitTags(final OsmTagKey maxSpeedTag, final String speedLimit) {
        final Map<String, String> tags = new HashMap<>();
        tags.put(maxSpeedTag.unwrap(), speedLimit);
        tags.put(maxSpeedTag.withSource(), TnSourceValue.DERIVED_SPEED_CATEGORY.unwrap());
        return tags;
    }

    private Optional<String> computeDerivedSpeedLimit(final Map<String, String> waySectionTags) {
        final Optional<String> adminLevel = adminL1NameExtractor.extract(waySectionTags);
        final Optional<OsmHighwayValue> highwayValue = retrieveHighwayType(waySectionTags);

        if (highwayValue.isPresent() && adminLevel.isPresent()) {
            if (adminLevel.get().equals(TnAdminValue.L1_NAME_JAKARTA.unwrap())) {
                return computeJakartaSpeedLimit(highwayValue.get());
            }
        }
        return Optional.empty();
    }

    private Optional<OsmHighwayValue> retrieveHighwayType(final Map<String, String> waySectionTags) {
        final Optional<String> highwayValue = highwayExtractor.extract(waySectionTags);
        return highwayValue.isPresent() ? OsmHighwayValue.optionalValueOf(highwayValue.get()) : Optional.empty();
    }

    private boolean shouldHaveMaxSpeedForward(final WaySection waySection) {
        return !waySection.hasKey(OsmTagKey.MAXSPEED_FORWARD.unwrap()) && (
                waySection.hasTag(OsmTagKey.ONEWAY.unwrap(), DefaultTagValue.FORWARD_ONEWAY.unwrap())
                || isDoubleWay(waySection));
    }

    private boolean shouldHaveMaxSpeedBackward(final WaySection waySection) {
        return !waySection.hasKey(OsmTagKey.MAXSPEED_BACKWARD.unwrap()) && (
                waySection.hasTag(OsmTagKey.ONEWAY.unwrap(), DefaultTagValue.BACKWARD_ONEWAY.unwrap())
                || isDoubleWay(waySection));
    }

    private boolean isDoubleWay(final WaySection waySection) {
        return waySection.hasTag(OsmTagKey.ONEWAY.unwrap(), "no") || !waySection.hasKey(OsmTagKey.ONEWAY.unwrap());
    }

    private Optional<String> computeJakartaSpeedLimit(final OsmHighwayValue highwayValue) {
        final String speedLimit;
        switch (highwayValue) {
            case MOTORWAY :
                speedLimit = "100";
                break;
            case TRUNK :
                speedLimit = "80";
                break;
            case PRIMARY :
            case SECONDARY :
            case TERTIARY :
            case UNCLASSIFIED :
                speedLimit = "50";
                break;
            case SERVICE :
            case LIVING_STREET :
            case TRACK :
            case ROAD :
                speedLimit = "30";
                break;
            default:
                speedLimit = null;
        }
        return speedLimit != null ? Optional.of(speedLimit) : Optional.empty();
    }
}