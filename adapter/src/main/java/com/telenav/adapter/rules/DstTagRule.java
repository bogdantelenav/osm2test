package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.DstExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Map;
import java.util.Optional;


/**
 * Update the 'tn__dst_pattern' tag with the key 'dst_pattern'
 *
 * @author dianat2
 */
public class DstTagRule extends Enhancer<WaySection> {

    private final DstExtractor dstExtractor;

    public DstTagRule(final DstExtractor dstExtractor) {
        this.dstExtractor = dstExtractor;
    }

    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> entityTags = waySection.tags();
        final Optional<String> entityDstTagValue = dstExtractor.extract(entityTags);

        final WaySection adaptedEntity = waySection.withoutTag(TnTagKey.TN_DST.unwrap());

        if (entityDstTagValue.isPresent()) {
            return adaptedEntity.withTag(TnTagKey.TN_DST.withoutTelenavPrefix(),
                    entityDstTagValue.get());
        }
        return adaptedEntity;
    }
}