package com.telenav.adapter.rules;

import com.telenav.adapter.bean.Fow;
import com.telenav.adapter.component.extractors.HighwayExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;

import java.util.List;
import java.util.Map;
import java.util.Optional;


/**
 * Add the 'fow' tag based on rst.
 * <p>
 * The FOW value can be inferred based on OSM tags via the following rules:
 * <p>
 * UNDEFINED : highway=unclassified
 * MOTORWAY : highway=motorway
 * MULTIPLE_CARRIAGEWAY : -
 * SINGLE_CARRIAGEWAY : -
 * ROUNDABOUT : junction=roundabout
 * or Cul-De-Sac rule: - specifies that it refers to a circular or elliptical loop at the end of a dead-end road.
 * a) ways that have junction = roundabout tag and are connected to only one way.
 * - oneway here doesn't refer to the direction of the way,
 * but that the roundabout is connected to a way and a way only
 * (due to the fact that we check the existence of junction=roundabout tag on way section
 * we won't be checking if it is connected to a way only, because the tag existence
 * includes this case)
 * b) In OSM there is a tag (highway = turning_circle) that is essentially referring to a
 * dead end road where you cand u-turn. But this tag is placed on the last node of the way
 * and not the way itself
 * TRAFFICSQUARE : -
 * SLIPROAD : highway= *_link (motorway, trunk, primary, secondary, tertiary)
 * OTHER : anything that is not included in one of the above categories, falls under OTHER
 * (won't be included, because MULTIPLE_CARRIAGEWAY, SINGLE_CARRIAGEWAY, TRAFFICSQUARE are not handled due to
 * missing a generic rule to incorporate all cases)
 * <p>
 * For more info, check: https://spaces.telenav.com:8443/display/~horea.meleg@telenav.com/Traffic+Support+-+Form+Of+Way+Research
 * Note: MULTIPLE_CARRIAGEWAY, SINGLE_CARRIAGEWAY, TRAFFICSQUARE are not handled, because there isn't a
 *
 * @author lcira
 */
public class FormOfWayRule extends Enhancer<WaySection> {

    public static final String FOW_TAG_KEY = "fow";
    public static final String OSM_HIGHWAY_KEY = "highway";
    public static final String OSM_JUNCTION_KEY = "junction";
    public static final String OSM_ROUNDABOUT_JUNCTION_VALUE = "roundabout";
    public static final String OSM_UNCLASSIFIED_HIGHWAY_VALUE = "unclassified";
    public static final String OSM_MOTORWAY_HIGHWAY_VALUE = "motorway";
    public static final String OSM_MOTORWAY_LINK_HIGHWAY_VALUE = "motorway_link";
    public static final String OSM_TRUNK_LINK_HIGHWAY_VALUE = "trunk_link";
    public static final String OSM_PRIMARY_LINK_HIGHWAY_VALUE = "primary_link";
    public static final String OSM_SECONDARY_LINK_HIGHWAY_VALUE = "secondary_link";
    public static final String OSM_TERTIARY_LINK_HIGHWAY_VALUE = "tertiary_link";
    private static final String OSM_TURNING_CIRCLE_VALUE = "turning_circle";
    private final HighwayExtractor highwayExtractor;


    public FormOfWayRule(final HighwayExtractor highwayExtractor) {
        this.highwayExtractor = highwayExtractor;
    }


    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> tags = waySection.tags();

        final Optional<String> highwayOptional = highwayExtractor.extract(tags);

        if (highwayOptional.isPresent()) {

            final String highway = highwayOptional.get();

            if (tags.containsKey(OSM_JUNCTION_KEY) &&
                    tags.get(OSM_JUNCTION_KEY).equals(OSM_ROUNDABOUT_JUNCTION_VALUE)) {
                return waySection.withTag(FOW_TAG_KEY, Fow.ROUNDABOUT.name());
            }

            if (OSM_MOTORWAY_HIGHWAY_VALUE.equals(highway)) {
                return waySection.withTag(FOW_TAG_KEY, Fow.MOTORWAY.name());
            }

            if (OSM_MOTORWAY_LINK_HIGHWAY_VALUE.equals(highway) ||
                    OSM_TRUNK_LINK_HIGHWAY_VALUE.equals(highway) ||
                    OSM_PRIMARY_LINK_HIGHWAY_VALUE.equals(highway) ||
                    OSM_SECONDARY_LINK_HIGHWAY_VALUE.equals(highway) ||
                    OSM_TERTIARY_LINK_HIGHWAY_VALUE.equals(highway)) {
                return waySection.withTag(FOW_TAG_KEY, Fow.SLIPROAD.name());
            }

            if (OSM_UNCLASSIFIED_HIGHWAY_VALUE.equals(highway)) {
                return waySection.withTag(FOW_TAG_KEY, Fow.UNDEFINED.name());
            }
        }
        // Cul-De-Sac rule
        if (isTurningCircle(waySection)) {
            return waySection.withTag(FOW_TAG_KEY, Fow.ROUNDABOUT.name());
        }

        return waySection;
    }

    private boolean isTurningCircle(WaySection waySection) {
        final List<Node> nodes = waySection.nodes();
        final Node lastNode = nodes.get(nodes.size() - 1);
        return lastNode.tags().containsKey(OSM_HIGHWAY_KEY)
                && lastNode.tags().get(OSM_HIGHWAY_KEY).equals(OSM_TURNING_CIRCLE_VALUE);
    }

}
