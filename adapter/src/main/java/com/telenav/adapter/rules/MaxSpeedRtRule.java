package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.MaxSpeedRtExtractor;
import com.telenav.adapter.component.extractors.OneWayRtExtractor;
import com.telenav.adapter.utils.SpeedNormalizer;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import com.telenav.map.entity.WaySection;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


/**
 * Add maxspeed specific tags tn__maxspeed_forward, tn__maxspeed_backward and
 * 'tn__maxspeed:conditional'.
 *
 * @author petrum
 */
public class MaxSpeedRtRule extends Enhancer<WaySection> {

    private final OneWayRtExtractor oneWayExtractor;
    private final MaxSpeedRtExtractor maxSpeedExtractor;

    public MaxSpeedRtRule(OneWayRtExtractor oneWayExtractor, MaxSpeedRtExtractor maxSpeedExtractor) {
        this.oneWayExtractor = oneWayExtractor;
        this.maxSpeedExtractor = maxSpeedExtractor;
    }


    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> waySectionTags = new HashMap<>(waySection.tags());
        final String speedUnit = waySectionTags.get(TnTagKey.TN_SPEED_UNIT.unwrap());
        if (speedUnit != null) {
            final Optional<Map<String, String>> maxSpeedTags = maxSpeedExtractor.extract(waySectionTags);
            maxSpeedTags.ifPresent(tags -> waySectionTags.putAll(maxSpeedTags(waySectionTags, tags, speedUnit)));

            final Optional<String> maxSpeedConditionalTag = maxSpeedConditionalTag(waySectionTags, speedUnit);
            maxSpeedConditionalTag.ifPresent(tagValue ->
                    waySectionTags.put(OsmTagKey.MAXSPEED_CONDITIONAL.telenavPrefix(), tagValue));
        }
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), waySectionTags)
                .withSequenceNumber(waySection.sequenceNumber());
    }

    private Map<String, String> maxSpeedTags(final Map<String, String> allTags, final Map<String, String> maxSpeedTags,
                                             final String speedUnit) {
        final String maxSpeedValue = maxSpeedTags.get(OsmTagKey.MAXSPEED.unwrap());
        return maxSpeedValue != null ? buildMaxSpeedTagsFromMaxSpeed(allTags, maxSpeedValue, speedUnit) :
                buildMaxSpeedTagsFromMaxSpeedsPerDirection(maxSpeedTags, speedUnit);
    }

    private Map<String, String> buildMaxSpeedTagsFromMaxSpeed(final Map<String, String> allTags, final String maxSpeed,
                                                              final String speeUnit) {
        final Map<String, String> newTags = new HashMap<>();
        final Optional<String> normalizedMaxSpeed = SpeedNormalizer.normalize(maxSpeed, speeUnit);
        if (normalizedMaxSpeed.isPresent()) {
            final Optional<String> oneWay = oneWayExtractor.extract(allTags);
            if (!oneWay.isPresent() || oneWay.get().equals("no")) {
                newTags.put(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), normalizedMaxSpeed.get());
                newTags.put(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), normalizedMaxSpeed.get());
            } else {
                if (oneWay.get().equals(DefaultTagValue.FORWARD_ONEWAY.unwrap()) || oneWay.get().equals("yes")) {
                    newTags.put(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), normalizedMaxSpeed.get());
                } else if (oneWay.get().equals(DefaultTagValue.BACKWARD_ONEWAY.unwrap())) {
                    newTags.put(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), normalizedMaxSpeed.get());
                }
            }
        }
        return newTags;
    }

    private Map<String, String> buildMaxSpeedTagsFromMaxSpeedsPerDirection(final Map<String, String> maxSpeedTags,
                                                                           final String speedUnit) {
        final Map<String, String> finalTags = new HashMap<>();
        SpeedNormalizer.normalize(maxSpeedTags.get(OsmTagKey.MAXSPEED_BACKWARD.unwrap()), speedUnit)
                .ifPresent(value -> finalTags.put(OsmTagKey.MAXSPEED_BACKWARD.telenavPrefix(), value));
        SpeedNormalizer.normalize(maxSpeedTags.get(OsmTagKey.MAXSPEED_FORWARD.unwrap()), speedUnit)
                .ifPresent(value -> finalTags.put(OsmTagKey.MAXSPEED_FORWARD.telenavPrefix(), value));
        return finalTags;
    }

    private Optional<String> maxSpeedConditionalTag(final Map<String, String> tags,
                                                    final String speedUnit) {
        final String maxSpeedConditional = tags.get(OsmTagKey.MAXSPEED_CONDITIONAL.unwrap());
        return SpeedNormalizer.normalize(maxSpeedConditional, speedUnit);
    }
}
