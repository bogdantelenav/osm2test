package com.telenav.adapter.rules;

import com.telenav.adapter.bean.RoadType;
import com.telenav.adapter.component.extractors.FrontageExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.unidb.RoadCategoryTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Map;
import java.util.Optional;

import static com.telenav.adapter.bean.RoadType.identifierFor;

/**
 * if tag frontage_road exists and its value is 'yes', changed to 'frontage'
 */
public class FrontageRule extends Enhancer<WaySection> {

    private static final String UNIDB_FRONTAGE_TAG_KEY = "frontage";
    private static final String OSM_FRONTAGE_ROAD_TAG_KEY = "frontage_road";

    private final FrontageExtractor extractor;

    public FrontageRule(final FrontageExtractor extractor) {
        this.extractor = extractor;
    }

    @Override
    public WaySection apply(final WaySection entity) {
        final Map<String, String> entityTags = entity.tags();
        final Optional<String> frontageValue = extractor.extract(entityTags);
        if (frontageValue.isPresent()) {
            if (!entityTags.containsKey(RoadCategoryTagKey.ROAD_TYPE.unwrap())) {
                entityTags.put(RoadCategoryTagKey.ROAD_TYPE.unwrap(), String.valueOf(identifierFor(RoadType.FRONTAGE_ROAD)));
            }
            entityTags.remove(OSM_FRONTAGE_ROAD_TAG_KEY);
            entityTags.put(UNIDB_FRONTAGE_TAG_KEY, "yes");
        }
        entity.replaceTags(entityTags);
        return entity;
    }
}