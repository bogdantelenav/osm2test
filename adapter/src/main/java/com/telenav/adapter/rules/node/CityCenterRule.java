package com.telenav.adapter.rules.node;

import com.telenav.adapter.component.CityService;
import com.telenav.adapter.component.extractors.AltNameExtractor;
import com.telenav.adapter.component.extractors.Extractor;
import com.telenav.adapter.component.extractors.NameExtractor;
import com.telenav.adapter.component.extractors.cityCenterExtractor.CityCenterExtractor;
import com.telenav.adapter.component.fc.TagService;
import com.telenav.adapter.entity.LangCodeBag;
import com.telenav.adapter.entity.dto.MemberRelation;
import com.telenav.adapter.utils.AdminLevelUtil;
import com.telenav.adapter.utils.IsoUtil;
import com.telenav.adapter.utils.LangCodeUtil;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.Node;
import com.telenav.datapipelinecore.bean.Member;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.telenav.adapter.utils.OsmInfo.*;
import static com.telenav.adapter.utils.OsmKeys.*;
import static com.telenav.adapter.utils.UniDbKeys.*;

@Slf4j
public class CityCenterRule extends Enhancer<Map<String, String>> {

    /*
        Data.
     */
    private static final Map<String, String> PLACE_TAG_TO_CAT_ID_VALUE = Stream.of(new Object[][]{
            {PLACE_TAG_NEIGHBORHOOD_VALUE, "9709"},
            {PLACE_TAG_CITY_VALUE_IN_UNIDB, "4444"},
            {PLACE_TAG_HAMLET_VALUE_IN_UNIDB, "9998"},
    }).collect(Collectors.toMap(data -> (String) data[0], data -> (String) data[1]));

    private final List<MemberRelation> memberRelations;
    private final long nodeId;
    private final CityCenterExtractor capitalExtractor;
    private final AltNameExtractor altNameExtractor;
    private final NameExtractor nameExtractor;
    private final List<LangCodeBag> langCodesByCountry;
    private final CityCenterExtractor placeExtractor;
    private final CityCenterExtractor addrStreetExtractor;
    private final CityCenterExtractor adminLevelExtractor;
    private final double latitude;
    private final double longitude;

    /*
        Admin centre relevant value extractors.
     */
    public CityCenterRule(final long nodeId,
                          final double latitude,
                          final double longitude,
                          final List<MemberRelation> memberRelations,
                          final List<LangCodeBag> langCode) {
        this.nodeId = nodeId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.placeExtractor = new CityCenterExtractor(PLACE_TAG_KEY_IN_OSM);
        this.addrStreetExtractor = new CityCenterExtractor(ADDRSTREET_TAG_KEY_IN_OSM);
        this.adminLevelExtractor = new CityCenterExtractor(ADMIN_LEVEL_TAG_KEY_IN_OSM, "original_osm_admin_level");
        this.memberRelations = memberRelations;
        this.langCodesByCountry = langCode;
        this.capitalExtractor = new CityCenterExtractor(CAPITAL_TAG_KEY_IN_OSM);
        this.altNameExtractor = new AltNameExtractor();
        this.nameExtractor = new NameExtractor();
    }

    @Override
    public final Map<String, String> apply(final Map<String, String> tags) {
        if (memberRelations == null || memberRelations.isEmpty()) {
            return tags;
        }

        final Long adminCenterRelationId = getAdminCenterRelationId(nodeId, memberRelations);

        if (adminCenterRelationId != null) {

            /*
                Some nodes don't have the 'admin_level' tag which is needed for some enhancements.
                In this case we add the 'admin_level' tag from the relation.
            */
            addAdminLevel(nodeId, memberRelations, adminCenterRelationId, tags);

            log.info("Enhancing node: " + nodeId + " as admin_centre for realtion: " + adminCenterRelationId);

            // Enhance city center.
            enhance(tags, (tagsParam) -> enhance(tagsParam, CITY_CENTER_TAG_KEY_IN_UNIDB, CITY_CENTER_TAG_VALUE_IN_UNIDB));

            // Enhance place.
            enhanceWithExtractor(placeExtractor, tags, CityCenterRule.EnhancementCodeWithValue.PLACE_ENHANCER_FUNC);

            // Enhance cat id.
            enhance(tags, CityCenterRule.EnhancementCode.CAT_ID_ENHANCEMENT_FUNC);

            // Population: as in OSM - do nothing.

            // Enhance addr street.
            enhanceWithExtractor(addrStreetExtractor, tags, CityCenterRule.EnhancementCodeWithValue.ADDR_STREET_ENHANCEMENT_FUNC);

            // Enhance admin level.
            final boolean enhancedAdminLevel = enhanceWithExtractor(adminLevelExtractor, tags, CityCenterRule.EnhancementCodeWithValue.ADMIN_LEVEL_ENHANCEMENT_FUNC);

            // Enhance admin level related data.
            enhanceAdminLevels(memberRelations, tags);

            // Postal code as in OSM - do nothing.

            // Enhance ISO, capital, name, alt_name
            enhanceWithIsoRelatedData(nodeId, latitude, longitude, tags, enhancedAdminLevel);

            for (Map.Entry<String, String> entry : tags.entrySet()) {
                if (entry.getValue() == null) {
                    log.warn("Null tag value for node: " + nodeId + "->" + entry.getKey() + ":" + entry.getValue());
                }
            }
        }

        return tags;
    }

    private Long getAdminCenterRelationId(final long nodeId,
                                          final List<MemberRelation> relations) {

        // Filter relations that have the node as city centre.
        final List<MemberRelation> relationsWithNodeAsAdminCentre = relations.stream()
                .filter(memberRelation -> {

                    final Member nodeMember = memberRelation.getMember();

                    final String nodeRole = nodeMember.getMemberRole();

                    return (nodeRole.equals(OSM_ADMIN_CENTER_MEMBER_ROLE) ||
                            // Some cities in Germany have this label instead of 'admin_center'
                            nodeRole.equals(OSM_LABEL_MEMBER_ROLE) &&
                                    nodeId == nodeMember.getId().longValue() &&
                                    memberRelation.getTags().containsKey(ADMIN_LEVEL_TAG_KEY_IN_OSM));
                }).collect(Collectors.toList());

        for (final MemberRelation memberRelation : relationsWithNodeAsAdminCentre) {
            if (CityService.isCityMemberRelation(memberRelation)) {
                return memberRelation.getId();
            }
        }
        return null;
    }

    private void enhanceWithIsoRelatedData(final long nodeId,
                                           final double latitudeAsDegrees,
                                           final double longitudeAsDegrees,
                                           final Map<String, String> newTags,
                                           final boolean enhancedAdminLevel) {

        String iso2 = IsoUtil.getIso2(newTags);

        if (StringUtils.isEmpty(iso2)) {
            iso2 = IsoUtil.getIso2(latitudeAsDegrees, longitudeAsDegrees);
        }
        if (StringUtils.isEmpty(iso2)) {
            return;
        }

        final String iso3 = IsoUtil.getIso3(iso2);

        if (iso3 == null) {
            log.error("Cannot get iso3 for iso2=" + iso2 +
                    " and Node with id=" + nodeId +
                    " (" + latitudeAsDegrees + "," +
                    longitudeAsDegrees + ")");
        } else {
            // Enhance with ISO3.
            enhance(newTags, (tags) -> enhance(tags, ISO_TAG_KEY_IN_UNIDB, iso3));
        }

        // Enhance capital
        enhanceWithExtractor(capitalExtractor, newTags, CityCenterRule.EnhancementCodeWithValue.CAPITAL_ENHANCEMENT_FUNC);

        // name:<lang_code>: as in OSM - do nothing.
        // alt_name:<lang_code>: as in OSM - do nothing.

        /*
            Enhance capital order.
            In order to add the 'capital_order' tag we need to make sure that the node
            is a capital - i.e. has the OSM 'capital' tag. Moreover, we need to
         */
        if (newTags.containsKey(CAPITAL_TAG_KEY_IN_OSM) && enhancedAdminLevel) {

            enhancedCapitalOrder(newTags);

        }

        final String lowerCaseIso2 = iso2.toLowerCase();

        // Enhance name.
        enhanceWithExtractor(nameExtractor,
                newTags,
                (value, tags) -> LangCodeUtil.enhanceWithLangCode(value,
                        OsmTagKey.NAME.unwrap(),
                        lowerCaseIso2,
                        tags,
                        langCodesByCountry));

        // Enhance alt_name.
        enhanceWithExtractor(altNameExtractor,
                newTags,
                (value, tags) -> LangCodeUtil.enhanceWithLangCode(value,
                        OsmTagKey.ALT_NAME.unwrap(),
                        lowerCaseIso2,
                        tags,
                        langCodesByCountry));

    }

    private boolean enhancedCapitalOrder(Map<String, String> newTags) {

        return enhance(newTags, (tags) -> {

            final String capital = tags.get(CAPITAL_TAG_KEY_IN_UNIDB_AND_OSM);

            // Is country capital.
            if (CAPITAL_TAG_VALUE_IN_UNIDB.equals(capital)) {

                tags.put(CAPITAL_ORDER_TAG_KEY_PREFIX_IN_UNIDB + COUNTRY_CAPITAL_CAPITAL_ORDER_VALUE, CAPITAL_ORDER_TAG_VALUE_IN_UNIDB);
                return true;
            }

            // This is the UniDB RELATION admin level - i.e. the previously enhanced.
            final String adminLevel = tags.get(ADMIN_LEVEL_TAG_KEY_IN_UNIDB);

            if (ADMIN_LEVELS_TO_INFER_CAPITAL_ORDER.contains(adminLevel)) {

                int adminOrder = AdminLevelUtil.getAdminOrderByAdminLevel(adminLevel);

                /*
                    The state capital has been added earlier, in case the admin order is 1, this is a misleading
                    OSM case.
                */
                if (CAPITAL_ORDER_UNIDB_ALLOWED_VALUES.contains(adminOrder) //&& adminOrder != 1
                ) {

                    tags.put(CAPITAL_ORDER_TAG_KEY_PREFIX_IN_UNIDB + adminOrder, CAPITAL_ORDER_TAG_VALUE_IN_UNIDB);
                    return true;
                }
            }

            return false;
        });
    }

    private MemberRelation getMemberByTagValue(final String key,
                                               final String val,
                                               final List<MemberRelation> relations) {

        final Predicate<MemberRelation> byTagValue = relation -> {

            final Map<String, String> osmEntityTags = relation.getMember()
                    .getOsmEntityTags();

            return osmEntityTags != null &&
                    osmEntityTags.containsKey(key) &&
                    osmEntityTags.get(key).equals(val);
        };

        final List<MemberRelation> res = relations.stream()
                .filter(byTagValue)
                .collect(Collectors.toList());

        return (res.isEmpty()) ? null : res.get(0);
    }


    private Long getAdminCenterRelationId(final Node node,
                                          final List<MemberRelation> relations) {

        // Filter relations that have the node as city centre.
        final List<MemberRelation> relationsWithNodeAsAdminCentre = relations.stream()
                .filter(memberRelation -> {

                    final Member nodeMember = memberRelation.getMember();

                    final String nodeRole = nodeMember.getMemberRole();

                    return (nodeRole.equals(OSM_ADMIN_CENTER_MEMBER_ROLE) ||
                            // Some cities in Germany have this label instead of 'admin_center'
                            nodeRole.equals(OSM_LABEL_MEMBER_ROLE) &&
                                    node.id() == nodeMember.getId().longValue() &&
                                    memberRelation.getTags().containsKey(ADMIN_LEVEL_TAG_KEY_IN_OSM));
                }).collect(Collectors.toList());

        for (final MemberRelation memberRelation : relationsWithNodeAsAdminCentre) {
            if (CityService.isCityMemberRelation(memberRelation)) {
                return memberRelation.getId();
            }
        }
        return null;
    }


    private boolean enhanceAdminLevels(final List<MemberRelation> memberRelations,
                                       final Map<String, String> tags) {
        boolean enhanced = false;
        for (final MemberRelation memberRelation : memberRelations) {
            if (CityService.isCityMemberRelation(memberRelation)) {
                final Map<String, String> memberRelationTags = memberRelation.getTags();
                TagService.addIfNonEmpty(tags, "l1", memberRelationTags.get("l1"));
                TagService.addIfNonEmpty(tags, "l2", memberRelationTags.get("l2"));
                TagService.addIfNonEmpty(tags, "l3", memberRelationTags.get("l3"));
                TagService.addIfNonEmpty(tags, "link_count", memberRelationTags.get("link_count"));
            }
        }
        return enhanced;
    }

    private boolean enhanceWithExtractor(final Extractor extractor,
                                         final Map<String, String> tags,
                                         final CityCenterRule.EnhancementCodeWithValue code) {

        final Optional<String> optional = extractor.extract(tags);

        if (optional.isPresent()) {

            return code.execute(optional.get(), tags);

        }

        return false;
    }


    private boolean enhance(final Map<String, String> tags,
                            final String key,
                            final String value) {

        tags.put(key, value);

        return true;
    }

    private boolean enhance(final Map<String, String> tags,
                            final CityCenterRule.EnhancementCode code) {
        return code.execute(tags);
    }

    /*
        Helper functions.
    */
    @FunctionalInterface
    public interface EnhancementCodeWithValue extends Serializable {

        CityCenterRule.EnhancementCodeWithValue PLACE_ENHANCER_FUNC = (value, tags) -> {

            if (OSM_CORESPONDENTS_FOR_UNIDB_NEIGHBORHOOD.contains(value)) {

                tags.put(PLACE_TAG_KEY_IN_UNIDB, "neighborhood");

            } else if (OSM_CORESPONDENTS_FOR_UNIDB_CITY.contains(value)) {

                tags.put(PLACE_TAG_KEY_IN_UNIDB, "city");

            } else if (OSM_CORESPONDENTS_FOR_UNIDB_HAMLET.contains(value)) {

                tags.put(PLACE_TAG_KEY_IN_UNIDB, "hamlet");

            }

            return true;
        };

        CityCenterRule.EnhancementCodeWithValue ADDR_STREET_ENHANCEMENT_FUNC = (value, tags) -> {

            final String placeTagValue = tags.get(PLACE_TAG_KEY_IN_UNIDB);

            if (placeTagValue != null) {

                final String catId = PLACE_TAG_TO_CAT_ID_VALUE.get(placeTagValue);

                if (catId != null) {

                    tags.put(CAT_ID_TAG_KEY_IN_UNIDB, catId);

                    return true;
                }
            }

            return false;
        };

        CityCenterRule.EnhancementCodeWithValue ADMIN_LEVEL_ENHANCEMENT_FUNC = (value, tags) -> {

            final String uniDbValue = ADMIN_LEVEL_OSM_TO_UNI_VALUES.get(value);

            if (uniDbValue != null) {

                tags.put(ADMIN_LEVEL_TAG_KEY_IN_UNIDB, uniDbValue);
                return true;
            }

            return false;
        };

        CityCenterRule.EnhancementCodeWithValue CAPITAL_ENHANCEMENT_FUNC = (value, tags) -> {

            final String uniDbAdminLevel = tags.get(ADMIN_LEVEL_TAG_KEY_IN_UNIDB);

            /*
                In UniDb 'capital':'yes' is valid ONLY for country capitals,
                while in OSM for other administrative areas - i.e. keeping
                the 'capital' tag with another value than 'yes' for admin levels different than "L1" is wrong.
                Why L1? because in the "administrative-level-mapping.csv" all the L1_Admin_Types correspond to countries.
                If this changes, then the logic would need to get updated..
            */
            if (ADMIN_LEVEL_1.equals(uniDbAdminLevel)) {

                tags.put(CAPITAL_TAG_KEY_IN_UNIDB_AND_OSM, "yes");

            } else {

                tags.remove(CAPITAL_TAG_KEY_IN_UNIDB_AND_OSM);
            }

            return true;
        };

        boolean execute(String value, Map<String, String> tags);

    }

    @FunctionalInterface
    private interface EnhancementCode extends Serializable {

        CityCenterRule.EnhancementCode CAT_ID_ENHANCEMENT_FUNC = (tags) -> {
            if (tags.containsKey(PLACE_TAG_KEY_IN_UNIDB)) {

                final String placeTagValue = tags.get(PLACE_TAG_KEY_IN_UNIDB);

                final String catId = PLACE_TAG_TO_CAT_ID_VALUE.get(placeTagValue);

                if (catId != null) {

                    tags.put(CAT_ID_TAG_KEY_IN_UNIDB, catId);

                    return true;
                }
            }

            return false;
        };

        boolean execute(Map<String, String> tags);

    }

    private void addAdminLevel(final long nodeId,
                               final List<MemberRelation> relations,
                               final Long adminCenterRelationId,
                               final Map<String, String> tags) {

        if (adminCenterRelationId == null) {
            return;
        }

        for (final MemberRelation relation : relations) {
            if (relation.getId() == adminCenterRelationId) {
                log.warn("Added 'admin_level' tag from relation: " + adminCenterRelationId +
                        " for node: " + nodeId);

                if (relation.getTags().containsKey("original_osm_admin_level")) {
                    tags.put(ADMIN_LEVEL_TAG_KEY_IN_OSM, relation.getTags().get("original_osm_admin_level"));
                }
//                else {
//                    tags.put(ADMIN_LEVEL_TAG_KEY_IN_OSM, relation.getTags().get(ADMIN_LEVEL_TAG_KEY_IN_OSM));
//                }
                return;
            }
        }

        log.warn("Couldn't add 'admin_level' tag from relation: " + adminCenterRelationId +
                " for node: " + nodeId);
    }
}
