package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.TimezoneExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.TagAdjuster;

import java.util.Map;
import java.util.Optional;


/**
 * Update the 'tn__language_code' tag with the key 'language_code'
 *
 * @author dianat2
 */
public class TimezoneTagRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private final TimezoneExtractor timezoneExtractor;

    public TimezoneTagRule(final TimezoneExtractor timezoneExtractor) {
        this.timezoneExtractor = timezoneExtractor;
    }

    @Override
    public T apply(final T entity) {
        final Map<String, String> entityTags = entity.tags();
        final Optional<String> entityTimezoneTagValue = timezoneExtractor.extract(entityTags);

        final T adaptedEntity = entity.withoutTag(TnTagKey.TN_TIMEZONE.unwrap());

        if (entityTimezoneTagValue.isPresent()) {
            return adaptedEntity.withTag(TnTagKey.TN_TIMEZONE.withoutTelenavPrefix(),
                    entityTimezoneTagValue.get());
        }
        return adaptedEntity;
    }
}