package com.telenav.adapter.rules.node;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import lombok.extern.slf4j.Slf4j;

import java.util.*;
import java.util.regex.Pattern;

/**
 * @author roxanal
 */
@Slf4j
public class NameRule extends Enhancer<Map<String, String>> {

    private static final long serialVersionUID = 1L;
    private static final String TWO_CHARS_LOCALIZED_NAME_REGEXP = "^(name:[a-zA-Z]{2})$";

    @Override
    public Map<String, String> apply(final Map<String, String> tags) {

        final List<String> twoCharsLocalizedKeys = getTwoCharsLocalizedKeys(tags);

        if (!twoCharsLocalizedKeys.isEmpty()) {

            for (final String twoCharsLocalizedKey : twoCharsLocalizedKeys) {

                final String value = tags.get(twoCharsLocalizedKey);

                try {

                    final String threeCharsLocalizedKey = to3CharsLocalizedKey(twoCharsLocalizedKey);

                    tags.remove(twoCharsLocalizedKey);

                    tags.put(threeCharsLocalizedKey, value);


                } catch (MissingResourceException e) {

                    log.error(e.getMessage());

                }
            }
        }

        return tags;
    }

    private String to3CharsLocalizedKey(final String twoCharsLocalizedKey) {

        final String[] split = twoCharsLocalizedKey.split(":");

        return split[0] + ":" + Locale.forLanguageTag(split[1]).getISO3Language();
    }

    private List<String> getTwoCharsLocalizedKeys(final Map<String, String> tags) {

        final List<String> result = new LinkedList<>();

        for (final String key : tags.keySet()) {

            if (isTwoCharsLocalizedName(key)) {
                result.add(key);
            }
        }
        return result;
    }

    private boolean isTwoCharsLocalizedName(final String key) {
        return Pattern.matches(TWO_CHARS_LOCALIZED_NAME_REGEXP, key);
    }
}