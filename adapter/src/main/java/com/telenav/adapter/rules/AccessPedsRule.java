package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.FootExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.TagAdjuster;

import java.util.HashSet;
import java.util.Map;
import java.util.Optional;

/**
 * @author petrum
 */
public class AccessPedsRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private static final HashSet<String> VALUES_FOR_NO = new HashSet<String>() {{
        add("unknown");
        add("military");
        add("impassable");
        add("undefined");
        add("private;customers");
        add("customers;private");
        add("hunting");
        add("private;destination");
        add("fishing;hunting");
        add("restricted");
        add("Unknown");
        add("curstomers;private");
        add("private1");
        add("unclassified");
        add("unknwon");
        add("mo");
    }};
    private final FootExtractor footExtractor;

    public AccessPedsRule(final FootExtractor footExtractor) {
        this.footExtractor = footExtractor;
    }

    @Override
    public T apply(final T entity) {
        Map<String, String> tags = entity.tags();
        final Optional<String> footValueOptional = footExtractor.extract(tags);

        if (footValueOptional.isPresent()) {
            switch (footValueOptional.get()) {
                case "yes":
                case "designated":
                case "permissive":
                case "use_sidepath":
                case "destination":
                case "official":
                    return entity.withTag(OsmTagKey.FOOT.unwrap(), "yes");
                case "no":
                case "private":
                    return entity.withTag(OsmTagKey.FOOT.unwrap(), "no");
                default:
                    if (VALUES_FOR_NO.contains(footValueOptional.get())) {
                        return entity.withTag(OsmTagKey.FOOT.unwrap(), "no");
                    } else {
                        return entity.withTag(OsmTagKey.FOOT.unwrap(), "yes");
                    }
            }
        } else {
            return entity;
        }
    }
}
