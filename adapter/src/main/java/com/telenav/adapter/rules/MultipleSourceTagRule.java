package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import lombok.AllArgsConstructor;
import lombok.Builder;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;


/**
 * The purpose of the class is to decide which source will be used in establishing the final value of a given tag. The
 * possible sources are the following ones: images, OSM, probes and ISA. If an image detection is present then that value
 * will become the final value of the tag. Otherwise the final value will be taken from probes, then OSM. If there is no OSM value
 * then the ISA value will be taken into account.
 *
 * @author olehd
 */
@Builder
@AllArgsConstructor
public class MultipleSourceTagRule implements Function<Map<String, String>, Map<String, String>> {

    private final String osmTagName;
    private final String probeTagName;
    private final String imageTagName;
    private final String isaTagName;
    private final String manualTagName;
    private final String deletedTagName;
    private final OsmTagKey finalTagName;

    /**
     * Add to the received tag collection the final tag, together with a source tag: 'finalTagName = value',
     * 'source:finalTagName = source'. The tag used to populate the final tag will be removed.
     *
     * @param tags the tag collection containing multiple source tags
     * @return a new tag collection
     */
    @Override
    public Map<String, String> apply(final Map<String, String> tags) {
        if (checkTagValue(tags, manualTagName)) {
            return buildFinalTags(tags, manualTagName, TnSourceValue.MANUAL, tags.get(manualTagName));
        } else if (checkTagValue(tags, imageTagName)) {
            return buildFinalTags(tags, imageTagName, TnSourceValue.IMAGES, tags.get(imageTagName));
        } else if (checkTagValue(tags, deletedTagName)) {
            return buildFinalTags(tags, deletedTagName, TnSourceValue.DELETED, tags.get(deletedTagName));
        } else if (checkTagValue(tags, probeTagName)) {
            return buildFinalTags(tags, probeTagName, TnSourceValue.PROBES, tags.get(probeTagName));
        } else if (checkTagValue(tags, osmTagName)) {
            return buildFinalTags(tags, osmTagName, TnSourceValue.OSM, tags.get(osmTagName));
        } else if (checkTagValue(tags, isaTagName)) {
            return buildFinalTags(tags, isaTagName, TnSourceValue.ISA, tags.get(isaTagName));
        }
        return tags;
    }

    private boolean checkTagValue(final Map<String, String> tags, String tagKey) {
        return null != tagKey && tags.containsKey(tagKey);
    }

    private Map<String, String> buildFinalTags(final Map<String, String> tags, final String mostPreciseTagName,
                                               final TnSourceValue source, final String newTagValue) {
        final Map<String, String> finalTags = new HashMap<>(tags);
        finalTags.remove(mostPreciseTagName);
        finalTags.put(finalTagName.unwrap(), newTagValue);
        finalTags.put(finalTagName.withSource(), source.unwrap());
        return finalTags;
    }
}