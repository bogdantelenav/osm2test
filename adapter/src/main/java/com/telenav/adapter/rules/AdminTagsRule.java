package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.adminTags.*;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.TagAdjuster;

import java.util.Map;
import java.util.Optional;


/**
 * Update the 'tn__l1:left' tag with the key 'l1:left'
 * Update the 'tn__l1:right' tag with the key 'l1:right'
 * Update the 'tn__l2:left' tag with the key 'l2:left'
 * Update the 'tn__l2:right' tag with the key 'l2:right'
 * Update the 'tn__l3:left' tag with the key 'l3:left'
 * Update the 'tn__l3:right' tag with the key 'l3:right'
 *
 * @author dianat2
 */
public class AdminTagsRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private static final String ADMIN_ORDER_TAG_KEY = "admin_order";
    private static final String ADMIN_TYPE_TAG_KEY = "admin_type";

    private final AdminL1LeftExtractor adminL1LeftExtractor;
    private final AdminL1RightExtractor adminL1RightExtractor;
    private final AdminL2LeftExtractor adminL2LeftExtractor;
    private final AdminL2RightExtractor adminL2RightExtractor;
    private final AdminL3LeftExtractor adminL3LeftExtractor;
    private final AdminL3RightExtractor adminL3RightExtractor;


    public AdminTagsRule(AdminL1LeftExtractor adminL1LeftExtractor,
                         AdminL1RightExtractor adminL1RightExtractor,
                         AdminL2LeftExtractor adminL2LeftExtractor,
                         AdminL2RightExtractor adminL2RightExtractor,
                         AdminL3LeftExtractor adminL3LeftExtractor,
                         AdminL3RightExtractor adminL3RightExtractor) {
        this.adminL1LeftExtractor = adminL1LeftExtractor;
        this.adminL1RightExtractor = adminL1RightExtractor;
        this.adminL2LeftExtractor = adminL2LeftExtractor;
        this.adminL2RightExtractor = adminL2RightExtractor;
        this.adminL3LeftExtractor = adminL3LeftExtractor;
        this.adminL3RightExtractor = adminL3RightExtractor;
    }


    @Override
    public T apply(final T entity) {
        final Map<String, String> entityTags = entity.tags();
        final Optional<String> entityAdminL1LeftTagValue = adminL1LeftExtractor.extract(entityTags);
        final Optional<String> entityAdminL1RightTagValue = adminL1RightExtractor.extract(entityTags);
        final Optional<String> entityAdminL2LeftTagValue = adminL2LeftExtractor.extract(entityTags);
        final Optional<String> entityAdminL2RightTagValue = adminL2RightExtractor.extract(entityTags);
        final Optional<String> entityAdminL3LeftTagValue = adminL3LeftExtractor.extract(entityTags);
        final Optional<String> entityAdminL3RightTagValue = adminL3RightExtractor.extract(entityTags);

        T adaptedEntity = entity
                .withoutTag(TnTagKey.TN_ADMIN_L1_LEFT.unwrap())
                .withoutTag(TnTagKey.TN_ADMIN_L1_RIGHT.unwrap())
                .withoutTag(TnTagKey.TN_ADMIN_L2_LEFT.unwrap())
                .withoutTag(TnTagKey.TN_ADMIN_L2_RIGHT.unwrap())
                .withoutTag(TnTagKey.TN_ADMIN_L3_LEFT.unwrap())
                .withoutTag(TnTagKey.TN_ADMIN_L3_RIGHT.unwrap());

        if (entityAdminL1LeftTagValue.isPresent()) {
            adaptedEntity = adaptedEntity.withTag(TnTagKey.TN_ADMIN_L1_LEFT.withoutTelenavPrefix(),
                    entityAdminL1LeftTagValue.get());
        }
        if (entityAdminL1RightTagValue.isPresent()) {
            adaptedEntity = adaptedEntity.withTag(TnTagKey.TN_ADMIN_L1_RIGHT.withoutTelenavPrefix(),
                    entityAdminL1RightTagValue.get());
        }
        if (entityAdminL2LeftTagValue.isPresent()) {
            adaptedEntity = adaptedEntity.withTag(TnTagKey.TN_ADMIN_L2_LEFT.withoutTelenavPrefix(),
                    entityAdminL2LeftTagValue.get());
        }
        if (entityAdminL2RightTagValue.isPresent()) {
            adaptedEntity = adaptedEntity.withTag(TnTagKey.TN_ADMIN_L2_RIGHT.withoutTelenavPrefix(),
                    entityAdminL2RightTagValue.get());
        }
        if (entityAdminL3LeftTagValue.isPresent()) {
            adaptedEntity = adaptedEntity.withTag(TnTagKey.TN_ADMIN_L3_LEFT.withoutTelenavPrefix(),
                    entityAdminL3LeftTagValue.get());
        }
        if (entityAdminL3RightTagValue.isPresent()) {
            adaptedEntity = adaptedEntity.withTag(TnTagKey.TN_ADMIN_L3_RIGHT.withoutTelenavPrefix(),
                    entityAdminL3RightTagValue.get());
        }

        return adaptedEntity;
    }

}