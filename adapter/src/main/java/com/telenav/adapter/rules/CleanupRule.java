package com.telenav.adapter.rules;

import com.telenav.adapter.rules.whitelist.WhiteList;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.map.entity.TagAdjuster;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * Remove all the tags prefixed with "tn__".
 *
 * @author ioanao
 */
public class CleanupRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private static final String TELENAV_PREFIX = "tn__";
    private final WhiteList whiteList;

    public CleanupRule(final WhiteList whiteList) {
        if (Objects.isNull(whiteList)) {
            throw new IllegalArgumentException("whitelist cannot be null!");
        }
        this.whiteList = whiteList;
    }

    @Override
    public T apply(final T entity) {
        final List<String> tagKeysToRemove = entity.tags().keySet().stream()
                .filter(tagKey -> tagKey.startsWith(TELENAV_PREFIX) && !isTagWhiteListed(tagKey))
                .collect(Collectors.toList());
        return entity.withoutTags(tagKeysToRemove);
    }

    private boolean isTagWhiteListed(final String tagKey) {
        return whiteList.tags().contains(tagKey);
    }

}