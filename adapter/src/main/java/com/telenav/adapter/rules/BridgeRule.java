package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.BridgeExtractor;
import com.telenav.adapter.component.mapper.OpenStreetMapToUniDbBridgeMapper;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.datapipelinecore.unidb.UnidbTagValue;
import com.telenav.map.entity.TagAdjuster;

import java.util.Map;
import java.util.Optional;

/**
 * Bridge tag rule for unifying different bridge values according to uniDb specs.
 *
 * @param <T> The entity that will have the bridge values unified.
 * @author Andrei Puf
 */
public class BridgeRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private final BridgeExtractor bridgeExtractor;
    private final OpenStreetMapToUniDbBridgeMapper openStreetMapToUniDbBridgeMapper;

    /**
     * Initializes the BridgeRule using a BridgeExtractor reference.
     * @param bridgeExtractor - bridge extractor rule
     * @param openStreetMapToUniDbBridgeMapper - mapper
     */
    public BridgeRule(BridgeExtractor bridgeExtractor,
                      OpenStreetMapToUniDbBridgeMapper openStreetMapToUniDbBridgeMapper) {
        this.bridgeExtractor = bridgeExtractor;
        this.openStreetMapToUniDbBridgeMapper = openStreetMapToUniDbBridgeMapper;
    }

    @Override
    public T apply(T entity) {
        final Map<String, String> tags = entity.tags();
        final Optional<String> bridgeValue = bridgeExtractor.extract(tags);
        final Optional<UnidbTagValue> uniDbBridgeValue;

        if (!bridgeValue.isPresent()) {
            return entity;
        }

        uniDbBridgeValue = openStreetMapToUniDbBridgeMapper.map(bridgeValue.get());

        if (uniDbBridgeValue.isPresent()) {
            return entity.withTag(UnidbTagKey.BRIDGE.unwrap(), uniDbBridgeValue.get().unwrap());
        }

        return entity.withoutTag(UnidbTagKey.BRIDGE.unwrap());

    }
}
