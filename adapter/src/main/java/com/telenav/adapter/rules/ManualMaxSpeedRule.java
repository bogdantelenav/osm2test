package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.WaySection;

import java.util.Map;

import static com.telenav.adapter.utils.ManualEditsUtil.alterManualEditsTag;


/**
 * @author roxanal
 */
public class ManualMaxSpeedRule extends Enhancer<WaySection> {

    /**
     * All tags which are of the form: 'tn__source:{feature} = value' become 'tn__value:{feature} = missing / the
     * feature tag's value'
     *
     * @param waySection - the way section to enhance
     * @return - the enhanced way section
     */
    @Override
    public WaySection apply(final WaySection waySection) {
        Map<String, String> tags = waySection.tags();
        tags = alterManualEditsTag(tags, OsmTagKey.MAXSPEED.unwrap());
        tags = alterManualEditsTag(tags, OsmTagKey.MAXSPEED_FORWARD.unwrap());
        tags = alterManualEditsTag(tags, OsmTagKey.MAXSPEED_BACKWARD.unwrap());
        tags = alterManualEditsTag(tags, OsmTagKey.MAXSPEED_CONDITIONAL.unwrap());

        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), tags)
                .withSequenceNumber(waySection.sequenceNumber());
    }
}