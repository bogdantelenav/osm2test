package com.telenav.adapter.rules;

import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.map.entity.WaySection;

import java.util.*;


/**
 * Update the 'oneway' tag. The value is taken from one of the following tags: 'tn__images_oneway', 'oneway',
 * 'tn__probes_oneway'.
 *
 * @author irinap
 */
public class OneWayRule extends Enhancer<WaySection> {

    private static final Set<String> ALLOWED_ONEWAY_VALUES = new HashSet<>(Arrays.asList("yes", "no", "-1"));

    public OneWayRule() {
        super(new HashSet<>(Collections.singletonList(ManualOneWayRule.class)));
    }

    @Override
    public WaySection apply(final WaySection waySection) {
        Map<String, String> tags = waySection.tags();
        tags = MultipleSourceTagRule.builder()
                .imageTagName(OsmTagKey.ONEWAY.withSource(TnSourceValue.IMAGES))
                .probeTagName(OsmTagKey.ONEWAY.withSource(TnSourceValue.PROBES))
                .osmTagName(OsmTagKey.ONEWAY.unwrap())
                .finalTagName(OsmTagKey.ONEWAY)
                .build()
                .apply(tags);
        tags.remove(OsmTagKey.ONEWAY.withSource()); // source:oneway not in unidb spec

        final String oneWayValue = tags.get(OsmTagKey.ONEWAY.unwrap());
        if (!ALLOWED_ONEWAY_VALUES.contains(oneWayValue)) {
            tags.remove(OsmTagKey.ONEWAY.unwrap()); //remove illegal values for oneway
        }
        return waySection.replaceTags(tags);
    }
}