package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.OneWayExtractor;
import com.telenav.adapter.component.extractors.TurnLaneExtractor;
import com.telenav.adapter.exception.CorruptWaySectionException;
import com.telenav.adapter.utils.turnLanes.*;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.map.entity.TagAdjuster;
import com.telenav.map.entity.WaySection;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.util.CollectionAccumulator;

import java.util.Arrays;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.telenav.adapter.rules.LaneTypeRule.CORRUPTED_WAY_SECTION_KEY;
import static com.telenav.adapter.utils.turnLanes.OsmToUniDbTurnLanesEnhancer.*;

/**
 * Converts turn lanes OSM values to UniDB values.
 *
 * @author adrianal
 */
public class TurnLaneRule<T extends TagAdjuster<T>> extends Enhancer<T> {

    private final TurnLaneExtractor turnLaneExtractor;
    private final OneWayExtractor oneWayExtractor;
    private final OsmToUniDbTurnLanesEnhancer enhancerFw;
    private final OsmToUniDbTurnLanesEnhancer enhancerBw;
    private final OsmToUniDbTurnLanesEnhancer enhancerBothWays;
    private final OsmToUniDbTurnLanesEnhancer enhancer;

    private CollectionAccumulator<Long> invalidTurnLanesWayIds;

    public TurnLaneRule(final TurnLaneExtractor turnLaneExtractor,
                        final OneWayExtractor oneWayExtractor,
                        final CollectionAccumulator<String> invalidLaneValues) {
        this.turnLaneExtractor = turnLaneExtractor;
        this.oneWayExtractor = oneWayExtractor;
        this.enhancerFw = new OsmToUniDbTurnLanesForwardEnhancer(invalidLaneValues);
        this.enhancerBw = new OsmToUniDbTurnLanesBackwardEnhancer(invalidLaneValues);
        this.enhancerBothWays = new OsmToUniDbTurnLanesBothWaysEnhancer(invalidLaneValues);
        this.enhancer = new OsmToUniDbTurnLanesGeneralEnhancer(invalidLaneValues);
    }

    /**
     * Convert turn lanes from OSM which are represented as strings, to ints. As ints they are represented in UniDB
     * format.
     * When a turn lane in osm has multiple values (more then one arrow per pavement sign) it is separated
     * by ";".In such scenario values individually are converted to appropriate int values and summed up.
     * TODO this is temporary solution and is subject to change after research is complete
     *  @param turnLaneExtractor the extractor for turn lane values
     * @param oneWayExtractor   the extractor for oneway values
     * @param invalidLaneValues invalidLaneValues spark accumulator
     * @param invalidTurnLanesWayIds invalidTurnLanesWayIds spark accumulator
     */
    public TurnLaneRule(final TurnLaneExtractor turnLaneExtractor,
                        final OneWayExtractor oneWayExtractor,
                        final CollectionAccumulator<String> invalidLaneValues,
                        final CollectionAccumulator<Long> invalidTurnLanesWayIds) {
        this.turnLaneExtractor = turnLaneExtractor;
        this.oneWayExtractor = oneWayExtractor;
        this.enhancerFw = new OsmToUniDbTurnLanesForwardEnhancer(invalidLaneValues);
        this.enhancerBw = new OsmToUniDbTurnLanesBackwardEnhancer(invalidLaneValues);
        this.enhancerBothWays = new OsmToUniDbTurnLanesBothWaysEnhancer(invalidLaneValues);
        this.enhancer = new OsmToUniDbTurnLanesGeneralEnhancer(invalidLaneValues);
        this.invalidTurnLanesWayIds = invalidTurnLanesWayIds;
    }

    @Override
    public T apply(T entity) {
        final Map<String, String> tags = entity.tags();
        final Optional<String> turnLaneValue = turnLaneExtractor.extract(tags);
        final Optional<String> turnLaneForwardValue = turnLaneExtractor.extract(tags, TURN_LANES_FORWARD);
        final Optional<String> turnLaneBackwardValue = turnLaneExtractor.extract(tags, TURN_LANES_BACKWARD);
        Optional<String> turnLaneBothWaysValue = turnLaneExtractor.extract(tags, TURN_LANES_BOTH_WAYS);

        final Optional<Map<String, String>> oneWayTagValue = oneWayExtractor.extract(tags);

        T enhanced = null;
        try {
            if (oneWayTagValue.isPresent() && isOneWay(oneWayTagValue.get())) {
                if (turnLaneForwardValue.isPresent() || turnLaneBackwardValue.isPresent()) {
                    return entity.withTag(CORRUPTED_WAY_SECTION_KEY, "yes");
                }
                enhanced = enhancer.enhanceFromTurnLane(turnLaneValue, entity);
            } else {
                enhanced = enhancerFw.enhanceFromTurnLane(turnLaneForwardValue, entity);
                enhanced = enhancerFw.enhanceFromLaneNumber(enhanced);
                enhanced = enhancerBw.enhanceFromTurnLane(turnLaneBackwardValue, enhanced);
                enhanced = enhancerBw.enhanceFromLaneNumber(enhanced);
                enhanced = enhancerBothWays.enhanceFromTurnLane(turnLaneBothWaysValue, enhanced);
                enhanced = enhancerBothWays.enhanceFromLaneNumber(enhanced);
                enhanced = enhanceTurnLanesFromTurnLanesBackwardAndTurnLanesForward(enhanced);
            }
        } catch (final NumberFormatException | CorruptWaySectionException e) {
            if (invalidTurnLanesWayIds  != null) {
                invalidTurnLanesWayIds.add(((WaySection) entity).strictIdentifier().wayId());
            }
            enhanced = entity.withTag(CORRUPTED_WAY_SECTION_KEY, "yes");

        }
        return enhanced;
    }

    private boolean isOneWay(final Map<String, String> tags) {
        return tags.containsKey(OsmTagKey.ONEWAY.unwrap()) &&
                !tags.get(OsmTagKey.ONEWAY.unwrap()).equals("no");
    }

    private T enhanceTurnLanesFromTurnLanesBackwardAndTurnLanesForward(final T waySection) {
        final Map<String, String> tags = waySection.tags();
        final String turnLanesForward = getOrElse(tags, TURN_LANES_FORWARD, TN_TURN_LANES_FORWARD);
        final String turnLanesBackward = getOrElse(tags, TURN_LANES_BACKWARD, TN_TURN_LANES_BACKWARD);
        final String turnLanesBothWays = getOrElse(tags, TURN_LANES_BOTH_WAYS, TN_TURN_LANES_BOTH_WAYS);
        final String turnLanesValue = getTurnLanesValues(turnLanesForward,
                turnLanesBothWays,
                turnLanesBackward);
        if (!StringUtils.isEmpty(turnLanesValue)) {
            tags.put(TurnLaneExtractor.TURN_LANES, turnLanesValue);
        }
        tags.remove(TN_LANES_FORWARD);
        tags.remove(TN_LANES_BACKWARD);
        tags.remove(TN_LANES_BOTH_WAYS);
        tags.remove(TN_TURN_LANES_FORWARD);
        tags.remove(TN_TURN_LANES_BACKWARD);
        tags.remove(TN_TURN_LANES_BOTH_WAYS);
        return waySection.replaceTags(tags);
    }

    private String getTurnLanesValues(final String... turnLanesValuesArray) {
        return Arrays.stream(turnLanesValuesArray)
                .filter(turnLanesValue -> !StringUtils.isEmpty(turnLanesValue))
                .collect(Collectors.joining(TURN_LANE_SEPARATOR));
    }

    private String getOrElse(final Map<String, String> tags,
                             final String key,
                             final String elseKey) {
        String val = tags.get(key);
        if (val == null) {
            val = tags.get(elseKey);
        }
        return val;
    }
}