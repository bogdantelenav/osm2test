package com.telenav.adapter.rules;

import com.telenav.adapter.component.extractors.adminTags.AdminL1NameExtractor;
import com.telenav.adapter.component.extractors.HighwayExtractor;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnAdminValue;
import com.telenav.map.entity.WaySection;

import java.util.Map;
import java.util.Optional;


/**
 * @author adrianal
 */
public class MotorcycleNoRule extends Enhancer<WaySection> {

    private final HighwayExtractor highwayExtractor;
    private final AdminL1NameExtractor adminL1NameExtractor;


    public MotorcycleNoRule(final HighwayExtractor highwayExtractor, final AdminL1NameExtractor adminL1NameExtractor) {
        this.highwayExtractor = highwayExtractor;
        this.adminL1NameExtractor = adminL1NameExtractor;
    }


    @Override
    public WaySection apply(final WaySection waySection) {
        final Map<String, String> waySectionTags = waySection.tags();
        if (hasMotorwayTags(waySectionTags) && canBeAppliedOnRegion(waySectionTags)) {
            waySectionTags.put(OsmTagKey.MOTORCYCLE.unwrap(), "no");
        }
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), waySectionTags)
                .withSequenceNumber(waySection.sequenceNumber());
    }

    private boolean canBeAppliedOnRegion(final Map<String, String> tags) {
        final Optional<String> adminL1Name = adminL1NameExtractor.extract(tags);
        return adminL1Name.isPresent() && adminL1Name.get().equals(TnAdminValue.L1_NAME_JAKARTA.unwrap());
    }

    private boolean hasMotorwayTags(final Map<String, String> tags) {
        final Optional<String> highwayValue = highwayExtractor.extract(tags);
        return highwayValue.isPresent() && isMotorway(highwayValue.get());
    }

    private boolean isMotorway(final String highwayValue) {
        return highwayValue.equals("motorway") || highwayValue.equals("motorway_link");
    }
}
