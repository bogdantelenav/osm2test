package com.telenav.adapter.rules;

import com.telenav.adapter.utils.IsoUtil;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.datapipelinecore.unidb.DefaultTagValue;
import com.telenav.datapipelinecore.unidb.UnidbTagKey;
import com.telenav.map.entity.WaySection;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

/**
 * Cleans max speed tags ('maxspeed:forward' or 'maxspeed:backward') in the USA
 * regarding the states restrictions
 *
 * @author odrab
 */
@Slf4j
public class CleanSpeedLimitRule extends Enhancer<WaySection> {

    public static final int DEFAULT_MAX_SPEED_LIMIT_IN_MILES = 99;
    public static final int DEFAULT_MAX_SPEED_LIMIT_IN_KM = 160;
    public static final int DEFAULT_MIN_SPEED_LIMIT = 5;
    private final Map<Long, Integer> speedLimitByUsStates;
    private final Map<String, String> speedLimitsByEuCountries;


    public CleanSpeedLimitRule(final Map<Long, Integer> speedLimitByUsStates, Map<String, String> speedLimitsByEuCountries) {
        this.speedLimitByUsStates = speedLimitByUsStates;
        this.speedLimitsByEuCountries = speedLimitsByEuCountries;
    }

    @Override
    public WaySection apply(final WaySection waySection) {
        final String waySectionIso = IsoUtil.getIso3(waySection);
        Map<String, String> waySectionTags = waySection.tags();
        final int speedLimitOfState;
        if (!speedLimitByUsStates.isEmpty() &&
                "USA".equals(waySectionIso) &&
                waySectionTags.containsKey("l2:right") &&
                waySectionTags.get("l2:right") != null) {
            speedLimitOfState = speedLimitByUsStates
                    .get(Long.valueOf(waySectionTags.get("l2:right")));
        } else if (speedLimitsByEuCountries.containsKey(waySectionIso) &&
                        speedLimitsByEuCountries.get(waySectionIso) != null) { // case Germany and Isle of Man where we don't have a speed limit
            speedLimitOfState = Integer.parseInt(speedLimitsByEuCountries.get(waySectionIso));
        } else {
            if (DefaultTagValue.MILES.unwrap().equals(waySectionTags.get(UnidbTagKey.SPEED_UNIT.unwrap()))) {
                speedLimitOfState = DEFAULT_MAX_SPEED_LIMIT_IN_MILES; // mph
            } else {
                speedLimitOfState = DEFAULT_MAX_SPEED_LIMIT_IN_KM; // kph
            }
        }
        removeInvalidSpeedLimit(waySectionTags, OsmTagKey.MAXSPEED_FORWARD.unwrap(), speedLimitOfState);
        removeInvalidSpeedLimit(waySectionTags, OsmTagKey.MAXSPEED_BACKWARD.unwrap(), speedLimitOfState);
        removeInvalidSpeedLimit(waySectionTags, OsmTagKey.MINSPEED_FORWARD.unwrap(), speedLimitOfState);
        removeInvalidSpeedLimit(waySectionTags, OsmTagKey.MINSPEED_BACKWARD.unwrap(), speedLimitOfState);
        return waySection.withTags(waySectionTags);
    }

    private void removeInvalidSpeedLimit(final Map<String, String> waySectionTags, final String speedLimitTag, final int speedLimitOfState) {
        if (waySectionTags.containsKey(speedLimitTag)) {
            try {
                int speedLimit = Integer.parseInt(waySectionTags.get(speedLimitTag));
                if (speedLimit % 5 != 0 || speedLimit < DEFAULT_MIN_SPEED_LIMIT || speedLimit > speedLimitOfState) {
                    waySectionTags.remove(speedLimitTag);
                }
            } catch (NumberFormatException e) {
                waySectionTags.remove(speedLimitTag);
            }
        }
    }

}