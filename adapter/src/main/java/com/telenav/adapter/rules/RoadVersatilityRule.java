package com.telenav.adapter.rules;

import com.google.common.collect.Sets;
import com.telenav.adapter.utils.InvalidTagsCleaner;
import com.telenav.adapter.utils.OsmTagKeysArchiver;
import com.telenav.datapipelinecore.enhancer.Enhancer;
import com.telenav.datapipelinecore.osm.OsmTagKey;
import com.telenav.datapipelinecore.telenav.TnSourceValue;
import com.telenav.map.entity.WaySection;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;


/**
 * Update the 'motorcar' and 'motorcycle' tags. The value is taken from one of the following tags:
 * 'tn__manual_{feature}', 'tn__validated_{feature}', 'tn__deleted_{feature}', '{feature}', 'tn__probes_{feature}'.
 *
 * @author roxanal
 */
public class RoadVersatilityRule extends Enhancer<WaySection> {

    private static final Set<OsmTagKey> OSM_TAGS_TO_ARCHIVE = Sets.newHashSet(OsmTagKey.MOTORCAR, OsmTagKey.MOTORCYCLE);


    public RoadVersatilityRule() {
        super(new HashSet<>(Collections.singletonList(ManualRoadVersatilityRule.class)));
    }


    @Override
    public WaySection apply(final WaySection waySection) {
        Map<String, String> tags = waySection.tags();
        final String motorcycleValue = tags.get(OsmTagKey.MOTORCYCLE.unwrap());
        final String motorcarValue = tags.get(OsmTagKey.MOTORCAR.unwrap());
        if ((motorcycleValue == null && containsManualEditTag(tags, OsmTagKey.MOTORCYCLE)) ||
                ((motorcarValue == null && containsManualEditTag(tags, OsmTagKey.MOTORCAR)))) {
            return waySection;
        }
        if (motorcycleValue != null) {
            tags.put(OsmTagKey.MOTORCYCLE.telenavPrefix(), motorcycleValue);
        }
        if (motorcarValue != null) {
            tags.put(OsmTagKey.MOTORCAR.telenavPrefix(), motorcarValue);
        }

        tags = new OsmTagKeysArchiver(OSM_TAGS_TO_ARCHIVE).archiveTags(tags);
        tags = constructFinalTags(tags);

        tags = InvalidTagsCleaner.deleteTagsWithInvalidValue(tags, OsmTagKey.MOTORCAR.unwrap());
        tags = InvalidTagsCleaner.deleteTagsWithInvalidValue(tags, OsmTagKey.MOTORCYCLE.unwrap());
        return new WaySection(waySection.strictIdentifier().wayId(), waySection.nodes(), tags)
                .withSequenceNumber(waySection.sequenceNumber());
    }

    private Map<String, String> constructFinalTags(final Map<String, String> tags) {
        final MultipleSourceTagRule motorcarEnhancer = MultipleSourceTagRule.builder()
                .osmTagName(OsmTagKey.MOTORCAR.telenavPrefix())
                .probeTagName(OsmTagKey.MOTORCAR.withSource(TnSourceValue.PROBES))
                .manualTagName(OsmTagKey.MOTORCAR.withSource(TnSourceValue.MANUAL))
                .deletedTagName(OsmTagKey.MOTORCAR.withSource(TnSourceValue.DELETED))
                .finalTagName(OsmTagKey.MOTORCAR).build();
        final MultipleSourceTagRule motorcycleEnhancer = MultipleSourceTagRule.builder()
                .osmTagName(OsmTagKey.MOTORCYCLE.telenavPrefix())
                .probeTagName(OsmTagKey.MOTORCYCLE.withSource(TnSourceValue.PROBES))
                .manualTagName(OsmTagKey.MOTORCYCLE.withSource(TnSourceValue.MANUAL))
                .deletedTagName(OsmTagKey.MOTORCYCLE.withSource(TnSourceValue.DELETED))
                .finalTagName(OsmTagKey.MOTORCYCLE).build();
        return motorcarEnhancer
                .andThen(motorcycleEnhancer)
                .apply(tags);
    }

    private boolean containsManualEditTag(final Map<String, String> tags, final OsmTagKey osmTag) {
        return tags.containsKey(osmTag.withSource(TnSourceValue.MANUAL)) ||
                tags.containsKey(osmTag.withSource(TnSourceValue.DELETED));
    }
}