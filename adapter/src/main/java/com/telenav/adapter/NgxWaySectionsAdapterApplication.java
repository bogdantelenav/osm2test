package com.telenav.adapter;

import com.telenav.adapter.component.extractors.*;
import com.telenav.adapter.component.extractors.adminTags.*;
import com.telenav.adapter.component.mapper.OpenStreetMapToUniDbAccessMapper;
import com.telenav.adapter.component.mapper.OpenStreetMapToUniDbBridgeMapper;
import com.telenav.adapter.component.mapper.OpenStreetMapToUniDbSurfaceMapper;
import com.telenav.adapter.rules.*;
import com.telenav.adapter.rules.whitelist.NgxWhiteList;
import com.telenav.datapipelinecore.enhancer.ChainedEnhancer;
import com.telenav.map.entity.WaySection;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.util.CollectionAccumulator;

import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * @author adrianal
 */
public class NgxWaySectionsAdapterApplication {

    public static final String CUSTOM_WAY_ID_TAG_KEY = "way_id";
    private final RoadTypeExtractor roadTypeExtractor = new RoadTypeExtractor();
    private final Map<String, String> speedLimitsByEuCountries;
    private final Map<Long, Integer> speedLimitByUsStates;
    private Map<Long, List<String>> wayIdsToIsoFc;
    private Boolean fcFunctionality;
    private CollectionAccumulator<String> invalidLaneValues;

    public NgxWaySectionsAdapterApplication() {
        this.wayIdsToIsoFc = Collections.emptyMap();
        this.speedLimitByUsStates = Collections.emptyMap();
        this.speedLimitsByEuCountries = Collections.emptyMap();
    }

    public NgxWaySectionsAdapterApplication(final Map<Long, Integer> speedLimitByUsStates,
                                            final Map<String, String> speedLimitsByEuCountries,
                                            final Map<Long, String> wayIdToAdminLevel) {
        this.speedLimitByUsStates = speedLimitByUsStates;
        this.roadTypeExtractor.setBroadcastedWayIdToAdminLevel(wayIdToAdminLevel);
        this.speedLimitsByEuCountries = speedLimitsByEuCountries;
    }

    public JavaRDD<WaySection> adapt(final JavaRDD<WaySection> waySections) {
        ChainedEnhancer.Builder<WaySection> builder = new ChainedEnhancer.Builder<>(new RemoveInvalidHighwayCombination())
                .enhancer(new RoadTypeRule(roadTypeExtractor))
                .enhancer(new RoadSubTypeRule(new RoadSubTypeExtractor()))
                .enhancer(new MaxSpeedRtRule(new OneWayRtExtractor(), new MaxSpeedRtExtractor()))
                .enhancer(new MinSpeedRtRule())
                .enhancer(new SpeedCategoryRule(new HighwayBasedSpeedCategoryExtractor(),
                        new MaxSpeedBasedSpeedCategoryExtractor()))
                .enhancer(new HighwayWaySectionFixerRule(new HighwayExtractor()))
                .enhancer(new TunnelFixerRule())
                .enhancer(new RampFixerRule())
                .enhancer(new MaxSpeedFixerRule(new MaxSpeedExtractor()))
                .enhancer(new RoundaboutRule(new RoundaboutExtractor()))
                .enhancer(new TollRule<>(new TollExtractor()))
                .enhancer(new MotorCarRule<>())
                .enhancer(new WaysSurfaceRule<>(new SurfaceExtractor(), new OpenStreetMapToUniDbSurfaceMapper()))
                .enhancer(new WaysAccessRule<>(new AccessExtractor(), new OpenStreetMapToUniDbAccessMapper()))
                .enhancer(new WaySectionApplicableToTagRule(new AccessExtractor(), new MotorCarExtractor()))
                .enhancer(new BridgeRule<>(new BridgeExtractor(), new OpenStreetMapToUniDbBridgeMapper()))
                .enhancer(new ManualOneWayRule())
                .enhancer(new OneWayImagesValueRule())
                .enhancer(new OneWayProbesValueRule())
                .enhancer(new OneWayRule())
                .enhancer(new ManualMaxSpeedRule())
                .enhancer(new MaxSpeedRule())
                .enhancer(new MaxSpeedOnewayRule<>())
                .enhancer(new MaxSpeedSourceRule<>())
                .enhancer(new ManualRoadVersatilityRule())
                .enhancer(new RoadVersatilityRule())
                .enhancer(new RoadWidthRule(new RoadWidthExtractor()))
                .enhancer(new SpeedUnitRule())
                .enhancer(new HeightRemoverRule())
                .enhancer(new CleanSpeedLimitRule(speedLimitByUsStates, speedLimitsByEuCountries)) // this rule should be after any speed limit related rule
                .enhancer(new NameRule<>()) //needs to be before language tag rule (not strictly)
                .enhancer(new AltNameRule(new TnAltNameExtractor(), new LanguageExtractor()))
                .enhancer(new RefRule(new RefExtractor(), new LanguageExtractor()))
                .enhancer(new FrontageRule(new FrontageExtractor()))
                .enhancer(new RailFerryRule<>(new RouteExtractor(), new MotorCarExtractor(),
                        new MotorCycleExtractor()));

        if (Boolean.TRUE.equals(fcFunctionality)) {
            builder = builder.enhancer(new RoadCategoryRule(new RoadFunctionalClassExtractor(), wayIdsToIsoFc));
        }

        final ChainedEnhancer<WaySection> adapterEnhancer = builder.enhancer(new FormOfWayRule(new HighwayExtractor()))
                .enhancer(new DrivingSideRule(new DrivingSideExtractor()))
                .enhancer(new RoadShieldRule(new RoadShieldExtractor(), new LanguageExtractor()))
                //let the TurnLaneRule BEFORE other lane rules
                .enhancer(new TurnLaneRule<>(new TurnLaneExtractor(), new OneWayExtractor(), invalidLaneValues))
                .enhancer(new LaneTypeRule())
                .enhancer(new LaneRule(new LaneExtractor(), new OneWayExtractor()))
                .enhancer(new LaneCatRule(new LaneExtractor()))
                .enhancer(new HovRule(new HovExtractor()))
                .enhancer(new AdminTagsRule<>(
                        new AdminL1LeftExtractor(),
                        new AdminL1RightExtractor(),
                        new AdminL2LeftExtractor(),
                        new AdminL2RightExtractor(),
                        new AdminL3LeftExtractor(),
                        new AdminL3RightExtractor()))
                .enhancer(new DstTagRule(new DstExtractor()))
                .enhancer(new LanguageTagRule<>(new LanguageExtractor()))
                .enhancer(new CountryIsoTagRule<>(new CountryIsoExtractor()))
                .enhancer(new TimezoneTagRule<>(new TimezoneExtractor()))
                .enhancer(new RailwayWaySectionCleanupRule())
                .enhancer(new AccessPedsRule<>(new FootExtractor()))
                .enhancer(new CleanupRule<>(new NgxWhiteList()))
                .enhancer(new CleanFootRule())
                .build();

        return waySections.map(adapterEnhancer::apply);
    }

    public NgxWaySectionsAdapterApplication fcFunctionality(final Boolean fcFunctionality) {
        this.fcFunctionality = fcFunctionality;
        return this;
    }

    public NgxWaySectionsAdapterApplication wayIdsToIsoFc(final Map<Long, List<String>> wayIdsToIsoFc) {
        if (Boolean.TRUE.equals(fcFunctionality)) {
            this.wayIdsToIsoFc = wayIdsToIsoFc;
        }
        return this;
    }

    public NgxWaySectionsAdapterApplication invalidLaneValues(CollectionAccumulator<String> invalidLaneValues) {
        this.invalidLaneValues = invalidLaneValues;
        return this;
    }

    public NgxWaySectionsAdapterApplication invalidTurnLanesWayIds(CollectionAccumulator<Long> invalidTurnLanesWayIds) {
        return this;
    }
}