package com.telenav.adapter.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class AdministrativeOrder implements Serializable {

    private String adminOrder;
    private String adminType;
}
