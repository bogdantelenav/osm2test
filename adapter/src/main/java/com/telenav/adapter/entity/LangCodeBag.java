package com.telenav.adapter.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
@Builder
public class LangCodeBag implements Serializable {

    private final String iso2;
    private final List<String> langCodes;
}
