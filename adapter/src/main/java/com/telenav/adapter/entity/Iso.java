package com.telenav.adapter.entity;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
public class Iso implements Serializable {

    private final String iso2;
    private final String iso3;
}
