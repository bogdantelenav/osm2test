package com.telenav.adapter.entity.dto;

import com.telenav.datapipelinecore.bean.Member;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.Map;

@Data
@AllArgsConstructor
public class MemberRelation implements Serializable {

    private long id;
    private Member member;
    private Map<String, String> tags;

    @Override
    public String toString() {
        return "[id=" + id +
                ", member=" + member.toString() +
                ", tags=" + toString(tags) + "]";
    }

    private String toString(final Map<String, String> tags) {
        StringBuilder res = new StringBuilder("{");
        if (tags != null) {
            for (Map.Entry<String, String> entry : tags.entrySet()) {
                res.append("(").append(entry.getKey()).append(",").append(entry.getValue()).append(")");
            }
        }
        res.append("}");
        return res.toString();
    }
}
