package com.telenav.adapter;

import com.telenav.adapter.component.attributes.service.*;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.ost.ArgumentContainer;
import com.telenav.relationenhancer.core.turnrestriction.common.RestrictionEnhancerJob;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.util.CollectionAccumulator;

import static org.apache.commons.lang.StringUtils.isEmpty;

/**
 * The current job integrates detections from external sources by overriding existing detections or
 * removing the detections from entities that do not match any sidefile detection of a specific type.
 * (There are cases where a specific detection is completely removed and replaced with new detections
 * created from the input sidefiles.Please check {@link RelationAttributeService}, {@link NodeAttributeService},
 * {@link WayAttributeService}, {@link WaySectionAttributeService} for a detailed description of how
 * each entity related data is integrated).
 * <p>
 * Besides the previous mentioned use case, the current job removes specific attributes (tags) from the OSM entities
 * and replaces them with tags coming from an external source, having the same key and value.
 * The newly added attributes (defined as enhancements) are matched to the corresponding entity.
 * If the corresponding entity does not have enhancement data, it will still remain without the initial
 * OSM attribute.
 * The attributes to be removed are red from the config files whose paths are given as input.
 *
 * @author liviuc
 */
public class AttributesIntegratorJob {

    public static void main(String[] args) {

        final ArgumentContainer argumentContainer = new ArgumentContainer(args);
        // Entities input paths.
        final String waySectionsPath = argumentContainer.getOptional("way_sections_path", StringUtils.EMPTY);
        /**
         For turn-restriction enhancements we need input way section data. The implementation used for integrating
         turn-restrictions from sidefiles is the same as {@link RestrictionEnhancerJob}
         from the map-relation-enhancer project.
         */
        final String trWaySectionsPath = argumentContainer.getOptional("tr_way_sections_path", StringUtils.EMPTY);
        final String waysPath = argumentContainer.getOptional("ways_path", StringUtils.EMPTY);
        final String nodesPath = argumentContainer.getOptional("nodes_path", StringUtils.EMPTY);
        final String relationsPath = argumentContainer.getOptional("relations_path", StringUtils.EMPTY);
        // Entities detections paths.
        final String waySectionsSidefilePaths = argumentContainer.getOptional("way_sections_sidefile_paths", null);
        final String waysSidefilePaths = argumentContainer.getOptional("ways_sidefile_paths", null);
        final String nodesSidefilePaths = argumentContainer.getOptional("nodes_sidefile_paths", null);
        final String relationsSidefilePaths = argumentContainer.getOptional("relations_sidefile_paths", null);
        // Attributes config files paths.
        final String waySectionsConfigPath = argumentContainer.getOptional("way_sections_config_path", null);
        final String waysConfigPath = argumentContainer.getOptional("ways_config_path", null);
        final String nodesConfigPath = argumentContainer.getOptional("nodes_config_path", null);
        final String relationsConfigPath = argumentContainer.getOptional("relations_config_path", null);
        // Entities output paths.
        final String waySectionsOutputPath = argumentContainer.getOptional("way_sections_output_path", null);
        final String waysOutputPath = argumentContainer.getOptional("ways_output_path", null);
        final String nodesOutputPath = argumentContainer.getOptional("nodes_output_path", null);
        final String relationsOutputPath = argumentContainer.getOptional("relations_output_path", null);
        // Run local.
        final String runLocal = argumentContainer.getOptional("run_local", "false");
        // Join configs.
        final Boolean waySectionsUseBroadcastJoin = Boolean.parseBoolean(argumentContainer.getOptional("way_sections_broadcast_join", "false"));
        final Boolean waysUseBroadcastJoin = Boolean.parseBoolean(argumentContainer.getOptional("ways_path_broadcast_join", "false"));
        final Boolean nodesUseBroadcastJoin = Boolean.parseBoolean(argumentContainer.getOptional("nodes_path_broadcast_join", "false"));
        final Boolean relationsUseBroadcastJoin = Boolean.parseBoolean(argumentContainer.getOptional("relations_path_broadcast_join", "false"));
        // Source of enhancements.
        final String externalSource = argumentContainer.getOptional("source", "ExternalSource");

        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Attributes Integrator Job")
                .runLocal(Boolean.parseBoolean(runLocal))
                .build();

        final JavaSparkContext javaSparkContext = jobInitializer.javaSparkContext();

        final CollectionAccumulator<String> logs = javaSparkContext.sc().collectionAccumulator("logs");

        if (!isEmpty(waySectionsPath)) {
            new WaySectionAttributesProcessingService(waySectionsPath,
                    waySectionsOutputPath,
                    waySectionsSidefilePaths,
                    waySectionsConfigPath,
                    waySectionsUseBroadcastJoin,
                    logs)
                    .process(javaSparkContext);
        }

        if (!isEmpty(waysPath)) {
            new WayAttributesProcessingService(waysPath,
                    waysOutputPath,
                    waysSidefilePaths,
                    waysConfigPath,
                    waysUseBroadcastJoin,
                    logs)
                    .process(javaSparkContext);
        }

        if (!isEmpty(relationsPath)) {
            new RelationAttributesProcessingService(relationsPath,
                    trWaySectionsPath,
                    relationsOutputPath,
                    relationsSidefilePaths,
                    relationsConfigPath,
                    relationsUseBroadcastJoin,
                    externalSource,
                    logs)
                    .process(javaSparkContext);
        }

        if (!isEmpty(nodesPath)) {
            new NodeAttributesProcessingService(nodesPath,
                    nodesOutputPath,
                    nodesSidefilePaths,
                    nodesConfigPath,
                    nodesUseBroadcastJoin,
                    logs)
                    .process(javaSparkContext);
        }
    }
}
