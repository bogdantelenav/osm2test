package com.telenav.adapter.utils;

import java.util.*;

import static com.telenav.adapter.utils.OsmInfo.*;

public class UniDbKeys {

    /*
        UniDB tag keys.
     */
    public static final String CITY_CENTER_TAG_KEY_IN_UNIDB = "type";
    public static final String PLACE_TAG_KEY_IN_UNIDB = "place";
    public static final String STREET_NAME_TAG_KEY_IN_UNIDB = "street_name";
    public static final String ADMIN_LEVEL_TAG_KEY_IN_UNIDB = "admin_level";
    public static final String CAPITAL_ORDER_TAG_KEY_PREFIX_IN_UNIDB = "capital_order";
    public static final String LINK_COUNT_TAG_KEY_IN_UNIDB = "link_count";
    public static final String CAT_ID_TAG_KEY_IN_UNIDB = "cat_id";
    public static final String ISO_TAG_KEY_IN_UNIDB = "iso";
    public static final String CAPITAL_TAG_KEY_IN_UNIDB_AND_OSM = "capital";
    public static final String TYPE_TAG = "type";
    public static final String NAVN_TAG = "navn";
    /*
        UniDB tag values.
     */
    public static final Map<String, String> ADMIN_LEVEL_OSM_TO_UNI_VALUES = new HashMap();
    public static final String CITY_CENTER_TAG_VALUE_IN_UNIDB = "city_center";
    public static final List<String> CAPITAL_OSM_ALLOWED_VALUES = new ArrayList<>();
    public static final List<Integer> CAPITAL_ORDER_UNIDB_ALLOWED_VALUES = new ArrayList<>();
    public static final String CAPITAL_TAG_VALUE_IN_UNIDB = "yes";
    public static final String CAPITAL_ORDER_TAG_VALUE_IN_UNIDB = "yes";
    public static final int COUNTRY_CAPITAL_CAPITAL_ORDER_VALUE = 1;
    public static final String PLACE_TAG_NEIGHBORHOOD_VALUE= "neighborhood";


    static {
        ADMIN_LEVEL_OSM_TO_UNI_VALUES.put("2", ADMIN_LEVEL_1);
        ADMIN_LEVEL_OSM_TO_UNI_VALUES.put("4", ADMIN_LEVEL_2);
        ADMIN_LEVEL_OSM_TO_UNI_VALUES.put("6", ADMIN_LEVEL_3);
        ADMIN_LEVEL_OSM_TO_UNI_VALUES.put("7", ADMIN_LEVEL_4);
        ADMIN_LEVEL_OSM_TO_UNI_VALUES.put("8", ADMIN_LEVEL_5);
        ADMIN_LEVEL_OSM_TO_UNI_VALUES.put("9", ADMIN_LEVEL_5);
        //ADMIN_LEVEL_OSM_TO_UNI_VALUES.put("-", ADMIN_LEVEL_6);
        ADMIN_LEVEL_OSM_TO_UNI_VALUES.put("10", ADMIN_LEVEL_7);
        ADMIN_LEVEL_OSM_TO_UNI_VALUES.put("11", ADMIN_LEVEL_7);

        CAPITAL_ORDER_UNIDB_ALLOWED_VALUES.add(COUNTRY_CAPITAL_CAPITAL_ORDER_VALUE);
        CAPITAL_ORDER_UNIDB_ALLOWED_VALUES.add(2);
        CAPITAL_ORDER_UNIDB_ALLOWED_VALUES.add(8);
        CAPITAL_ORDER_UNIDB_ALLOWED_VALUES.add(9);
    }

    static {
        final Set<String> keySet = ADMIN_LEVEL_OSM_TO_UNI_VALUES.keySet();
        final int keySetSize = keySet.size();
        String arr[] = new String[keySet.size() + 1];
        System.arraycopy(keySet.toArray(), 0, arr, 0, keySetSize);
        arr[arr.length - 1] = "yes";
        CAPITAL_OSM_ALLOWED_VALUES.addAll(Arrays.asList(arr));
    }

}
