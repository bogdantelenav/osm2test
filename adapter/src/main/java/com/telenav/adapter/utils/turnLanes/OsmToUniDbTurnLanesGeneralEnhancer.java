package com.telenav.adapter.utils.turnLanes;

import com.telenav.adapter.component.extractors.TurnLaneExtractor;
import org.apache.spark.util.CollectionAccumulator;

/**
 * @author liviuc
 */
public class OsmToUniDbTurnLanesGeneralEnhancer extends OsmToUniDbTurnLanesEnhancer {

    public OsmToUniDbTurnLanesGeneralEnhancer() {
        super(null);
    }

    public OsmToUniDbTurnLanesGeneralEnhancer(final CollectionAccumulator<String> invalidLaneValues) {
        super(invalidLaneValues);
    }

    @Override
    public String getNewTag() {
        return TurnLaneExtractor.TURN_LANES;
    }

    @Override
    public String getNewTagTemp() {
        return null;
    }

    @Override
    public String getOldTag() {
        return "tn__original_" + TurnLaneExtractor.TURN_LANES;
    }

    @Override
    public boolean revertLanes() {
        return true;
    }

    @Override
    public String getLaneTypeName() {
        return null;
    }
}
