package com.telenav.adapter.utils;

/**
 * @author ioanao
 */
public class TagConstruction {

    public static String suffixWithLanguage(final String tagKey, final String languageCode) {
        return tagKey + ":" + languageCode;
    }
}