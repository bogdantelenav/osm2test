package com.telenav.adapter.utils;

import com.telenav.datapipelinecore.telenav.TnSourceValue;

import java.util.HashMap;
import java.util.Map;


/**
 * @author roxanal
 */
public class InvalidTagsCleaner {

    public static Map<String, String> deleteTagsWithInvalidValue(final Map<String, String> tags, final String tagName) {
        final Map<String, String> alteredTags = new HashMap<>(tags);
        final String tagValue = tags.get(tagName);
        if (tagValue != null && (tagValue.equals(TnSourceValue.MISSING.unwrap()))) {
            alteredTags.remove(tagName);
        }
        return alteredTags;
    }
}