package com.telenav.adapter.utils;

import com.telenav.datapipelinecore.bean.Member;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import static com.telenav.adapter.utils.OsmInfo.*;

public class AdminLevelUtil {

    public static List<String> getDistinctAdminLevelTagKeys(final List<List<Map.Entry<String, String>>> telenavAdminLevelTagsPerRelation) {
        return telenavAdminLevelTagsPerRelation.stream()
                .flatMap(val -> val.stream().map(entry -> entry.getKey()))
                .distinct()
                .collect(Collectors.toList());
    }

    public static List<List<Map.Entry<String, String>>> getTelenavAdminLevelTagsFromMembers(final List<Member> members) {

        return members.stream()
                .map(member ->
                        (member.getOsmEntityTags() == null) ?
                                null :
                                member.getOsmEntityTags().entrySet()
                                        .stream()
                                        .filter(val -> val.getKey().matches(TELENAV_ADMIN_LEVEL_TAG_KEY_REGEX))
                                        .collect(Collectors.toList())
                )
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    public static int getAdminOrderByAdminLevel(final String adminLevel) {

        int result = 0;

        switch (adminLevel) {

            case ADMIN_LEVEL_1:
                result = 0;
                break;

            case ADMIN_LEVEL_2:
                result = 1;
                break;

            case ADMIN_LEVEL_3:
                result = 2;
                break;

            case ADMIN_LEVEL_4:
                result = 8;
                break;

            default:
                result = 9;
                break;
        }

        return result;
    }
}
