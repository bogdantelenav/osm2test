package com.telenav.adapter.utils.turnLanes;

import org.apache.spark.util.CollectionAccumulator;

/**
 * @author liviuc
 */
public class OsmToUniDbTurnLanesBothWaysEnhancer extends OsmToUniDbTurnLanesEnhancer {

    public OsmToUniDbTurnLanesBothWaysEnhancer() {
        super(null);
    }

    public OsmToUniDbTurnLanesBothWaysEnhancer(final CollectionAccumulator<String> invalidLaneValues) {
        super(invalidLaneValues);
    }

    @Override
    public String getNewTag() {
        return TURN_LANES_BOTH_WAYS;
    }

    @Override
    public String getNewTagTemp() {
        return TN_TURN_LANES_BOTH_WAYS;
    }

    @Override
    public String getOldTag() {
        return "tn__original_" + TURN_LANES_BOTH_WAYS;
    }

    @Override
    public boolean revertLanes() {
        return false;
    }

    @Override
    public String getLaneTypeName() {
        return LANES_BOTH_WAYS;
    }
}
