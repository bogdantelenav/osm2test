package com.telenav.adapter.utils.connectivity.graph.builder;

import org.jgrapht.graph.DefaultUndirectedGraph;

/**
 * @author liviuc
 */
public class UndirectedGraphBuilder<V, E> extends GraphBuilder<DefaultUndirectedGraph, V, E> {

    @Override
    protected DefaultUndirectedGraph emptyGraph(final Class<E> edgeType) {
        return new DefaultUndirectedGraph<V, E>(edgeType);
    }
}
