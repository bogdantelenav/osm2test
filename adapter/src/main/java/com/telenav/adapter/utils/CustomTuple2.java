package com.telenav.adapter.utils;

import lombok.Setter;

import java.io.Serializable;

@Setter
public class CustomTuple2<T1, T2> implements Serializable {

    private T1 t1;
    private T2 t2;

    public CustomTuple2(final T1 t1, final T2 t2) {
        this.t1 = t1;
        this.t2 = t2;
    }

    public T1 _1() {
        return t1;
    }

    public T2 _2() {
        return t2;
    }
}
