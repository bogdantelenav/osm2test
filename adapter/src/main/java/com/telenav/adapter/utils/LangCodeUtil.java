package com.telenav.adapter.utils;

import com.telenav.adapter.entity.LangCodeBag;

import java.util.List;
import java.util.Map;

public class LangCodeUtil {

    public static boolean enhanceWithLangCode(final String name,
                                              final String tagKey,
                                              final String iso2,
                                              final Map<String, String> tags,
                                              final List<LangCodeBag> langCodesByCountry) {
        boolean enhanced = false;

        final List<String> langCodes = getLangCodes(iso2, langCodesByCountry);

        for (final String langCode : langCodes) {

            final String keyWithLangCode = tagKey + ":" + langCode;

            //if not from osm already
            if (!tags.containsKey(keyWithLangCode)) {

                tags.put(keyWithLangCode, name);

                enhanced = true;

            }
        }

        return enhanced;
    }

    public static List<String> getLangCodes(final String iso2,
                                            final List<LangCodeBag> langCodes) {
        for (final LangCodeBag langCodeBag : langCodes) {

            if (langCodeBag.getIso2().equalsIgnoreCase(iso2)) {

                return langCodeBag.getLangCodes();

            }
        }

        throw new RuntimeException("No lang code found for iso2=" + iso2);
    }
}
