package com.telenav.adapter.utils;

import com.telenav.adapter.component.CountryLocatorHolder;
import com.telenav.adapter.component.fc.TagService;
import com.telenav.adapter.component.repository.ResourceRepository;
import com.telenav.adapter.entity.Iso;
import com.telenav.datapipelinecore.telenav.TnTagKey;
import com.telenav.map.entity.Node;
import com.telenav.map.entity.WaySection;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Util involving iso operations.
 *
 * @author liviuc
 */
public class IsoUtil {

    private static final Map<String, String> ISO2_TO_ISO3;
    private static final Map<String, String> ISO3_TO_ISO2;
    private static final int DEFAULT_CAPACITY = 272;

    static {
        try {

            final List<Iso> isos = ResourceRepository.getIsos();

            ISO2_TO_ISO3 = new HashMap<>(DEFAULT_CAPACITY);
            ISO3_TO_ISO2 = new HashMap<>(DEFAULT_CAPACITY);
            for (final Iso iso : isos) {
                ISO2_TO_ISO3.put(iso.getIso2(), iso.getIso3());
                ISO3_TO_ISO2.put(iso.getIso3(), iso.getIso2());
            }

        } catch (final IOException e) {

            throw new RuntimeException(e);
        }
    }

    /**
     * The current method returns the 2 letters country code corresponding to
     * the given way section.
     * First, it checks whether the way section has the information among its tags -
     * i.e. "iso" or "tn__iso". If so, it returns the 2 letters equivalent.
     * If that wasn't the case it uses the nodes to perform country localization and
     * return the 2 letters country code.
     *
     * @param waySection way section.
     * @return ISO2 corresponding to the way section.
     */
    public static String getIso2(final WaySection waySection) {
        final Map<String, String> tags = waySection.tags();

        String iso3 = null;
        if (tags.containsKey("iso")) {
            iso3 = tags.get("iso");
        } else if (tags.containsKey(TnTagKey.TN_COUNTRY_ISO_CODE.unwrap())) {
            iso3 = tags.get(TnTagKey.TN_COUNTRY_ISO_CODE.unwrap());
        }

        if (iso3 != null) {
            return getIso2(iso3);
        }

        for (final Node node : waySection.nodes()) {

            final String iso2 = getIso2(node);

            if (iso2 != null) {

                break;
            }
        }
        return null;
    }

    /**
     * The current method returns the 3 letters country code corresponding to
     * the given way section.
     * First, it checks whether the way section has the information among its tags -
     * i.e. "iso" or "tn__iso". If so, it returns it.
     * If that wasn't the case it uses the nodes to perform country localization and
     * return the 3 letters country code based on the 2 letters country code retrieved via localization.
     *
     * @param waySection way section.
     * @return ISO3 corresponding to the way section.
     */
    public static String getIso3(WaySection waySection) {
        final Map<String, String> tags = waySection.tags();

        String iso3 = null;
        if (tags.containsKey("iso")) {
            iso3 = tags.get("iso");
        } else if (tags.containsKey(TnTagKey.TN_COUNTRY_ISO_CODE.unwrap())) {
            iso3 = tags.get(TnTagKey.TN_COUNTRY_ISO_CODE.unwrap());
        }

        if (iso3 == null) {

            for (final Node node : waySection.nodes()) {

                final String iso2 = getIso2(node);

                if (iso2 != null) {

                    iso3 = getIso3(iso2);
                    break;
                }
            }
        }

        return iso3;
    }

    /**
     * The current method uses country localization via {@link CountryLocatorHolder} to return
     * the 2 letters country code.
     *
     * @param node node.
     * @return ISO2 corresponding to the node.
     */
    public static String getIso2(final Node node) {
        return CountryLocatorHolder.getCountryForCoordinates(node.latitude().asDegrees(), node.longitude().asDegrees());
    }

    /**
     * The current method maps the 2 digit country code to the 3 digit country code.
     *
     * @param iso2 2 digit country code.
     * @return 3 digit country code.
     */
    public static String getIso3(final String iso2) {

        final String iso3 = ISO2_TO_ISO3.get(iso2);

        if (StringUtils.isEmpty(iso3)) {
            return null;
        }

        return iso3;
    }

    public static String getIso2(final double latitudeAsDegrees,
                                 final double longitudeAsDegrees) {
        return CountryLocatorHolder.getCountryForCoordinates(latitudeAsDegrees, longitudeAsDegrees);
    }

    /**
     * The current method gets the 2 digit country code from tags..
     *
     * @param tags - tags
     * @return 2 digit country code.
     */
    public static String getIso2(final Map<String, String> tags) {
        return getIso2(getIso3(tags));
    }

    private static String getIso3(final Map<String, String> tags) {
        return TagService.iso(tags);
    }

    /**
     * The current method maps the 3 digit country code to the 2 digit country code.
     *
     * @param iso3 3 digit country code.
     * @return 2 digit country code.
     */
    private static String getIso2(final String iso3) {

        final String iso2 = ISO3_TO_ISO2.get(iso3);

        if (StringUtils.isEmpty(iso2)) {
            return null;
        }

        return iso2;
    }
}
