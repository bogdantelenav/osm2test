package com.telenav.adapter.utils;

import com.google.common.collect.Sets;
import com.telenav.datapipelinecore.osm.OsmTagKey;

import java.util.*;


/**
 * After receiving a collection of {@link OsmTagKey}s, the class archives those tags. The archiving process of a tag has the
 * purpose of keeping the original value of the tag into another tag.
 *
 * @author ioanao
 */
public class OsmTagKeysArchiver {

    private final Set<OsmTagKey> tagsToArchive;


    /**
     * Initialize the instance with the tag(s) that have to be archived.
     *
     * @param tagsToArchive the list of tags to archive
     */
    public OsmTagKeysArchiver(final OsmTagKey... tagsToArchive) {
        this(Sets.newHashSet(tagsToArchive));
    }

    /**
     * Initialize the instance with a tag collection containing those tags that have to be archived.
     *
     * @param tagsToArchive the tags to archive
     */
    public OsmTagKeysArchiver(final Collection<OsmTagKey> tagsToArchive) {
        this.tagsToArchive = new HashSet<>(tagsToArchive);
    }


    /**
     * Archive the already provided tags from the tag collection received as parameter. The function replaces those tags
     * destined to be archived with new tags that have the following structure: tn__osm_tagName. For example if the
     * 'oneway' tag has to be archived then it will be replaced by 'tn__osm_oneway'.
     *
     * @param tags tag collection containing those tags that have to be archived
     * @return a new tag collection
     */
    public Map<String, String> archiveTags(final Map<String, String> tags) {
        final Map<String, String> initialTagsWithArchivedTags = new HashMap<>(tags);
        this.tagsToArchive.forEach(tagNameToArchive -> {
            if (initialTagsWithArchivedTags.containsKey(tagNameToArchive.unwrap())) {
                replaceTagName(initialTagsWithArchivedTags, tagNameToArchive);
            }
        });
        return initialTagsWithArchivedTags;
    }

    private void replaceTagName(final Map<String, String> tags, final OsmTagKey tagToReplace) {
        final String value = tags.get(tagToReplace.unwrap());
        tags.put(tagToReplace.archivedKeyName(), value);
        tags.remove(tagToReplace.unwrap());
    }
}