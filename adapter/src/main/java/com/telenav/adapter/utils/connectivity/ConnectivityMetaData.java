package com.telenav.adapter.utils.connectivity;

import lombok.Builder;
import lombok.Getter;

import java.util.Map;

/**
 * @author liviuc
 */
@Builder
@Getter
public class ConnectivityMetaData {

    private final Map<String, String> data;
}
