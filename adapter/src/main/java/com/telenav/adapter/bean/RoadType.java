package com.telenav.adapter.bean;

import java.util.HashMap;
import java.util.Map;


/**
 * Enum containing all the RT categories.
 *
 * @author petrum
 */
public enum RoadType {

    FREEWAY(0),
    FREEWAY_URBAN(1),
    HIGHWAY(2),
    THROUGHWAY(3),
    LOCAL(4),
    FRONTAGE_ROAD(5),
    VERY_LOW_SPEED(6),
    PRIVATE_ROAD(7),
    WALKWAY(8),
    NON_NAVIGABLE(9),
    FERRY(10),
    RAIL_FERRY(11),
    PUBLIC_VEHICLE_ONLY(12);

    private static final Map<RoadType, Integer> PRIORITY = new HashMap<>();
    static {
        PRIORITY.put(FERRY, 1);
        PRIORITY.put(PUBLIC_VEHICLE_ONLY, 2);
        PRIORITY.put(PRIVATE_ROAD, 3);
        PRIORITY.put(WALKWAY, 4);
        PRIORITY.put(FREEWAY, 5);
        PRIORITY.put(FREEWAY_URBAN, 6);
        PRIORITY.put(FRONTAGE_ROAD, 7);
        PRIORITY.put(HIGHWAY, 9);
        PRIORITY.put(THROUGHWAY, 10);
        PRIORITY.put(LOCAL,11);
        PRIORITY.put(VERY_LOW_SPEED, 12);
        PRIORITY.put(NON_NAVIGABLE, 13);

    }

    /**
     * Get the numeric identifier of the road sub type
     *
     * @param type the road sub type
     * @return The identifier for the given road type
     */
    public static int identifierFor(final RoadType type) {
        return type.identifier;
    }

    private final int identifier;

    RoadType(final int identifier) {
        this.identifier = identifier;
    }

    public static Integer priorityFor(final RoadType roadType) {

        final Integer priority = PRIORITY.get(roadType);

        if (priority == null) {
            throw new RuntimeException("No priority for road type: " + roadType);
        }

        return priority;
    }
}
