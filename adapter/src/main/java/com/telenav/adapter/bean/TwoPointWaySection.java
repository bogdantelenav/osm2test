package com.telenav.adapter.bean;

import com.telenav.map.entity.Node;
import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Builder
@Data
public class TwoPointWaySection implements Serializable {

    private Long wayId;
    private Long waySectionId;
    private Node fromNode;
    private Node toNode;
    private double slope;
    private int lineEquationCoefficientA;
    private int lineEquationCoefficientB;

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof TwoPointWaySection) {
            final TwoPointWaySection that = (TwoPointWaySection) obj;
            return this.wayId.longValue() == this.wayId.longValue() &&
                    this.waySectionId.longValue() == this.waySectionId.longValue() &&
                    this.fromNode.id() == this.fromNode.id() &&
                    this.toNode.id() == this.toNode.id() &&
                    this.slope == that.slope &&
                    this.lineEquationCoefficientA == that.lineEquationCoefficientA &&
                    this.lineEquationCoefficientB == that.lineEquationCoefficientB;
        }
        return false;
    }
}

