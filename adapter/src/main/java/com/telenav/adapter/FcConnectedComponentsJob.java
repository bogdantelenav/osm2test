package com.telenav.adapter;

import com.telenav.adapter.component.fc.TagService;
import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.adapter.utils.connectivity.Connectivity;
import com.telenav.adapter.utils.connectivity.service.ConnectivityCheckerService;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.map.entity.WaySection;
import com.telenav.ost.ArgumentContainer;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.List;
import java.util.Objects;

/**
 * This job determines the connectivity of the fc network by country
 * and provides as output geojsons corresponding to each connected component.
 *
 * @author liviuc .
 */
public class FcConnectedComponentsJob {

    public static void main(String[] args) {

        final ArgumentContainer argumentContainer = new ArgumentContainer(args);
        final String waySectionsPath = argumentContainer.getRequired("way_sections_parquet");
        final String outputPath = argumentContainer.getRequired("output_dir");
        final String runLocal = argumentContainer.getOptional("run_local", "false");

        SparkConf sparkConf = new SparkConf();
        sparkConf.set("spark.sql.parquet.binaryAsString", "true");
        sparkConf.setMaster("local[1]");
        sparkConf.setAppName("Test");

        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Connectivity Component")
                .runLocal(Boolean.parseBoolean(runLocal))
                .build();

        final JavaSparkContext javaSparkContext = jobInitializer.javaSparkContext();

        // Read way-sections.
        final JavaRDD<WaySection> waySections =
                RddReader.WaySectionReader.read(javaSparkContext, waySectionsPath)
                        .cache();

        // Distinct isos.
        final List<String> isos = waySections.map(waySection -> TagService.iso(waySection.tags()))
                .filter(Objects::nonNull)
                .distinct()
                .collect();

        // Output data by country.
        for (final String iso : isos) {
            final JavaRDD<WaySection> byCountry =
                    waySections.filter(waySection -> iso.equalsIgnoreCase(TagService.iso(waySection.tags())));

//            final Connectivity directedComponents = ConnectivityCheckerService.directed(javaSparkContext, byCountry);
//            ConnectivityCheckerService.saveGeojsons(directedComponents.getGeojsons(), outputPath + "/directed/" + iso + "/", javaSparkContext);
//            ConnectivityCheckerService.saveMetaData(directedComponents.getMetaData(), outputPath + "/directed/" + iso + "/", javaSparkContext);

            final Connectivity undirectedComponents = ConnectivityCheckerService.undirected(javaSparkContext, byCountry);
            ConnectivityCheckerService.saveGeojsons(undirectedComponents.getGeojsons(), outputPath + "/undirected/" + iso + "/", javaSparkContext);
            ConnectivityCheckerService.saveMetaData(undirectedComponents.getMetaData(), outputPath + "/undirected/" + iso + "/", javaSparkContext);
        }
    }
}
