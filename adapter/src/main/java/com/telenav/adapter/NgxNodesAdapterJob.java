package com.telenav.adapter;

import com.telenav.adapter.component.NodeService;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.ost.ArgumentContainer;
import com.telenav.pipelinevalidator.runner.PipelineValidatorRunner;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;

import java.util.List;

/**
 * @author liviuc
 */
public class NgxNodesAdapterJob {

    public static void main(String[] args) {

        final ArgumentContainer argumentContainer = new ArgumentContainer(args);
        // Paths to entities.
        final String nodesPath = argumentContainer.getRequired("nodes_parquet");
        final String relationsPath = argumentContainer.getRequired("relations_parquet");
        final String waySectionsPath = argumentContainer.getRequired("way_sections_parquet");
        final String waysPath = argumentContainer.getRequired("ways_parquet");
        // Output path
        final String outputPath = argumentContainer.getRequired("output_dir");
        final String runLocal = argumentContainer.getOptional("run_local", "");

        try (SparkSession sparkSession = new JobInitializer.Builder()
                .appName("Adapter Job - NGX nodes")
                .runLocal(Boolean.valueOf(runLocal))
                .build().sparkSession()) {

            final Dataset<Row> nodes = sparkSession.read().parquet(nodesPath).cache();

            final Dataset<Row> waySections = sparkSession.read().parquet(waySectionsPath);

            final Dataset<Row> ways = sparkSession.read().parquet(waysPath);

            final Dataset<Row> relations = sparkSession.read().parquet(relationsPath);

            // Get city center relations with node members
            final Dataset<Row> nodeCityCenterRelations = RelationService.nodeCityCenterRelations(relations);

            final NodeService nodeService = new NodeService(sparkSession);

            // Join them with nodes
            final Dataset<Row> nodesWithCityCenterRelations =
                    nodeService.joinNodesToCityCenterRelations(nodes, nodeCityCenterRelations);

            // NGX enrichment:
            final NgxNodesAdapterApplication ngxNodesAdapterApplication = new NgxNodesAdapterApplication();

            Dataset<Row> enhancedNodes = ngxNodesAdapterApplication.adapt(nodesWithCityCenterRelations);
            Dataset<Row> navnEnhancedNodes =
                    nodeService.enhanceNodesFromWaysSections(enhancedNodes,
                            waySections,
                            ways);

            navnEnhancedNodes.write()
                    .mode("Overwrite")
                    .parquet(outputPath + "/parquet");

            // Node type collisions
            final List<String> collisions = nodeService.getNodeTypeCollisionLog()
                    .value();
            JavaSparkContext.fromSparkContext(sparkSession.sparkContext())
                    .parallelize(collisions)
                    .coalesce(1)
                    .saveAsTextFile(outputPath + "/csv/node-type-collisions/");

            PipelineValidatorRunner.builder()
                    .nodesPath(outputPath + "/parquet")
                    .relationsPath(relationsPath)
                    .sourceJobName(NgxNodesAdapterJob.class.getSimpleName())
                    .reportPath(outputPath + "/validation_report")
                    .runLocal(Boolean.valueOf(runLocal))
                    .build()
                    .run();
        }
    }
}
