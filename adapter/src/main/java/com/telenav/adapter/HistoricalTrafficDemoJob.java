package com.telenav.adapter;

import com.telenav.datapipelinecore.io.reader.RddReader;
import com.telenav.datapipelinecore.io.writer.RddWriter;
import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.map.entity.WaySection;
import com.telenav.ost.ArgumentContainer;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.HashMap;
import java.util.Map;


/**
 * The job contains implementation of provider-independent part of HistoricalTraffic code.
 * This code is not for production.
 * <p>
 * The following tags with default demo values are assigned to all way sections:
 * spd_id:f=430
 * spd_id:t=141
 * spd_kph:f=105
 * spd_kph:t=105
 *
 * @author andriyz
 */
public class HistoricalTrafficDemoJob {

    private static final String POS_SPEED_PROFILE_ID = "spd_id:f";
    private static final String NEG_SPEED_PROFILE_ID = "spd_id:t";
    private static final String POS_AVERAGE_SPEED = "spd_kph:f";
    private static final String NEG_AVERAGE_SPEED = "spd_kph:t";

    /**
     * @param args - command line arguments in name=value format
     *             <p>
     *             Required:
     *             1. way_sections_parquet - path for parquet with way sections
     *             2. output_dir - path for output directory. This output will contain updated way sections data with new tags
     *             <p>
     *             Optional:
     *             1. run_local - set to "true" for local run (default value is "false")
     */
    public static void main(String[] args) {

        final ArgumentContainer argumentContainer = new ArgumentContainer(args);
        final String waySectionsPath = argumentContainer.getRequired("way_sections_parquet");
        final String outputPath = argumentContainer.getRequired("output_dir");
        final String runLocal = argumentContainer.getOptional("run_local", "false");

        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Historical Traffic Demo Job")
                .runLocal(Boolean.parseBoolean(runLocal))
                .build();

        try (JavaSparkContext javaSparkContext = jobInitializer.javaSparkContext()) {

            final JavaRDD<WaySection> waySections =
                    RddReader.WaySectionReader.read(javaSparkContext, waySectionsPath);
            final JavaRDD<WaySection> output = waySections.map(HistoricalTrafficDemoJob::addTags);

            RddWriter.WaySectionWriter.write(javaSparkContext, output, outputPath);
        }
    }

    private static WaySection addTags(WaySection ws) {
        final Map<String, String> newTags = new HashMap<>();
        newTags.put(POS_SPEED_PROFILE_ID, "430");
        newTags.put(NEG_SPEED_PROFILE_ID, "141");
        newTags.put(POS_AVERAGE_SPEED, "105");
        newTags.put(NEG_AVERAGE_SPEED, "105");
        return ws.withTags(newTags);
    }
}
