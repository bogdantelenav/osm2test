package com.telenav.adapter;

import com.telenav.datapipelinecore.JobInitializer;
import com.telenav.datapipelinecore.io.writer.RddWriter;
import com.telenav.map.entity.RawWay;
import com.telenav.map.storage.read.RawRowToRawWayConverter;
import com.telenav.ost.ArgumentContainer;
import com.telenav.ost.spark.storage.read.ParquetReader;
import com.telenav.pipelinevalidator.runner.PipelineValidatorRunner;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.util.CollectionAccumulator;

import java.io.IOException;


/**
 * @author dianat2
 */
public class NgxWaysAdapterJob {

    public static void main(String[] args) {

        final ArgumentContainer argumentContainer = new ArgumentContainer(args);
        final String waysPath = argumentContainer.getRequired("ways_parquet");
        final String outputPath = argumentContainer.getRequired("output_dir");
        final boolean runLocal = Boolean.parseBoolean(argumentContainer.getOptional("run_local", "false"));
        final JobInitializer jobInitializer = new JobInitializer.Builder()
                .appName("Adapter Job")
                .runLocal(runLocal)
                .build();

        final JavaRDD<RawWay> ways = new ParquetReader<>(jobInitializer.javaSparkContext(),
                new RawRowToRawWayConverter(), waysPath).read();
        JavaRDD<RawWay> adaptedSectionedWays = new NgxWaysAdapterApplication()
                .adapt(ways);

        RddWriter.RawWaysWriter.write(jobInitializer.javaSparkContext(), adaptedSectionedWays, outputPath + "/ways");

        PipelineValidatorRunner.builder()
                .waysPath(outputPath + "/ways")
                .sourceJobName(NgxWaysAdapterJob.class.getSimpleName())
                .reportPath(outputPath + "/validation_report")
                .runLocal(runLocal)
                .build()
                .run();
    }
}