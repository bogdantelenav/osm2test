package com.telenav.adapter.func;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

/**
 * @author liviuc
 */
public class NodeCityCenterRelationsFunc implements MapFunction<Row, Row> {

    @Override
    public Row call(final Row row) {

        final Row memberRelations = (Row) row.getList(1).get(0);

        final Row member = (Row) memberRelations.get(2);

        final Long nodeId = member.getLong(0);

        return RowFactory.create(row.get(0), row.get(1), nodeId);
    }
}
