package com.telenav.adapter.func;

import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * @author liviuc
 */
public class RawWayNodeFlatMpFunc {

    public static Iterator<Row> map(final Row way) {

        Row firstNodeId = null;
        Row lastNodeId = null;

        final List<Row> nodes = way.getList(way.fieldIndex("indexedNodes"));
        final int lastIndex = nodes.size() - 1;
        for (final Row row : nodes) {
            final int index = row.getInt(row.fieldIndex("index"));
            if (index == 0) {
                firstNodeId = id(row);
            } else if (index == lastIndex) {
                lastNodeId = id(row);
            }
        }

        return Arrays.asList(firstNodeId, lastNodeId)
                .iterator();
    }

    private static Row id(final Row node) {
        return RowFactory.create(node.getLong(node.fieldIndex("id")));
    }
}
