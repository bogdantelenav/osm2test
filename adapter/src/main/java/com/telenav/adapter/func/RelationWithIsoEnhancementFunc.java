package com.telenav.adapter.func;

import com.google.common.collect.ImmutableList;
import com.telenav.adapter.component.fc.Conditions;
import com.telenav.adapter.component.fc.ConditionsConjunction;
import com.telenav.adapter.component.fc.ConditionsDisjunction;
import com.telenav.datapipelinecore.bean.Relation;
import org.apache.spark.api.java.function.Function;

import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.telenav.adapter.component.fc.FcExemptionsWhiteList.*;
import static com.telenav.adapter.component.fc.FcInfo.FcRelatedRelationConditions.isExemptedName;
import static com.telenav.adapter.component.fc.FcInfo.FcRelatedRelationConditions.isExemptedRefConditionedByNetwork;
import static com.telenav.adapter.component.fc.TagService.*;

/**
 * @author liviuc .
 */
public class RelationWithIsoEnhancementFunc implements Function<Relation, Relation> {

    /*
        Enhancement conditions.
     */
    private static final Conditions<Relation> exemptedNameCondition =
            new ConditionsConjunction<Relation>(isExemptedName);
    private static final Conditions<Relation> exemptedRefConditionedByNetworkCondition =
            new ConditionsConjunction<Relation>(isExemptedRefConditionedByNetwork);
    private static final Conditions<Relation> noCondition =
            new ConditionsDisjunction<Relation>();

    /*
        Enhancement logic to execute for conditions.
     */
    private static final IsoLogic exemptedNameConditionLogic = (relation, routeRelationsMappingBroadcast) -> {

        final Map<String, String> tags = relation.getTags();

        final String name = name(tags);

        final String exemptedNetworkFc = EXEMPTED_NAME_CONDITIONED_FC.get(name);

        tags.put(FC_TAG, exemptedNetworkFc);
    };
    private static final IsoLogic exemptedRefConditionedByNetworkConditionLogic = (relation, routeRelationsMappingBroadcast) -> {

        final Map<String, String> tags = relation.getTags();

        final String ref = ref(tags);

        final String network = network(tags);

        final String exemptionKey = exemptionKey(ref, network);

        final String exemptedNetworkFc = EXEMPTED_REF_CONDITIONED_FC.get(exemptionKey);

        tags.put(FC_TAG, exemptedNetworkFc);
    };
    private static final IsoLogic routeRelationsMappingLogic = (relation, routeRelationsMappingBroadcast) -> {

        final Map<String, String> tags = relation.getTags();

        final String network = network(tags);

        for (final Map.Entry<String, Map<String, String>> stringMapEntry : routeRelationsMappingBroadcast.entrySet()) {

            for (Map.Entry<String, String> entry : stringMapEntry.getValue().entrySet()) {

                final String regex = entry.getValue();

                if (network != null && Pattern.matches(regex, network)) {

                    final String networkFc = entry.getKey();
                    tags.put(FC_TAG, networkFc);
                    break;
                }
            }
        }
    };
    /*
        Enhancement conditions to enhancement logic.
     */
    private static final Map<Conditions, IsoLogic> CONDITIONS_TO_ISO_LOGIC = Stream.of(new Object[][]{
            {noCondition, routeRelationsMappingLogic},
            {exemptedNameCondition, exemptedNameConditionLogic},
            {exemptedRefConditionedByNetworkCondition, exemptedRefConditionedByNetworkConditionLogic}
    }).collect(Collectors.toMap(data -> (Conditions) data[0], data -> (IsoLogic) data[1]));
    private static List<Conditions> ENHANCEMENT_CONDITIONS;

    static {
        /*
            !!! THIS IS IMPORTANT
            This is the order in which we want to evaluate the conditions.
            The first condition that is hit provides the logic to assign fc by.
        */
        ENHANCEMENT_CONDITIONS = ImmutableList.of(
                exemptedNameCondition,
                exemptedRefConditionedByNetworkCondition,
                noCondition
        );
    }

    /*
        Route mapping for relations.
     */
    private final Map<String, Map<String, String>> routeRelationsMappingBroadcast;

    public RelationWithIsoEnhancementFunc(final Map<String, Map<String, String>> routeRelationsMappingBroadcast) {
        this.routeRelationsMappingBroadcast = routeRelationsMappingBroadcast;
    }

    @Override
    public Relation call(final Relation relation) {

        for (final Conditions enhancementCondition : ENHANCEMENT_CONDITIONS) {

            if (enhancementCondition == noCondition || enhancementCondition.isTrue(relation)) {

                CONDITIONS_TO_ISO_LOGIC.get(enhancementCondition)
                        .execute(relation, routeRelationsMappingBroadcast);

                break;
            }
        }

        return relation;
    }

    @FunctionalInterface
    private interface IsoLogic {

        void execute(final Relation relation,
                     final Map<String, Map<String, String>> routeRelationsMappingBroadcast);
    }
}
