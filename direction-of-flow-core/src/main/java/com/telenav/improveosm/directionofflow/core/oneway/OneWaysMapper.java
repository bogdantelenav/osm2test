package com.telenav.improveosm.directionofflow.core.oneway;


import java.io.IOException;
import java.util.List;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.postgis.Geometry;
import org.postgis.LineString;
import org.postgis.Point;
import org.postgis.binary.BinaryWriter;
import com.telenav.improveosm.directionofflow.core.util.Constants;
import com.telenav.improveosm.directionofflow.core.util.Counters;


public class OneWaysMapper extends Mapper<LongWritable, Text, Text, Text> {

    private static final String PRIVATE_ROAD = "PRIVATE_ROAD";

    private static final int SRID = 4326;

    private double minPerc = 97.5;
    private int minTripCount = 25;


    @Override
    protected void map(final LongWritable inputKey, final Text inputValue, final Context context) {
        try {
            final AnalyzerLine line = AnalyzerLine.fromString(inputValue.toString());

            if (line.length() != null && line.length().compareTo(20.0) < 0) {
                context.getCounter(Counters.SHORT_EDGES).increment(1L);
                return;
            }

            final int total = line.max() + line.min();
            final double perc = (double) line.max() / total * 100;
            if (perc > minPerc && line.max() > minTripCount && !PRIVATE_ROAD.equals(line.type())) {
                if (!line.isOsmOneWay() || (line.isOsmOneWay() && line.isBackwards())) {
                    if (line.isFalseOneWay()) {
                        context.getCounter(Counters.FALSE_ONEWAYS).increment(1L);
                    }
                    line.switchNodes();
                    final String result = buildOuputValue(line, perc, total);
                    final String key = buildKey(line);
                    context.write(new Text(key), new Text(result));
                }
            }

        } catch (final Exception e) {
            System.err.println("Error for line: " + e);
            e.printStackTrace();
        }
    }

    private String buildKey(AnalyzerLine line) {
        return line.wayId() + Constants.SEP + line.firstNode() + Constants.SEP + line.endNode();
    }

    private String buildOuputValue(final AnalyzerLine line, final double perc, final int total) {
        final StringBuffer result = new StringBuffer();
        result.append(perc).append(Constants.SEP);
        result.append(computeConfidence(perc, total)).append(Constants.SEP);
        result.append("OPEN").append(Constants.SEP);
        result.append(line.type()).append(Constants.SEP);
        result.append(computePointGeomText(line.points())).append(Constants.SEP);
        result.append(total).append(Constants.SEP);
        final String order = (line.isBackwards()) ? "2" : "1";
        result.append(order);
        return result.toString();
    }

    private String computeConfidence(final double perc, final int trips) {
        if (trips < 200) {
            return "C3";
        }
        if ((perc > 99) && (trips > 400)) {
            return "C1";
        }
        return "C2";
    }

    private String computePointGeomText(final List<Point> points) {
        final Geometry multiPoint = new LineString(points.toArray(new Point[points.size()]));
        multiPoint.setSrid(SRID);
        return new BinaryWriter().writeHexed(multiPoint);
    }
}