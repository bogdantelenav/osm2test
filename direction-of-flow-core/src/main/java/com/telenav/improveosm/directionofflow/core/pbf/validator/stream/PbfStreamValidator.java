package com.telenav.improveosm.directionofflow.core.pbf.validator.stream;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.openstreetmap.osmosis.core.container.v0_6.EntityContainer;
import org.openstreetmap.osmosis.core.container.v0_6.WayContainer;
import org.openstreetmap.osmosis.core.domain.v0_6.Tag;
import org.openstreetmap.osmosis.core.task.v0_6.Sink;


/**
 * @author horatiuj
 */
public class PbfStreamValidator {

    private static final String SERVICE = "service";
    private final String pbfFolderPath;
    private final String outputPath;

    public static final String JUNCTION = "junction";
    public static final String ONEWAY = "oneway";
    public static final String ROUNDABOUT = "roundabout";
    public static final String HIGHWAY = "highway";
    long countService = 0;

    public PbfStreamValidator(final String pbfFolderPath, final String outputPath) {
        this.pbfFolderPath = pbfFolderPath;
        this.outputPath = outputPath;
    }

    public void process() {
        try {
            final List<PbfStreamReader> pbfStreamReaders = new ArrayList<>();
            final AtomicInteger pbfCount = new AtomicInteger(0);

            final Configuration conf = new Configuration();
            // conf.set("fs.defaultFS", "hdfs://10.230.2.63:8120");
            final FileSystem hdfs = FileSystem.get(conf);
            final RemoteIterator<LocatedFileStatus> fileStatusListIterator =
                    hdfs.listLocatedStatus(new Path(pbfFolderPath));

            final OutputStream out = hdfs.create(new Path(outputPath), false);

            while (fileStatusListIterator.hasNext()) {

                final LocatedFileStatus fileStatus = fileStatusListIterator.next();
                final InputStream inputStream = hdfs.open(fileStatus.getPath());

                final PbfStreamReader pbfStreamReader = new PbfStreamReader(hdfs, inputStream, new Sink() {

                    @Override
                    public void complete() {
                        if (pbfCount.addAndGet(1) == pbfStreamReaders.size()) {
                            System.out.println("done writing");
                        }
                    }

                    @Override
                    public void initialize(final Map<String, Object> metaData) {

                    }

                    @Override
                    public void process(final EntityContainer entityContainer) {

                        try {
                            if (entityContainer instanceof WayContainer) {
                                final long wayId = entityContainer.getEntity().getId();
                                final Collection<Tag> tags = entityContainer.getEntity().getTags();
                                boolean isRoundabout = false;
                                boolean isOneWay = false;
                                boolean isHighway = false;
                                boolean isService = false;

                                for (final Tag tag : tags) {
                                    if (JUNCTION.equalsIgnoreCase(tag.getKey()) && ROUNDABOUT
                                            .equalsIgnoreCase(tag.getValue())) {
                                        isRoundabout = true;
                                    }
                                    if (isOsmOneway(tag)) {
                                        isOneWay = true;
                                    }

                                    if (HIGHWAY.equalsIgnoreCase(tag.getKey())) {
                                        isHighway = true;
                                        if (SERVICE.equalsIgnoreCase(tag.getValue())) {
                                            isService = true;
                                        }
                                        if ("motorway".equalsIgnoreCase(tag.getValue())
                                                || "motorway_link".equalsIgnoreCase(tag.getValue())) {
                                            isOneWay = true;
                                        }
                                    }
                                    if (SERVICE.equalsIgnoreCase(tag.getKey())) {
                                        isService = true;
                                    }

                                }
                                if (isHighway) {
                                    final StringBuffer outputLine = new StringBuffer(wayId + "," + HIGHWAY);
                                    if (isOneWay) {
                                        outputLine.append(",").append(ONEWAY);
                                    }
                                    if (isRoundabout) {
                                        outputLine.append(",").append(ROUNDABOUT);
                                    }
                                    if (isService) {
                                        outputLine.append(",").append(SERVICE);

                                    }
                                    outputLine.append("\n");
                                    try {
                                        out.write(outputLine.toString().getBytes());
                                    } catch (final IOException e) {
                                        e.printStackTrace();
                                    }
                                }

                            }
                        } catch (final IllegalArgumentException e) {
                            e.printStackTrace();
                        }
                    }

                    private boolean isOsmOneway(final Tag tag) {
                        return ONEWAY.equalsIgnoreCase(tag.getKey()) && ("yes".equalsIgnoreCase(tag.getValue())
                                || "-1".equalsIgnoreCase(tag.getValue()) || "1".equalsIgnoreCase(tag.getValue()));
                    }

                    @Override
                    public void release() {

                    }
                });
                pbfStreamReaders.add(pbfStreamReader);

            }

            int count = 1;
            for (final PbfStreamReader pbfStreamReader : pbfStreamReaders) {
                System.out.println("Processing pbf nb " + count++ + " out of 7");
                pbfStreamReader.run();
            }

        } catch (final IOException e) {
            e.printStackTrace();
        }


    }



    public static void main(final String[] args) {
        final String pbfFolderPath = args[0];
        final String outputPath = args[1];
        if (pbfFolderPath == null || pbfFolderPath.isEmpty()) {
            System.err.println("PBF folder is missing");
            return;
        }
        if (outputPath == null || outputPath.isEmpty()) {
            System.err.println("Output file is missing");
            return;
        }
        new PbfStreamValidator(pbfFolderPath, outputPath).process();
    }
}
