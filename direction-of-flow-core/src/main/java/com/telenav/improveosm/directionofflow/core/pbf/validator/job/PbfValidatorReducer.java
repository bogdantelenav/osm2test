package com.telenav.improveosm.directionofflow.core.pbf.validator.job;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.output.MultipleOutputs;
import com.telenav.improveosm.directionofflow.core.util.Counters;


public class PbfValidatorReducer extends Reducer<Text, Text, Text, NullWritable> {

    private static final String ONEWAY = "oneway";
    private static final String ROUNDABOUT = "roundabout";
    private static final String SERVICE = "service";

    private MultipleOutputs<Text, NullWritable> mos;


    @Override
    protected void setup(final Context context) throws IOException, InterruptedException {
        mos = new MultipleOutputs<>(context);
    }


    @Override
    protected void cleanup(final Context context) throws IOException, InterruptedException {
        mos.close();
    }

    @Override
    protected void reduce(final Text key, final Iterable<Text> values, final Context context)
            throws IOException, InterruptedException {
        try {
            final List<String> acceptedValues = new ArrayList<String>();
            final List<String> stringValues = new ArrayList<>();

            for (final Text value : values) {
                stringValues.add(value.toString());

            }
            // if stringValues contain only analyzer lines => deleted
            // is string values contain only osm extracted lines => ignore

            if (containsOnlyAnalyzerOutput(stringValues)) {
                for (String value : stringValues) {
                    String[] splited = value.split(",");
                    String oneWayId = key.toString() + "," + splited[1] + "," + splited[2] + ",FALSE";
                    mos.write(new Text(oneWayId), NullWritable.get(), "deleted");
                    context.getCounter(Counters.DELETED_WAYS).increment(1L);
                }
                return;
            }
            if (containsOnlyOsmOutput(stringValues)) {
                return;
            }


            boolean isDeleted = false;
            boolean isEdited = false;

            for (final String value : stringValues){
                if (!isAnalyzerLine(value)) {
                    if (value.contains(ROUNDABOUT)){
                        context.getCounter(Counters.ROUNDABOUTS).increment(1L);
                        isDeleted = true;
                    }
                    if (value.contains(ONEWAY)){
                        context.getCounter(Counters.ONEWAYS).increment(1L);
                        isEdited = true;
                    }
                    if (value.contains(SERVICE)) {
                        context.getCounter(Counters.SERVICE).increment(1L);
                        isDeleted = true;
                    }
                } else {
                    acceptedValues.add(value);
                }
            }

            for (final String acceptedValue : acceptedValues) {
                String[] splited = acceptedValue.split(",");
                String oneWayId = key.toString() + "," + splited[1] + "," + splited[2] + ",FALSE";
                if (isDeleted) {
                    mos.write(new Text(oneWayId), NullWritable.get(), "deleted");
                } else if (isEdited) {
                    mos.write(new Text(oneWayId), NullWritable.get(), "edited");
                } else {
                    context.getCounter(Counters.ACCEPTED).increment(1L);
                    mos.write(new Text(acceptedValue), NullWritable.get(), "accepted");
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    private boolean containsOnlyOsmOutput(final List<String> stringValues) {
        for (final String value : stringValues) {
            if (isAnalyzerLine(value)) {
                return false;
            }
        }
        return true;
    }

    private boolean containsOnlyAnalyzerOutput(final List<String> stringValues) {
        for (final String value : stringValues) {
            if (!isAnalyzerLine(value)) {
                return false;
            }
        }
        return true;
    }

    private boolean isAnalyzerLine(final String value) {
        return value.split(",").length == 10;
    }
}
