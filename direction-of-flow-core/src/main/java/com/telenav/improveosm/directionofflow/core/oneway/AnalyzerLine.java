package com.telenav.improveosm.directionofflow.core.oneway;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.postgis.Point;


/**
 *
 *
 * @author dianabalan
 */

public class AnalyzerLine {

    final static int FW_TRIP_INDEX = 22;
    final static int BACK_TRIP_INDEX = 38;

    public static AnalyzerLine fromString(final String inputValue) {
        final String line = inputValue.toString();
        final String[] parts = line.split("\\|");

        final String idText = parts[0].replace("(", "").replace(")", "");
        final String[] ids = idText.split(",");
        final String wayId = ids[0];
        final String firstNode = ids[1];
        final String endNode = ids[2];

        final Double length = Double.valueOf(parts[5]);
        final Integer forwardTrips = Integer.valueOf(parts[FW_TRIP_INDEX].split(",")[0].replace("{", ""));
        final Integer backwardTrips = Integer.valueOf(parts[BACK_TRIP_INDEX].split(",")[0].replace("{", ""));

        final String[] points = parts[4].split(";");
        final List<Point> geoPoints = extractGeoPoints(points);

        final boolean isOsmOneWay = Boolean.valueOf(parts[2]);
        final String type = parts[7];
        return new AnalyzerLine(idText, wayId, firstNode, endNode, forwardTrips, backwardTrips, geoPoints, isOsmOneWay,
                type, length);
    }


    private static List<Point> extractGeoPoints(final String[] points) {
        final List<Point> geoPoints = new ArrayList<Point>();
        for (final String point : points) {
            final String lat = point.split(":")[0];
            final String lon = point.split(":")[1];
            geoPoints.add(new Point(Double.valueOf(lon), Double.valueOf(lat)));
        }
        return geoPoints;
    }

    private final String id;
    private final String wayId;
    private String firstNode;
    private String endNode;
    private Integer fwdTripsIndex;
    private Integer backTripsIndex;
    final Integer forwardTrips;
    final Integer backwardsTrips;
    final List<Point> points;
    private final boolean isOsmOneWay;
    private final String type;
    private final Double length;

    public AnalyzerLine(final String id, final String wayId, final String firstNode, final String endNode,
            final Integer forwardTrips, final Integer backwardsTrips, final List<Point> points,
            final boolean isOsmOneWay, final String type, final Double length) {
        this.id = id;
        this.wayId = wayId;
        this.firstNode = firstNode;
        this.endNode = endNode;
        this.forwardTrips = forwardTrips;
        this.backwardsTrips = backwardsTrips;
        this.points = points;
        this.isOsmOneWay = isOsmOneWay;
        this.type = type;
        this.length = length;
    }

    public Integer backTripsIndex() {
        return backTripsIndex;
    }

    public Integer backwardTrips() {
        return backwardsTrips;
    }

    public String endNode() {
        return endNode;
    }

    public String firstNode() {
        return firstNode;
    }

    public Integer forwardTrips() {
        return forwardTrips;
    }


    public Integer fwdTripsIndex() {
        return fwdTripsIndex;
    }


    public String id() {
        return id;
    }


    public boolean isBackwards() {
        return this.max().compareTo(this.backwardsTrips) == 0;
    }


    public boolean isFalseOneWay() {
        if (isBackwards() && this.isOsmOneWay()) {
            return true;
        }
        return false;
    }


    public boolean isOsmOneWay() {
        return isOsmOneWay;
    }


    public Double length() {
        return length;
    }


    public Integer max() {
        return Math.max(forwardTrips, backwardsTrips);
    }

    public Integer min() {
        return Math.min(forwardTrips, backwardsTrips);
    }


    public List<Point> points() {
        return points;
    }

    public void switchNodes() {
        if (isBackwards()) {
            final String temp = firstNode;
            firstNode = endNode;
            endNode = temp;
            Collections.reverse(points);
        }
    }


    public String type() {
        return type;
    }


    public String wayId() {
        return wayId;
    }




}
