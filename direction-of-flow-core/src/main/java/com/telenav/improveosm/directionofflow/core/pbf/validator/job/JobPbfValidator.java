package com.telenav.improveosm.directionofflow.core.pbf.validator.job;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.LazyOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.log4j.Logger;
import com.telenav.improveosm.directionofflow.core.util.Constants;


public class JobPbfValidator extends Configured implements Tool {

    private static final Logger logger = Logger.getLogger(JobPbfValidator.class);


    public static void main(final String[] args) {
        try {
            ToolRunner.run(new JobPbfValidator(), args);
        } catch (final Exception e) {
            logger.error(e);
        }
    }

    @Override
    public int run(final String[] args) {
        boolean jobIsSuccessful = false;
        try {
            final Configuration conf = getConf();
            conf.set("mapred.textoutputformat.separator", Constants.SEP);
            final Job job = new Job(conf, this.getClass().getName());
            job.setJarByClass(JobPbfValidator.class);
            job.setMapperClass(PbfValidatorMapper.class);
            job.setReducerClass(PbfValidatorReducer.class);

            job.setOutputKeyClass(Text.class);
            job.setOutputValueClass(Text.class);
            FileInputFormat.addInputPaths(job, args[0]);
            FileOutputFormat.setOutputPath(job, new Path(args[1]));
            LazyOutputFormat.setOutputFormatClass(job, TextOutputFormat.class);
            job.waitForCompletion(true);


            jobIsSuccessful = job.isSuccessful();
        } catch (final Exception e) {
            e.printStackTrace();
        }
        final int returnCode = jobIsSuccessful ? 0 : 1;
        return returnCode;
    }

    public boolean checkMandatoryParameters(final Configuration conf, final Logger logger, final String... params) {
        boolean status = true;
        for (final String param : params) {
            if (conf.get(param) == null) {
                logger.error("Missing parameter " + param);
                status = false;
                break;
            }
        }
        return status;
    }

}
