package com.telenav.improveosm.directionofflow.core.util;


public class Constants {

    public static final String MIN_PERC = "minPercentage";
    public static final String MIN_TRIPS = "minTrips";
    public static final String SEP = ",";
}
