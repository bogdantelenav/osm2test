package com.telenav.improveosm.directionofflow.core.oneway;

import com.telenav.improveosm.directionofflow.core.util.Counters;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;


/**
 * 
 * 
 * @author dianabalan
 */
public class OneWaysReducer extends Reducer<Text, Text, Text, Text> {

    @Override
    protected void reduce(final Text key, final Iterable<Text> values, final Context context) throws IOException,
            InterruptedException {

        Text uniqueValue = new Text();
        int count = 0;
        for (final Text value : values) {
            count++;
            if (count > 1) {
                context.getCounter(Counters.DUPLICATE_SEGMENTS).increment(1L);
                return;
            }
            uniqueValue = value;
        }

        context.write(key, uniqueValue);

    }
}
