package com.telenav.improveosm.directionofflow.core.pbf.validator.job;


import com.telenav.improveosm.directionofflow.core.util.Constants;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;


public class PbfValidatorMapper extends Mapper<LongWritable, Text, Text, Text> {

    @Override
    protected void map(final LongWritable inputKey, final Text inputValue, final Context context) {
        try {
            final String[] parts = inputValue.toString().split(Constants.SEP);
            context.write(new Text(parts[0]), inputValue);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

}
