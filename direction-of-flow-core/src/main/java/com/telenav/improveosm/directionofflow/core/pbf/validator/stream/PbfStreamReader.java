package com.telenav.improveosm.directionofflow.core.pbf.validator.stream;

import java.io.DataInputStream;
import java.io.InputStream;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import org.apache.hadoop.fs.FileSystem;
import org.openstreetmap.osmosis.core.task.v0_6.Sink;
import org.openstreetmap.osmosis.pbf2.v0_6.impl.PbfDecoder;
import org.openstreetmap.osmosis.pbf2.v0_6.impl.PbfStreamSplitter;


/**
 * PbfReader which accepts stream instead of file as input
 *
 * @author horatiuj
 */
public class PbfStreamReader {

    private final Sink sink;
    private final InputStream inputStream;
    public static final int WORKERS = 4;
    public final FileSystem hdfs;

    public PbfStreamReader(final FileSystem hdfs, final InputStream inputStream, final Sink sink) {
        this.sink = sink;
        this.inputStream = inputStream;
        this.hdfs = hdfs;
    }

    public void run() {
        final ExecutorService executorService = Executors.newFixedThreadPool(WORKERS);

        sink.initialize(Collections.<String, Object>emptyMap());

        final PbfStreamSplitter streamSplitter = new PbfStreamSplitter(new DataInputStream(inputStream));
        final PbfDecoder pbfDecoder = new PbfDecoder(streamSplitter, executorService, WORKERS + 1, sink);
        pbfDecoder.run();

        sink.complete();

        sink.release();
        executorService.shutdownNow();
        streamSplitter.release();
    }
}
