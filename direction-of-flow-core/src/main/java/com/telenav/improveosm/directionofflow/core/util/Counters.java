package com.telenav.improveosm.directionofflow.core.util;


/**
 *
 *
 * @author dianabalan
 */
public enum Counters {

    DUPLICATE_SEGMENTS, ROUNDABOUTS, ONEWAYS, DELETED_WAYS, FALSE_ONEWAYS, SHORT_EDGES, SERVICE, TOTAL, ACCEPTED;
}
